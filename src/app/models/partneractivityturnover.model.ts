import { BaseDomain } from "./base-domain.model";
import { PartnerXItemComment } from "./clientturnover.model";

export class PartnerActivityTurnover extends BaseDomain {
    public partnerId: number;
    public itemGroupId: number;
    public itemGroupCode: string;
    public itemGroupName: string;
    public bestRankingItemId: number;
    public bestRankingItemCode: string;
    public bestRankingItemName: string;

    public listPrice: number;
    public currencyId: number;
    public currencyName: string;

    public itemGroupTurnoverLast3Months: number;
    public itemGroupTurnoverTotal: number;

    public itemGroupL12: number;
    public itemGroupL11: number;
    public itemGroupL10: number;
    public itemGroupL9: number;
    public itemGroupL8: number;
    public itemGroupL7: number;
    public itemGroupL6: number;
    public itemGroupL5: number;
    public itemGroupL4: number;
    public itemGroupL3: number;
    public itemGroupL2: number;
    public itemGroupL1: number;
    public itemGroupLC: number;

    lastComment: PartnerXItemComment;
    itemComments: PartnerXItemComment[];
  }