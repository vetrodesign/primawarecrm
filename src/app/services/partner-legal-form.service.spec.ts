import { TestBed } from '@angular/core/testing';

import { PartnerLegalFormService } from './partner-legal-form.service';

describe('PartnerLegalFormService', () => {
  let service: PartnerLegalFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerLegalFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
