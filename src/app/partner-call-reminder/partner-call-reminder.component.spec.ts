import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerCallReminderComponent } from './partner-call-reminder.component';

describe('PartnerCallReminderComponent', () => {
  let component: PartnerCallReminderComponent;
  let fixture: ComponentFixture<PartnerCallReminderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerCallReminderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerCallReminderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
