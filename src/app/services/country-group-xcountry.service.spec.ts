import { TestBed } from '@angular/core/testing';

import { CountryGroupXcountryService } from './country-group-xcountry.service';

describe('CountryGroupXcountryService', () => {
  let service: CountryGroupXcountryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CountryGroupXcountryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
