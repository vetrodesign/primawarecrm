import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ItemGroup } from 'app/models/itemgroup.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemGroupService {

  constructor(private apiService: ApiService) { }

  async getAllItemGroupsAsync(): Promise<any[]> {
    const apiURL = `/itemgroup/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getItemGroupsAsyncByID(): Promise<any[]> {
    const apiURL = `/itemgroup/getbycustomerid`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getItemGroupByItemGroupId(itemGroupId: number): Promise<any> {
    const apiURL = `/itemgroup/GetItemGroupByItemGroupId`;
    return await this.apiService.PostAsync<any>(apiURL, itemGroupId, Constants.mrkApi);
  }

  async getMaxActCmpsForItemGroupIdsAsync(): Promise<any[]> {
    const apiURL = `/itemgroup/GetMaxActCmpsForItemGroupIds`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async createItemGroupAsync(item: ItemGroup): Promise<any> {
    const apiURL = `/itemgroup/create`;
    return await this.apiService.PostAsync<any>(apiURL, item, Constants.mrkApi);
  }

  async getFilteredByCodeSmall(code: string): Promise<any[]> {
    let c = JSON.stringify(code);
    const apiURL = `/itemgroup/getfilteredbycodesmall`;
    return await this.apiService.PostAsync<any[]>(apiURL, c, Constants.mrkApi);
  }

  async updateItemGroupAsync(item: ItemGroup): Promise<any> {
    const apiURL = `/itemgroup/update`;
    return await this.apiService.PutAsync<any>(apiURL, item, Constants.mrkApi);
  }

  async updateMultipleAsync(itemIds: number[], item: ItemGroup): Promise<any> {
    let s = {'item': null, 'itemIds': null};
    s.item = item;
    s.itemIds = itemIds;
    const apiURL = `/itemgroup/updateMultiple`;
    return await this.apiService.PutAsync<any>(apiURL, s, Constants.mrkApi);
  }

  async deleteItemGroupAsync(ids: number[]): Promise<any> {
    const apiURL = `/itemgroup/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
  
  async getNextAvailableCodeAsync(item: string): Promise<any> {
    const apiURL = `/itemgroup/GetNextAvailableCode`;
    return await this.apiService.PostAsync<any>(apiURL, item, Constants.mrkApi);
  }

  async changeCustomCodeToItemsAndUpdateERP(itemGroupId: number, userName: string, userPost: string): Promise<any> {
    let body = {itemGroupId: itemGroupId, userName: userName, userPost: userPost};
    const apiURL = `/itemgroup/changeCustomCodes`;
    return await this.apiService.PostAsync<any>(apiURL, body, Constants.mrkApi);
  }
}