import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { PartnerLocationContact } from 'app/models/partnerlocationcontact.model';
import { HelperService } from 'app/services/helper.service';
import { ItemService } from 'app/services/item.service';
import { PartnerLocationContactService } from 'app/services/partner-location-contact.service';
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
  selector: 'app-partner-items-certificates',
  templateUrl: './partner-items-certificates.component.html',
  styleUrls: ['./partner-items-certificates.component.css']
})
export class PartnerItemsCertificatesComponent implements OnInit {
  @Input() isItemsCertificatesPopupOpen: boolean;
  @Input() partnerId: number;

  @Output() isItemsCertificatesPopupOpenOutput: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('manuallyInsertItemsCertificatesDataGrid') manuallyInsertItemsCertificatesDataGrid: DxDataGridComponent;
  @ViewChild('excelCopyContentItemsCertificatesDataGrid') excelCopyContentItemsCertificatesDataGrid: DxDataGridComponent;

  loaded: boolean;
  isExcelImportContentOpen: boolean;
  isExcelCopyContentOpen: boolean;
  isManuallyInsertContentOpen: boolean;
  shouldDisableSaveExcelFileBtn: boolean = true;
  uploadedCertificatesExcel: any;
  processedItemsCertificatesData: any;
  manuallyInsertItemsCertificatesData: any;
  excelCopyContentItemsCertificatesData: any;
  locationContactPersons: PartnerLocationContact[];
  excelCopySelectedRows: any[];

  constructor(
    private helperService: HelperService,
    private itemsService: ItemService,
    private partnerLocationContactService: PartnerLocationContactService
  ) {
    
  }

  ngOnInit(): void {
    this.getData();
  }

  initializeGridEvents(): void {
    if (this.isExcelCopyContentOpen && this.excelCopyContentItemsCertificatesDataGrid) {
      const gridElement = this.excelCopyContentItemsCertificatesDataGrid.instance.element();
      gridElement.addEventListener('paste', (event: ClipboardEvent) => this.handlePaste(event));
    }
  }

  getData() {
    this.loaded = true;
    Promise.all([this.getPartnerContact()]).then(() => {
      this.loaded = false;
    })
  }

  async getPartnerContact(): Promise<void> {
    await this.partnerLocationContactService.getPartnerLocationContactByPartnerID(this.partnerId).then(items => {
      if (items && items.length > 0) {
        this.locationContactPersons = items;
      } else {
        this.locationContactPersons = null;
      }
    });
  }

  onExcelImportClick() {
    this.isExcelImportContentOpen = true;
    this.isExcelCopyContentOpen = false;
    this.isManuallyInsertContentOpen = false;
  }

  onExcelCopyClick() {
    this.excelCopyContentItemsCertificatesData = [];

    setTimeout(() => {
      if (this.excelCopyContentItemsCertificatesDataGrid) {
        this.initializeGridEvents();
      }
    }, 0);

    this.isExcelImportContentOpen = false;
    this.isExcelCopyContentOpen = true;
    this.isManuallyInsertContentOpen = false;
  }

  onManuallyInsertClick() {
    this.manuallyInsertItemsCertificatesData = [];
    this.isExcelImportContentOpen = false;
    this.isExcelCopyContentOpen = false;
    this.isManuallyInsertContentOpen = true;
  }

  onUploadExcelFileChange(event: any) {
    const files: FileList = event.target.files;

    if (files.length > 0) {
      this.shouldDisableSaveExcelFileBtn = this.helperService.isExcelValidFileType(files[0]);
      this.uploadedCertificatesExcel = files[0];      
    }
  }

  async onVerifyItemsCertificatesClick() {
    if (this.isExcelImportContentOpen) {
      this.loaded = true;

      await this.itemsService.verifyItemsCertificatesExcelAsync(this.uploadedCertificatesExcel, this.partnerId).then((response) => {
        this.processedItemsCertificatesData = response;
        this.loaded = false;
      })
    }

    if (this.isManuallyInsertContentOpen) {
      this.loaded = true;

      let itemsToVerify = [];
      this.manuallyInsertItemsCertificatesData.forEach(item => {
        let itemToVerify = {
          invoiceNo: item.invoiceNo,
          itemCode: item.itemCode,
          itemBatch: item.itemBatch,
          partnerId: this.partnerId
        }

        itemsToVerify.push(itemToVerify);
      })

      await this.itemsService.verifyItemsCertificatesAsync(itemsToVerify).then((response) => {
        this.manuallyInsertItemsCertificatesData = response;
        this.loaded = false;
      })
    }

    if (this.isExcelCopyContentOpen) {
      this.loaded = true;

      let itemsToVerify = [];
      this.excelCopyContentItemsCertificatesData.forEach(item => {
        let itemToVerify = {
          invoiceNo: item.invoiceNo,
          itemCode: item.itemCode,
          itemBatch: item.itemBatch,
          partnerId: this.partnerId
        }

        itemsToVerify.push(itemToVerify);
      })

      await this.itemsService.verifyItemsCertificatesAsync(itemsToVerify).then((response) => {
        this.excelCopyContentItemsCertificatesData = response;
        this.loaded = false;
      })
    }
  }

  onHidingPopup(event: any) {
    this.isItemsCertificatesPopupOpen = false;
    this.isItemsCertificatesPopupOpenOutput.emit(false);
  }

  onRowInserting(e: any) {
    this.manuallyInsertItemsCertificatesData = [...this.manuallyInsertItemsCertificatesData, e.data];
  }

  public addItemCertificateData() {
    this.manuallyInsertItemsCertificatesDataGrid.instance.addRow();
  }

  handlePaste(event: ClipboardEvent) {
    event.preventDefault();

    const clipboardData = event.clipboardData || (window as any).clipboardData;
    const pastedData = clipboardData.getData('text/plain');
    const rowsData = pastedData.split('\n').filter(row => row.trim() !== '');

    rowsData.forEach((row) => {
      const cells = row.split('\t');

        this.excelCopyContentItemsCertificatesData.push({
          invoiceNo: cells[0]?.trim() || '',
          itemCode: cells[1]?.trim() || '',
          itemBatch: cells[2]?.trim() || '',
          itemName: cells[3]?.trim() || '',
          itemExpirationDate: cells[4]?.trim() || '',
          itemCertificatesFilePath: cells[5]?.trim() || '',
          observations: cells[6]?.trim() || '',
        });
    });

    this.excelCopyContentItemsCertificatesDataGrid.instance.refresh();
  }

  onSendItemsCertificates() {

  }

  getDisplayExprContact(item) {
    if (!item) {
      return '';
    } 

    return item.firstName + ' - ' + item.email + ' - ' + item.phoneNumber;
  }

  onExcelCopySelectionChanged(data: any) {
    this.excelCopySelectedRows = data.selectedRowsData;
  }
}

