import { TestBed } from '@angular/core/testing';

import { ImportExportGroupItemDataService } from './import-export-group-item-data.service';

describe('ImportExportGroupItemDataService', () => {
  let service: ImportExportGroupItemDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImportExportGroupItemDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
