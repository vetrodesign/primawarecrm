import { BaseDomain } from "./base-domain.model";

export class ContactCampaignAllocation extends BaseDomain {
    public postId: number;
    public ContactCampaignId: number;
    public orderNumber: number;
    public numberFromCampaign: number;
}
