import { BaseDomain } from "./base-domain.model";

export class PartnerContactPreferences extends BaseDomain {
    public partnerId: number;
    public dayOfWeek: number;
    public fromTime: any;
    public toTime: any;
    public preferredOfferDeliveryMethod: number;
    public otherPreferredOfferDeliveryMethodDetails: string;
}
