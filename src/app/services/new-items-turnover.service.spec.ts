import { TestBed } from '@angular/core/testing';

import { NewItemsTurnoverService } from './new-items-turnover.service';

describe('NewItemsTurnoverService', () => {
  let service: NewItemsTurnoverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NewItemsTurnoverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
