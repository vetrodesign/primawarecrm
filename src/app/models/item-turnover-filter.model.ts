export class TurnoverFilter {    
    supplierIds: number[];
    countryIds: number[];
    countyIds: number[];
    cityIds: number[];
    itemCodes: string[];
    itemIds: number[];
    supplierAgentIds: number[];
}

export class TurnoverHistoryFilter {
    itemId: number;
    itemTurnoverOperationId: number;
}

export class TurnoverPartnerMonthQuantityFilter {
    itemId: number;
    partnerId: number;
    month: number;
    year: number;
}