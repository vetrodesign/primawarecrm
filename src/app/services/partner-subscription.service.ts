import { Injectable } from '@angular/core';
import { PartnerSubscription } from 'app/models/partner-subscription.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerSubscriptionService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<PartnerSubscription[]> {
    const apiURL = `/partnersubscription/getall`;
    return await this.apiService.GetAsync<PartnerSubscription[]>(apiURL, Constants.mrkApi);
  }
}
