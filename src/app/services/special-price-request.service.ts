import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { SpecialPriceRequest } from 'app/models/special-price-request.model';

@Injectable({
  providedIn: 'root'
})
export class SpecialPriceRequestService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<SpecialPriceRequest[]> {
    const apiURL = `/SpecialPriceRequest/GetAll`;
    return await this.apiService.GetAsync<SpecialPriceRequest[]>(apiURL, Constants.mrkApi);
  }

  async getBySpecialPriceRequestId(specialPriceRequestId: number): Promise<SpecialPriceRequest[]> {
    const apiURL = `/SpecialPriceRequest/GetById?specialPriceRequestId=${specialPriceRequestId}`;
    return await this.apiService.GetAsync<SpecialPriceRequest[]>(apiURL, Constants.mrkApi);
  }

  async getAllByPartnerIdAsync(partnerId: number): Promise<SpecialPriceRequest[]> {
    const apiURL = `/SpecialPriceRequest/GetByPartnerId/` + partnerId;
    return await this.apiService.GetAsync<SpecialPriceRequest[]>(apiURL, Constants.mrkApi);
  }

  async createAsync(m: SpecialPriceRequest): Promise<any> {
    const apiURL = `/SpecialPriceRequest/create`;
    return await this.apiService.PostAsync<any>(apiURL, m, Constants.mrkApi);
  }
}
