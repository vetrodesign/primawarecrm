import { Component, OnInit, ViewChild } from '@angular/core';
import { ApisOverviewService } from 'app/services/apis-overview.service';

@Component({
  selector: 'app-weather-forecast',
  templateUrl: './weather-forecast.component.html',
  styleUrls: ['./weather-forecast.component.css']
})
export class WeatherForecastComponent implements OnInit {
  temperatureData: any[] = [];
  
  constructor(private apiOverview: ApisOverviewService) { }

  ngOnInit(): void {
    this.getData();
  }

  async getData() {
    await this.apiOverview.getWeatherForecastAsync().then(result => {
      if (result && result.trend_day) {
        // Convert raw data into a format suitable for dxChart
        this.temperatureData = result.trend_day.time.map((date, index) => ({
          date: new Date(date),  // Convert string to Date object
          temperature_max: result.trend_day.temperature_max[index],
          temperature_mean: result.trend_day.temperature_mean[index],
          temperature_min: result.trend_day.temperature_min[index],

          windspeed_mean: result.trend_day.windspeed_mean[index],
          windspeed_min: result.trend_day.windspeed_min[index],

          precipitation_probability: result.trend_day.precipitation_probability[index]
        }));
      }
    })
  }

  customizeLabel = (data) => {
    return {
      visible: true, 
      verticalAlignment: 'bottom',
      horizontalAlignment: 'center',
      font: { size: 7, color: '#000000' },
      backgroundColor: 'white',
      customizeText: () => `${data.value}`
    };
  };
}
