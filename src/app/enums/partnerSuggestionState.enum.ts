export enum PartnerSuggestionStateEnum {
    Save = 1,
    NotInterested = 2,
    Postpone = 3
}