import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerNameEquivalenceComponent } from './partner-name-equivalence.component';

describe('PartnerNameEquivalenceComponent', () => {
  let component: PartnerNameEquivalenceComponent;
  let fixture: ComponentFixture<PartnerNameEquivalenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerNameEquivalenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerNameEquivalenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
