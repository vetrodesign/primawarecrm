import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { PartnerStatus } from 'app/models/partner-status.model';

@Injectable({
  providedIn: 'root'
})
export class PartnerStatusService {
  constructor(private apiService: ApiService) { }

  async getAllPartnerStatusesAsync(): Promise<PartnerStatus[]> {
    const apiURL = `/partnerStatus/getall`;
    return await this.apiService.GetAsync<PartnerStatus[]>(apiURL, Constants.commonApi);
  }
}
