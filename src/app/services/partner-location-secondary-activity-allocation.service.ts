import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PartnerLocationXSecondaryActivityAllocation } from 'app/models/partnerlocationxsecondaryactivityallocation.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerLocationSecondaryActivityAllocationService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<PartnerLocationXSecondaryActivityAllocation[]> {
    const apiURL = `/partnerLocationXSecondaryActivityAllocation/getall`;
    return await this.apiService.GetAsync<PartnerLocationXSecondaryActivityAllocation[]>(apiURL, Constants.mrkApi);
  }

  async getAllByPartnerLocationIdAsync(partnerLocationId: number): Promise<PartnerLocationXSecondaryActivityAllocation[]> {
    const apiURL = `/partnerLocationXSecondaryActivityAllocation/GetAllByPartnerLocationId/`;
    return await this.apiService.PostAsync<PartnerLocationXSecondaryActivityAllocation[]>(apiURL, partnerLocationId, Constants.mrkApi);
  }

  async createMultipleAsync(p: PartnerLocationXSecondaryActivityAllocation[]): Promise<any> {
    const apiURL = `/partnerLocationXSecondaryActivityAllocation/CreateMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async deleteMultipleAsync(p: PartnerLocationXSecondaryActivityAllocation[]): Promise<any> {
    const apiURL = `/partnerLocationXSecondaryActivityAllocation/DeleteMultiple`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, p, Constants.mrkApi);
  }}

