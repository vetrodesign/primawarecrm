import { TestBed } from '@angular/core/testing';

import { CaenCodeService } from './caen-code.service';

describe('CaenCodeService', () => {
  let service: CaenCodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CaenCodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
