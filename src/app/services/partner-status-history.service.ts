import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Constants } from "app/constants";
import { PartnerStatusHistory } from "app/models/partner-status-history.model";

@Injectable({
    providedIn: 'root'
})
export class PartnerStatusHistoryService {
    constructor(private apiService: ApiService) { }

    async getByPartnerId(partnerId: number): Promise<PartnerStatusHistory> { 
        const apiURL = `/partnerStatusHistory/getbypartnerid?partnerId=${partnerId}`;
        return await this.apiService.GetAsync<PartnerStatusHistory>(apiURL, Constants.mrkApi);
    }

    async getLastByPartnerId(partnerId: number): Promise<PartnerStatusHistory> { 
        const apiURL = `/partnerStatusHistory/getlastbypartnerid?partnerId=${partnerId}`;
        return await this.apiService.GetAsync<PartnerStatusHistory>(apiURL, Constants.mrkApi);
    }

    async createPartnerStatusHistory(p: PartnerStatusHistory): Promise<any> {
        const apiURL = `/partnerStatusHistory/create`;
        return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
    }
}