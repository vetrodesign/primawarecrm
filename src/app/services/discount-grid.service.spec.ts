import { TestBed } from '@angular/core/testing';

import { DiscountGridService } from './discount-grid.service';

describe('DiscountGridService', () => {
  let service: DiscountGridService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiscountGridService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
