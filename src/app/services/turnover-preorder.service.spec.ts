import { TestBed } from '@angular/core/testing';

import { TurnoverPreorderService } from './turnover-preorder.service';

describe('TurnoverPreorderService', () => {
  let service: TurnoverPreorderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TurnoverPreorderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
