import { Component, OnInit, ViewChild, Input, AfterViewInit, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { DxDataGridComponent, DxTooltipComponent, DxValidatorComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { PartnerXItemComment } from 'app/models/clientturnover.model';
import { TurnoverSectionTypeEnum } from 'app/enums/turnoverSectionTypeEnum';
import * as _ from 'lodash';
import { TurnoverCommentComponent } from 'app/turnover-comment/turnover-comment.component';
import { NewItemsTurnover } from 'app/models/newitemsturnover.model';
import { NewItemsTurnoverService } from 'app/services/new-items-turnover.service';
import { PartnerItemState } from 'app/models/partneritemstate.model';
import { Brand } from 'app/models/brand.model';
import { on } from 'devextreme/events';
import { VipOfferItemsDetailsService } from 'app/services/vip-offer-items-details.service';
import { VipOfferItems } from 'app/models/vip-offer-items.model';
import { PriceCompositionTypeEnum } from 'app/enums/priceCompositionTypeEnum';
import { ItemService } from 'app/services/item.service';
import { ItemRelatedService } from 'app/services/item-related.service';

@Component({
  selector: 'app-new-items-turnover',
  templateUrl: './new-items-turnover.component.html',
  styleUrls: ['./new-items-turnover.component.css']
})
export class NewItemsTurnoverComponent implements OnInit, AfterViewInit {
  @Input() newItemsTurnoverActions: IPageActions;
  @Input() postId: number;
  @Input() clientId: number;
  @Input() partnerItemState: PartnerItemState[];
  @Input() brands: Brand[];
  @Input() vipOfferItems: VipOfferItems[];
  @Input() items: any[];
  @Input() customerId: number;
  @Input() showCharts: boolean;
  @Input() partners: any;
  @Input() relatedItems: any;

  @Output() foundItemBySearchFilter: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() isComponentShown: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('popupComponent') popupComponent: TurnoverCommentComponent;
  @ViewChild('tooltipItemCode') tooltipItemCode: DxTooltipComponent;
  @ViewChild('stockTooltipItemCode') stockTooltipItemCode: DxTooltipComponent;
  @ViewChild('tooltipItemName') tooltipItemName: DxTooltipComponent;
  @ViewChild('tooltipItemComment') tooltipItemComment: DxTooltipComponent;
  
  loaded: boolean;
  newItemsTurnovers: NewItemsTurnover[];
  selectedClientTurnover: NewItemsTurnover;
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  groupedText: string;
  timeout: number;  
  tooltipData: any = {};
  displayData: boolean;
  previouslyCheckedProperty: string | null = null;
  section: number;
  stockTooltipData: any = {};
  newItemsTurnoverInfo: string;
  itemNameTooltipData: any = {};
  itemNameTooltipTimeout: number;
  treeListDataSource: any = [];
  itemCommentTooltipTimeout: number;  
  itemCommentTooltipData: any = {};
  selectedItemComments: any;
  isItemCommentsPopupOpen: boolean;

  constructor(
    private newItemsTurnoverService: NewItemsTurnoverService, 
    private translationService: TranslateService,
    private itemService: ItemService,
    private vipOfferItemDetailsService: VipOfferItemsDetailsService,
    private notificationService: NotificationService,
    private itemXRelatedService: ItemRelatedService) {
      
    this.section = TurnoverSectionTypeEnum.NewItemsTurnover; 
    this.groupedText = this.translationService.instant('groupedText');
    this.newItemsTurnovers = [];

    this.newItemsTurnoverInfo = this.translationService.instant("newItemsTurnoverInfo");
  }

  ngOnInit(): void {
    if (this.newItemsTurnoverActions)
      this.newItemsTurnoverActions.CanAdd = false;
  }

  async ngOnChanges(changes: SimpleChanges): Promise<void> {
    if (changes.items && this.items && this.items.length > 0) {
      await this.getData();
    }
  }

  canUpdate() {
    return this.newItemsTurnoverActions ? this.newItemsTurnoverActions.CanUpdate : false;
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }

  async getData() {
    if (this.clientId && this.postId) {
      this.treeListDataSource = [];
      this.loaded = true;
      
      await this.newItemsTurnoverService.getData(this.clientId, this.postId, this.section).then(async items => {
        this.newItemsTurnovers = (items && items.length > 0) ? items : [];
        const currentDate = new Date().setHours(0, 0, 0, 0);

        this.newItemsTurnovers.forEach(i => {
          i.itemComments = _.orderBy(i.itemComments, ['created'], ['desc']);

          i.itemComments.forEach(comment => {
            const { partnerItemStateId } = comment;
          
            // Reset all boolean values to false
            comment.isInvoiced = false;
            comment.notUsed = false;
            comment.hasStock = false;
            comment.atOrder = false;
          
            let name = this.partnerItemState?.find(x => x.id == partnerItemStateId)?.name;

            // Update the boolean value based on the partnerItemStateId
            switch (name) {
              case "Facturat":
                comment.isInvoiced = true;
                break;
              case "Nu Foloseste":
                comment.notUsed = true;
                break;
              case "Are Stoc":
                comment.hasStock = true;
                break;
              case "La Comanda":
                comment.atOrder = true;
                break;
              default:
                comment.isInvoiced = false;
                comment.notUsed = false;
                comment.hasStock = false;
                comment.atOrder = false;
                break;
            }
          });

          const todayObjects = i.itemComments.filter(obj => {
            const objDate = new Date(obj.created).setHours(0, 0, 0, 0);
            return objDate === currentDate;
          });
        
          if (todayObjects.length > 0) {
            i.lastComment = todayObjects.reduce((prev, curr) => {
              const prevDate = new Date(prev.created);
              const currDate = new Date(curr.created);
              return currDate > prevDate ? curr : prev;
            });
          } else {
            i.lastComment = new PartnerXItemComment();;
            i.lastComment.isInvoiced = false;
            i.lastComment.notUsed = false;
            i.lastComment.hasStock = false;
            i.lastComment.atOrder = false;
          }

          i.lastComment.shoppingCartQuantity = null;
        });

        let itemGroupIds = this.newItemsTurnovers.map(m => m.itemGroupId);
        let itemIds = this.items.filter(f => itemGroupIds.includes(f.itemGroupId)).map(m => m.id);

        await this.newItemsTurnoverService.getChildrenData(this.clientId, this.postId, this.section, itemIds).then(result => {
          if (result && result.length > 0) {
            
            result.forEach(i => {
              i.itemComments = _.orderBy(i.itemComments, ['created'], ['desc']);
    
              i.itemComments.forEach(comment => {
                const { partnerItemStateId } = comment;
              
                // Reset all boolean values to false
                comment.isInvoiced = false;
                comment.notUsed = false;
                comment.hasStock = false;
                comment.atOrder = false;
              
                let name = this.partnerItemState?.find(x => x.id == partnerItemStateId)?.name;
    
                // Update the boolean value based on the partnerItemStateId
                switch (name) {
                  case "Facturat":
                    comment.isInvoiced = true;
                    break;
                  case "Nu Foloseste":
                    comment.notUsed = true;
                    break;
                  case "Are Stoc":
                    comment.hasStock = true;
                    break;
                  case "La Comanda":
                    comment.atOrder = true;
                    break;
                  default:
                    comment.isInvoiced = false;
                    comment.notUsed = false;
                    comment.hasStock = false;
                    comment.atOrder = false;
                    break;
                }
              });
    
              const todayObjects = i.itemComments.filter(obj => {
                const objDate = new Date(obj.created).setHours(0, 0, 0, 0);
                return objDate === currentDate;
              });
            
              if (todayObjects.length > 0) {
                i.lastComment = todayObjects.reduce((prev, curr) => {
                  const prevDate = new Date(prev.created);
                  const currDate = new Date(curr.created);
                  return currDate > prevDate ? curr : prev;
                });
              } else {
                i.lastComment = new PartnerXItemComment();;
                i.lastComment.isInvoiced = false;
                i.lastComment.notUsed = false;
                i.lastComment.hasStock = false;
                i.lastComment.atOrder = false;
              }
    
              i.lastComment.shoppingCartQuantity = null;
            });

            let j = 1;
            for (let i=0; i<this.newItemsTurnovers.length; i++) {
              let itemGroup = {
                id: j++,
                parentId: null,
                partnerId: this.newItemsTurnovers[i].partnerId,
                itemGroupId: this.newItemsTurnovers[i].itemGroupId,
                itemCode: this.newItemsTurnovers[i].itemGroupCode,
                itemName: this.newItemsTurnovers[i].itemGroupName,
                listPrice: this.newItemsTurnovers[i].listPrice,
                currencyId: this.newItemsTurnovers[i].currencyId,
                currencyName: this.newItemsTurnovers[i].currencyName,
                lastComment: this.newItemsTurnovers[i].lastComment,
                itemComments: this.newItemsTurnovers[i].itemComments,
                l12: this.newItemsTurnovers[i].l12,
                l11: this.newItemsTurnovers[i].l11,
                l10: this.newItemsTurnovers[i].l10,
                l9: this.newItemsTurnovers[i].l9,
                l8: this.newItemsTurnovers[i].l8,
                l7: this.newItemsTurnovers[i].l7,
                l6: this.newItemsTurnovers[i].l6,
                l5: this.newItemsTurnovers[i].l5,
                l4: this.newItemsTurnovers[i].l4,
                l3: this.newItemsTurnovers[i].l3,
                l2: this.newItemsTurnovers[i].l2,
                l1: this.newItemsTurnovers[i].l1,
                lc: this.newItemsTurnovers[i].lc
              }

              let items = result.filter(f => f.itemGroupId === this.newItemsTurnovers[i].itemGroupId && f.currentStock);
              if (items && items.length > 0) {
                this.treeListDataSource.push(itemGroup);

                items?.forEach(item => {
  
                  let childItem = {
                    id: j++,
                    parentId: itemGroup.id,
                    ...item
                  }
  
                  this.treeListDataSource.push(childItem);
                })
              }
            }
          } else {
            let j = 1;
            for (let i=0; i<this.newItemsTurnovers.length; i++) {
              let itemGroup = {
                id: j++,
                parentId: null,
                partnerId: this.newItemsTurnovers[i].partnerId,
                itemGroupId: this.newItemsTurnovers[i].itemGroupId,
                itemCode: this.newItemsTurnovers[i].itemGroupCode,
                itemName: this.newItemsTurnovers[i].itemGroupName,
                listPrice: this.newItemsTurnovers[i].listPrice,
                currencyId: this.newItemsTurnovers[i].currencyId,
                currencyName: this.newItemsTurnovers[i].currencyName,
                lastComment: this.newItemsTurnovers[i].lastComment,
                itemComments: this.newItemsTurnovers[i].itemComments,
                l12: this.newItemsTurnovers[i].l12,
                l11: this.newItemsTurnovers[i].l11,
                l10: this.newItemsTurnovers[i].l10,
                l9: this.newItemsTurnovers[i].l9,
                l8: this.newItemsTurnovers[i].l8,
                l7: this.newItemsTurnovers[i].l7,
                l6: this.newItemsTurnovers[i].l6,
                l5: this.newItemsTurnovers[i].l5,
                l4: this.newItemsTurnovers[i].l4,
                l3: this.newItemsTurnovers[i].l3,
                l2: this.newItemsTurnovers[i].l2,
                l1: this.newItemsTurnovers[i].l1,
                lc: this.newItemsTurnovers[i].lc
              }

              this.treeListDataSource.push(itemGroup);
            }
          }

          this.isComponentShown.emit(this.newItemsTurnovers && this.newItemsTurnovers.length > 0);
          this.loaded = false;
        })
      });
    }
  }

  applyColumnFilter(filterValue, columnName: string) {
    if (this.dataGrid && this.dataGrid.instance) {
      this.dataGrid.instance.option("searchPanel.text", filterValue);

    var found = false;
    if (this.newItemsTurnovers && this.newItemsTurnovers.length > 0 && this.newItemsTurnovers.find(x => x.itemGroupCode.toLowerCase().includes(filterValue?.toLowerCase())) != null) {
      found = true;
    }
    if (this.newItemsTurnovers && this.newItemsTurnovers.length > 0 && this.newItemsTurnovers.find(x => x.itemGroupName.toLowerCase().includes(filterValue?.toLowerCase())) != null) {
      found = true;
    }
    if (found) {
      this.foundItemBySearchFilter.emit(true);

      // setTimeout(() => {
      //   var scrollableContent = document.getElementById('newItemsTurnover');
      //   if (scrollableContent) {
      //     scrollableContent.scrollIntoView({ behavior: 'smooth' });
      //   }
      // }, 100);
    } else {
      this.foundItemBySearchFilter.emit(false);
    }
    }
}

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && e.data.lastComment != null && e.data.lastComment.isInvoiced) {
      e.rowElement.style.backgroundColor = '#a1ffa1';
    }
    if (e.rowType === 'data' && e.data && e.data.priceCompositionType == PriceCompositionTypeEnum.SinglePriceList) {
      e.rowElement.style.color = '#4169e1';
    }
    if (e.rowType === 'data' && e.data && e.data.priceCompositionType == PriceCompositionTypeEnum.SinglePriceStock) {
      e.rowElement.style.color = '#ff3729';
    }
    if (e.rowType === 'data' && e.data && e.data.priceCompositionType == PriceCompositionTypeEnum.SinglePriceCraft) {
      e.rowElement.style.color = '#01910d';
    }

    if (e.rowType === 'header') {
      e.rowElement.style.backgroundColor = 'rgb(242, 242, 242)'; 
      e.rowElement.style.color = '#010101';
    }
  }

  public refreshDataGrid() {
    this.getData();
    this.dataGrid.instance.refresh();
  }

  public async openDetails(row: any) {
    this.selectedClientTurnover = row;
    setTimeout(() => {
      this.popupComponent.openPopup();
    }, 0);
  }

  public async onRowUpdated(event: any): Promise<void> {
    if (event && event.data && event.data.lastComment) {
      let comment = event.data.lastComment;

      if (comment.isInvoiced || comment.hasStock || comment.atOrder) {
        this.openDetails(event.data);
        return;
      }
      this.popupComponent.saveComment(event.data.lastComment, event.data.itemId);
    }

  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";

      if (event.dataField === 'lastComment.comment' && event.row && event.row.data) {
        const rowData = event.row.data;
        const allowEditing = rowData?.lastComment?.notUsed ||
                            rowData?.lastComment?.hasStock ||
                            rowData?.lastComment?.isInvoiced ||
                            rowData?.lastComment?.atOrder;
        event.editorOptions.disabled = !allowEditing;
      }

      if (event.dataField === 'lastComment.isInvoiced' && event.row && event.row.data) {
        const rowData = event.row.data;
        const allowEditing = !rowData?.lastComment?.notUsed &&
                            !rowData?.lastComment?.hasStock &&
                            !rowData?.lastComment?.atOrder;
        event.editorOptions.disabled = !allowEditing;
      }

      if (event.dataField === 'lastComment.notUsed' && event.row && event.row.data) {
        const rowData = event.row.data;
        const allowEditing = !rowData?.lastComment?.isInvoiced &&
                            !rowData?.lastComment?.hasStock &&
                            !rowData?.lastComment?.atOrder;
        event.editorOptions.disabled = !allowEditing;
      }

      if (event.dataField === 'lastComment.hasStock' && event.row && event.row.data) {
        const rowData = event.row.data;
        const allowEditing = !rowData?.lastComment?.notUsed &&
                            !rowData?.lastComment?.isInvoiced &&
                            !rowData?.lastComment?.atOrder;
        event.editorOptions.disabled = !allowEditing;
      }

      if (event.dataField === 'lastComment.atOrder' && event.row && event.row.data) {
        const rowData = event.row.data;
        const allowEditing = !rowData?.lastComment?.notUsed &&
                            !rowData?.lastComment?.hasStock &&
                            !rowData?.lastComment?.isInvoiced;
        event.editorOptions.disabled = !allowEditing;
      }
  }

  isItemOnVIP(itemId: number) {
    return (this.vipOfferItems.filter(x => x.itemId == itemId).length > 0);
}

setItemCodeTooltip(event) {
  if (event && event.rowType === 'data' && event.column.dataField === 'itemCode' && event.data.parentId) {
    on(event.cellElement, 'mouseover', arg => {
      this.timeout = window.setTimeout(async () => {

        this.tooltipData = null;
        let stockPromise = this.vipOfferItemDetailsService.GetTiersForItemAsync(null, event.data.itemId, this.clientId).then(r => {
          if (r && r.length > 0) {
            this.tooltipData = r;
          } else { this.tooltipData = []; }
        });

        Promise.all([stockPromise]).then(r => {this.tooltipItemCode.instance.show(arg.target);})
      }, 1000);
    });

    on(event.cellElement, 'mouseout', arg => {
      if (this.timeout) {
        window.clearTimeout(this.timeout);
      }
      this.tooltipItemCode.instance.hide();
    });
  }
  if (event && event.rowType === 'data' && event.column.dataField === 'currentStock') {
    on(event.cellElement, 'mouseover', arg => {
      this.timeout = window.setTimeout(async () => {

        this.stockTooltipData = null;
        let stockPromise = this.itemService.GetStocksOnSiteAsync(event.data.latestItemId).then(r => {
          if (r && r.length > 0) {
            this.stockTooltipData = r;
          } else { this.stockTooltipData = []; }
        });

        Promise.all([stockPromise]).then(r => {this.stockTooltipItemCode.instance.show(arg.target);})
      }, 1000);
    });

    on(event.cellElement, 'mouseout', arg => {
      if (this.timeout) {
        window.clearTimeout(this.timeout);
      }
      this.stockTooltipItemCode.instance.hide();
    });
  }
}

  onCellPrepared(e) {
    this.setItemCodeTooltip(e);
    this.setItemNameTooltip(e);
    this.setItemCommentTooltip(e);
    if (e.rowType === 'data') {
      e.cellElement.style.paddingTop = '2px';
      e.cellElement.style.paddingBottom = '1px';
    }
  }

  setItemCommentTooltip(event) {
    if (event && event.rowType === 'data' && event.column.dataField === 'lastComment.comment') {
      on(event.cellElement, 'mouseover', arg => {
        this.itemCommentTooltipTimeout = window.setTimeout(async () => {

          this.itemCommentTooltipData = null;
          if (event.data.itemComments && event.data.itemComments.length > 0) {
            let orderedComments = _.orderBy(event.data.itemComments, 'created', ['desc']);
            let lastComment = orderedComments[0].comment;

            this.itemCommentTooltipData = lastComment;
          } else {
            this.itemCommentTooltipData = [];
          }

          this.tooltipItemComment.instance.show(arg.target);
        }, 1000);
      });

      on(event.cellElement, 'mouseout', arg => {
        if (this.itemCommentTooltipTimeout) {
          window.clearTimeout(this.itemCommentTooltipTimeout);
        }
        this.tooltipItemComment.instance.hide();
      });
    }
  }

  priceCurrencyExpr(data) {
    if (!data || !data?.listPrice) {
      return '';
    }
    return data.listPrice.toFixed(2) + ' ' + data.currencyName;
  }

  setItemNameTooltip(event) {
    if (event && event.rowType === 'data' && event.column.dataField === 'itemName') {
      on(event.cellElement, 'mouseover', arg => {
        this.itemNameTooltipTimeout = window.setTimeout(async () => {

          this.itemNameTooltipData = null;
          let itemXRelatedPromise = this.itemXRelatedService.getItemXRelatedByItemIdForTurnoverAsync(this.clientId, event.data.itemId).then(r => {
            if (r && r.length > 0) {
              let icon = '';       

              this.itemNameTooltipData = r.map(item => {
                switch(item.relatedItemTypeId) {
                  case 1:
                    icon = 'equivalence-icon.png';
                    break;
                  case 2:
                    icon = 'complementarity-icon.png';
                    break;
                  case 3:
                    icon = 'superiority-icon.png';
                    break;
        
                  case 4:
                    icon = 'inferiority-icon.png';
                    break;
        
                  case 5:
                    icon = 'concurrency-icon.png';
                    break;
        
                  default:
                    break;
                }

                item.icon = icon;
                return item;
              })
            } else { this.itemNameTooltipData = []; }
          });

          Promise.all([itemXRelatedPromise]).then(r => {this.tooltipItemName.instance.show(arg.target);})
        }, 1000);
      });

      on(event.cellElement, 'mouseout', arg => {
        if (this.itemNameTooltipTimeout) {
          window.clearTimeout(this.itemNameTooltipTimeout);
        }
        this.tooltipItemName.instance.hide();
      });
    }
  }

  getRelatedItemPrice(data) {
    if (!data)
      return '';

    return data.price.toFixed(2) + ' ' + data.currencyName;
  }

  onHidingRelatedItemsTooltip(e) {
    const isMouseInsidePopover = e.component.content().parentNode.matches(":hover");
    e.cancel = isMouseInsidePopover;
    if (!e.component.content().parentNode.getAttribute("mouselistener")) {
      e.component.content().parentNode.addEventListener("mouseleave", (arg) => {
        e.component.hide();
      });
      e.component.content().parentNode.setAttribute("mouselistener", true);
    }
  }

  hasItemsRelated(itemId: number) {
    return (this.relatedItems.filter(x => x.itemId == itemId).length > 0);
  }

  onEditingStart(event: any) {
    const nonEditableFields = ['lastComment.shoppingCartQuantity', 'lastComment.hasStock', 'lastComment.atOrder'];

    if (nonEditableFields.includes(event.column.dataField) && event.data?.parentId === null) {
      event.cancel = true;
    }
  }

  getMonth(offset: number): string {
    const currentMonth = new Date().getMonth();
    const monthIndex = (currentMonth - offset + 12) % 12;
    const monthsInRomanian = [
      "Ian", "Feb", "Mar", "Apr", "Mai", "Iun",
      "Iul", "Aug", "Sep", "Oct", "Noi", "Dec"
    ];
    if (offset == 0) {
      return monthsInRomanian[monthIndex] + ' Curent';
    } else {
      return monthsInRomanian[monthIndex];
    }
  }

  onHidingItemCommentTooltip(e) {
    const isMouseInsidePopover = e.component.content().parentNode.matches(":hover");
    e.cancel = isMouseInsidePopover;
    if (!e.component.content().parentNode.getAttribute("mouselistener")) {
      e.component.content().parentNode.addEventListener("mouseleave", (arg) => {
        e.component.hide();
      });
      e.component.content().parentNode.setAttribute("mouselistener", true);
    }
  }

  openItemComments(row: any) {
    this.isItemCommentsPopupOpen = true;
    this.selectedItemComments = _.orderBy(row.itemComments, 'created', ['desc']);
    this.selectedItemComments.forEach(i => {
      if (i.isInvoiced) {
        i.status = 'Facturat';
      }
      if (i.hasStock) {
        i.status = 'Are stoc';
      }
      if (i.atOrder) {
        i.status = 'La comanda ';
      }
      if (i.notUsed) {
        i.status = 'Nu folosesc';
      }
    });
  }

  onHidingVipTiersTooltip(e) {
    const isMouseInsidePopover = e.component.content().parentNode.matches(":hover");
    e.cancel = isMouseInsidePopover;
    if (!e.component.content().parentNode.getAttribute("mouselistener")) {
      e.component.content().parentNode.addEventListener("mouseleave", (arg) => {
        e.component.hide();
      });
      e.component.content().parentNode.setAttribute("mouselistener", true);
    }
  }
}

