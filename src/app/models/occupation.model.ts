
export class Occupation {
    id: number;
    code: string;
    name: string;

    uniqueInLocation: boolean;
    uniqueInCustomer: boolean;

    occupationCodeId: number;
    departmentType: number;

    rowVersion: any;
}
