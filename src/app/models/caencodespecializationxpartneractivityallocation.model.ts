export class CaenCodeSpecializationXPartnerActivityAllocation {
    public id: number;
    public caenCodeSpecializationId: number;
    public partnerActivityAllocationId: number;
}
