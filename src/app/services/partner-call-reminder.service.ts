import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PartnerCallReminder } from 'app/models/partnercallreminder.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerCallReminderService {
  constructor(private apiService: ApiService) { }

  async getByPartnerAndPostIdAsync(partnerId: number, postId: number): Promise<any> {
    let filter = {'partnerId': partnerId, 'postId': postId}
    const apiURL = `/partner/GetPartnerCallReminders`;
    return await this.apiService.PostAsync<any>(apiURL, filter,  Constants.mrkApi);
  }

  async create(p: PartnerCallReminder): Promise<any> {
    const apiURL = `/partner/UpsertPartnerCallReminders`;
    return await this.apiService.PostAsync<any>(apiURL, [p],  Constants.mrkApi);
  }

  async update(p: PartnerCallReminder): Promise<any> {
    const apiURL = `/partner/UpsertPartnerCallReminders`;
    return await this.apiService.PostAsync<any>(apiURL, [p],  Constants.mrkApi);
  }

  async delete(id: number[]): Promise<any> {
    const apiURL = `/partner/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, id,  Constants.mrkApi);
  }
}
