import { BaseDomain } from "./base-domain.model";
import { VipOfferItemsDetails } from "./vip-offer-items-details.model";

export class VipOfferItems extends BaseDomain {
    vipOfferId: number;
    itemId: number;
    basePrice: number;
    isActive: boolean;


    itemCode: string;
    itemShortNameRo: string;
    itemCodeAndShortNameRo: string;


    vipOfferItemsDetails: VipOfferItemsDetails[];
}
