import { BaseDomain } from "./base-domain.model";

export class ApiLogs extends BaseDomain {
    apiServiceId: number;
    logMessage: string;
}