import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class SpecialOfferService {
  constructor(private apiService: ApiService) { }

  //GetERPItemLotStockAsync
  async getERPItemLotStockAsync(itemIds: number[]): Promise<any> {
    const apiURL = `/specialoffer/GetERPItemLotStock`;
    return await this.apiService.PostAsync<any>(apiURL, itemIds, Constants.mrkApi);
  }

  
}