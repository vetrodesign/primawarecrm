import { TestBed } from '@angular/core/testing';

import { PartnerLocationContactXMessagingAppService } from './partner-location-contact-x-messaging-app.service';

describe('PartnerLocationContactXMessagingAppService', () => {
  let service: PartnerLocationContactXMessagingAppService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerLocationContactXMessagingAppService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
