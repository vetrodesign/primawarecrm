import { TestBed } from '@angular/core/testing';

import { DiscountGridDetailsService } from './discount-grid-details.service';

describe('DiscountGridDetailsService', () => {
  let service: DiscountGridDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiscountGridDetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
