export enum ERPSyncSource {
    PrimawareMarketing = 1,
    PrimawareCRM = 2,
    PrimawareSRM = 3,
    EMAGRO = 4,
    EMAGBG = 5,
    EMAGHU = 6,
    CEL = 7,
    Magento = 8,
    ANAF = 9
  }
  