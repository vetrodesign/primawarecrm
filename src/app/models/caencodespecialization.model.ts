export class CaenCodeSpecialization {
    public id: number;
    public code: string;
    public description: string;
    public caenCodeId: number;
}