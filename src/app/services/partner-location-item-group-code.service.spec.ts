import { TestBed } from '@angular/core/testing';

import { PartnerLocationItemGroupCodeService } from './partner-location-item-group-code.service';

describe('PartnerLocationItemGroupCodeService', () => {
  let service: PartnerLocationItemGroupCodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerLocationItemGroupCodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
