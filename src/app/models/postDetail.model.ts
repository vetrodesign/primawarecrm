import { BaseDomain } from "./base-domain.model";

export class PostDetail extends BaseDomain {
    postId: number;
    emailBackup: string;
    phone: string;
    phoneInterior: string;
    mobilePhone: string;
    whatsApp: string;
    webSite: string;
    skype: string;
    facebook: string;
    youTube: string;
    isActive: boolean;
}