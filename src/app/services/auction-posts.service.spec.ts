import { TestBed } from '@angular/core/testing';

import { AuctionPostsService } from './auction-posts.service';

describe('AuctionPostsService', () => {
  let service: AuctionPostsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuctionPostsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
