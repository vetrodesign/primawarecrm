import { BaseDomain } from "./base-domain.model";

export class PartnerReviewQuestion extends BaseDomain
{
    public question: string;
    public customerId: number;
    public isActive: boolean;

    public response: string;
}