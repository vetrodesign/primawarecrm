import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { SupplierPriceListXPartnerLocation } from 'app/models/supplierpricelistxpartnerlocation.model';

@Injectable({
  providedIn: 'root'
})
export class SupplierPriceListPartnerLocationService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<any[]> {
    const apiURL = `/supplierpricelistxpartnerlocation/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getAllByPartnerLocationAsync(partnerLocationId: number): Promise<any[]> {
    const apiURL = `/supplierpricelistxpartnerlocation/getallbypartnerlocation`;
    return await this.apiService.PostAsync<any[]>(apiURL, partnerLocationId, Constants.mrkApi);
  }

  async createAsync(p: SupplierPriceListXPartnerLocation): Promise<any> {
    const apiURL = `/supplierpricelistxpartnerlocation/create`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async createMultipleAsync(p: SupplierPriceListXPartnerLocation[]): Promise<any> {
    const apiURL = `/supplierpricelistxpartnerlocation/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async updateAsync(p: SupplierPriceListXPartnerLocation): Promise<any> {
    const apiURL = `/supplierpricelistxpartnerlocation/update`;
    return await this.apiService.PutAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async deleteAsync(p: SupplierPriceListXPartnerLocation[]): Promise<any> {
    const apiURL = `/supplierpricelistxpartnerlocation/deleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }
}
