import { BaseDomain } from "./base-domain.model";

export class AssociatedItem extends BaseDomain {
    public itemId: number;
    public partnerId: number;
}
