import { Injectable } from '@angular/core';
import { Occupation } from 'app/models/occupation.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class OccupationService {

  constructor(private apiService: ApiService) { }

  async getAllOccupationsAsync(): Promise<Occupation[]> {
    const apiURL = `/Occupation/getall`;
    return await this.apiService.GetAsync<Occupation[]>(apiURL, Constants.commonApi);
  }

  async createOccupationsAsync(office: Occupation): Promise<any> {
    const apiURL = `/Occupation/create`;
    return await this.apiService.PostAsync<any>(apiURL, office, Constants.commonApi);
  }

  async updateOccupationsAsync(office: Occupation): Promise<any> {
    const apiURL = `/Occupation/update`;
    return await this.apiService.PutAsync<any>(apiURL, office, Constants.commonApi);
  }

  async deleteOccupationsAsync(ids: number[]): Promise<any> {
    const apiURL = `/Occupation/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
