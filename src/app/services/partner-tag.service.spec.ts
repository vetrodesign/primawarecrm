import { TestBed } from '@angular/core/testing';

import { PartnerTagService } from './partner-tag.service';

describe('PartnerTagService', () => {
  let service: PartnerTagService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerTagService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
