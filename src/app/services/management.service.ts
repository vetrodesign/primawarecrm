import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Management } from 'app/models/management.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ManagementService {

  constructor(private apiService: ApiService) { }

  async getAllManagementsAsync(): Promise<Management[]> {
    const apiURL = `/management/getall`;
    return await this.apiService.GetAsync<Management[]>(apiURL, Constants.ssoApi);
  }

  async getManagementsAsyncByID(): Promise<Management[]> {
    const apiURL = `/management/getbycustomerid/`;
    return await this.apiService.GetAsync<Management[]>(apiURL, Constants.ssoApi);
  }

  async getManagementsBySiteID(): Promise<Management[]> {
    const apiURL = `/management/GetAllByCustomerLocation`;
    return await this.apiService.GetAsync<Management[]>(apiURL, Constants.ssoApi);
  }
}
