import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportExportItemComponent } from './import-export-item.component';

describe('ImportExportItemComponent', () => {
  let component: ImportExportItemComponent;
  let fixture: ComponentFixture<ImportExportItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportExportItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportExportItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
