import { Component, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Constants } from 'app/constants';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { ProductConventionShortEnum } from 'app/enums/productConventionEnum';
import { ItemTurnoverComponent } from 'app/item-turnover/item-turnover.component';
import { Partner } from 'app/models/partner.model';
import { PartnerLocationXSalePriceList } from 'app/models/partnerLocationXSalePriceList.model';
import { PartnerLocation } from 'app/models/partnerlocation.model';
import { PartnerLocationDTO } from 'app/models/partnerlocationdto.model';
import { PartnerLocationXSecondaryActivityAllocation } from 'app/models/partnerlocationxsecondaryactivityallocation.model';
import { SalePriceList } from 'app/models/salepricelist.model';
import { SalePriceListFilterVM } from 'app/models/salepricelistfilter.model';
import { PriceApprovedTooltipDataRequest, SpecialPriceRequestItem } from 'app/models/special-price-request.model';
import { AuthService } from 'app/services/auth.service';
import { CurrencyRateService } from 'app/services/currency-rate.service';
import { ItemService } from 'app/services/item.service';
import { NotificationService } from 'app/services/notification.service';
import { PartnerLocationSalePriceListService } from 'app/services/partner-location-sale-price-list.service';
import { PartnerLocationSecondaryActivityAllocationService } from 'app/services/partner-location-secondary-activity-allocation.service';
import { PartnerLocationService } from 'app/services/partner-location.service';
import { PartnerService } from 'app/services/partner.service';
import { SalePriceListService } from 'app/services/sale-price-list.service';
import { SpecialPriceRequestItemsService } from 'app/services/special-price-request-items.service';
import { SupplierPriceListPartnerLocationService } from 'app/services/supplier-price-list-partner-location.service';
import { DxDataGridComponent, DxSelectBoxComponent, DxValidatorComponent } from 'devextreme-angular';
import { environment } from 'environments/environment';
import * as moment from 'moment';

const missingConventionPlaceholder: string = '(lipsa conventie)';

@Component({
  selector: 'app-partner-sales-price-list',
  templateUrl: './partner-sales-price-list.component.html',
  styleUrls: ['./partner-sales-price-list.component.css']
})
export class PartnerSalesPriceListComponent implements OnInit {
  @Input() partner: Partner;
  @Input() salePriceListDS: any;
  @Input() supplierSalePriceListDS: any;
  @Input() currencies: any;
  @Input() selectedPartnerLocation: PartnerLocation;
  @Input() partnerLocations: any;
  @Input() partnerActivity: any;
  @Input() partnerActivityAllocation: any;
  @Input() initialPartnerLocation: any;
  @Input() users: any;
  @Input() posts: any;

  @ViewChild('salesPriceListValidationGroup') salesPriceListValidationGroup: DxValidatorComponent;
  @ViewChild('supplierSalesPriceListValidationGroup') supplierSalesPriceListValidationGroup: DxValidatorComponent;
  @ViewChild('initialPartnerLocationPartnerPrincipalActivityRef', { static: false }) initialPartnerLocationPartnerPrincipalActivityInstance: DxSelectBoxComponent;
  @ViewChild('implicitLocationPartnerPrincipalActivityRef', { static: false }) implicitLocationPartnerPrincipalActivityInstance: DxSelectBoxComponent;
  @ViewChild('locationPartnerPrincipalActivityRef', { static: false }) locationPartnerPrincipalActivityInstance: DxSelectBoxComponent;

  @ViewChild('specialPriceRequestHistoryDataGrid') specialPriceRequestHistoryDataGrid: DxDataGridComponent;
  @ViewChild('itemTurnoverComponent') itemTurnoverComponent: ItemTurnoverComponent;

  loaded: boolean;
  selectedSalesPriceListPartnerLocation: number;
  salesPriceList: any;
  activities: any;
  selectedPartnerActivityAllocationId: number;
  salesPriceListByLocation: { [key: string]: number[] } = {};
  allSupplierSalesPriceList: any[] = [];
  activitiesByLocation: { [key: string]: number[] } = {};
  isOnSavePartnerLocation: boolean = true;
  implicitPostId: number;
  isOnSave: boolean = true;
  allLocationNames: string;
  implicitLocation: any;
  sortedSalePriceListDS: any;
  sortedSupplierSalePriceListDS: any;
  sortedPartnerActivityAllocation: any;
  sortedPartnerLocations: any;
  specialPriceRequestHistory: SpecialPriceRequestItem[] = [];
  specialPriceRequests: SpecialPriceRequestItem[] = [];
  isOwner: boolean;
  salePriceLists: SalePriceList[];
  selectedSpecialPriceRequestId: number;
  allPriceApprovedTooltipData: any[] = [];
  itemDS: any;
  priceApprovedTooltipData: { [key: number]: any } = {};
  isItemTurnoverPopupOpened: boolean;
  filterTurnoverItemCode: string;
  isDataLoading: boolean;
  partnersDS: any;
  specialPriceRequestsDataLoaded: boolean;

  constructor(private readonly partnerLocationSalePriceListService: PartnerLocationSalePriceListService,
    private readonly notificationService: NotificationService,
    private readonly currencyService: CurrencyRateService,
    private readonly partnerLocationService: PartnerLocationService,
    private readonly partnerLocationSecondaryActivityAllocationService: PartnerLocationSecondaryActivityAllocationService,
    private readonly partnerService: PartnerService,
    private readonly salePriceListService: SalePriceListService,
    private readonly specialPriceRequestsItemsService: SpecialPriceRequestItemsService,
    private readonly authenticationService: AuthService,
    private readonly itemsService: ItemService,
    private readonly partnerLocationSupplierSalePriceListService: SupplierPriceListPartnerLocationService
  ) { 
    this.getDisplayExprSalePriceLists = this.getDisplayExprSalePriceLists.bind(this);
    this.getDisplayExprPartnerActivity = this.getDisplayExprPartnerActivity.bind(this);
    this.getDisplayExprSupplierSalePriceLists = this.getDisplayExprSupplierSalePriceLists.bind(this);

    this.authenticationService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authenticationService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
  }

  async ngOnInit() {
    await this.getActivitiesAndSalePriceList();

    if (this.initialPartnerLocationPartnerPrincipalActivityInstance && this.initialPartnerLocationPartnerPrincipalActivityInstance.instance) {
      this.initialPartnerLocationPartnerPrincipalActivityInstance.instance.repaint();
    }
    if (this.implicitLocationPartnerPrincipalActivityInstance && this.implicitLocationPartnerPrincipalActivityInstance.instance) {
      this.implicitLocationPartnerPrincipalActivityInstance.instance.repaint();
    }
    if (this.locationPartnerPrincipalActivityInstance && this.locationPartnerPrincipalActivityInstance.instance) {
      this.locationPartnerPrincipalActivityInstance.instance.repaint();
    }
  }

  ngAfterViewInit() {
    this.implicitLocation = this.partnerLocations.find(f => f.isImplicit === true) || this.partnerLocations.find(f => f.isActive === true);
  }

  async getActivitiesAndSalePriceList() {
    this.sortedPartnerLocations = this.partnerLocations.sort((a, b) => {
      if (a.isImplicit) return -1;
      if (b.isImplicit) return 1;
      return 0;
    });

    const promises = [];
  
    //this.loaded = true; -- temporary hidden because it covers some UI

    for (const location of this.sortedPartnerLocations) {
      const locationId = location.id;

      const supplierSalePriceListPromise = this.partnerLocationSupplierSalePriceListService
      .getAllByPartnerLocationAsync(locationId)
      .then(items => {
        if (items) {
          const supplierSalesPriceListIds = items.map(m => m.supplierPriceListId);
          this.allSupplierSalesPriceList.push(...supplierSalesPriceListIds);

          this.partnerLocations.map(m => {
            if (m.id === locationId) {
              m.existingSupplierSalesPriceListIds = items.map(m => m.supplierPriceListId);
            }

            return m;
          });
        }
      });

      const salePriceListPromise = this.partnerLocationSalePriceListService
        .getAllByPartnerLocationAsync(locationId)
        .then(items => {
          if (items) {
            const salesPriceListIds = items.map(m => m.salePriceListId);
            this.salesPriceListByLocation[locationId] = salesPriceListIds;

            this.partnerLocations.map(m => {
              if (m.id === locationId) {
                m.existingSalesPriceListIds = items.map(m => m.salePriceListId);
              }

              return m;
            });
          }
        });

        const activityPromise = this.partnerLocationSecondaryActivityAllocationService
        .getAllByPartnerLocationIdAsync(locationId)
        .then(items => {
          if (items) {
            const activitiesIds = items.map(m => m.partnerActivityAllocationId);
            this.activitiesByLocation[locationId] = activitiesIds;

            this.partnerLocations.map(m => {
              if (m.id === locationId) {
                m.existingPartnerSecondaryActivityAllocationIds = items.map(m => m.partnerActivityAllocationId);
              }

              return m;
            });
          }
        });

      promises.push(supplierSalePriceListPromise, salePriceListPromise, activityPromise);
    }

    await Promise.all(promises).then(() => {
      this.loaded = false;
    })
  }

  async getSpecialPriceRequestData() {
    this.specialPriceRequestsDataLoaded = true;
    await this.getSalePriceListsForSpecialPriceRequests();
    await this.getItems();
    await this.getPartners();
    await this.getPartnerSpecialPriceRequests(true);
    this.specialPriceRequestsDataLoaded = false;
  }

  async ngOnChanges(changes: SimpleChanges): Promise<void> {
    this.sortedSalePriceListDS = [...this.salePriceListDS];
    this.sortedSupplierSalePriceListDS = [...this.supplierSalePriceListDS];
    if (this.partner.id) {
      this.isOnSave = false;
    }

    if (changes.partnerLocations && this.partnerLocations && this.partnerLocations.length > 0) {

      this.allLocationNames = this.partnerLocations.map(m => m.name).join(", ");
      this.implicitLocation = this.partnerLocations.find(f => f.isImplicit === true) || this.partnerLocations.find(f => f.isActive === true);

      this.sortedPartnerActivityAllocation = [ ...this.partnerActivityAllocation ];

      this.selectedSalesPriceListPartnerLocation = this.partnerLocations[0].id;
      this.selectedPartnerActivityAllocationId = this.partnerLocations[0].partnerActivityAllocationId;

      await this.getActivitiesAndSalePriceList();
    }
  }

  getDisplayExprPartnerActivity(value: any) {
    if (value && this.partnerActivity) {
      const item = this.partnerActivity.find(x => x.id === value.partnerActivityId);
      if (item) {
        return value.saleZoneCode + ' - ' + item.name + ' - ' + value.partnerActivityClassification;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  getDisplayExprSalePriceLists(item) {
    if (!item) {
      return '';
    }
    let c = (this.currencies && this.currencies.length > 0) ? this.currencies.find(i => i.id === item.currencyId) : null;
    return item.code + ' - ' + item.name  + ' - ' + c?.name;
  }

  getDisplayExprSupplierSalePriceLists(item) {
    if (!item) {
      return '';
    }
    let c = (this.currencies && this.currencies.length > 0) ? this.currencies.find(i => i.id === item.currencyId) : null;
    return item.name  + ' - ' + c?.name;
  }

  async getBaseCurrency(): Promise<any> {
    await this.currencyService.getBaseCurrency().then(x => {
      this.currencies = x;
    });
  }

  async getSalesPriceList() { 
    if (this.partner && this.partner.id) {
      if (this.partner.isPrincipalActivityUniqueForAllLocations) {
        await this.partnerLocationSalePriceListService.getAllByPartnerAsync(this.partner.id).then(items => {
          if (items) {
            var arr = items.map(m => m.salePriceListId);
            this.salesPriceList = Array.from(new Set(arr));
          }
        })
      } else {     
        await this.partnerLocationSalePriceListService.getAllByPartnerLocationAsync(this.selectedSalesPriceListPartnerLocation).then(items => {
          if (items) {         
            var arr = items.map(m => m.salePriceListId);
            this.salesPriceList = Array.from(new Set(arr));
          }
        })
      }
    }
  }

  async getActivities() {
    if (this.partner && this.partner.id) {
      if (this.partner.isPrincipalActivityUniqueForAllLocations) {
        await this.partnerLocationSecondaryActivityAllocationService.getAllByPartnerLocationIdAsync(this.partner.id).then(items => {
          if (items) {
            var arr = items.map(m => m.partnerActivityAllocationId);
            this.activities = Array.from(new Set(arr)).push(this.selectedPartnerActivityAllocationId);
          }
        })
      } else {
        await this.partnerLocationSecondaryActivityAllocationService.getAllByPartnerLocationIdAsync(this.selectedSalesPriceListPartnerLocation).then(items => {
          if (items) {
            
            var arr = items.map(m => m.partnerActivityAllocationId);
            this.activities = [ this.selectedPartnerActivityAllocationId, ...arr ];
          }
        })
      }
    }
  }

  async onSalesPriceListSaveClick() {
    this.loaded = true;

    const processLocation = async (item) => {
      let selectedPl = this.partnerLocations.find(f => f.id === item.id);

      if (this.partner.isPrincipalActivityUniqueForAllLocations) {
        item.partnerActivityAllocationId = this.implicitLocation.partnerActivityAllocationId;
      }
      const r = await this.partnerLocationService.updatePartnerLocationPartnerActivityAllocationAsync(item);
      if (r) {
        item.rowVersion = r.rowVersion;
       
        // Create SalePriceListXPartnerLocation
        const existingSalePriceListIds = selectedPl.existingSalesPriceListIds;
        let deleteSalePriceListPartnerLocationAllocation: PartnerLocationXSalePriceList[] = [];
        existingSalePriceListIds?.filter(x => !this.salesPriceListByLocation[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].includes(x)).forEach(id => {
          const c = new PartnerLocationXSalePriceList();
          c.partnerLocationId = item.id;
          c.salePriceListId = id;
          deleteSalePriceListPartnerLocationAllocation.push(c);
        });

        let createSalePriceListIds: PartnerLocationXSalePriceList[] = [];
        this.salesPriceListByLocation[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].filter(x => !existingSalePriceListIds.includes(x)).forEach(id => {
          const c = new PartnerLocationXSalePriceList();
          c.partnerLocationId = Number(item.id);
          c.salePriceListId = id;
          createSalePriceListIds.push(c);
        });

        if (createSalePriceListIds.length > 0 || deleteSalePriceListPartnerLocationAllocation.length > 0) {
          await this.partnerLocationSalePriceListService.updateMultipleAsync(Number(item.id), createSalePriceListIds.map(m => m.salePriceListId), deleteSalePriceListPartnerLocationAllocation.map(m => m.salePriceListId));
        }

        // Create PartnerSecondaryActivityAllocation
        const existingSecondaryActivityAllocationIds = selectedPl.existingPartnerSecondaryActivityAllocationIds;
        let deleteSecondaryActivityAllocation: PartnerLocationXSecondaryActivityAllocation[] = [];
        existingSecondaryActivityAllocationIds?.filter(x => !this.activitiesByLocation[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].includes(x)).forEach(id => {
          const c = new PartnerLocationXSecondaryActivityAllocation();
          c.partnerLocationId = item.id;
          c.partnerActivityAllocationId = id;
          deleteSecondaryActivityAllocation.push(c);
        });
        if (deleteSecondaryActivityAllocation.length > 0) {
          await this.partnerLocationSecondaryActivityAllocationService.deleteMultipleAsync(deleteSecondaryActivityAllocation);
        }
        let createSecondaryActivityAllocationIds: PartnerLocationXSecondaryActivityAllocation[] = [];
        this.activitiesByLocation[this.partner.isPrincipalActivityUniqueForAllLocations ? this.implicitLocation.id : item.id].filter(x => !existingSecondaryActivityAllocationIds.includes(x)).forEach(id => {
          const c = new PartnerLocationXSecondaryActivityAllocation();
          c.partnerLocationId = Number(item.id);
          c.partnerActivityAllocationId = id;
          createSecondaryActivityAllocationIds.push(c);
        });
        if (createSecondaryActivityAllocationIds.length > 0) {
          await this.partnerLocationSecondaryActivityAllocationService.createMultipleAsync(createSecondaryActivityAllocationIds).then(() => {});
        }
      }
    };

    for (const location of this.partnerLocations) {
      await processLocation(location).then(() => {})
    }

    await this.getActivitiesAndSalePriceList().then(() => {
      this.notificationService.alert('top', 'center', 'Liste de pret - Datele au fost modificate cu succes!', NotificationTypeEnum.Green, true);
      this.loaded = false;
    })
  }

  partnerLocationsDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.name;
  }

  async onSelectedLocationValueChanged(event: any) {
    if (event && event.value && this.salesPriceList) {
      let selectedPl = this.partnerLocations.find(f => f.id === event.value);
      if (selectedPl) {
        await this.getSalesPriceList();
      }
    }
  }

  isOnSavePartnerLocationOutputChange(event: any) {
    this.isOnSavePartnerLocation = event;
  }

  async checkForImplicitAgent()  {
    this.implicitPostId = null;
    if (this.partner && this.partner.isClient && this.isOnSave && this.initialPartnerLocation && this.initialPartnerLocation.countryId && this.initialPartnerLocation.countyId &&
      this.initialPartnerLocation.cityId && this.initialPartnerLocation.partnerActivityAllocationId) {
        await this.partnerService.findSalesAgentBasedOnLocationAndSpecIdAsync(this.initialPartnerLocation.countryId,this.initialPartnerLocation.countyId,this.initialPartnerLocation.cityId,this.initialPartnerLocation.partnerActivityAllocationId).then(r => {
          if (r) {
            this.implicitPostId = r;
            if (!this.partner.hasAssignedSalePost || this.partner.hasAssignedSalePost  == undefined || this.partner.hasAssignedSalePost == null) {
              this.partner.salesAgentId= r;
            }
          }
        })
    }
    if (this.partner && this.partner.isClient && !this.isOnSave && this.partnerLocations.length > 0) {
      var implicitPartnerLocation = this.partnerLocations.find(x => x.isImplicit);
      if (implicitPartnerLocation && implicitPartnerLocation.countryId && implicitPartnerLocation.countyId && implicitPartnerLocation.cityId && implicitPartnerLocation.partnerActivityAllocationId)        
        {
        await this.partnerService.findSalesAgentBasedOnLocationAndSpecIdAsync(implicitPartnerLocation.countryId,implicitPartnerLocation.countyId,implicitPartnerLocation.cityId,implicitPartnerLocation.partnerActivityAllocationId).then(r => {
          if (r) {
            this.implicitPostId = r;
            if (!this.partner.hasAssignedSalePost || this.partner.hasAssignedSalePost  == undefined || this.partner.hasAssignedSalePost == null) {
              this.partner.salesAgentId= r;
            }
          }
       })
      }
    }
  }

  partnerActivityChange(e: any) {
    if (this.isOnSave && e && e.value) {
      let pa = this.partnerActivityAllocation.find(y => y.id === e.value);
      if (pa) {
       
        this.sortedSalePriceListDS = this.salePriceListDS.filter(x => x.salePriceListXProductConventionList && x.salePriceListXProductConventionList.map(y => y.productConventionId).includes(pa.productConventionId));
        if (this.initialPartnerLocation.countryId) {
          let currency = 1;
          currency = this.initialPartnerLocation.countryId == 33 ? 19 : currency;
          currency = this.initialPartnerLocation.countryId == 99 ? 61 : currency;
          this.initialPartnerLocation.salePriceListIds = [];
          this.initialPartnerLocation.salePriceListIds = this.salePriceListDS.filter(x => x.salePriceListXProductConventionList && x.currencyId == currency && x.salePriceListXProductConventionList.map(y => y.productConventionId).includes(pa.productConventionId) && x.isImplicit && x.status).map(y => y.id);
          }
      }
    }

    this.checkForImplicitAgent();
  }

  async selectedPartnerActivityChanged(e: any) {
    if (e && e.value && this.partnerActivityAllocation && this.partnerActivityAllocation.length) {
      let pa = this.partnerActivityAllocation.find(y => y.id === e.value);
      if (pa) {
        if (this.isOnSavePartnerLocation) {
          this.sortedSalePriceListDS = this.salePriceListDS.filter(x => x.salePriceListXProductConventionList && x.salePriceListXProductConventionList.map(y => y.productConventionId).includes(pa.productConventionId));
          if (this.implicitLocation.countryId) {
            let currency = 1;
            currency = this.implicitLocation.countryId == 33 ? 19 : currency;
            currency = this.implicitLocation.countryId == 99 ? 61 : currency;
            this.implicitLocation.salePriceListIds = [];
            this.implicitLocation.salePriceListIds = this.salePriceListDS.filter(x => x.salePriceListXProductConventionList && x.currencyId == currency && x.salePriceListXProductConventionList.map(y => y.productConventionId).includes(pa.productConventionId) && x.isImplicit && x.status).map(y => y.id);
          }
         
        } else {
          let salePriceListFilter = new SalePriceListFilterVM();
          salePriceListFilter.productConventionIds = [pa.productConventionId];
          if (this.implicitLocation && this.implicitLocation.baseCurrencyId && this.implicitLocation.alternativeCurrencyId) {
            salePriceListFilter.currencyIds = [this.implicitLocation.baseCurrencyId, this.implicitLocation.alternativeCurrencyId];
          } else if (this.initialPartnerLocation && this.initialPartnerLocation.baseCurrencyId && this.initialPartnerLocation.alternativeCurrencyId) {
            salePriceListFilter.currencyIds = [this.initialPartnerLocation.baseCurrencyId, this.initialPartnerLocation.alternativeCurrencyId];
          } else {
            salePriceListFilter.currencyIds = [1];
          }
  
          await this.salePriceListService.getSalePriceListByFilter(salePriceListFilter).then(items => {
            if (items && items.length > 0) {
              this.sortedSalePriceListDS = items;
            }
          })
        }
      }
    }

    this.checkForImplicitAgent();
  }

  onInitialLocationSalePriceListOpen(event: any) {
    let arr = this.salePriceListDS.slice().sort((a, b) => {
      const aSelected = this.initialPartnerLocation.salePriceListIds && this.initialPartnerLocation.salePriceListIds.includes(a.id);
      const bSelected = this.initialPartnerLocation.salePriceListIds && this.initialPartnerLocation.salePriceListIds.includes(b.id);
      if (aSelected && !bSelected) return -1;
      if (!aSelected && bSelected) return 1;
      return 0;
    });
  }

  onInitialLocationSupplierSalePriceListOpen(event: any) {
    let arr = this.supplierSalePriceListDS.slice().sort((a, b) => {
      const aSelected = this.initialPartnerLocation.supplierSalePriceListIds && this.initialPartnerLocation.supplierSalePriceListIds.includes(a.id);
      const bSelected = this.initialPartnerLocation.supplierSalePriceListIds && this.initialPartnerLocation.supplierSalePriceListIds.includes(b.id);
      if (aSelected && !bSelected) return -1;
      if (!aSelected && bSelected) return 1;
      return 0;
    });
  }

  async getSalePriceListsForSpecialPriceRequests(): Promise<any> {
    if (this.isOwner) {
      await this.salePriceListService.getAllSalePriceListsWithProductConventionsAsync().then(i => {
        this.salePriceLists = i;
      });
    } else {
      await this.salePriceListService.getSalePriceListsAsyncByIDWithProductConventions().then(i => {
        this.salePriceLists = i;
      });
    }
  }

  getProductConventionType(partnerProductConventionId: number, item: any): number {
    switch(partnerProductConventionId) {
      case ProductConventionShortEnum.C0:
        return item.c0;
      case ProductConventionShortEnum.D0:
        return item.d0;
      case ProductConventionShortEnum.R0:
        return item.r0;
      default:
        return 0;
    }
  }

  async getItems(): Promise<any> {
    await this.itemsService.getAllItemSmallAsync().then(items => {
      this.itemDS = {
        paginate: true,
        pageSize: 15,
        store: items
      };
    });
  }

  async getPriceApprovedTooltipData(shouldGetHistory?: boolean) {
    var request = new PriceApprovedTooltipDataRequest();

    if (!shouldGetHistory) {
      request.itemIds = this.specialPriceRequests.map(m => m.itemId);
    } else if (shouldGetHistory) {
      request.itemIds = this.specialPriceRequestHistory.map(m => m.itemId);
    }

    request.partnerId = this.partner.id;
    request.shouldGetHistory = shouldGetHistory;

    await this.specialPriceRequestsItemsService.getPriceApprovedTooltipData(request).then(data => {
      this.allPriceApprovedTooltipData = data.map(m => {
        let item = this.itemDS.store.find(f => f.id === m.itemId);
        let itemData = null;
        if (item) {
          itemData = item.code + " - " + item.name;
        }

        return {
          ...m, 
          created: moment(m.created).format('DD.MM.YYYY').toString(),
          priceRequested: m.priceRequested != null ? parseFloat(m.priceRequested).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '',
          priceApproved: m.priceApproved != null ? parseFloat(m.priceApproved).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '',
          quantity: Number(m.quantity).toLocaleString('en-US'),
          approvedQuantity: m.approvedQuantity && Number(m.approvedQuantity).toLocaleString('en-US'),
          itemData: itemData
        }
      });
    });
  }

  async getPartnerSpecialPriceRequests(shouldGetHistory?: boolean) {
    if (this.partner && this.partner.id) {

      if (this.implicitLocation) {
        this.partnerService.getPartnerProductConventionIdsByLocationsAsync([this.implicitLocation.id]).then((r: PartnerLocationDTO[]) => {
          if (r) {        
            const item = r.find(e => e.locationId == this.implicitLocation.id);
            if (item) {
              const conventionId = item.conventionId;

              if (conventionId) {
                this.implicitLocation.productConventionName = ProductConventionShortEnum[conventionId];
                this.implicitLocation.productConventionId = conventionId;
              } else {
                this.implicitLocation.productConventionName = missingConventionPlaceholder;
              }
            } else {
              this.implicitLocation.productConventionName = missingConventionPlaceholder;
            }          
          }
        });
      }

      if (!shouldGetHistory) {
        this.specialPriceRequests = [];
      }

      this.loaded = true;
      await this.specialPriceRequestsItemsService.getAllByPartnerIdAsync(this.partner.id, shouldGetHistory).then(async items => {
        if (items && items.length > 0) {
          let allRequests = items.map(item => {
            item.createdFormated = moment(new Date(item.created)).format('DD.MM.YYYY');

            let partnerProductConventionName = this.implicitLocation?.productConventionName;
            let partnerProductConventionId = this.implicitLocation?.productConventionId;

            let salePriceList = this.salePriceLists.find(f => f.id === item.salePriceListId && f.salePriceListXProductConventionList && f.salePriceListXProductConventionList.map(m => m.productConventionId).includes(partnerProductConventionId));

            let salePriceListCode;
            if (salePriceList) {
              if (salePriceList.name && salePriceList.name.toLowerCase().replace(/\s+/g, '').includes(Constants.salePriceListPpName)) {
                salePriceListCode = `PP${partnerProductConventionName}`;
              } else {
                salePriceListCode = `${partnerProductConventionName}`;
              }
            } else {
              salePriceListCode = `${partnerProductConventionName}`;
            }

            let auxBaxValue = item.baxValue ? (item.quantity / item.baxValue) % 1 !== 0 ? ((item.quantity / item.baxValue).toFixed(2)) : (item.quantity / item.baxValue) : 0;
            item.baxValueComputed = auxBaxValue.toString();
            
            let productConventionType = this.getProductConventionType(partnerProductConventionId, item);

            let auxDiscountGrid = productConventionType ? Math.round(((item.priceOfferedWithoutTva - productConventionType) / productConventionType) * 100) : 0;
            let auxCmp = item.discountGridCmp ? Math.round(((item.priceOfferedWithoutTva - item.discountGridCmp) / item.discountGridCmp) * 100) : 0;
            item.discountGridCmpComputed = `${salePriceListCode} ${auxDiscountGrid}%(${auxCmp}%)`; 
            item.auxDiscountGrid = auxDiscountGrid;
            item.auxCmp = auxCmp;
            item.salePriceListCode = salePriceListCode;

            let auxDiscountGridRequested = productConventionType ? Math.round(((item.priceRequestedWithoutTva - productConventionType) / productConventionType) * 100) : 0;
            let auxCmpRequested = item.discountGridCmp ? Math.round(((item.priceRequestedWithoutTva - item.discountGridCmp) / item.discountGridCmp) * 100) : 0;
            item.discountGridCmpRequestedComputed = `${salePriceListCode} ${auxDiscountGridRequested}%(${auxCmpRequested}%)`; 
            item.auxDiscountGridRequested = auxDiscountGridRequested;
            item.auxCmpRequested = auxCmpRequested;
        
            let auxDiscountGridApproved = productConventionType ? Math.round(((item.priceApproved - productConventionType) / productConventionType) * 100) : 0;
            let auxCmpRequestedApproved = item.discountGridCmp ? Math.round(((item.priceApproved - item.discountGridCmp) / item.discountGridCmp) * 100) : 0;
            item.approvedDiscountGridCmp = item.priceApproved ? `${salePriceListCode} ${auxDiscountGridApproved}%(${auxCmpRequestedApproved}%)` : "";
            item.auxDiscountGridApproved = auxDiscountGridApproved;
            item.auxCmpRequestedApproved = auxCmpRequestedApproved;

            if (item.isRejected === true || item.isRejected === false) {
              let user = this.users.find(f => f.id === item.modifiedBy);
              if (user) {
                let post = this.posts?.find(x => x.id === user.postId);
                item.approvingUser = user.firstName + " " + user.lastName;

                if (post) {
                  item.approvingUser = post.code + " " + item.approvingUser;
                }
              }
            }

            item.d0Andc0 = `${item.d0 ? item.d0.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '0.00'} (${item.c0 ? item.c0.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '0.00'})`;

            item.priceOfferedWithoutTva = item.priceOfferedWithoutTva && +item.priceOfferedWithoutTva.toFixed(2);
            item.priceRequestedWithoutTva = item.priceRequestedWithoutTva && +item.priceRequestedWithoutTva.toFixed(2);
            item.priceApproved = item.priceApproved && +item.priceApproved.toFixed(2);

            let splitTurnoverRs = item.turnOverRs.split(" / ");
            let formattedSplitTurnoverRs = splitTurnoverRs.map(m => {
              return Number(m).toLocaleString('en-US');
            })
            item.turnoverRsComputed = formattedSplitTurnoverRs.join(" / ");

            if (item.isRejected === true) {
              item.priceApproved = item.priceOfferedWithoutTva;
              item.approvedQuantity = item.quantity;
            }

            return item;
          });

          if (!shouldGetHistory) {
            if (this.selectedSpecialPriceRequestId) {
              this.specialPriceRequests = allRequests.filter(f => (f.isRejected === null || f.isRejected === undefined) && f.specialPriceRequestId === this.selectedSpecialPriceRequestId);
            } else {
              this.specialPriceRequests = allRequests.filter(f => f.isRejected === null || f.isRejected === undefined);
            }
          }

          if (shouldGetHistory) {
            this.specialPriceRequestHistory = allRequests.filter(f => f.isRejected === false || f.isRejected === true);
          }
        }
        
        await this.getPriceApprovedTooltipData(shouldGetHistory);
        this.loaded = false;
      })
    }
  }

  getDisplayExprItems(item) {
    if (!item) {
      return '';
    }

    return item.code + ' - ' + item.name;
  }

  getDisplayExprPartners(partner) {
    if (!partner) {
      return '';
    }

    const partnerName: string = partner.name;
    const partnerCode: string = partner.code;

    let name: string = '(lipsa nume)';
    let code: string = '(lipsa cod)';

    if (partnerName !== undefined && partnerName !== null && partnerName !== '') {
      name = partnerName;
    }

    if (partnerCode !== undefined && partnerCode !== null && partnerCode !== '') {
      code = partnerCode;
    }

    return name + ' - ' + code;
  }

  generateTooltipHistoryId(index: number): string {
    return `priceApprovedInfo-${index}`;
  }

  onHidingHistory(e) {
    const isMouseInsidePopover = e.component.content().parentNode.matches(":hover");
    e.cancel = isMouseInsidePopover;
    if (!e.component.content().parentNode.getAttribute("mouselistener")) {
      e.component.content().parentNode.addEventListener("mouseleave", (arg) => {
        e.component.hide();
      });
      e.component.content().parentNode.setAttribute("mouselistener", true);
    }
  }

  onMouseEnterHistory(itemId: number, id: number) {
    const data = this.allPriceApprovedTooltipData.filter(entry => entry.itemId === itemId);
    this.priceApprovedTooltipData[id] = data;
  }

  openItemTurnover(itemId: number) {
    if (itemId) {
      let itemCode = this.itemDS.store.find(f => f.id === itemId)?.code;
      
      this.filterTurnoverItemCode = itemCode;
      this.isItemTurnoverPopupOpened = true;

      setTimeout(() => {
        if (this.itemTurnoverComponent) {
          this.itemTurnoverComponent.loadData();
        }
      });
    }
  }

  displayItemTurnoverPopupChange(event : any) {
    event ? this.isDataLoading = true : this.isDataLoading = false;
  }

  async getPartners() {
    await this.partnerService.getAllPartnersSmallAsync().then(partners => {
      this.partnersDS = {
        paginate: true,
        pageSize: 15,
        store: partners
      }
    })
  }

  async onSpecialPriceRequestsHistoryClick() {
    await this.getSpecialPriceRequestData();
  }

  onSupplierPriceListClick() {
    if (this.allSupplierSalesPriceList && this.allSupplierSalesPriceList.length > 0) {
      let listId = this.allSupplierSalesPriceList[0];

      var url = environment.MRKPrimaware + '/' + Constants.supplierPriceList + '?id=' + listId;
      window.open(url, '_blank');  
    }
  }

  onPrincipalActivityUniqueChange(event: any) {
    if (event && event.previousValue === true && event.value === false) {
      this.sortedPartnerLocations.forEach(location => {
        if (!this.salesPriceListByLocation[location.id] || this.salesPriceListByLocation[location.id].length === 0) {
          this.salesPriceListByLocation[location.id] = this.salesPriceListByLocation[this.implicitLocation.id];
        }
      })
    }
  }
}
