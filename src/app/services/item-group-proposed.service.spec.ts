import { TestBed } from '@angular/core/testing';

import { ItemGroupProposedService } from './item-group-proposed.service';

describe('ItemGroupProposedService', () => {
  let service: ItemGroupProposedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemGroupProposedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
