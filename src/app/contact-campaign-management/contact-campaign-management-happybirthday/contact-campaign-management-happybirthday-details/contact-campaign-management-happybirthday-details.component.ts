import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ContactCampaignTypeEnum } from 'app/enums/contactCampaignTypeEnum';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { ContactCampaignHappyBirthDay } from 'app/models/contact-campaign.model';
import { Post } from 'app/models/post.model';
import { AuthService } from 'app/services/auth.service';
import { ContactCampaignService } from 'app/services/contact-campaign-management.service';
import { NotificationService } from 'app/services/notification.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent, DxValidatorComponent } from 'devextreme-angular';

@Component({
  selector: 'app-contact-campaign-management-happybirthday-details',
  templateUrl: './contact-campaign-management-happybirthday-details.component.html',
  styleUrls: ['./contact-campaign-management-happybirthday-details.component.css']
})
export class ContactCampaignManagementHappyBirthDayDetailsComponent implements OnInit {
  @Input() selectedContactCampaign: any;
  @Input() items: any;
  @Input() itemsGroup: any;
  @Input() contactCampaignType: any;

  @Input() actions: any;
  @Output() visible: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('itemsDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('itemsGridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;

  contactCampaign: any;
  selectedPost: Post;
  isTabActive: string;
  selectedRows: any[];
  selectedRowIndex = -1;

  allocatedItems: any[] = [];

  popupVisible: boolean;
  postIds: any;
  itemIds: any;
  itemsData: any;

  itemStates: { id: number; name: string }[] = [];

  contactCampaingDetailsInfo: string;

  constructor(private authService: AuthService, private contactCampaignService: ContactCampaignService, private notificationService: NotificationService, private translationService: TranslateService) { 

    this.contactCampaingDetailsInfo = this.translationService.instant("contactCampaingDetailsInfo");
  }

  ngOnInit(): void {
    this.isTabActive = this.authService.getCustomerPageTitleTheme();
  }

  async loadData() {
    this.contactCampaign = new ContactCampaignHappyBirthDay();
    this.postIds = [];
    
    if (this.selectedContactCampaign) {
      this.contactCampaign = this.selectedContactCampaign;
      //this.getContactCampaignItems();
    } else {
      this.contactCampaign = new ContactCampaignHappyBirthDay();
      this.contactCampaign.customerId = Number(this.authService.getUserCustomerId());
      this.contactCampaign.isActive = true;
      this.postIds = [];
    }
    setTimeout(() => {
      this.setGridInstances();
      if (this.dataGrid) {
        this.dataGrid.instance.columnOption('isActive', 'filterValue', ['1']);
      }
    }, 200);

    this.contactCampaign.type = ContactCampaignTypeEnum.HappyBirthDay;
  }

  // async getContactCampaignItems() {
  //   if (this.selectedMarketingCampaign && this.selectedMarketingCampaign.id) {
  //     await this.marketingCampaignService.getItemsByMarketingCampaignIdAsync(this.selectedMarketingCampaign.id).then(results => {
  //       this.allocatedItems = results;
  //       if (results && results.length > 0) {
  //         this.marketingCampaignService.getDataMarketingCampaignItemsAsync(this.allocatedItems.map(i => i.itemId)).then(res => {
  //           if (res && res.length > 0) {
  //             this.itemsData = res;
  //           }
  //         });
  //       }
  //     })
  //   }
  // }

  setGridInstances() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  getDisplayExprPost(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  back() {
    this.contactCampaign = null;
    this.visible.emit(false);
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data)  {
      if (e.data.isActive == false) 
        {
          e.rowElement.style.backgroundColor = '#ff9966';
        }
        else {     
          e.rowElement.style.backgroundColor = '#a8ffd2';
        }
    }
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  getDisplayExprItems(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.shortNameRO;
  }

  itemDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  postDisplayExpr(post) {
    if (!post) {
      return '';
    }
    return post.code + ' - ' + post.companyPost;
  }

  async saveData() {
    if (this.validationGroup.instance.validate().isValid) {
      await this.contactCampaignService.createAsync(this.contactCampaign).then(result => {
        if (result) {
          this.notificationService.alert('top', 'center', 'Campanii Contactare - Datele au fost create cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Campanii Contactare - Datele nu au fost create!', NotificationTypeEnum.Red, true)
        }
        this.back();
      })
    }
  }

  async updateData() {
    if (this.validationGroup.instance.validate().isValid) {
      if (this.contactCampaign.parentCampaign.isActive) {
        this.contactCampaign.parentCampaign.observationsToStop = null;
      }

      await this.contactCampaignService.updateAsync(this.contactCampaign).then(result => {
        if (result) {
          this.contactCampaign = result;
          this.notificationService.alert('top', 'center', 'Campanii Contactare - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Campanii Contactare - Datele nu au fost modificate!', NotificationTypeEnum.Red, true)
        }
      })
    }
  }
}
