import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ContactCampaign } from 'app/models/contact-campaign.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ContactCampaignService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<any> {
    const apiURL = `/ContactCampaign/GetAll`;
    return await this.apiService.GetAsync<ContactCampaign[]>(apiURL, Constants.mrkApi);
  }

  async getAllByCustomerIdAsync(customerId: number): Promise<any> {
    const apiURL = `/ContactCampaign/GetByCustomerId?customerId=${customerId}`;
    return await this.apiService.GetAsync<ContactCampaign[]>(apiURL, Constants.mrkApi);
  }

  async getAllByCustomerIdAndTypeAsync(customerId: number, type: number): Promise<any> {
    const apiURL = `/ContactCampaign/GetByCustomerIdAndType?customerId=${customerId}&type=${type}`;
    return await this.apiService.GetAsync<ContactCampaign[]>(apiURL, Constants.mrkApi);
  }

  async getGeographicalAreaEntitiesDataAsync(geographicalAreaId: number): Promise<any> {
    const apiURL = `/ContactCampaign/GetGeographicalAreaEntitiesData`;
    return await this.apiService.PostAsync<any>(apiURL, geographicalAreaId, Constants.mrkApi);
  }

  async getContactCampaignDescriptionDataAsync(postId?: number): Promise<any> {
    const apiURL = `/ContactCampaign/GetContactCampaignDescriptionData`;
    return await this.apiService.PostAsync<any>(apiURL, { postId: postId }, Constants.mrkApi);
  }

  async createAsync(m: ContactCampaign): Promise<any> {
    const apiURL = `/ContactCampaign/create`;
    return await this.apiService.PostAsync<any>(apiURL, m, Constants.mrkApi);
  }

  async updateAsync(m: ContactCampaign): Promise<any> {
    const apiURL = `/ContactCampaign/update`;
    return await this.apiService.PutAsync<any>(apiURL, m, Constants.mrkApi);
  }

  async deleteMultipleAsync(m: Number[], observationsToStop: string): Promise<any> {
    const apiURL = `/ContactCampaign/deleteMultiple`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, { ids: m, observationsToStop: observationsToStop }, Constants.mrkApi);
  }
}
