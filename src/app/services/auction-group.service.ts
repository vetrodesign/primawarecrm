import { Injectable } from '@angular/core';
import { AuctionGroup } from 'app/models/auction-group.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class AuctionGroupService {

  constructor(private apiService: ApiService) { }

  async getAllAuctionGroupsAsync(): Promise<AuctionGroup[]> {
    const apiURL = `/auctiongroup/getAll`;
    return await this.apiService.GetAsync<AuctionGroup[]>(apiURL, Constants.mrkApi);
  }

  async getAllAuctionGroupsByCustomerIdAsync(): Promise<AuctionGroup[]> {
    const apiURL = `/auctiongroup/getByCustomerId`;
    return await this.apiService.GetAsync<AuctionGroup[]>(apiURL, Constants.mrkApi);
  }

  async createAuctionGroupAsync(auction: AuctionGroup): Promise<any> {
    const apiURL = `/auctiongroup/create`;
    return await this.apiService.PostAsync<any>(apiURL, auction, Constants.mrkApi);
  }

  async updateAuctionGroupAsync(auction: AuctionGroup): Promise<any> {
    const apiURL = `/auctiongroup/update`;
    return await this.apiService.PutAsync<any>(apiURL, auction, Constants.mrkApi);
  }

  async deleteAuctionGroupsAsync(ids: number[]): Promise<any> {
    const apiURL = `/auctiongroup/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
