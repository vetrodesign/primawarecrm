import { BaseDomain } from "./base-domain.model";

export class AuctionType extends BaseDomain{
    public code: string;
    public name: string;
    public description: string;
}