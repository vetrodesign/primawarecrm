import { Injectable } from '@angular/core';
import { AuctionGroupXCpvCode } from 'app/models/auction-group-cpv-code.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class AuctionGroupCpvCodeService {

  constructor(private apiService: ApiService) { }

  async getAllAuctionGroupCpvCodes(): Promise<any[]> {
    const apiURL = `/AuctionGroupXCpvCode/GetAll`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.mrkApi);
  }

  async getAllAuctionGroupCpvCodesByAuctionGroupIdAsync(auctionGroupId: number): Promise<any[]> {
    const apiURL = `/AuctionGroupXCpvCode/GetAllByAuctionGroupId`;
    return await this.apiService.PostAsync<any>(apiURL, auctionGroupId, Constants.mrkApi);
  }

  async createMultipleAuctionGroupCpvCodeAsync(i: AuctionGroupXCpvCode[]): Promise<any> {
    const apiURL = `/AuctionGroupXCpvCode/CreateMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }

  async deleteAuctionGroupCpvCodeAsync(i: AuctionGroupXCpvCode[]): Promise<any> {
    const apiURL = `/AuctionGroupXCpvCode/DeleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }
}
