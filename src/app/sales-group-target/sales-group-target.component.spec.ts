import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesGroupTargetComponent } from './sales-group-target.component';

describe('SalesGroupTargetComponent', () => {
  let component: SalesGroupTargetComponent;
  let fixture: ComponentFixture<SalesGroupTargetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesGroupTargetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesGroupTargetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
