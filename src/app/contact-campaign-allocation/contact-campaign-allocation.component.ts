import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { IPageActions } from 'app/models/ipageactions';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { Post } from 'app/models/post.model';
import { PostService } from 'app/services/post.service';
import { DxDataGridComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { Constants } from 'app/constants';
import { PageActionService } from 'app/services/page-action.service';
import { ActivatedRoute } from '@angular/router';
import { ClientModuleMenuItemsService } from 'app/services/client-module-menu-items.service';
import { ContactCampaignAllocation } from 'app/models/contact-campaign-allocation.model';
import { ContactCampaignAllocationService } from 'app/services/contact-campaign-allocation.service';
import { ContactCampaignAllocationDetailsComponent } from './contact-campaign-allocation-details/contact-campaign-allocation-details.component';
import { ContactCampaignService } from 'app/services/contact-campaign-management.service';
import { ContactCampaign } from 'app/models/contact-campaign.model';

@Component({
  selector: 'app-contact-campaign-allocation',
  templateUrl: './contact-campaign-allocation.component.html',
  styleUrls: ['./contact-campaign-allocation.component.css']
})
export class ContactCampaignAllocationComponent implements OnInit, AfterViewInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('contactCampaignAllocationDetails') contactCampaignAllocationDetails: ContactCampaignAllocationDetailsComponent;

  loaded: boolean;
  displayData: boolean;
  actions: IPageActions;

  contactCampaignAllocations: ContactCampaignAllocation[];
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  isOwner: boolean = false;
  detailsVisibility: boolean;
  customerId: string;
  items: any;
  itemsGroup: any;
  selectedContactCampaignAllocation: ContactCampaignAllocation;
  groupedText: string;
  posts: Post[];
  contactCampaigns: ContactCampaign[];

  infoButton: string;

  constructor(private authenticationService: AuthService,
    private translationService: TranslateService, private contactCampaignAllocationService: ContactCampaignAllocationService, private pageActionService: PageActionService,
    private notificationService: NotificationService, private route: ActivatedRoute,
    private clientModuleMenuItemsService: ClientModuleMenuItemsService, private postService: PostService,  private contactCampaignService: ContactCampaignService) {
    this.getDisplayExprPosts = this.getDisplayExprPosts.bind(this);
    this.getDisplayExprContactCampaigns = this.getDisplayExprContactCampaigns.bind(this);

    this.loaded = true;
    Promise.all([this.getPosts(), this.getContactCampaigns()]).then(x => {
      this.loaded = false;
    });

    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();

    this.contactCampaignAllocations = [];

    
  }

 

  ngOnInit(): void {
    this.getData();
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }

    if (this.dataGrid) {
      this.dataGrid.instance.columnOption('isActive', 'filterValue', ['1']);
    }
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && !e.data.isActive) {
      e.rowElement.style.backgroundColor = '#ffaf82';
    }
  }

  getData() {
    this.loaded = true;
    Promise.all([this.getContactCampignAllocations()]).then(x => {
      this.loaded = false;
    });
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }

  async getContactCampignAllocations() {
    this.contactCampaignAllocations = [];
    await this.contactCampaignAllocationService.getAllAsync().then(items => {
      this.contactCampaignAllocations = items;
    });
  }


  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.contactCampaignAllocation).then(result => {
      this.actions = result;
    });
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public refreshDataGrid() {
    this.getContactCampignAllocations();
    this.dataGrid.instance.refresh();
  }

  public deleteRecords(data: any) {
    this.contactCampaignAllocationService.deleteMultipleAsync(this.selectedRows.map(x => x.id)).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Alocare Campanii Contactare - Datele au fost sterse cu succes!',
          NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Alocare Campanii Contactare - Datele nu au fost sterse!', NotificationTypeEnum.Red, true)
      }
      this.refreshDataGrid();
    });
  }

  public async openDetails(row: any) {
    this.selectedContactCampaignAllocation = row;
    this.loaded = true;
    setTimeout(async () => {
      await this.contactCampaignAllocationDetails.loadData().then(function () {
        this.detailsVisibility = true;
        this.loaded = false;
      }.bind(this));
    }, 0);
  }

  changeVisible() {
    this.detailsVisibility = false;
    this.refreshDataGrid();
  }


  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  async getContactCampaigns() {
    if (this.isOwner) {
      await this.contactCampaignService.getAllAsync().then(data => {
        this.contactCampaigns = data;
      });
    } else {
      await this.contactCampaignService.getAllByCustomerIdAsync(Number(this.authenticationService.getUserCustomerId())).then(data => {
        this.contactCampaigns = data;
      });
    }
  }

  async getPosts() {
    if (this.isOwner) {
      await this.postService.getAllPostsAsync().then(data => {
        this.posts = data;
      });
    } else {
      await this.postService.getPostAsyncByID().then(data => {
        this.posts = data;
      });
    }
  }

  getDisplayExprPosts(item) {
    if (!item) {
      return '';
    }

    return item.code + ' - ' + item.companyPost;
  }

  getDisplayExprContactCampaigns(item) {
    if (!item) {
      return '';
    }

    return item.code + ' - ' + item.name;
  }
}
