import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { environment } from 'environments/environment';
import { TurnoverFilter, TurnoverHistoryFilter, TurnoverPartnerMonthQuantityFilter } from 'app/models/item-turnover-filter.model';
import { Constants } from 'app/constants';
import { TurnoverItemHistory, TurnoverPartnerMonthQuantity } from 'app/models/turnover-item-history.model';

@Injectable({
  providedIn: 'root'
})
export class ItemTurnoverService {

  constructor(private apiService: ApiService) { }

  async getItemTurnoverSuppliers(filter: TurnoverFilter): Promise<any[]> {
    const apiURL = `/itemturnover/GetItemTurnoverSuppliers`;
    return await this.apiService.PostAsync<any[]>(apiURL, filter, Constants.srmApi);
  }

  async getTurnoverItemsBySupplierIds(filter: TurnoverFilter): Promise<any[]> {
    const apiURL = `/itemturnover/GetTurnoverItemsBySupplierIds`;
    return await this.apiService.PostAsync<any[]>(apiURL, filter, Constants.srmApi);
  }

  async getItemTurnoverDetailXPartnerByFilter(filter: TurnoverHistoryFilter): Promise<TurnoverItemHistory[]> {
    const apiURL = `/itemturnover/GetTurnoverItemsXPartnerByFilter`;
    return await this.apiService.PostAsync<TurnoverItemHistory[]>(apiURL, filter, Constants.srmApi);
  }

  async getCountItemTurnoverDetailXPartner(filter: TurnoverHistoryFilter): Promise<number> {
    const apiURL = `/itemturnover/GetCountTurnoverItemsXPartner`;
    return await this.apiService.PostAsync<number>(apiURL, filter, Constants.srmApi);
  }

  async getItemTurnoverMonthQuantityXPartner(filter: TurnoverPartnerMonthQuantityFilter): Promise<TurnoverPartnerMonthQuantity[]> {
    const apiURL = `/itemturnover/GetItemTurnoverMonthQuantityXPartner`;
    return await this.apiService.PostAsync<TurnoverPartnerMonthQuantity[]>(apiURL, filter, Constants.srmApi);
  }
}

