
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PartnerTurnoverContact } from 'app/models/partnerturnovercontact.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerTurnoverContactService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<any[]> {
    const apiURL = `/PartnerTurnoverContact/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getByPartnerAndPostIdAsync(partnerId: number, postId: number): Promise<any> {
    let filter = {'partnerId': partnerId, 'postId': postId}
    const apiURL = `/PartnerTurnoverContact/GetByPartnerIdAndPostId`;
    return await this.apiService.PostAsync<any>(apiURL, filter, Constants.mrkApi);
  }

  async createAsync(paymentInstrument: PartnerTurnoverContact): Promise<any> {
    const apiURL = `/PartnerTurnoverContact/create`;
    return await this.apiService.PostAsync<any>(apiURL, paymentInstrument, Constants.mrkApi);
  }

  async updateAsync(paymentInstrument: PartnerTurnoverContact): Promise<any> {
    const apiURL = `/PartnerTurnoverContact/update`;
    return await this.apiService.PutAsync<any>(apiURL, paymentInstrument, Constants.mrkApi);
  }
}
