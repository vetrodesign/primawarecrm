import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PartnerLocationSchedule } from 'app/models/partnerlocationschedule.model';
import { Constants } from 'app/constants';


@Injectable({
  providedIn: 'root'
})
export class PartnerLocationScheduleService {
  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<PartnerLocationSchedule[]> {
    const apiURL = `/partnerLocationSchedule/getAll`;
    return await this.apiService.GetAsync<PartnerLocationSchedule[]>(apiURL, Constants.mrkApi);
  }

  async getByCustomerIdAsync(): Promise<PartnerLocationSchedule[]> {
    const apiURL = `/partnerLocationSchedule/getByCustomerId`;
    return await this.apiService.GetAsync<PartnerLocationSchedule[]>(apiURL, Constants.mrkApi);
  }

  async getItemByPartnerLocationIdAsync(partnerLocationId: number): Promise<PartnerLocationSchedule[]> {
    const apiURL = `/partnerLocationSchedule/getItemByPartnerLocationId`;
    return await this.apiService.PostAsync<PartnerLocationSchedule[]>(apiURL, partnerLocationId, Constants.mrkApi);
  }

  async create(partnerLocationSchedule: PartnerLocationSchedule): Promise<any> {
    const apiURL = `/partnerLocationSchedule/create`;
    return await this.apiService.PostAsync<any>(apiURL, partnerLocationSchedule, Constants.mrkApi);
  }

  async update(partnerLocationSchedule: PartnerLocationSchedule): Promise<any> {
    const apiURL = `/partnerLocationSchedule/update`;
    return await this.apiService.PutAsync<any>(apiURL, partnerLocationSchedule, Constants.mrkApi);
  }

}
