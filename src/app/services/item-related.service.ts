import { Injectable } from '@angular/core';
import { ItemXRelated } from 'app/models/itemxrelated.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemRelatedService {

  constructor(private apiService: ApiService) { }

  async getAll(): Promise<any[]> {
    const apiURL = `/itemxrelated/GetAll`;
    return await this.apiService.GetAsync<any[]>(apiURL,Constants.mrkApi);
  }

  async getItemXRelatedByItemIdAsync(itemId: number): Promise<any[]> {
    const apiURL = `/itemxrelated/GetByItemId`;
    return await this.apiService.PostAsync<any[]>(apiURL, itemId,Constants.mrkApi);
  }

  async getItemXRelatedByItemIdForTurnoverAsync(partnerId: number, itemId: number): Promise<any[]> {
    let body = { partnerId: partnerId, itemId: itemId };
    const apiURL = `/itemxrelated/GetByItemIdForTurnover`;
    return await this.apiService.PostAsync<any[]>(apiURL, body, Constants.mrkApi);
  }

  async createItemXRelatedAsync(i: ItemXRelated): Promise<any> {
    const apiURL = `/itemxrelated/create`;
    return await this.apiService.PostAsync<any>(apiURL, i,Constants.mrkApi);
  }

  async updateItemXRelatedAsync(i: ItemXRelated): Promise<any> {
    const apiURL = `/itemxrelated/update`;
    return await this.apiService.PutAsync<any>(apiURL, i,Constants.mrkApi);
  }

  async deleteItemXRelatedAsync(ids: number[]): Promise<any> {
    const apiURL = `/itemxrelated/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids,Constants.mrkApi);
  }
}
