import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { SalesZone } from 'app/models/saleszone.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class SalesZoneService {
  constructor(private apiService: ApiService) { }

  async getAllSalesZonesAsync(): Promise<SalesZone[]> {
    const apiURL = `/SalesZone/getall`;
    return await this.apiService.GetAsync<SalesZone[]>(apiURL, Constants.mrkApi);
  }

  async getSalesZonesByCustomerId(): Promise<SalesZone[]> {
    const apiURL = `/SalesZone/getbycustomerid/`;
    return await this.apiService.GetAsync<SalesZone[]>(apiURL, Constants.mrkApi);
  }

  async createSalesZoneTypeAsync(salesZone: SalesZone): Promise<any> {
    const apiURL = `/SalesZone/create`;
    return await this.apiService.PostAsync<any>(apiURL, salesZone, Constants.mrkApi);
  }

  async updateSalesZoneAsync(salesZone: SalesZone): Promise<any> {
    const apiURL = `/SalesZone/update`;
    return await this.apiService.PutAsync<any>(apiURL, salesZone, Constants.mrkApi);
  }

  async deleteSalesZonesAsync(ids: number[]): Promise<any> {
    const apiURL = `/SalesZone/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
