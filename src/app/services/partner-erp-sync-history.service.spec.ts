import { TestBed } from '@angular/core/testing';

import { PartnerErpSyncHistoryService } from './partner-erp-sync-history.service';

describe('PartnerErpSyncHistoryService', () => {
  let service: PartnerErpSyncHistoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerErpSyncHistoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
