export class PartnerSmall {
    id: number;
    name: string;
    code: string;
}