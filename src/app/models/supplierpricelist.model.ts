import { BaseDomain } from "./base-domain.model";

export class SupplierPriceList extends BaseDomain {
    name: string;
    partnerLocationId: number;
    paymentInstrumentId: number;
    currencyId: number;

    from: Date;
    to: Date;

    discount: number;
    discountAdvance: number;
    paymentTerm: number;
    graceTerm: number;

    isActive: boolean;
    discountType: number;
    discountApplicability: number;
}
