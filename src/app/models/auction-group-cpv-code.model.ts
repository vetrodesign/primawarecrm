export class AuctionGroupXCpvCode {
    public id: number;
    public auctionGroupId: number;
    public cpvCodeId: number;
}