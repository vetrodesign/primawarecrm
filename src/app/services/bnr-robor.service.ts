import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { BnrRobor, BnrRoborRequest } from 'app/models/bnr-robor.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class BnrRoborService {

  constructor(private apiService: ApiService) { }

  async getBnrRoborData(bnrRoborRequest: BnrRoborRequest): Promise<BnrRobor> {
    const apiURL = `/bnrrobor/GetBnrRoborData`;
    return await this.apiService.PostAsync<any>(apiURL, bnrRoborRequest, Constants.ssoApi);
  }
}
