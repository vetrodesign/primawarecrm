export class BaseDomain {
    id: number;
    rowVersion: any;
    created: Date;
    modified: Date;
    createdBy: number;
    modifiedBy: number;

    customerId: number;
    siteId: number;
}