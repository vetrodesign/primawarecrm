import { Injectable } from '@angular/core';
import { ItemGroupCategoryXCode } from 'app/models/itemgroupcategoryxcode.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemGroupCategoryCodeService {

  constructor(private apiService: ApiService) { }

  async getAll(): Promise<any[]> {
    const apiURL = `/itemGroupCategoryXCode/GetAll`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.mrkApi);
  }

  async getAllByItemGroupCategoryIdAsync(partnerLocationId: number): Promise<any[]> {
    const apiURL = `/itemGroupCategoryXCode/GetAllByItemGroupCategoryId`;
    return await this.apiService.PostAsync<any>(apiURL, partnerLocationId, Constants.mrkApi);
  }

  async createMultipleItemGroupCategoryCodeAsync(i: ItemGroupCategoryXCode[]): Promise<any> {
    const apiURL = `/itemGroupCategoryXCode/CreateMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }

  async deleteItemGroupCategoryCodeAsync(i: ItemGroupCategoryXCode[]): Promise<any> {
    const apiURL = `/itemGroupCategoryXCode/DeleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }
}

