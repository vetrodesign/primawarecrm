import { BaseDomain } from "./base-domain.model";
import { Partner } from "./partner.model";

export class ContactCampaignPartners extends BaseDomain {
    contactCampaignTypeId: number;
    partnerId: number;
    partnerName: string;
    partnerFiscalCode: string;
}

export class ContactCampaignPartnersModel extends ContactCampaignPartners {
    contactCampaignPartners: Partner[];
    currentDayContactedHappyBirthDayCampaignPartners: number;
    currentDayContactedLocationCampaignPartners: number;
}
