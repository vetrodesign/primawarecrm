import { TestBed } from '@angular/core/testing';

import { ItemGroupCategoryService } from './item-group-category.service';

describe('ProductCategoryService', () => {
  let service: ItemGroupCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemGroupCategoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
