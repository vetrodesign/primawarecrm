export enum AuctionResultEnum {
    TotalWin = 1,
    PartialWin = 2,
    TotalRejected = 3,
    Disqualified = 4
  }
