import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { PartnerContactPreferences } from 'app/models/partner-contact-preferences.model';

@Injectable({
  providedIn: 'root'
})
export class PartnerContactPreferencesService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<PartnerContactPreferences[]> {
    const apiURL = `/PartnerContactPreferences/GetAll`;
    return await this.apiService.GetAsync<PartnerContactPreferences[]>(apiURL, Constants.mrkApi);
  }

  async getAllByPartnerIdAsync(customerId): Promise<PartnerContactPreferences[]> {
    const apiURL = `/PartnerContactPreferences/GetByPartnerId/` + customerId;
    return await this.apiService.GetAsync<PartnerContactPreferences[]>(apiURL, Constants.mrkApi);
  }

  async createAsync(m: PartnerContactPreferences): Promise<any> {
    const apiURL = `/PartnerContactPreferences/create`;
    return await this.apiService.PostAsync<any>(apiURL, m, Constants.mrkApi);
  }

  async updateAsync(pif: PartnerContactPreferences): Promise<any> {
    const apiURL = `/PartnerContactPreferences/update`;
    return await this.apiService.PutAsync<any>(apiURL, pif, Constants.mrkApi);
  }

  async upsertAsync(pif: PartnerContactPreferences): Promise<any> {
    const apiURL = `/PartnerContactPreferences/upsert`;
    return await this.apiService.PostAsync<any>(apiURL, pif, Constants.mrkApi);
  }
}
