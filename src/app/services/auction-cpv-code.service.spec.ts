import { TestBed } from '@angular/core/testing';

import { AuctionCpvCodeService } from './auction-cpv-code.service';

describe('AuctionCpvCodeService', () => {
  let service: AuctionCpvCodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuctionCpvCodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
