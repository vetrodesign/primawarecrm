import { BaseDomain } from "./base-domain.model";

export class Management extends BaseDomain {
    code: string;
    name: string;
    description: string;
    customerId: number;
    siteId: number;
    isImplicit: boolean = false;
    eShop: boolean;
}
