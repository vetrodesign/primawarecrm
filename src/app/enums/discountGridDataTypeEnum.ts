export enum BenefitTypeEnum {
    SalePriceList = 1,
    BenefitValue = 2,
    BenefitPercent = 3
  }