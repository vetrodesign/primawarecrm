import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ContactCampaignAllocation } from 'app/models/contact-campaign-allocation.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ContactCampaignAllocationService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<ContactCampaignAllocation[]> {
    const apiURL = `/ContactCampaignAllocation/GetAll`;
    return await this.apiService.GetAsync<ContactCampaignAllocation[]>(apiURL, Constants.mrkApi);
  }

  async getAllByCustomerIdAsync(customerId): Promise<ContactCampaignAllocation[]> {
    const apiURL = `/ContactCampaignAllocation/GetByCustomerId/` + customerId;
    return await this.apiService.GetAsync<ContactCampaignAllocation[]>(apiURL, Constants.mrkApi);
  }

  async createAsync(m: ContactCampaignAllocation): Promise<any> {
    const apiURL = `/ContactCampaignAllocation/create`;
    return await this.apiService.PostAsync<any>(apiURL, m, Constants.mrkApi);
  }

  async updateAsync(m: ContactCampaignAllocation): Promise<any> {
    const apiURL = `/ContactCampaignAllocation/update`;
    return await this.apiService.PutAsync<any>(apiURL, m, Constants.mrkApi);
  }

  async deleteMultipleAsync(m: Number[]): Promise<any> {
    const apiURL = `/ContactCampaignAllocation/deleteMultiple`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, m, Constants.mrkApi);
  }
}
