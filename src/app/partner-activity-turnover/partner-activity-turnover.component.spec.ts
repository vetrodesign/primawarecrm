import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerActivityTurnoverComponent } from './partner-activity-turnover.component';

describe('PartnerActivityTurnoverComponent', () => {
  let component: PartnerActivityTurnoverComponent;
  let fixture: ComponentFixture<PartnerActivityTurnoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerActivityTurnoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerActivityTurnoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
