import { TestBed } from '@angular/core/testing';

import { PartnerContractService } from './partner-contract.service';

describe('PartnerContractService', () => {
  let service: PartnerContractService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerContractService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
