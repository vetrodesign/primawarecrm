import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { AdminComponent } from 'app/admin/admin.component';
import { CustomerComponent } from 'app/customer/customer.component';
import { NotificationsComponent } from 'app/notifications/notifications.component';
import { AuthGuard } from 'app/helpers/auth.guard';
import { Constants } from 'app/constants';
import { SetDataComponent } from 'app/set-data/set-data.component';


import { PartnerComponent } from 'app/partner/partner.component';
import { ExceptionLogComponent } from 'app/exception-log/exception-log.component';
import { OrderComponent } from 'app/order/order.component';
import { AuctionsComponent } from 'app/auctions/auctions.component';
import { SalesTargetComponent } from 'app/sales-target/sales-target.component';
import { ImportExportItemComponent } from 'app/import-export-item/import-export-item.component';
import { DailyReportComponent } from 'app/daily-report/daily-report.component';
import { TurnoverComponent } from 'app/turnover/turnover.component';
import { CustomerInterestsReportComponent } from 'app/customer-interests-report/customer-interests-report.component';
import { PartnerObservationsDetailsComponent } from 'app/partner-observations-details/partner-observations-details.component';
import { ContactCampaignPartnersComponent } from 'app/contact-campaign-partners/contact-campaign-partners.component';
import { ContactCampaignManagementComponent } from 'app/contact-campaign-management/contact-campaign-management.component';
import { ContactCampaignAllocationComponent } from 'app/contact-campaign-allocation/contact-campaign-allocation.component';
import { PartnerDailyReceiptsComponent } from 'app/partner-daily-receipts/partner-daily-receipts.component';
import { PartnerNameEquivalenceComponent } from 'app/partner-name-equivalence/partner-name-equivalence.component';
import { SpecialPriceRequestComponent } from 'app/special-price-request/special-price-request.component';
import { TurnoverDetailsComponent } from 'app/turnover-details/turnover-details.component';
import { MyClientsComponent } from 'app/my-clients/my-clients.component';
import { PartnerReviewQuestionComponent } from 'app/partner-review-question/partner-review-question.component';
import { PartnerReviewReportComponent } from 'app/partner-review-report/partner-review-report.component';


export const AdminLayoutRoutes: Routes = [
    // {
    //   path: '',
    //   children: [ {
    //     path: 'dashboard',
    //     component: DashboardComponent
    // }]}, {
    // path: '',
    // children: [ {
    //   path: 'userprofile',
    //   component: UserProfileComponent
    // }]
    // }, {
    //   path: '',
    //   children: [ {
    //     path: 'icons',
    //     component: IconsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'notifications',
    //         component: NotificationsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'maps',
    //         component: MapsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'typography',
    //         component: TypographyComponent
//     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'upgrade',
    //         component: UpgradeComponent
    //     }]
    // }
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'upgrade',        component: UpgradeComponent },
    { path: 'notifications',        component: NotificationsComponent },


    { path: Constants.admin,        component: AdminComponent, canActivate: [AuthGuard] },
    { path: Constants.partner,        component: PartnerComponent, canActivate: [AuthGuard] },
    { path: Constants.contactCampaignManagement, component: ContactCampaignManagementComponent, canActivate: [AuthGuard]},  
    { path: Constants.contactCampaignAllocation, component: ContactCampaignAllocationComponent, canActivate: [AuthGuard]},  
    { path: Constants.contactCampaignPartners,        component: ContactCampaignPartnersComponent, canActivate: [AuthGuard] },
    { path: Constants.partnerObservationsDetails,        component: PartnerObservationsDetailsComponent, canActivate: [AuthGuard] },
    { path: Constants.partnerNameEquivalence,        component: PartnerNameEquivalenceComponent, canActivate: [AuthGuard] },
    { path: Constants.customer,        component: CustomerComponent, canActivate: [AuthGuard] },
    { path: Constants.userProfile,   component: UserProfileComponent, canActivate: [AuthGuard]},
    
    { path: Constants.importAuctions,   component: AuctionsComponent, canActivate: [AuthGuard]},
    { path: Constants.importExportItemData,   component: ImportExportItemComponent, canActivate: [AuthGuard]},
    
    { path: Constants.order,   component: OrderComponent, canActivate: [AuthGuard]},
    { path: Constants.dailyReport,   component: DailyReportComponent, canActivate: [AuthGuard]},
    { path: Constants.customerInterestsReport,   component: CustomerInterestsReportComponent, canActivate: [AuthGuard]},
    { path: Constants.salesTarget,   component: SalesTargetComponent, canActivate: [AuthGuard]},
    { path: Constants.turnOver,   component: TurnoverDetailsComponent, canActivate: [AuthGuard]},
    { path: Constants.partnerDailyReceipts, component: PartnerDailyReceiptsComponent, canActivate: [AuthGuard]},
    { path: Constants.specialPriceRequest, component: SpecialPriceRequestComponent, canActivate: [AuthGuard]},
    { path: Constants.myClients, component: MyClientsComponent, canActivate: [AuthGuard]},
    { path: Constants.partnerReviewQuestion, component: PartnerReviewQuestionComponent, canActivate: [AuthGuard]},
    { path: Constants.partnerReviewReport, component: PartnerReviewReportComponent, canActivate: [AuthGuard]},
    
    { path: Constants.setData,   component: SetDataComponent},
    { path: Constants.logs,        component: ExceptionLogComponent, canActivate: [AuthGuard] },
    
    { path: '**', redirectTo: 'customer' }
];
