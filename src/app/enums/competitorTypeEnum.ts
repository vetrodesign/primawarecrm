export enum CompetitorTypeEnum {
    No = 0,
    PossibleCompetitor = 1,
    Yes = 2
  }