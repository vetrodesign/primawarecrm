import { BaseDomain } from "./base-domain.model";

export class ItemType extends BaseDomain {
    code: string;
    name: string;
    isActive: boolean;
}