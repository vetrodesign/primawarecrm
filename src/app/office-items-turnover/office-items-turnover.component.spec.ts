import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficeItemsTurnoverComponent } from './office-items-turnover.component';

describe('OfficeItemsTurnoverComponent', () => {
  let component: OfficeItemsTurnoverComponent;
  let fixture: ComponentFixture<OfficeItemsTurnoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfficeItemsTurnoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficeItemsTurnoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
