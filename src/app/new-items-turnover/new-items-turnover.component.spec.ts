import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewItemsTurnoverComponent } from './new-items-turnover.component';

describe('NewItemsTurnoverComponent', () => {
  let component: NewItemsTurnoverComponent;
  let fixture: ComponentFixture<NewItemsTurnoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewItemsTurnoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewItemsTurnoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
