import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerInterestsReportComponent } from './customer-interests-report.component';

describe('CustomerInterestsReportComponent', () => {
  let component: CustomerInterestsReportComponent;
  let fixture: ComponentFixture<CustomerInterestsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerInterestsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerInterestsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
