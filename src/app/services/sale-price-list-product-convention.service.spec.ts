import { TestBed } from '@angular/core/testing';

import { SalePriceListProductConventionService } from './sale-price-list-product-convention.service';

describe('SalePriceListProductConventionService', () => {
  let service: SalePriceListProductConventionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalePriceListProductConventionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
