import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { CountryGroupXCountry } from 'app/models/countrygroupxcountry.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class CountryGroupXcountryService {

  constructor(private apiService: ApiService) { }

  async getAllCountryGroupXCountryAsync(): Promise<any[]> {
    const apiURL = `/CountryGroupXCountry/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.commonApi);
  }

  async createCountryGroupXCountryAsync(country: CountryGroupXCountry): Promise<any> {
    const apiURL = `/CountryGroupXCountry/create`;
    return await this.apiService.PostAsync<any>(apiURL, country, Constants.commonApi);
  }

  async createMultipleCountryGroupXCountryAsync(countries: CountryGroupXCountry[]): Promise<any> {
    const apiURL = `/CountryGroupXCountry/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, countries, Constants.commonApi);
  }

  async updateCountryGroupXCountryAsync(country: CountryGroupXCountry): Promise<any> {
    const apiURL = `/CountryGroupXCountry/update`;
    return await this.apiService.PutAsync<any>(apiURL, country, Constants.commonApi);
  }

  async deleteCountryGroupXCountryAsync(countryGroupId: number, countryId: number): Promise<any> {
    const apiURL = `/CountryGroupXCountry/delete/` + countryGroupId + `/` + countryId;
    return await this.apiService.DeleteSingleAsync<any>(apiURL, Constants.commonApi);
  }

  async deleteMultipleCountryGroupXCountryAsync(countries: CountryGroupXCountry[]): Promise<any> {
    const apiURL = `/CountryGroupXCountry/deleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, countries, Constants.commonApi);
  }
}
