import { TestBed } from '@angular/core/testing';

import { SalePriceListService } from './sale-price-list.service';

describe('SalePriceListService', () => {
  let service: SalePriceListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalePriceListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
