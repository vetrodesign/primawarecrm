import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ClientTypeEnum } from 'app/enums/clientTypeEnum';
import { ContactCampaignLocationSortTypeEnum } from 'app/enums/contactCampaignLocationSortTypeEnum';
import { ContactCampaignTypeEnum } from 'app/enums/contactCampaignTypeEnum';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { ProductConventionEnum } from 'app/enums/productConventionEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { City } from 'app/models/city.model';
import { ContactCampaignLocation } from 'app/models/contact-campaign.model';
import { GeographicalArea } from 'app/models/geographicalarea.model';
import { Post } from 'app/models/post.model';
import { AuthService } from 'app/services/auth.service';
import { ContactCampaignService } from 'app/services/contact-campaign-management.service';
import { NotificationService } from 'app/services/notification.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent, DxValidatorComponent } from 'devextreme-angular';
import * as _ from 'lodash';

@Component({
  selector: 'app-contact-campaign-management-location-details',
  templateUrl: './contact-campaign-management-location-details.component.html',
  styleUrls: ['./contact-campaign-management-location-details.component.css']
})
export class ContactCampaignManagementLocationDetailsComponent implements OnInit {
  @Input() selectedContactCampaign: any;
  @Input() items: any;
  @Input() itemsGroup: any;
  @Input() contactCampaignType: any;
  @Input() countries: any;
  @Input() counties: any;
  @Input() cities: City[];
  @Input() countriesDS: any;
  @Input() countiesDS: any;
  @Input() citiesDS: any;
  @Input() geographicalAreas: GeographicalArea[];
  @Input() contactCampaigns: any;

  @Input() actions: any;
  @Output() visible: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('itemsDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('itemsGridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;

  contactCampaign: any;
  selectedPost: Post;
  isTabActive: string;
  selectedRows: any[];
  selectedRowIndex = -1;

  allocatedItems: any[] = [];

  popupVisible: boolean;
  postIds: any;
  itemIds: any;
  itemsData: any;

  itemStates: { id: number; name: string }[] = [];
  productConventions: { id: number; name: string }[] = [];
  clientTypes: { id: number; name: string }[] = [];
  sortTypes: { id: number; name: string }[] = [];

  contactCampaingDetailsInfo: string;
  loaded: boolean;
  isOwner: boolean;

  constructor(private authService: AuthService, private contactCampaignService: ContactCampaignService, private notificationService: NotificationService, private translationService: TranslateService
  ) { 

    this.contactCampaingDetailsInfo = this.translationService.instant("contactCampaingDetailsInfo");
    this.isOwner = this.authService.isUserOwner();

    for (let n in ContactCampaignLocationSortTypeEnum) {
      if (typeof ContactCampaignLocationSortTypeEnum[n] === 'number') {
        this.sortTypes.push({
          id: <any>ContactCampaignLocationSortTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }

    for (let n in ProductConventionEnum) {
      if (typeof ProductConventionEnum[n] === 'number') {
        this.productConventions.push({
          id: <any>ProductConventionEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }

    for (let n in ClientTypeEnum) {
      if (typeof ClientTypeEnum[n] === 'number') {
        const id = <any>ClientTypeEnum[n];
    
        if (id === 0) {
          continue;
        }
  
        const name = (id === 1) ? 'Activ (Pierdut/Nou)' : this.translationService.instant(n);
    
        this.clientTypes.push({
          id: id,
          name: name
        });
      }
    }
  }

  ngOnInit(): void {
    this.isTabActive = this.authService.getCustomerPageTitleTheme();
  }

  async loadData() {
    this.contactCampaign = new ContactCampaignLocation();
    this.postIds = [];
    
    if (this.selectedContactCampaign) {

      this.contactCampaign = this.selectedContactCampaign;
    } else {
      this.contactCampaign = new ContactCampaignLocation();
      this.contactCampaign.customerId = Number(this.authService.getUserCustomerId());
      this.contactCampaign.isActive = true;
      this.postIds = [];
      this.contactCampaign.productConventionIds = this.productConventions.map(m => m.id);
      this.contactCampaign.clientTypes = this.clientTypes.map(m => m.id);
      this.contactCampaign.lastContactDateSortType = ContactCampaignLocationSortTypeEnum.Descending;
      this.contactCampaign.caLastYearSortType = ContactCampaignLocationSortTypeEnum.NoneSortType;
      this.contactCampaign.caLast2YearsSortType = ContactCampaignLocationSortTypeEnum.NoneSortType;
      this.contactCampaign.pfLastYearSortType = ContactCampaignLocationSortTypeEnum.NoneSortType;
      this.contactCampaign.naLastYearSortType = ContactCampaignLocationSortTypeEnum.NoneSortType;
      this.contactCampaign.pfLast2YearsSortType = ContactCampaignLocationSortTypeEnum.NoneSortType;
      this.contactCampaign.naLast2YearsSortType = ContactCampaignLocationSortTypeEnum.NoneSortType;
      
      if (this.contactCampaigns && this.contactCampaigns.length > 0) {
        this.contactCampaigns = _.orderBy(this.contactCampaigns, ['parentCampaign.id'], ['desc']);
        let lastContactCampaign = this.contactCampaigns[0];
        let lastContactCampaignCode = lastContactCampaign.parentCampaign.code;

        var newContactCampaignCode = this.incrementContactCampaignCode(lastContactCampaignCode);
        this.contactCampaign.code = newContactCampaignCode;
      }
    }
    setTimeout(() => {
      this.setGridInstances();
      if (this.dataGrid) {
        this.dataGrid.instance.columnOption('isActive', 'filterValue', ['1']);
      }
    }, 200);

    this.contactCampaign.type = ContactCampaignTypeEnum.Location;
  }

  setGridInstances() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  padStart(str: string, targetLength: number, padString: string = '0'): string {
    str = String(str);
    if (str.length >= targetLength) return str;
    targetLength = targetLength - str.length;
    if (targetLength > padString.length) {
      padString += padString.repeat(targetLength / padString.length);
    }
    return padString.slice(0, targetLength) + str;
  }

  incrementContactCampaignCode(code: string): string {
    const match = code.match(/(\D+)(\d+)/); 
  
    if (match) {
      const prefix = match[1];
      let number = parseInt(match[2], 10); 
      number++; 
  
      const incrementedNumber = this.padStart(number.toString(), match[2].length, '0');
  
      return prefix + incrementedNumber;
    } else {
      return code;
    }
  }

  getDisplayExprPost(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  back() {
    this.contactCampaign = null;
    this.visible.emit(false);
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data)  {
      if (e.data.isActive == false) 
        {
          e.rowElement.style.backgroundColor = '#ff9966';
        }
        else {     
          e.rowElement.style.backgroundColor = '#a8ffd2';
        }
    }
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  getDisplayExprItems(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.shortNameRO;
  }

  itemDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  postDisplayExpr(post) {
    if (!post) {
      return '';
    }
    return post.code + ' - ' + post.companyPost;
  }

  async saveData() {
    if (this.validationGroup.instance.validate().isValid) {
      this.loaded = true;
      this.contactCampaign.geographicalAreasIds = [this.contactCampaign.geographicalAreaId];
      await this.contactCampaignService.createAsync(this.contactCampaign).then(result => {
        if (result) {
          this.notificationService.alert('top', 'center', 'Campanii Contactare - Datele au fost create cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Campanii Contactare - Datele nu au fost create!', NotificationTypeEnum.Red, true)
        }

        this.loaded = false;
        this.back();
      })
    }
  }

  async updateData() {
    if (this.validationGroup.instance.validate().isValid) {
      if (this.contactCampaign.parentCampaign.isActive) {
        this.contactCampaign.parentCampaign.observationsToStop = null;
      }

      this.loaded = true;
      this.contactCampaign.childCampaign.geographicalAreasIds = [this.contactCampaign.childCampaign.geographicalAreaId];
      await this.contactCampaignService.updateAsync(this.contactCampaign).then(result => {
        if (result) {
          this.contactCampaign = result;
          this.notificationService.alert('top', 'center', 'Campanii Contactare - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true);
          this.back();
        } else {
          this.notificationService.alert('top', 'center', 'Campanii Contactare - Datele nu au fost modificate!', NotificationTypeEnum.Red, true)
        }

        this.loaded = false;
      })
    }
  }

  getDisplayExprGeographicalAres(item) {
    if (!item) {
      return '';
    }
    return item.name + ' - ' + item.description;
  }

  getDisplayExprSortType(item) {
    if (!item) {
      return '';
    }
    return item.name;
  }

  onCitiesChanged(e: any) {
    
    if (e.value && e.value.length > 0) {
      var countryIds = this.cities.filter(x => e.value.includes(x.id)).map(y => y.countryId);
      var countiesIds = this.cities.filter(x => e.value.includes(x.id)).map(y => y.countyId);
      if (!this.contactCampaign.childCampaign) {
        
      }

      if (this.contactCampaign && (!this.contactCampaign.countriesIds || this.contactCampaign.countriesIds.length === 0) && (!this.contactCampaign.countiesIds || this.contactCampaign.countiesIds.length === 0)) {
        this.countriesDS = {
          paginate: true,
          pageSize: 15,
          store: this.countries.filter(i => countryIds.includes(i.id))
        };
  
        this.countiesDS = {
          paginate: true,
          pageSize: 15,
          store: this.counties.filter(i => countiesIds.includes(i.id))
        };

        let city = this.cities.find(x => e.value.includes(x.id));
        if (!this.contactCampaign.countriesIds || this.contactCampaign.countriesIds.length === 0) {
          this.contactCampaign.countriesIds = [city?.countryId];
        } else {
          if (!this.contactCampaign.countriesIds.includes(city?.countryId)) {
            this.contactCampaign.countriesIds.push(city?.countryId);
          }
        }

      if (!this.contactCampaign.countiesIds || this.contactCampaign.countiesIds.length === 0) {
        this.contactCampaign.countiesIds = [city?.countyId];
      } else {
        if (!this.contactCampaign.countiesIds.includes(city?.countyId)) {
          this.contactCampaign.countiesIds.push(city?.countyId);
        } 
        else if (this.contactCampaign.countiesIds.includes(city?.countyId)) {
          this.contactCampaign.countiesIds = this.contactCampaign.countiesIds.filter(f => f === city?.countyId);
        }
      } 
      }

    } else {
      this.restoreDS();
    } 

    if (!(e.value && e.value.length > 0) && this.contactCampaign.countiesIds && this.contactCampaign.countiesIds.length > 0) {
      this.citiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.cities.filter(i => this.contactCampaign.countiesIds.includes(i.countyId))
      };

      var countryIdsX = this.counties.filter(x => this.contactCampaign.countiesIds.includes(x.id)).map(y => y.countryId);
      this.countriesDS = {
        paginate: true,
        pageSize: 15,
        store: this.countries.filter(i => countryIdsX.includes(i.id))
      };
    }
    
    if (!(e.value && e.value.length > 0) && this.contactCampaign.countriesIds && this.contactCampaign.countriesIds.length > 0) {
      this.countiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.counties.filter(i => this.contactCampaign.countriesIds.includes(i.countryId))
      };
    }
  }

  onCountiesChanged(e: any) {
    
    if (e.value && e.value.length) {
      if (!this.contactCampaign.childCampaign) {
        this.citiesDS = {
          paginate: true,
          pageSize: 15,
          store: this.cities.filter(i => e.value.includes(i.countyId))
        };
  
        var countryIds = this.counties.filter(x => e.value.includes(x.id)).map(y => y.countryId);
        this.countriesDS = {
          paginate: true,
          pageSize: 15,
          store: this.countries.filter(i => countryIds.includes(i.id))
        };
      }

      let county = this.counties.find(x => e.value.includes(x.id));
      if (!this.contactCampaign.countriesIds || this.contactCampaign.countriesIds.length === 0) {
        this.contactCampaign.countriesIds = [county?.countryId];
      } else {
        if (!this.contactCampaign.countriesIds.includes(county?.countryId)) {
          this.contactCampaign.countriesIds.push(county?.countryId);
        }
      }


    } else {
      this.restoreDS();
    }

    if (!(e.value && e.value.length > 0) && this.contactCampaign.countriesIds && this.contactCampaign.countriesIds.length > 0) {
      this.countiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.counties.filter(i => this.contactCampaign.countriesIds.includes(i.countryId))
      };

      this.countriesDS = {
        paginate: true,
        pageSize: 15,
        store: this.countries
      };

      this.citiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.cities.filter(i => this.contactCampaign.countriesIds.includes(i.countryId))
      };
    } 
  }

  onCountriesChanged(e: any) {
    
    if (e.value && e.value.length) {
      if (!this.contactCampaign.childCampaign) {
        this.citiesDS = {
          paginate: true,
          pageSize: 15,
          store: this.cities.filter(i => e.value.includes(i.countryId))
        };

        if (this.contactCampaign && (!this.contactCampaign.citiesIds || this.contactCampaign.citiesIds.length === 0)) {
          this.countiesDS = {
            paginate: true,
            pageSize: 15,
            store: this.counties.filter(i => e.value.includes(i.countryId))
          };
        } 

        if (this.contactCampaign && this.contactCampaign.countiesIds && this.contactCampaign.countiesIds.length > 0) {
          var countyIdsX = this.counties.filter(x => this.contactCampaign.countiesIds.includes(x.id)).map(y => y.id);
          this.citiesDS = {
            paginate: true,
            pageSize: 15,
            store: this.cities.filter(i => countyIdsX.includes(i.countyId))
          };
        }
      }
    }
    else {
      this.restoreDS();
    }

    if (!(e.value && e.value.length > 0) && this.contactCampaign.countiesIds && this.contactCampaign.countiesIds.length > 0) {
      this.citiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.cities.filter(i => this.contactCampaign.countiesIds.includes(i.countyId))
      };
    } 
  }

  restoreDS() {
    this.citiesDS = {
      paginate: true,
      pageSize: 15,
      store: this.cities
    };
    this.countiesDS = {
      paginate: true,
      pageSize: 15,
      store: this.counties
    };
    this.countriesDS = {
      paginate: true,
      pageSize: 15,
      store: this.countries
    };
  }

  getDisplayExprCities(item) {
    if (!item) {
      return '';
    }
    return item.name + ' - ' + item.countyName + ' - ' + item.countryName;
  } 
}
