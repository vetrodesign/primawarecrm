import { TestBed } from '@angular/core/testing';

import { ItemGroupCategoryCodeService } from './item-group-category-code.service';

describe('ItemGroupCategoryCodeService', () => {
  let service: ItemGroupCategoryCodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemGroupCategoryCodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
