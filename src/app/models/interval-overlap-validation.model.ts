import { DiscountGridDetails } from "./discount-grid-details.model";

export class IntervalOverlapValidation {
    doesOverlap: boolean;
    overlappedDiscountGridDetails: DiscountGridDetails;
}
