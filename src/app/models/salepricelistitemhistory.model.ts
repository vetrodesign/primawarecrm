import { SalePriceListItem } from "./salepricelistitem.model";

export class SalePriceListItemHistory {
    id: string;
    salePriceListItemId: number;
    itemId: number;
    listPrice: number;

    constructor(salePriceListItem?: SalePriceListItem) {
        if (salePriceListItem) {
            this.salePriceListItemId = Number(salePriceListItem.id);
            this.itemId = salePriceListItem.itemId;
            this.listPrice = salePriceListItem.listPrice;
        }
    }
}
