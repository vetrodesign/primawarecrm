import { Component, OnInit, ViewChild, Input } from '@angular/core';
import * as xlsx from 'xlsx';
import { DxDataGridComponent } from 'devextreme-angular';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { ImportAuction } from 'app/models/importauction.model';
import * as moment from 'moment';
import { AuctionService } from 'app/services/auction.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { NotificationService } from 'app/services/notification.service';
import { AuctionType } from 'app/models/auction-type.model';
import { AuctionGroup } from 'app/models/auction-group.model';
import { PartnerLocationService } from 'app/services/partner-location.service';
import * as _ from 'lodash';
import { PartnerNameEquivalence } from 'app/models/partnernameequivalence.model';
import { PartnerNameEquivalenceService } from 'app/services/partner-name-equivalence.service';

@Component({
  selector: 'app-import-auctions',
  templateUrl: './import-auctions.component.html',
  styleUrls: ['./import-auctions.component.css']
})
export class ImportAuctionsComponent implements OnInit {
  @Input() auctionTypes: AuctionType[];
  @Input() auctionGroups: AuctionGroup[];
  @Input() posts: any[];
  @Input() countriesDS: any;
  @Input() countiesDS: any;

  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  animationVisible: boolean;
  file: File;
  buffer: any;
  loaded: boolean;
  reqKeys: any[] = ['Licitatii'];
  headerColumns: any[];
  gridDataSource: any[];
  selectedRows: any[];
  showGenerateBtn: boolean;
  showImportBtn: boolean;

  auctionPopup: boolean;
  selectedAuction: any;
  auction: any;

  urlItems: any[];
  showSpinner: boolean;
  showGrid: boolean;
  showError: boolean;
  keysRequired: string;
  header: string;
  constructor(private authService: AuthService,
     private translationService: TranslateService,
     private auctionService: AuctionService,
     private partnerNameEquivalenceService: PartnerNameEquivalenceService,
     private partnerLocationsService: PartnerLocationService,
     private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.header = 'Extensie fisier acceptata: .xlsx. Coloana obligatorie in header fisier: Licitatii';
  }

  onSelectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.showImportBtn = (this.selectedRows.length > 0) && (this.selectedRows.filter(x => x.partnerId === null || x.countryId == null ||x.countyId == null).length == 0) ? true : false;
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && (e.data.partnerId == null || e.data.countryId == null || e.data.countyId == null)) {
         e.rowElement.style.backgroundColor = '#ffaf82';
     } 
  }

  uploadData(event) {
    this.headerColumns = [];
    this.file = null;
    this.file = event.target.files[0];
    this.readFile();
    this.showGenerateBtn = true;
    this.showError = false;
    this.showGrid = false;
    this.showImportBtn = false;
  }

  readFile() {
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.buffer = fileReader.result;
      const data = new Uint8Array(this.buffer);
      const arr = new Array();
      for (let i = 0; i !== data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      const bstr = arr.join('');
      const workbook = xlsx.read(bstr, { type: 'binary' });
      const first_sheet_name = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[first_sheet_name];

      this.urlItems = [];
      for (const property in worksheet) {
        if (worksheet[property]?.l?.display) {
          let url = {'url': null, 'data': null};
          url.url = worksheet[property]?.l?.display;
          url.data = worksheet[property]?.v;
          this.urlItems.push(url);
        }
        
      }
      this.headerColumns = xlsx.utils.sheet_to_json(worksheet, { raw: true, defval: null });
    };
    fileReader.readAsArrayBuffer(this.file);
  }

  async generateDataGrid() {
    this.loaded = true;
    let readSource = [];
    let index = 0;
    let error = false;
    let fileKeys = [];
    this.headerColumns.forEach(item => {
      const di = {};
      for (const propName in item) {
        if (item[propName] !== null && item[propName] !== undefined) {
          di[propName] = item[propName];
          if (index === 0) {
            fileKeys.push(propName);
          }
        }
      }
      index++;
      readSource.push(di);
    });

    let data = [];
    let item = null;

    readSource.forEach((rs, index) => {
      if (((rs.Licitatii.indexOf('[') !== -1 && rs.Licitatii.indexOf(']')!== -1) || (rs.Licitatii.indexOf('-') !== -1)) && !isNaN(rs.Licitatii.charAt(0)) && (rs.Licitatii.charAt(1) === '.' || rs.Licitatii.charAt(2) === '.' || rs.Licitatii.charAt(3) === '.')) {
        if (item && Object.keys(item).length > 1) {
          data.push(item);    
        }
        item = new ImportAuction();
        item.key = index;
        item.customerId = this.authService.getUserCustomerId();
        item.code = rs.Licitatii;

        item.url = this.urlItems.find(x => x.data == item.code).url;
        item.url = item.url.replace("redirect=true&amp;", "");
        item.url = item.url.replace("reset=true&amp;", "");

        item.orderNumber = Number(item.code.split('.').shift());
        item.acquisitionType = item.code.substring(item.code.indexOf(".") + 2,item.code.indexOf(" - "));
        item.acquisitionNumber = this.extractLastWord(item.code);
        item.acquisitionName = rs.Licitatii.indexOf(']')!== -1 ? item.code.substring(item.code.indexOf("]") + 1) : item.code.substring(item.code.indexOf("-") + 1);

      } else
        if (rs.Licitatii.indexOf('Localizare') !== -1 && item) {
          item.location = rs.Licitatii;
          item.country = item.location.substring(item.location.indexOf(":") + 2,item.location.indexOf(" ("));
          item.county = item.location.substring(item.location.indexOf("(") + 1,item.location.indexOf(")"));

        } else
          if ((rs.Licitatii.indexOf('Valoare Estimata') !== -1 || rs.Licitatii.indexOf('Valoare estimata') !== -1 || rs.Licitatii.indexOf('Valoare estimativa') !== -1 || rs.Licitatii.indexOf('Valoare fara TVA') !== -1) && item) {
            item.value = rs.Licitatii;
            let nr = Number(item.value.substring(item.value.indexOf(":") + 2,item.value.indexOf(" RON")).replaceAll(" ", ""));
            item.estimatedValue = isNaN(nr) ? 0 : nr;  

          } else
            if (rs.Licitatii.indexOf('Data licitatie') !== -1 && item) {
              item.auctionDates = rs.Licitatii;
              if (rs.Licitatii.indexOf('Termen limita')!== -1) {
                let ad = moment(item.auctionDates.substring(item.auctionDates.indexOf("licitatie:") + 11,item.auctionDates.indexOf(",")), 'DD-MM-YYYY HH:mm').toDate();
                if (ad instanceof Date && !isNaN(ad.valueOf())) {
                  item.acquisitionDate = ad;
                }
                let ed = moment(item.auctionDates.substring(item.auctionDates.indexOf("ita:") + 5), 'DD-MM-YYYY HH:mm').toDate(); 
                if (ed instanceof Date && !isNaN(ed.valueOf())) {
                  item.endDate = ed;
                }
              } else {
                let ad = moment(item.auctionDates.substring(item.auctionDates.indexOf(":") + 2), 'DD-MM-YYYY HH:mm').toDate();
                if (ad instanceof Date && !isNaN(ad.valueOf())) {
                  item.acquisitionDate = ad;
                }
              }
            } else
              if (item) {
                item.auctioner = item.auctioner ? item.auctioner : rs.Licitatii;
                item.acquisitionerName =  item.acquisitionerName ?  item.acquisitionerName : rs.Licitatii;
              }

              if (readSource.length - 1 === index) {
                data.push(item);
              }
    })

    if (data && data.length > 200) {
      this.notificationService.alert('top', 'center', 'Licitatii - Datele nu au fost generate! Fisierul trebuie sa contina maxim 200 de inregistrari!', NotificationTypeEnum.Red, true);
      return;
    }

    await this.auctionService.validateAuctionsAsync(data).then(items => {
      if (items && items.length > 0) {
        this.gridDataSource = items;
      }
      this.loaded = false;
    }).catch(error => {
      this.loaded = false;
      this.notificationService.alert('top', 'center', 'Licitatii - A aparut o eroare la generearea de fisier! Va rugam sa incercati cu alt fisier!', NotificationTypeEnum.Red, true);
    });

    this.reqKeys.forEach(key => {
      if (fileKeys.indexOf(key) === -1) {
        error = true;
      }
    });
    if (this.gridDataSource.length > 0 && !error) {
      this.showGrid = true;
    } else {
      this.showError = true;
    }
    this.showGenerateBtn = false;
  }

  openDetails(data: any) {
    this.auction = data;
    //this.selectedAuction = data;
    this.selectedAuction = _.clone(data, true);
    this.auctionPopup = true;
  }

  validateSelectedMap(): boolean {
    if (this.auctionPopup == true && this.selectedAuction && this.selectedAuction.countryId != null && this.selectedAuction.countyId != null) {
      if (this.auction.country == this.selectedAuction.country && this.auction.county == this.selectedAuction.county) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

  extractLastWord(input) {
    const words = input.split(' ');
    if (words.length >= 2) {
      const lastWord = words[words.length - 1];
      const beforeLastWord = words[words.length - 2];
      if (beforeLastWord === '-') {
        return lastWord;
      }
    }
    return null;
  }

  mapAuction(map?: boolean) {
    if(map) {      
      const t = this.auction.acquisitionerName;

      let p = new PartnerNameEquivalence();
      p.name = t;
      p.partnerId = this.selectedAuction.partnerId;
      this.partnerNameEquivalenceService.createPartnerNameEquivalenceAsync(p);

      this.gridDataSource.forEach(item => {
        if (item.acquisitionerName === t) {
          item.partnerId = this.selectedAuction.partnerId;
          item.acquisitionerName = this.selectedAuction.acquisitionerName;
          item.country = this.selectedAuction.country;
          item.county = this.selectedAuction.county;
          item.countryId = this.selectedAuction.countryId;
          item.countyId = this.selectedAuction.countyId;
        }
      });

      this.auctionPopup = false;
    } else {
      this.auctionPopup = false;
    }
  }

  partnerChange(e) {
    if (e) {
      this.selectedAuction.partnerId = e;
      this.getLocations(this.selectedAuction.partnerId);
    }
  }

  selectedPartnerChange(e) {
    if (e) {
      this.selectedAuction.acquisitionerName = e;
    }
  }

  async getLocations(partnerId: number) {
    await this.partnerLocationsService.getPartnerLocationsByPartnerID(partnerId).then(locations => {
      if (locations && locations.length > 0) {
        this.selectedAuction.countryId = locations.find(x => x.isImplicit)?.countryId;
        this.selectedAuction.countyId = locations.find(x => x.isImplicit)?.countyId;

        this.selectedAuction.country = this.countriesDS.store.find(x => x.id === this.selectedAuction.countryId)?.name;
        this.selectedAuction.county = this.countiesDS.store.find(x => x.id === this.selectedAuction.countyId)?.name;
      }
    })
  }

   public async import() {
     if (this.selectedRows && this.selectedRows.length > 0) {
      this.showSpinner = true;
      this.loaded = true;

      let selectedRows = this.selectedRows.length;
      this.selectedRows.forEach(item => {
        let type = this.auctionTypes.find(x => x.name === item.acquisitionType);
        item.acquisitionNumber =  item.acquisitionNumber ?  item.acquisitionNumber : type?.code;
        item.auctionTypeId = type?.id ? type?.id : null;
        item.auctionGroupId = null;
      })

      let sendRows = this.selectedRows.filter(y => y.auctionTypeId);
      this.auctionService.createMultipleAuctionAsync(sendRows).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', `Datele au fost importate cu succes! Din ${selectedRows} de inregistrari selectate, au fost mapate cu dictionarul de tip licitatie si trimise catre inserare ${sendRows.length} de inregistrari, iar dintre acestea s-au importat cu succes ${r}!`,
          (sendRows.length === r) ? NotificationTypeEnum.Green : NotificationTypeEnum.Yellow, true);
          this.gridDataSource = [];
          this.loaded = false;
          } else {
          this.notificationService.alert('top', 'center', `Nu s-a reusit importarea  a ${sendRows.length} de inregistrarilor! Verificati duplicatele sau daca se mapeaza tip licitatie!`, NotificationTypeEnum.Yellow, true);
          this.loaded = false;
        }
        this.showImportBtn = false;
      }).catch(error => 
        {
          this.notificationService.alert('top', 'center', 'Licitatii - Datele nu au fost inserate! A aparut o eroare!', NotificationTypeEnum.Red, true);
        });
     }
  }

  getDisplayAuctionGroupExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  getDisplayExprCountries(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  getDisplayExprCounties(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  getPostDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }
}
