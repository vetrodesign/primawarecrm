import { TestBed } from '@angular/core/testing';

import { PartnerReviewQuestionService } from './partner-review-question.service';

describe('PartnerReviewQuestionService', () => {
  let service: PartnerReviewQuestionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerReviewQuestionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
