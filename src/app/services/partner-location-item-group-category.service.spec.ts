import { TestBed } from '@angular/core/testing';

import { PartnerLocationItemGroupCategoryService } from './partner-location-item-group-category.service';

describe('PartnerLocationItemGroupCategoryService', () => {
  let service: PartnerLocationItemGroupCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerLocationItemGroupCategoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
