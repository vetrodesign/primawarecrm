import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerFinancialInfoComponent } from './partner-financial-info.component';

describe('PartnerFinancialInfoComponent', () => {
  let component: PartnerFinancialInfoComponent;
  let fixture: ComponentFixture<PartnerFinancialInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerFinancialInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerFinancialInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
