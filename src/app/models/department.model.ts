import { BaseDomain } from "./base-domain.model";
import { Office } from "./office.model";

export class Department extends BaseDomain {
    name: string;
    description: string;
    departmentType: number;
    
    offices: Office[];
}
