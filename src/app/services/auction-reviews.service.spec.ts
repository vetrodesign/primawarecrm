import { TestBed } from '@angular/core/testing';

import { AuctionReviewsService } from './auction-reviews.service';

describe('AuctionReviewsService', () => {
  let service: AuctionReviewsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuctionReviewsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
