export enum OrderStatusTypeEnum {
    Canceled = 1,
    New = 2,
    Processing = 3,
    Validated = 4,
    Delivered = 5,
    Storno = 6
}