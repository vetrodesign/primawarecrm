import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import { DxButtonModule, DxDataGridModule, DxButtonGroupModule, DxDropDownBoxModule, DxTagBoxModule,
 DxSelectBoxModule, DxPieChartModule, DxChartModule, DxSpeedDialActionModule, DxListModule, DxValidationSummaryModule,
 DxValidationGroupModule, DxTextBoxModule, DxValidatorModule, DxFormModule, DxTreeListModule, DxTextAreaModule,
 DxDateBoxModule, DxFileUploaderModule, DxNumberBoxModule, DxPopupModule, DxSwitchModule, DxCheckBoxModule, DxMapModule, DxBulletModule, DxScrollViewModule, DxLoadPanelModule, DxTooltipModule, DxPopoverModule, DxRadioGroupModule, 
 DxTabPanelModule,
 DxTabsModule,
 DxAutocompleteModule} from 'devextreme-angular';
import { AdminComponent } from '../../admin/admin.component';
import { CustomerComponent } from '../../customer/customer.component';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { SetDataComponent } from 'app/set-data/set-data.component';
import { SpinnerComponent } from 'app/spinner/spinner.component';
import { ImportComponent } from 'app/import/import.component';
import { PartnerComponent } from 'app/partner/partner.component';
import { PartnerDetailsComponent } from 'app/partner-details/partner-details.component';
import { MapComponent } from 'app/helpers/map/map.component';
import { ExceptionLogComponent } from 'app/exception-log/exception-log.component';
import { TranslateModule } from 'app/services/translate/translate.module';
import { TitleComponent } from 'app/components/title/title.component';
import { PartnerSearchFilterComponent } from 'app/partner-search-filter/partner-search-filter.component';
import { PartnerLegalFormComponent } from 'app/partner-legal-form/partner-legal-form.component';
import { PartnerLocationContactComponent } from 'app/partner-location-contact/partner-location-contact.component';
import { PartnerReviewComponent } from 'app/partner-review/partner-review.component';
import { PartnerDropdownSearchComponent } from 'app/partner-dropdown-search/partner-dropdown-search.component';
import { PartnerFinancialInfoComponent } from 'app/partner-financial-info/partner-financial-info.component';
import { OrderComponent } from 'app/order/order.component';
import { OrderDetailsComponent } from 'app/order-details/order-details.component';
import { OrderSearchFilterComponent } from 'app/order-search-filter/order-search-filter.component';
import { ImportAuctionsComponent } from 'app/import-auctions/import-auctions.component';
import { AuctionsComponent } from 'app/auctions/auctions.component';
import { AuctionTypeComponent } from 'app/auction-type/auction-type.component';
import { AuctionGroupComponent } from 'app/auction-group/auction-group.component';
import { AuctionSearchFilterComponent } from 'app/auction-search-filter/auction-search-filter.component';
import { AuctionsDetailsComponent } from 'app/auctions-details/auctions-details.component';
import { AuctionReviewsComponent } from 'app/auction-reviews/auction-reviews.component';
import { DailyReportComponent } from 'app/daily-report/daily-report.component';
import { SalesTargetComponent } from 'app/sales-target/sales-target.component';
import { SalesTargetAllocationComponent } from 'app/sales-target-allocation/sales-target-allocation.component';
import { SalesGroupTargetComponent } from 'app/sales-group-target/sales-group-target.component';
import { PartnerNameEquivalenceComponent } from 'app/partner-name-equivalence/partner-name-equivalence.component';
import { ImportExportItemComponent } from 'app/import-export-item/import-export-item.component';
import { SalesTargetReportComponent } from 'app/sales-target-report/sales-target-report.component';
import { PartnerLocationScheduleComponent } from 'app/partner-location-schedule/partner-location-schedule.component';
import { TurnoverComponent } from 'app/turnover/turnover.component';
import { TurnoverDetailsComponent } from 'app/turnover-details/turnover-details.component';
import { PartnerCallReminderComponent } from 'app/partner-call-reminder/partner-call-reminder.component';
import { ClientTurnoverComponent } from 'app/client-turnover/client-turnover.component';
import { PartnerActivityTurnoverComponent } from 'app/partner-activity-turnover/partner-activity-turnover.component';
import { NewItemsTurnoverComponent } from 'app/new-items-turnover/new-items-turnover.component';
import { TurnoverCommentComponent } from 'app/turnover-comment/turnover-comment.component';
import { YearlyTurnoverComponent } from 'app/yearly-turnover/yearly-turnover.component';
import { PartnerContractDetailsComponent } from 'app/components/partner-contract-details/partner-contract-details.component';
import { CustomerInterestsReportComponent } from 'app/customer-interests-report/customer-interests-report.component';
import { PartnerObservationsComponent } from 'app/partner-observations/partner-observations.component';
import { TooltipifyDirective } from 'app/directives/tooltipify.directive';
import { OfficeItemsTurnoverComponent } from 'app/office-items-turnover/office-items-turnover.component';
import { MandatoryPromotionTurnoverComponent } from 'app/mandatory-promotion-turnover/mandatory-promotion-turnover.component';
import { LiquidationOfferTurnoverComponent } from 'app/liquidation-offer-turnover/liquidation-offer-turnover.component';
import { PartnerWebsiteComponent } from 'app/partner-website/partner-website.component';
import { AgentOtherItemTurnoverComponent } from 'app/agent-other-item-turnover/agent-other-item-turnover.component';
import { PartnerSuggestionComponent } from 'app/partner-suggestion/partner-suggestion.component';
import { PartnerObservationsDetailsComponent } from 'app/partner-observations-details/partner-observations-details.component';
import { ContactCampaignPartnersComponent } from 'app/contact-campaign-partners/contact-campaign-partners.component';
import { CrmPartnerSearchFilterComponent } from 'app/crm-partner-search-filter/crm-partner-search-filter.component';
import { ContactCampaignManagementComponent } from 'app/contact-campaign-management/contact-campaign-management.component';
import { ContactCampaignManagementHappyBirthdayComponent } from 'app/contact-campaign-management/contact-campaign-management-happybirthday/contact-campaign-management-happybirthday.component';
import { ContactCampaignManagementLocationComponent } from 'app/contact-campaign-management/contact-campaign-management-location/contact-campaign-management-location.component';
import { ContactCampaignManagementHappyBirthDayDetailsComponent } from 'app/contact-campaign-management/contact-campaign-management-happybirthday/contact-campaign-management-happybirthday-details/contact-campaign-management-happybirthday-details.component';
import { ContactCampaignManagementLocationDetailsComponent } from 'app/contact-campaign-management/contact-campaign-management-location/contact-campaign-management-location-details/contact-campaign-management-location-details.component';
import { ContactCampaignAllocationComponent } from 'app/contact-campaign-allocation/contact-campaign-allocation.component';
import { ContactCampaignAllocationDetailsComponent } from 'app/contact-campaign-allocation/contact-campaign-allocation-details/contact-campaign-allocation-details.component';
import { PartnerFinancialDataComponent } from 'app/partner-financial-data/partner-financial-data.component';
import { PartnerDailyReceiptsComponent } from 'app/partner-daily-receipts/partner-daily-receipts.component';
import { PartnerItemsComponent } from 'app/partner-items/partner-items.component';
import { PartnerSpecializationsComponent } from 'app/partner-specializations/partner-specializations.component';
import { PartnerSalesPriceListComponent } from 'app/partner-sales-price-list/partner-sales-price-list.component';
import { SpecialPriceRequestComponent } from 'app/special-price-request/special-price-request.component';
import { ItemTurnoverComponent } from 'app/item-turnover/item-turnover.component';
import { ItemTurnoverHistoryComponent } from 'app/item-turnover-history/item-turnover-history.component';
import { DailyWhatsappOffersComponent } from 'app/daily-whatsapp-offers/daily-whatsapp-offers.component';
import { PartnerItemsCertificatesComponent } from 'app/partner-items/partner-items-certificates/partner-items-certificates.component';
import { MyClientsComponent } from 'app/my-clients/my-clients.component';
import { OfficeDrinksTurnoverComponent } from 'app/office-drinks-turnover/office-drinks-turnover.component';
import { PartnerSubscriptionComponent } from 'app/partner-subscription/partner-subscription.component';
import { PartnerShoppingCartComponnet } from 'app/turnover-details/partner-shopping-cart/partner-shopping-cart.component';
import { PartnerAuthorizationComponent } from 'app/partner-authorization/partner-authorization.component';
import { BnrRoborComponent } from 'app/bnr-robor/bnr-robor.component';
import { CurrencyEvolutionChartComponent } from 'app/currency-evolution-chart/currency-evolution-chart.component';
import { EuriborDataComponent } from 'app/euribor-data/euribor-data.component';
import { WeatherForecastComponent } from 'app/weather-forecast/weather-forecast.component';
import { PartnerReviewReportComponent } from 'app/partner-review-report/partner-review-report.component';
import { PartnerReviewQuestionComponent } from 'app/partner-review-question/partner-review-question.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    DxButtonModule,
    DxDataGridModule,
    DxDataGridModule,
    DxDropDownBoxModule,
    DxDataGridModule,
    DxTagBoxModule,
    DxSelectBoxModule,
    DxPieChartModule,
    DxChartModule,
    DxSpeedDialActionModule,
    DxListModule,
    DxValidationGroupModule,
    DxValidationSummaryModule,
    DxTextBoxModule,
    DxValidatorModule,
    DxFormModule,
    DxSelectBoxModule,
    DxTreeListModule,
    DxTextAreaModule,
    DxDateBoxModule,
    DxBulletModule,
    DxMapModule,
    DxSwitchModule,
    DxFileUploaderModule,
    DxNumberBoxModule,
    DxPopupModule,
    DxCheckBoxModule,
    TranslateModule,
    DxScrollViewModule,
    DxLoadPanelModule,
    DxTooltipModule,
    DxPopoverModule,
    DxRadioGroupModule,
    DxTabPanelModule,
    DxTabsModule,
    DxPopoverModule,
    DxAutocompleteModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    UpgradeComponent,
    AdminComponent,
    CustomerComponent,
    GridToolbarComponent,
    SetDataComponent,
    SpinnerComponent,
    ImportComponent,
    PartnerComponent,
    PartnerObservationsDetailsComponent,
    ContactCampaignManagementComponent,
    ContactCampaignManagementHappyBirthdayComponent,
    ContactCampaignManagementHappyBirthDayDetailsComponent,
    ContactCampaignManagementLocationDetailsComponent,
    ContactCampaignAllocationComponent,
    ContactCampaignAllocationDetailsComponent,
    ContactCampaignManagementLocationComponent,
    ContactCampaignPartnersComponent,
    PartnerDetailsComponent,
    PartnerSearchFilterComponent,
    CrmPartnerSearchFilterComponent,
    PartnerLegalFormComponent,
    PartnerLocationContactComponent,
    PartnerReviewComponent,
    PartnerDropdownSearchComponent,
    MapComponent,
    ExceptionLogComponent,
    TitleComponent,
    PartnerFinancialInfoComponent,
    OrderComponent,
    OrderDetailsComponent,
    OrderSearchFilterComponent,
    ImportAuctionsComponent,
    AuctionsComponent,
    AuctionTypeComponent,
    AuctionGroupComponent,
    AuctionSearchFilterComponent,
    AuctionsDetailsComponent,
    AuctionReviewsComponent,
    DailyReportComponent,
    SalesTargetComponent,
    SalesTargetAllocationComponent,
    SalesGroupTargetComponent,
    PartnerNameEquivalenceComponent,
    ImportExportItemComponent,
    SalesTargetReportComponent,
    PartnerLocationScheduleComponent,
    TurnoverComponent,
    TurnoverDetailsComponent,
    PartnerCallReminderComponent,
    PartnerSuggestionComponent,
    ClientTurnoverComponent,
    PartnerActivityTurnoverComponent,
    NewItemsTurnoverComponent,
    TurnoverCommentComponent,
    YearlyTurnoverComponent,
    PartnerContractDetailsComponent,
    CustomerInterestsReportComponent,
    PartnerObservationsComponent,
    TooltipifyDirective,
    OfficeItemsTurnoverComponent,
    MandatoryPromotionTurnoverComponent,
    LiquidationOfferTurnoverComponent,
    PartnerWebsiteComponent,
    AgentOtherItemTurnoverComponent,
    PartnerFinancialDataComponent,
    PartnerDailyReceiptsComponent,
    PartnerItemsComponent,
    PartnerSpecializationsComponent,
    PartnerSalesPriceListComponent,
    SpecialPriceRequestComponent,
    ItemTurnoverComponent,
    ItemTurnoverHistoryComponent,
    DailyWhatsappOffersComponent,
    PartnerItemsCertificatesComponent,
    MyClientsComponent,
    OfficeDrinksTurnoverComponent,
    PartnerSubscriptionComponent,
    PartnerShoppingCartComponnet,
    PartnerAuthorizationComponent,
    BnrRoborComponent,
    CurrencyEvolutionChartComponent,
    EuriborDataComponent,
    WeatherForecastComponent,
    PartnerReviewQuestionComponent,
    PartnerReviewReportComponent
  ],
  providers: [
    DecimalPipe
  ],
  exports: []
})

export class AdminLayoutModule {}
