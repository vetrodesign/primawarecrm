import { Injectable } from '@angular/core';
import { CaenCodeCategory } from 'app/models/caencodecategory.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';


@Injectable({
  providedIn: 'root'
})
export class CaenCodeCategoryService {
  constructor(private apiService: ApiService) { }

  async getAllCaenCodeCategorysAsync(): Promise<CaenCodeCategory[]> {
    const apiURL = `/CaenCodeCategory/getall`;
    return await this.apiService.GetAsync<CaenCodeCategory[]>(apiURL, Constants.commonApi);
  }

  async createCaenCodeCategoryAsync(caenCodeCategory: CaenCodeCategory): Promise<any> {
    const apiURL = `/CaenCodeCategory/create`;
    return await this.apiService.PostAsync<any>(apiURL, caenCodeCategory, Constants.commonApi);
  }

  async updateCaenCodeCategoryAsync(caenCodeCategory: CaenCodeCategory): Promise<any> {
    const apiURL = `/CaenCodeCategory/update`;
    return await this.apiService.PutAsync<any>(apiURL, caenCodeCategory, Constants.commonApi);
  }

  async deleteCaenCodeCategoryAsync(ids: number[]): Promise<any> {
    const apiURL = `/CaenCodeCategory/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
