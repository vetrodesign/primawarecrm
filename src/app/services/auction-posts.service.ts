
import { Injectable } from '@angular/core';
import { AuctionPosts } from 'app/models/auction-posts.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})

export class AuctionPostsService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<any[]> {
    const apiURL = `/auctionposts/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getByCustomerIdAsync(customerId: number): Promise<any[]> {
    const apiURL = `/auctionposts/getByCustomerIdAsync`;
    return await this.apiService.PostAsync<any[]>(apiURL, customerId, Constants.mrkApi);
  }

  async createMultipleAsync(p: AuctionPosts[]): Promise<any> {
    const apiURL = `/auctionposts/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async updateAsync(p: AuctionPosts): Promise<any> {
    const apiURL = `/auctionposts/update`;
    return await this.apiService.PutAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async deleteAsync(p: AuctionPosts[]): Promise<any> {
    const apiURL = `/auctionposts/deleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }
}
