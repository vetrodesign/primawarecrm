import { BaseDomain } from "./base-domain.model";

export class SalesTarget extends BaseDomain {
    orderNumber: number;
    code: string;
    name: string;
    description: string;

    saleGroupTargetId: number;
    targetType: number;
    customTypeText: string;

    isActive: boolean;
}