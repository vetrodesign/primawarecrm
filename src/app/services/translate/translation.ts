import { InjectionToken } from '@angular/core';
// imported translations
import { langEnName, langEnTrans } from './lang-en';
import { langRoName, langRoTrans } from './lang-ro';
// translation token
export const TRANSLATIONS = new InjectionToken<string>('translations');
// all translations
export const dictionary = {
    [langEnName]: langEnTrans,
    [langRoName]: langRoTrans
};
// providers
export const TRANSLATION_PROVIDERS = [
    { provide: TRANSLATIONS, useValue: dictionary }
];
