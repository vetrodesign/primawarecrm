import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Constants } from 'app/constants';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { Brand } from 'app/models/brand.model';
import { PartnerXItemComment } from 'app/models/clientturnover.model';
import { ItemXRelated, RelatedItemType } from 'app/models/itemxrelated.model';
import { PartnerSearchFilter } from 'app/models/partnerSearchFilter.model';
import { PartnerItemState } from 'app/models/partneritemstate.model';
import { TurnoverPreorder } from 'app/models/turnover-preorder.model';
import { AuthService } from 'app/services/auth.service';
import { ClientTurnoverService } from 'app/services/client-turnover.service';
import { ItemRelatedService } from 'app/services/item-related.service';
import { ItemService } from 'app/services/item.service';
import { NotificationService } from 'app/services/notification.service';
import { PartnerService } from 'app/services/partner.service';
import { TranslateService } from 'app/services/translate';
import { TurnoverPreorderService } from 'app/services/turnover-preorder.service';
import { DxValidatorComponent } from 'devextreme-angular';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-turnover-comment',
  templateUrl: './turnover-comment.component.html',
  styleUrls: ['./turnover-comment.component.css']
})
export class TurnoverCommentComponent implements OnInit {
@Input() selectedClientTurnover: any;
@Input() partnerItemState: PartnerItemState[];
@Input() brands: Brand[];
@Input() items: any[];
@Input() clientId: number;
@Input() postId: number;
@Input() customerId: number;
@Input() section: number;
@Input() partnersInput: any;

@Output() refreshDataEvent: EventEmitter<any> = new EventEmitter<any>();
@ViewChild('validationGroup') validationGroup: DxValidatorComponent;

popupVisible: boolean;
isTabActive: string;
partners: any;
detailsTabs: any;
relatedItemTypes: any[] = [];
relatedItems: any[];

  constructor(  private clientTurnoversService: ClientTurnoverService, 
    private translationService: TranslateService,
    private authenticationService: AuthService,
    private itemRelatedService: ItemRelatedService,
    private itemService: ItemService,
    private turnoverPreorderService: TurnoverPreorderService,
    private partnerService: PartnerService,
    private notificationService: NotificationService) { 
      this.isTabActive = this.authenticationService.getCustomerPageTitleTheme();
    }

  ngOnInit(): void {
    this.partners = {
      paginate: true,
      pageSize: 15,
      store: []
    };

    for (let n in RelatedItemType) {
      if (typeof RelatedItemType[n] === 'number') {
        this.relatedItemTypes.push({
          id: <any>RelatedItemType[n],
          name: this.translationService.instantForcedRo(n)
        });
      }
    }

    this.detailsTabs = [
    {
      id: 1,
      name: 'Oferte',
      'isActive': false
    },
    {
      id: 2,
      name: 'Istoric Comentarii',
      'isActive': true
    }];
  }

  async toggleMode(tab: any) {
    this.detailsTabs.find(x => x.isActive).isActive = false;
    tab.isActive = true;
    if (tab.id === 1) {
     
    }

    if (tab.id === 2) {
    
    }
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && e.data.quantity != null && e.data.quantity > 0) {
      e.rowElement.style.backgroundColor = '#a1ffa1';
    }
  }

  async loadItemCommentsHistoryStatus() {
    if (this.selectedClientTurnover && this.selectedClientTurnover.itemComments && this.selectedClientTurnover.itemComments.length > 0) {
      this.selectedClientTurnover.itemComments.forEach(i => {
        if (i.isInvoiced) {
          i.status = 'Facturat';
        }
        if (i.hasStock) {
          i.status = 'Are stoc';
        }
        if (i.atOrder) {
          i.status = 'La comanda ';
        }
        if (i.notUsed) {
          i.status = 'Nu folosesc';
        }
      });
    }
  }

  async getRelatedItems() {
    try {
      const rItems = await this.itemRelatedService.getItemXRelatedByItemIdAsync(this.selectedClientTurnover.itemId);
      this.relatedItems = (rItems && rItems.length > 0) ? rItems : [];
  
      if (this.relatedItems.length > 0) {
        let ids = this.relatedItems.map(x => x.relatedItemId);
  
        // Fetch item stocks and computed sales prices concurrently
        const [itemStocks, cmPrices] = await Promise.all([
          this.itemService.getItemStocks(ids),
          this.partnerService.getPartnerComputedSalesPrices(this.clientId)
        ]);
  
        if ((itemStocks && itemStocks.length > 0) || (cmPrices && cmPrices.length > 0)) {
          this.relatedItems.forEach(i => {
            i.stock = itemStocks[i.id] || 0;
            const cmPrice = cmPrices.find(x => x.itemId == i.id);
            i.price = cmPrice ? cmPrice.price : 0;
          });
  
          // Filter related items with price > 0 and stock > 0
          this.relatedItems = this.relatedItems.filter(x => x.price > 0 && x.stock > 0);
        }
      }
    } catch (error) {
      console.error("Error fetching related items:", error);
    }
  }

  checkTabDisabled(tab) {
    if (this.selectedClientTurnover.lastComment.notUsed && (tab.id == 1 || tab.id == 2)) {
      this.detailsTabs.find(x => x.isActive).isActive = false;
      this.detailsTabs[2].isActive = true;
      return true;
    }  else {
      return false;
    }
  }

  checkStockChange(data) {
    if (data.hasStock) {
      data.isInvoiced = false;
      data.notUsed = false;
      data.atOrder = false;

      data.hasStockFromMe = true;
      data.callBackDate = data.callBackDate ? data.callBackDate : this.addDaysToDate(30);
    }
  }

  checkInvoicedChange(data) {
    if (data.isInvoiced) {
      data.hasStock = false;
      data.notUsed = false;
      data.atOrder = false;
    }
  }

  checkNotUsedChange(data) {
    if (data.notUsed) {
      data.isInvoiced = false;
      data.hasStock = false;
      data.atOrder = false;
      data.callBackDate = data.callBackDate ? data.callBackDate : this.addDaysToDate(360);
    }
  }

  checkAtOrderChange(data) {
    if (data.atOrder) {
      data.isInvoiced = false;
      data.notUsed = false;
      data.hasStock = false;
      data.callBackDate = data.callBackDate ? data.callBackDate : this.addDaysToDate(180);
    }
  }

   addDaysToDate(days): Date {
    let currentDate = new Date();
    currentDate.setDate(currentDate.getDate() + days);
    return currentDate;
  }

  popupCloseEvent(data) {
    this.popupVisible = false;
  }

  async openPopup() {
    this.popupVisible = true;
    if (this.selectedClientTurnover && this.selectedClientTurnover.itemId) {
      await this.getRelatedItems();
      await this.loadItemCommentsHistoryStatus();
    }
  }

  closePopup() {
    this.popupVisible = false;
  }

  async saveComment(item: PartnerXItemComment, itemId: number) {
    if ( this.clientId &&
      this.postId &&
      this.customerId &&
      (!this.validationGroup || this.validationGroup.instance.validate().isValid)) {

      item.customerId = this.customerId;
      item.partnerId = this.clientId;
      item.postId = this.postId;
      item.section = this.section;

      let bestRankingItemId = itemId ? itemId : item.bestRankingItemId;
      item.itemId = item.itemId ? item.itemId : bestRankingItemId;

      item.partnerItemStateId = null;

      if (item.isInvoiced) {
        item.partnerItemStateId = this.partnerItemState.find(x => x.name == "Facturat")?.id;
        item.comment = item.comment ? item.comment : '';
      } else  
      if (item.notUsed) {
        item.partnerItemStateId = this.partnerItemState.find(x => x.name == "Nu Foloseste")?.id;
        item.comment = item.comment ? item.comment : '';
        item.callBackDate = item.callBackDate ? item.callBackDate : this.addDaysToDate(360);
      } 
      if (item.hasStock) {
        item.partnerItemStateId = this.partnerItemState.find(x => x.name == "Are Stoc")?.id;
        item.comment = item.comment ? item.comment : '';
      }
      if (item.atOrder) {
        item.partnerItemStateId = this.partnerItemState.find(x => x.name == "La Comanda")?.id;
        item.comment = item.comment ? item.comment : '';
      }

      item.monthlyOrderedQuantity = item.shoppingCartQuantity ? item.shoppingCartQuantity : item.monthlyOrderedQuantity;

      await this.clientTurnoversService.updatePartnerXItemComment(item).then(r => {
        this.notificationService.alert('top', 'center', 'Rulaj - Datele au fost modificate cu succes!',
        NotificationTypeEnum.Green, true)

        let createPreorders = [];
        if (!item.id) {
          //se vor crea commentarii si precomanda
          let items = (this.relatedItems && this.relatedItems.length > 0) ? this.relatedItems?.filter(x => x.quantity > 0) : [];
          let createItems = [];
          if (items && items.length > 0) {

            items.forEach(x => {
              let i = new PartnerXItemComment();
              i.customerId = this.customerId;
              i.partnerId = this.clientId;
              i.postId = this.postId;
              i.section = this.section;
              i.itemId = x.relatedItemId;
              i.partnerItemStateId = this.partnerItemState.find(x => x.name == "Facturat")?.id;
              i.monthlyOrderedQuantity = item.shoppingCartQuantity ? item.shoppingCartQuantity : x.quantity;
              i.comment = item.comment ? item.comment : '';
              createItems.push(i);

              let j = new TurnoverPreorder();
              j.customerId = this.customerId;
              j.partnerId = this.clientId;
              j.postId = this.postId;
              j.itemId = x.relatedItemId;
              j.quantity = item.shoppingCartQuantity ? item.shoppingCartQuantity : x.quantity;
              createPreorders.push(j);
            });
            this.clientTurnoversService.updatePartnerXItemComments(createItems);
          }
          //se face precomanda / cosul
        }
        if (item && item.isInvoiced) {
          let initial = TurnoverPreorder.FromPartnerXItemComment(item);
          createPreorders.push(initial);
          this.turnoverPreorderService.createMultipleTurnoverPreorderAsync(createPreorders);
        }      

        this.closePopup();
        this.refreshDataEvent.emit();
      }).catch(ex => {
        this.closePopup();
        this.refreshDataEvent.emit();
      });
    }
  }

  getDisplayExprPartners(item) {
    if (!item) {
        return '';
      }
      return item.code + ' - ' + item.name;
  }

  displayCodeExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  priceXQuantityExpr(data) {
    if (data && data.price && data.quantity) {
      return data.price * data.quantity;
    } else {
      return '';
    }
  }

  onPartnerChanged(e) {
    if (e && e.target && e.target.value && e.target.value.length > 4) {
      let searchFilter = new PartnerSearchFilter();
      searchFilter.name = e.target.value;
      searchFilter.isActive = true;
      this.partnerService.getPartnersByFilter(searchFilter).then(items => {
        this.partners.store.push(...items.filter(x => !this.partners.store.map(y => y.id).includes(x.id)));
      })
    }
  }

  openSpecialPriceRequest() {
    if (this.selectedClientTurnover && this.partnersInput.store && this.partnersInput.store.length > 0) {
      var url = environment.CRMPrimaware + '/' + Constants.specialPriceRequest + '?clientCode=' + this.partnersInput.store[0].code + '&selectedClientTurnover=' + this.selectedClientTurnover.itemId;
      window.open(url, '_blank');
    }
  }
}
