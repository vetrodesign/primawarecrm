import { Injectable } from '@angular/core';
import { VipOfferItems } from 'app/models/vip-offer-items.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { SyncVipOfferDetailsItem } from 'app/models/vip-offer.model';

@Injectable({
  providedIn: 'root'
})
export class VipOfferItemsService {

  constructor(private apiService: ApiService) { }

 
  async getAllAsync(vipOfferId?: number, itemId?: number): Promise<VipOfferItems[]> {
    var apiURL = `/vipOfferItems/getall`;
    if (vipOfferId) {
      apiURL+= '?vipOfferId=' + vipOfferId;
    }
    if (itemId) {
      apiURL+= '?itemId=' + itemId;
    }
    return await this.apiService.GetAsync<VipOfferItems[]>(apiURL, Constants.mrkApi);
  }

  //Valid means it has tiers and is in salePriceLists!!
  async getAllValidAsync(): Promise<VipOfferItems[]> {
    var apiURL = `/vipOfferItems/getallvalid`;
    return await this.apiService.GetAsync<VipOfferItems[]>(apiURL, Constants.mrkApi);
  }

  async createAsync(vpi: VipOfferItems): Promise<any> {
    const apiURL = `/vipOfferItems/create`;
    return await this.apiService.PostAsync<any>(apiURL, vpi, Constants.mrkApi);
  }

  async updateAsync(vpi: VipOfferItems): Promise<any> {
    const apiURL = `/vipOfferItems/update`;
    return await this.apiService.PutAsync<any>(apiURL, vpi, Constants.mrkApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/vipOfferItems/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }

  async GetCartonQuantityForItemIds(ids: number[]): Promise<any> {
    const apiURL = `/buffer/GetCartonQuantityForItemIds`;
    return await this.apiService.PostAsync<any>(apiURL, ids, Constants.mrkApi);
  }

  async SyncItemPriceAsync(specialOfferData: VipOfferItems[], syncVipOfferDetailsItem : SyncVipOfferDetailsItem): Promise<boolean> {
    syncVipOfferDetailsItem.vipOfferData =  specialOfferData;
    const apiURL = `/vipOfferItems/SyncVipItemsToSalePriceList`;
    return await this.apiService.PostAsync<boolean>(apiURL, syncVipOfferDetailsItem, Constants.mrkApi);
  }
  
}
