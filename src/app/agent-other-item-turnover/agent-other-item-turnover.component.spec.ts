import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentOtherItemTurnoverComponent } from './agent-other-item-turnover.component';

describe('AgentOtherItemTurnoverComponent', () => {
  let component: AgentOtherItemTurnoverComponent;
  let fixture: ComponentFixture<AgentOtherItemTurnoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentOtherItemTurnoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentOtherItemTurnoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
