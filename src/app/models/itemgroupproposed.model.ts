import { BaseDomain } from "./base-domain.model";

export class ItemGroupProposed extends BaseDomain {
    public code: string;
    public nameRO: string;
    public nameENG: string;
    public shortNameRO: string;
    public shortNameENG: string;

    public currencyId: number;
    public customCodeId: number;
    public itemGroupCodeId: number;
    public itemGroupCategoryId: number;

    public clientId: number;
    public supplierId: number;
    public identifiedManufacturerId: number;
    public quantity: number;
    public price: number;
    public dateOfPurchase: Date;
    public observations: string;
    public brandId: number;
}
