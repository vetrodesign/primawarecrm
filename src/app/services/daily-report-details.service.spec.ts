import { TestBed } from '@angular/core/testing';

import { DailyReportDetailsService } from './daily-report-details.service';

describe('DailyReportDetailsService', () => {
  let service: DailyReportDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DailyReportDetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
