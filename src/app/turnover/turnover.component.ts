import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PartnerSearchFilter } from 'app/models/partnerSearchFilter.model';
import { PartnerDropdownSearchComponent } from 'app/partner-dropdown-search/partner-dropdown-search.component';
import { PartnerReviewService } from 'app/services/partner-review.service';
import { PartnerService } from 'app/services/partner.service';
import { TurnoverDetailsComponent } from 'app/turnover-details/turnover-details.component';
import { environment } from 'environments/environment';
import { Constants } from 'app/constants';
import { UsersService } from 'app/services/user.service';
import { AuthService } from 'app/services/auth.service';
import { PartnerLocation } from 'app/models/partnerlocation.model';
import { PartnerLocationScheduleComponent } from 'app/partner-location-schedule/partner-location-schedule.component';
import { DxValidatorComponent } from 'devextreme-angular';
import { DaysOfWeek } from 'app/enums/dayOfTheWeekEnum';
import { PartnerLocationService } from 'app/services/partner-location.service';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import * as moment from 'moment';
import { PartnerContactPreferences } from 'app/models/partner-contact-preferences.model';
import { PartnerContactPreferencesService } from 'app/services/partner-contact-preferences.service';
import { DeliveryOfferPreferences } from 'app/enums/deliveryOfferPreferencesEnum';

@Component({
  selector: 'app-turnover',
  templateUrl: './turnover.component.html',
  styleUrls: ['./turnover.component.css']
})
export class TurnoverComponent implements OnInit {
  
  clientId: number;
  partnerId: number;
  partners: any;
  showPartnerSuggestion: boolean;
  showButtons: boolean;
  isSpecialPriceRequest: boolean;
  partnerSugestionPopup: boolean;
  postId: number;
  showPartnerSchedule: boolean;
  implicitPartnerLocation: PartnerLocation = new PartnerLocation();
  daysOfWeek: { value: number, label: string }[];
  deliveryOfferPreferences: { value: number, label: string }[];
  partnerContactPreferences: PartnerContactPreferences = new PartnerContactPreferences();
  customerId: number;
  
  @ViewChild('partnerDropdownSearch') partnerDropdownSearch: PartnerDropdownSearchComponent;
  @ViewChild('turnoverDetails') turnoverDetailsComponent: TurnoverDetailsComponent;
  @ViewChild('partnerLocationScheduleComponent') partnerLocationScheduleComponent: PartnerLocationScheduleComponent;
  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;

  constructor(private partnerService: PartnerService, private route: ActivatedRoute, private partnerReviewsService: PartnerReviewService, private userService: UsersService, private authService: AuthService,
    private partnerLocationService: PartnerLocationService, private translationService: TranslateService, private partnerContactPreferencesService: PartnerContactPreferencesService, private notificationService: NotificationService,
    private authenticationService: AuthService
  ) {
    this.customerId = Number(this.authenticationService.getUserCustomerId());
   }

  ngOnInit(): void {
    this.partners = {
      paginate: true,
      pageSize: 15,
      store: []
    };

    this.getUserPostId();

    if (this.route.snapshot.queryParamMap.get('clientId') !== undefined && this.route.snapshot.queryParamMap.get('clientId') !== null) {
      const clientId = Number(this.route.snapshot.queryParamMap.get('clientId'));
      this.partnerService.getPartnerByPartnerIdsAsync([clientId]).then(items => {
        this.partners.store.push(...items.filter(x => !this.partners.store.map(y => y.id).includes(x.id)));
        this.partnerId = items[0].id;
        this.setShowButtons();
      });

      this.partnerReviewsService.getPartnerReviewsByPartnerID(clientId).then(reviews => {
        // check if there is at least 1 review created in the last 6 months
        const sixMonthsAgo = new Date();
        sixMonthsAgo.setMonth(sixMonthsAgo.getMonth() - 6);
        this.showPartnerSuggestion = !reviews.some(s => new Date(s.created) > sixMonthsAgo);
      });
    }

    if (this.route.snapshot.queryParamMap.get('isSpecialPriceRequest') !== undefined && this.route.snapshot.queryParamMap.get('isSpecialPriceRequest') !== null) {
      this.isSpecialPriceRequest = Boolean(this.route.snapshot.queryParamMap.get('isSpecialPriceRequest'));
    }
  }

  partnerChange(e) {
    if (e && e.value) {
      this.clientId = e.value;     
      setTimeout(() => {
        this.turnoverDetailsComponent.loadData();
        this.setShowButtons();
      }, 100);
    }
  }

  onPartnerChanged(e) {
    if (e && e.target && e.target.value && e.target.value.length > 4) {
      let searchFilter = new PartnerSearchFilter();
      searchFilter.name = e.target.value;
      searchFilter.isActive = true;
      this.partnerService.getPartnersByFilter(searchFilter).then(items => {
        this.partners.store.push(...items.filter(x => !this.partners.store.map(y => y.id).includes(x.id)));
      })
    }
  }

  getUserPostId() {
    this.userService.getByUsernameOrEmail(this.authService.getUserUserName(), null).then(user => {
      if (user && user.length > 0 ) {
        if (user[0] && user[0].postId) {
          this.postId = user[0].postId;
        }
      }
    });
  }

  getDisplayExprPartners(item) {
    if (!item) {
        return '';
      }
      return item.code + ' - ' + item.name;
  }

  backToPartner() {
    var url = environment.CRMPrimaware + '/' + Constants.partner + '?id=' + this.partnerId;
    window.open(url, '_self');
  }

  setShowButtons() {
    if (this.partnerId) {
      this.showButtons = true;
    } else {
      this.showButtons = false;
    }
  }

  onRefresh() {
    this.turnoverDetailsComponent.loadData();
  }

  openPartnerSugestion() {
    this.partnerSugestionPopup = true;
  }

  async openPartnerSchedule() {
    if (this.clientId) {
      
      this.daysOfWeek = Object.keys(DaysOfWeek)
      .filter(key => isNaN(Number(key)))
      .map(key => ({
        value: DaysOfWeek[key as keyof typeof DaysOfWeek],
        label: this.translationService.instant(key)
      }));

      this.deliveryOfferPreferences = Object.keys(DeliveryOfferPreferences)
      .filter(key => isNaN(Number(key)))
      .map(key => ({
        value: DeliveryOfferPreferences[key as keyof typeof DeliveryOfferPreferences],
        label: this.translationService.instant(key)
      }));

      this.showPartnerSchedule = true;

      await this.partnerLocationService.getPartnerLocationsIncludingInactiveByPartnerID(this.clientId).then(async locations => {
        this.implicitPartnerLocation = (locations && locations.length > 0) ? (locations.find(f => f.isImplicit) || locations.find(f => f.isActive)) : new PartnerLocation();

        if (this.implicitPartnerLocation && this.implicitPartnerLocation.id) {
          this.partnerLocationScheduleComponent.getPartnerLocationSchedule(this.implicitPartnerLocation.id);  
        }

        await this.partnerContactPreferencesService.getAllByPartnerIdAsync(this.clientId).then(resp => {
          if (resp && resp.length > 0) {
            this.partnerContactPreferences = resp[0];
          }
        })
      });
    }
  }

  customizeDateBoxButtons(e) {
    let items = e.component.option("toolbarItems");
    items.shift();
    e.component.option("toolbarItems", items);
  }

  async onSavePartnerContactAndPreferences() {
    if (this.validationGroup.instance.validate().isValid) {
      await this.partnerLocationScheduleComponent.saveData();   

      this.partnerContactPreferences.partnerId = this.clientId;
      this.partnerContactPreferences.fromTime = this.partnerContactPreferences.fromTime ? moment(this.partnerContactPreferences.fromTime, "HH:mm").format("HH:mm") : null;
      this.partnerContactPreferences.toTime = this.partnerContactPreferences.toTime ? moment(this.partnerContactPreferences.toTime, "HH:mm").format("HH:mm") : null;

      await this.partnerContactPreferencesService.upsertAsync(this.partnerContactPreferences).then(resp => {
        if (resp && !resp.isError) {
          this.notificationService.alert('top', 'center', 'Preferinte Contact Partener - Datele au fost salvate cu succes!', NotificationTypeEnum.Green, true);
          this.showPartnerSchedule = false;
        } else {
          this.notificationService.alert('top', 'center', 'Preferinte Contact Partener - Datele nu au fost salvate! A aparut o eroare!', NotificationTypeEnum.Red, true)
        }
      })
    }
  }
}
