import { Component, OnInit, ViewChild, AfterViewInit, Input, OnChanges } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { TranslateService } from 'app/services/translate';
import { Department } from 'app/models/department.model';
import { LogException } from 'app/models/logexception.model';
import { LogService } from 'app/services/log.service';
import { CustomersService } from 'app/services/customers.service';
import { Customer } from 'app/models/customer.model';
import { Constants } from 'app/constants';
import { PageActionService } from 'app/services/page-action.service';

@Component({
  selector: 'app-exception-log',
  templateUrl: './exception-log.component.html',
  styleUrls: ['./exception-log.component.css']
})
export class ExceptionLogComponent implements OnInit, AfterViewInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;

  actions: IPageActions;
  logs: LogException[];
  customers: Customer[];
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  isOwner: boolean = false;
  customerId: string;
  departments: Department[];
  groupedText: string;
  constructor(
    private translationService: TranslateService, private logService: LogService, private customersService: CustomersService,
    private pageActionService: PageActionService) {
    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();
    this.getCustomers();
    this.getLogs();
  }

  ngOnInit(): void {
  }

  canExport() {
    return this.actions.CanExport;
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  getCustomers() {
    this.customersService.getAllCustomersAsync().then(customers => {
      this.customers = customers;
    });
  }

  getLogs() {
    this.logs = [];
    this.logService.getAllLogsAsync().then(items => {
      this.logs = items;
    });
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.logs).then(result => {
      this.actions = result;
    });
  }
  public addRole() {
    this.dataGrid.instance.addRow();
  }

  public refreshDataGrid() {
    this.getLogs();
    this.dataGrid.instance.refresh();
  }

  public async openDetails(row: any) {
    this.dataGrid.instance.editRow(row.rowIndex);
    this.rowIndex = row.rowIndex;

  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }
}
