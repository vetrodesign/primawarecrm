import { BaseDomain } from "./base-domain.model";

export class VipOfferItemsDetails extends BaseDomain {
    vipOfferId: number;
    itemId: number;
    productConvention: number;
    currencyId: number;
    baxValue: number;

    salePriceListRONId: number;
    salePriceListEURId: number;
    salePriceListUSDId: number;
    salePriceListBGNId: number;
    salePriceListHUFId: number;

    Observations: String;
    price: number;
    priceInList: number;
    diffrenceFromMax: string;
}
