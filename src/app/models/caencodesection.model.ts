export class CaenCodeSection {
    public id: number;
    public code: string;
    public description: string;
    public caenCodeCategoryId: number;
}