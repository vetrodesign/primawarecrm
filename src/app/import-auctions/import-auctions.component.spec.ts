import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportAuctionsComponent } from './import-auctions.component';

describe('ImportAuctionsComponent', () => {
  let component: ImportAuctionsComponent;
  let fixture: ComponentFixture<ImportAuctionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportAuctionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportAuctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
