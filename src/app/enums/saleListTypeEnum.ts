export enum SaleListTypeEnum {
    Implicit = 1,
    Predetermined = 2,
}