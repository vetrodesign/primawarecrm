import { BaseDomain } from "./base-domain.model";

export class SpecialPriceRequest extends BaseDomain {
    postId: number;
    partnerId: number;
    endureTransportId: number;
    transportCityId?: number;
    cityNemoDistance?: string;
    orderTypeId?: number;
    deliveryTypeId?: number;
    paymentInstrumentId: number;
    paymentTerm: string;
    deliveryTerm: string;
    clientRequestInfo: string;
    clientRequestComments: string;
    isActive: boolean;
}

export class ValidateSpeciaPriceRequestItemData {
    specialPriceRequestItem: SpecialPriceRequestItem;
}

export class SpecialPriceRequestItem extends BaseDomain {
    specialPriceRequestId: number;
    itemId: number;
    quantity: number;
    baxValue?: number;
    d0?: number;
    c0?: number;
    r0?: number;
    discountGridCmp?: number;
    discountGridRequestedCmp?: number;
    stock?: number;
    turnOverRs: string;
    priceOfferedWithoutTva: number;
    priceRequestedWithoutTva: number;
    priceApproved?: number;
    approvedPaymentConditions?: string;
    competingPartnerId?: number;
    approvedDiscountGridCmp?: string;
    isRejected?: boolean;
    isSentToSalesAgent: boolean;
    salePriceListId?: number;
    createdFormated: string;
    discountGridCmpComputed: string;
    discountGridCmpRequestedComputed: string;
    baxValueComputed: string;
    approvingUser: string;
    d0Andc0: string;
    approvedQuantity: number;
    turnoverRsComputed: string;
    currency: any;
    itemGrossWeight: number;
    itemVolume: number;

    auxDiscountGrid: number;
    auxCmp: number;
    auxDiscountGridRequested: number;
    auxCmpRequested: number;
    auxDiscountGridApproved: number;
    auxCmpRequestedApproved: number;
    salePriceListCode: string;
    isStockAvailable: boolean;
    isAtOrder: boolean;
}

export class SpecialPriceRequestItemData {
    specialPriceRequestItemsVMs: SpecialPriceRequestItem[];
    headerInfo: SpecialPriceRequestHeaderInfo;
}

export class SpecialPriceRequestHeaderInfo {
    partnerId: number;
    partnerCode: string;
    partnerName: string;
    partnerType: string;
    partnerTradeRegisterNumber: string;
    partnerCountry: string;
    partnerCounty: string;
    partnerCity: string;
    partnerCaLastYear: string;
    partnerNaLastYear: string;
    partnerVdCurrentYear: any;
    endureTransport: string;
    transportCity: string;
    cityNemoDistance: string;
    orderType: string;
    deliveryType: string;
    paymentInstrument: string;
    paymentTerm: string;
    deliveryTerm: string;
    clientRequestInfo: string;
    clientRequestComments: string;
    postName: string;
    requestItemsRowsData: RequestItemsRowsData[];
}

export class RequestItemsRowsData {
    itemName: string;
    quantity: any;
    priceOffered: any;
    priceRequested: any;
    competingPartner: string;
}

export class ValidatedSpecialPriceRequestItems {
    itemId: number;
    itemName: string;
    quantity: any;
    baxValue: any;
    d0: any;
    c0: any;
    priceOffered: any;
    discountGridCmp: any;
    discountGridRequestedCmp: any;
    priceRequested: any;
    competingPartner: string;
    stockDragan01: any;
    turnOverRs: string;
    priceApproved: any;
    approvedPaymentConditions: string;
    approvedDiscountGridCmp: any;
    isRejected?: boolean;
    created: Date;
    createdBy: number;
    approvingUser: string;
    approvedQuantity: any;
    specialPriceRequestId: number;
    clientRequestComments: string;
    cmp: number;
    specialPriceRequestItemId: number;
    originalBaxValue: number;
    isAtOrder: boolean;
    currencyName: string;
}

export class UpdateAndValidateSpecialPriceRequestItems {
    specialPriceRequestItemsVMs: SpecialPriceRequestItem[];
    validatedSpecialPriceRequestItems: ValidatedSpecialPriceRequestItems[];
}

export class PriceApprovedTooltipDataRequest {
    itemIds: number[];
    partnerId: number;
    shouldGetHistory?: boolean;
}
