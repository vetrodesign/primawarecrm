import { Injectable } from '@angular/core';
import { ImportExportDataSalePriceList } from 'app/models/importexportdatasalepricelist.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ImportExportDataSalePriceListService {

  constructor(private apiService: ApiService) { }

  async getAll(): Promise<any[]> {
    const apiURL = `/ImportExportDataSalePriceList/GetAll`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.crmApi);
  }

  async createAsync(items: ImportExportDataSalePriceList[]): Promise<any> {
    const apiURL = `/ImportExportDataSalePriceList/Create`;
    return await this.apiService.PostAsync<any>(apiURL, items, Constants.crmApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/ImportExportDataSalePriceList/Delete`;
    return await this.apiService.PostAsync<any>(apiURL, ids, Constants.crmApi);
  }
}

