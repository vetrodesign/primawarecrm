import { Component, OnInit } from '@angular/core';
import { Language } from 'app/models/language.model';
import { TranslateService } from 'app/services/translate';
import { Constants } from 'app/constants';
import { MenuService } from 'app/services/menu/menu.service';
import { Router } from '@angular/router';
import { AuthService } from 'app/services/auth.service';
import { HelperService } from 'app/services/helper.service';
import { environment } from 'environments/environment';

declare const $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  languages: Language[];
  selectedLan: string;
  shouldShowProfile: boolean;
  currentUser: string;
  notifications: string[];

  constructor(private translateService: TranslateService, private menuService: MenuService,
    private router: Router, private authenticationService: AuthService, private helperService: HelperService) {
    this.menuService.menuItems.subscribe(items => {
      this.menuItems = items;
    });
    this.notifications = [];
    this.menuService.subject.subscribe(n => {
      if (n) {
        this.notifications = n;
      }
    })
    this.languages = this.translateService.getLangs();
    const langCode = JSON.parse(localStorage.getItem(Constants.clientLang));
    this.selectedLan = langCode ?
      this.translateService.instant(this.languages.find(l => l.code === langCode).name)
      : this.translateService.instant('english');
  }

  ngOnInit() {
  }

  toggleShowProfile() {
    if (this.helperService.getBrowserName() !== 'chrome') {
      this.shouldShowProfile = !this.shouldShowProfile;
    }
  }

  logout() {
    this.toggleShowProfile();
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  isMobileMenu() {
    if (window.innerWidth > 991) {
      return false;
    }
    return true;
  };
  selectCulture(lang: Language) {
    this.translateService.use(lang);
    this.selectedLan = this.translateService.instant(lang.name);
  }

  goToSSO() {
    this.authenticationService.removeToken();
    window.location.href = environment.SSOPrimaware;
  }
}
