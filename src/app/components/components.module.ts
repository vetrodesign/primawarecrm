import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DxButtonModule, DxDataGridModule } from 'devextreme-angular';
import { SidebarNavItemComponent } from './sidebar-nav-item/sidebar-nav-item.component';
import { TranslateModule } from 'app/services/translate/translate.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    FormsModule
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    SidebarNavItemComponent
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    SidebarNavItemComponent,
  ],
  providers: []
})
export class ComponentsModule { }
