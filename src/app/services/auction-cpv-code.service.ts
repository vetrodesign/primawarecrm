import { Injectable } from '@angular/core';
import { AuctionXCpvCode } from 'app/models/auction-cpv-code.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class AuctionCpvCodeService {

  constructor(private apiService: ApiService) { }

  async getAllAuctionCpvCodes(): Promise<any[]> {
    const apiURL = `/AuctionXCpvCode/GetAll`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.mrkApi);
  }

  async getAllAuctionCpvCodesByAuctionIdAsync(auctionId: number): Promise<any[]> {
    const apiURL = `/AuctionXCpvCode/GetAllByAuctionId`;
    return await this.apiService.PostAsync<any>(apiURL, auctionId, Constants.mrkApi);
  }

  async createMultipleAuctionCpvCodeAsync(i: AuctionXCpvCode[]): Promise<any> {
    const apiURL = `/AuctionXCpvCode/CreateMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }

  async deleteAuctionCpvCodeAsync(i: AuctionXCpvCode[]): Promise<any> {
    const apiURL = `/AuctionXCpvCode/DeleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }
}
