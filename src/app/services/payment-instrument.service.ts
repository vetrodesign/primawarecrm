import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PaymentInstrument } from 'app/models/paymentinstrument.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PaymentInstrumentService {

  constructor(private apiService: ApiService) { }

  async getAllPaymentInstrumentsAsync(): Promise<any[]> {
    const apiURL = `/paymentinstrument/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.commonApi);
  }

  async getAllPaymentInstrumentsByCustomerIdAsync(): Promise<PaymentInstrument[]> {
    const apiURL = `/paymentinstrument/getallbycustomer`;
    return await this.apiService.GetAsync<PaymentInstrument[]>(apiURL, Constants.commonApi);
  }

  async createPaymentInstrumentAsync(paymentInstrument: PaymentInstrument): Promise<any> {
    const apiURL = `/paymentinstrument/create`;
    return await this.apiService.PostAsync<any>(apiURL, paymentInstrument, Constants.commonApi);
  }

  async updatePaymentInstrumentAsync(paymentInstrument: PaymentInstrument): Promise<any> {
    const apiURL = `/paymentinstrument/update`;
    return await this.apiService.PutAsync<any>(apiURL, paymentInstrument, Constants.commonApi);
  }

  async deletePaymentInstrumentAsync(ids: number[]): Promise<any> {
    const apiURL = `/paymentinstrument/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
