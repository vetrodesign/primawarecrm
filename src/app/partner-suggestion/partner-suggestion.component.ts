import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import * as _ from 'lodash';
import { PartnerReview } from 'app/models/partnerreview.model';
import { PartnerSuggestionService } from 'app/services/partner-suggestion.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { PartnerSuggestionStateEnum } from 'app/enums/partnerSuggestionState.enum';
import { PartnerLocationContactService } from 'app/services/partner-location-contact.service';
import { PartnerLocationContact } from 'app/models/partnerlocationcontact.model';
import { PartnerReviewQuestionService } from 'app/services/partner-review-question.service';
import { PartnerReviewQuestion } from 'app/models/partner-review-question.model';

@Component({
  selector: 'app-partner-suggestion',
  templateUrl: './partner-suggestion.component.html',
  styleUrls: ['./partner-suggestion.component.css']
})
export class PartnerSuggestionComponent implements OnInit {
  @Input() postId: number;
  @Input() clientId: number;
  @Input() customerId: number;
  @Input() partnerLocationContactId: number;

  @Output() hidePartnerSuggestionPopupChange: EventEmitter<any> = new EventEmitter<any>();

  loaded: boolean;
  groupedText: string;
  showPartnerSuggestionSaveBtn: boolean;
  partnerSuggestion: PartnerReview = new PartnerReview();
  shouldHidePartnerSuggestionComponent: boolean;
  locationContactPersons: PartnerLocationContact[];
  locationContactId: number;
  partnerReviewQuestions: PartnerReviewQuestion[] = [];

  constructor(
    private partnerSuggestionService: PartnerSuggestionService, 
    private partnerReviewQuestionsService: PartnerReviewQuestionService,
    private translationService: TranslateService,
    private notificationService: NotificationService,
    private partnerLocationContactService: PartnerLocationContactService) {
      
    this.groupedText = this.translationService.instant('groupedText');
  }

  async ngOnInit(): Promise<void> {
    await this.getData();
   }
 
  async getData() {
    Promise.all([this.getPartnerContact(), this.getReviewQuestions()]).then(() => {
    })
  }

  async getReviewQuestions() {
    await this.partnerReviewQuestionsService.getAllPartnerReviewQuestionsAsync().then(reviews => {
      this.partnerReviewQuestions = (reviews && reviews.length > 0) ? reviews.filter(x => x.isActive == true): [];
    });
  }

  public onTextAreaValueChanged(event) {
    this.showPartnerSuggestionSaveBtn = (event.value && event.value.trim().length >= 20);
  }

  public async savePartnerSuggestion(partnerSuggestionState: string) {
    this.partnerSuggestion.partnerId = this.clientId;
    this.partnerSuggestion.postId = this.postId;
    this.partnerSuggestion.partnerLocationContactId = this.locationContactId;

    this.partnerSuggestion.description = this.partnerReviewQuestions
    .map(q => q.response ? `${q.question} ~~ ${q.response}` : q.question)
    .join(' ||| ');

    switch (partnerSuggestionState) {
      case 'save':
        if (!this.locationContactId) {
          this.notificationService.alert('top', 'center', 'Sugestie Partener - Selectati persoana de contact!', NotificationTypeEnum.Red, true);
          return;
        }
        this.partnerSuggestion.partnerSuggestionState = PartnerSuggestionStateEnum.Save;
        break;

      case 'notInterested':
        if (!this.locationContactId) {
          this.notificationService.alert('top', 'center', 'Sugestie Partener - Selectati persoana de contact!', NotificationTypeEnum.Red, true);
          return;
        }

        this.partnerSuggestion.description = "";
        this.partnerSuggestion.partnerSuggestionState = PartnerSuggestionStateEnum.NotInterested;
        break;

      case 'postpone':
        this.partnerSuggestion.description = "";
        this.partnerSuggestion.partnerSuggestionState = PartnerSuggestionStateEnum.Postpone;
        break;

      default:
        break;
    }

    this.loaded = true;
    await this.partnerSuggestionService.createPartnerSuggestion(this.partnerSuggestion).then(r => {
      if (r) {
        this.notificationService.alert('top', 'center', 'Sugestie Partener - Datele au fost salvate cu succes!',
          NotificationTypeEnum.Green, true)
        this.hidePartnerSuggestionPopupChange.emit(true);
      } else {
        this.notificationService.alert('top', 'center', 'Sugestie Partener - Datele nu au fost salvata! A aparut o eroare!',
          NotificationTypeEnum.Red, true)
        this.hidePartnerSuggestionPopupChange.emit(false);
      }

      this.loaded = false;
    });
  }

  async getPartnerContact(): Promise<void> {
    await this.partnerLocationContactService.getPartnerLocationContactByPartnerID(this.clientId).then(items => {
      if (items && items.length > 0) {
        this.locationContactPersons = items;
      } else {
        this.locationContactPersons = null;
      }
    });
  }

  getDisplayExprContact(item) {
    if (!item) {
      return '';
    }

    return  item.phoneNumber + '  ' + item.firstName + '  ' + item.lastName ;
  }
}

