import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PartnerObservationsComponent } from 'app/partner-observations/partner-observations.component';

@Component({
  selector: 'app-partner-observations-details',
  templateUrl: './partner-observations-details.component.html',
  styleUrls: ['./partner-observations-details.component.css']
})
export class PartnerObservationsDetailsComponent implements OnInit, AfterViewInit {
  clientId: number;
  partnerName: string;
  @ViewChild('partnerObservationsComponent') partnerObservationsComponent: PartnerObservationsComponent;

  constructor( private route: ActivatedRoute) { }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      if (this.route.snapshot.queryParamMap.get('clientId') !== undefined && this.route.snapshot.queryParamMap.get('clientId') !== null) {
        const clientId = Number(this.route.snapshot.queryParamMap.get('clientId'));
        this.partnerObservationsComponent.loadPartnerObservationDetailsData(clientId);
      }
    })

    setTimeout(() => {
      if (this.route.snapshot.queryParamMap.get('partnerName') !== undefined && this.route.snapshot.queryParamMap.get('partnerName') !== null) {
        this.partnerName = this.route.snapshot.queryParamMap.get('partnerName');
      }
    });
  }
}
