export class DeliveryConditions {
    id: string;
    code: string;
    name: string;
    description: string;
}