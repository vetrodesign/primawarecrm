import { CaenCodeSpecializationXPartnerLocation } from "./caencodespecializationxpartner.model";

export class CustomerInterestsReport {
    partnerName: string;
    partnerContactName: string;
    partnerContactOccupation: number;
    partnerContactEmail: string;
    partnerContactPhone: string;
    partnerLastObservationDescription: string;
    
    sgf: boolean = false;
    cda: boolean = false;
    szo: boolean = false;
    alt: boolean = false;

    specializationIds: number[];
}