import { BaseDomain } from "./base-domain.model";

export class ClientTurnover extends BaseDomain {
    itemId: number;
    itemCode: string;
    itemName: string;
    listPrice: number;
    currentStock: number;
    cantBaxArticol: number;


    l12: number;
    l11: number;
    l10: number;
    l9: number;
    l8: number;
    l7: number;
    l6: number;
    l5: number;
    l4: number;
    l3: number;
    l2: number;
    l1: number;
    lc: number;

    lastComment: PartnerXItemComment;
    itemComments: PartnerXItemComment[];
}

export class PartnerXItemComment extends BaseDomain {
    partnerId: number;
    bestRankingItemId: number;
    postId: number;
    itemId: number;
    commentDate: Date;
    comment: string;
    section: number;
    partnerItemStateId: number;
    monthlyOrderedQuantity: number;
    brandId: string;
    supplierId: string;
    price: number;
    hasStockFromMe: boolean;
    callBackDate: Date;

    isInvoiced: boolean;
    notUsed: boolean;
    hasStock: boolean;
    atOrder: boolean;
    shoppingCartQuantity: number;
}

export class DailyWhatsappOffersEmail {
    selectedOffers: any;
    emailSubject: string;
    emailBody: string;
    personContactId: number;
    email: string;
    postId: number;
}






