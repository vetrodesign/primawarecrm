import { BaseDomain } from "./base-domain.model";

export class ContactCampaign extends BaseDomain {
    code: string;
    name: string;
    customerId: number;
    type: number;
    description: string;
    isActive: boolean;
    startDate: Date;
    endDate: Date;
    observationsToStop: string;
    agentMessage: string;
}

export class ContactCampaignHappyBirthDay extends ContactCampaign {
    DaysBefore: number;
    DaysAfter: number;
}

export class ContactCampaignLocation extends ContactCampaign {
    locationType: number;
    geographicalAreasIds: number[];
}