import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerObservationsComponent } from './partner-observations.component';

describe('PartnerObservationsComponent', () => {
  let component: PartnerObservationsComponent;
  let fixture: ComponentFixture<PartnerObservationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerObservationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerObservationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
