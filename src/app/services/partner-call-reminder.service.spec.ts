import { TestBed } from '@angular/core/testing';

import { PartnerCallReminderService } from './partner-call-reminder.service';

describe('PartnerCallReminderService', () => {
  let service: PartnerCallReminderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerCallReminderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
