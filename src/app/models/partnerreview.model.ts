import { BaseDomain } from "./base-domain.model";

export class PartnerReview extends BaseDomain
{
    public description: string;
    public postId: number;
    public partnerId: number;
    public partnerLocationContactId: number;
    public partnerSuggestionState: number;

    public response1: string;
    public response2: string;
    public response3: string;
    public response4: string;
    public response5: string;
}