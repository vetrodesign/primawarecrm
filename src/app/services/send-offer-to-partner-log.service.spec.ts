import { TestBed } from '@angular/core/testing';

import { SendOfferToPartnerLogService } from './send-offer-to-partner-log.service';

describe('SendOfferToPartnerLogService', () => {
  let service: SendOfferToPartnerLogService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SendOfferToPartnerLogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
