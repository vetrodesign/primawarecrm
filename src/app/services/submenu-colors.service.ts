import { Injectable} from "@angular/core";
import { MenuService } from "./menu/menu.service";

@Injectable({
    providedIn: 'root'
})
export class SubmenuColours {

    // #region Fields

    colours: any = {
        blue: '#1d4ed8',
        purple: '#7e22ce',
        green: '#15803d'
    }
    modules: any;

    // #endregion

    constructor(private menuService: MenuService) {
        this.getModules();
    }

    // #region Public Methods

    getColour(id: number) {
        let level = this.getLevel(id);

        switch (level) {
            case 1:
                return this.colours.blue;
            case 2:
                return this.colours.purple;
            case 3:
                return this.colours.green;
        }
    }

    // #endregion

    // #region Private Methods

    private getLevel(id: number) {
        let level = 0;
        let idToSearch = id;

        while (true) {
            let menu = this.getMenu(idToSearch);

            if (menu.subMenuId === 0 || level === 10) {
                break;
            } else {
                level += 1;
                idToSearch = menu.subMenuId;
            }
        }

        return level;
    }

    private getMenu(id: number) {
        const stack = [this.modules];
        while (stack?.length > 0) {
          const currentObj = stack.pop();
          
          if (currentObj?.id === id) {
            return currentObj;
          }
    
          Object.keys(currentObj).forEach(key => {
            if (typeof currentObj[key] === 'object' && currentObj[key] !== null) {
              stack.push(currentObj[key]);
            }
          });
        }
    }

    private getModules() {
        this.menuService.menuItems.subscribe(r => {
            if (r) {
                this.modules = r;
            }
        })
    }

    // #endregion
}