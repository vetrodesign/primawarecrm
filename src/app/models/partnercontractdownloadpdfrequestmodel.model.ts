export class PartnerContractDownloadPDFRequestModel {
    pdfData: any;
    documentTemplateName: string;
}