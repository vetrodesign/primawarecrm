import { Injectable } from '@angular/core';
import { User } from 'app/models/user.model';
import { ClientModule } from 'app/models/clientmodule.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable()
export class UsersService {
users: User[];
clientModules: ClientModule[];
  constructor(private apiService: ApiService) { }

  async getByPostIds(postIds): Promise<User[]> {
    const apiURL = `/user/getByPostIds/`;
    return await this.apiService.PostAsync<any>(apiURL, postIds, Constants.ssoApi);
  }

  async sendEmailAsync(email, userName): Promise<any> {
    const apiURL = `/user/sendemail/` + email + '/' + userName;
    return await this.apiService.GetAsync<any>(apiURL, Constants.ssoApi);
  }

  async getUsersAsync(): Promise<User[]> {
    const apiURL = `/user/getall`;
    return await this.apiService.GetAsync<User[]>(apiURL, Constants.ssoApi);
  }

  async getByUsernameOrEmail(userName, email): Promise<User[]> {
    const apiURL = `/user/getbyusername/` + userName + '/' + email;
    return await this.apiService.GetAsync<User[]>(apiURL, Constants.ssoApi);
  }

  async getActiveByUserName(userName): Promise<User[]> {
    const apiURL = `/user/getactivebyusername/` + userName;
    return await this.apiService.GetAsync<User[]>(apiURL, Constants.ssoApi);
  }

  async getUserAsyncByID(): Promise<User[]> {
    const apiURL = `/user/getuserbycustomerid`;
    return await this.apiService.GetAsync<User[]>(apiURL, Constants.ssoApi);
  }

  async getUsersAsyncByRoleID(roleId): Promise<User[]> {
    const apiURL = `/user/getbyroleid/` + roleId;
    return await this.apiService.GetAsync<User[]>(apiURL, Constants.ssoApi);
  }

  async createUserAsync(user: any): Promise<any> {
    const apiURL = `/user`;
    return await this.apiService.PostAsync<any>(apiURL, user, Constants.ssoApi);
  }

  async updateUserAsync(user: User): Promise<any> {
    const apiURL = `/user/` + user.id;
    return await this.apiService.PutAsync<any>(apiURL, user, Constants.ssoApi);
  }

  async deleteUserAsync(id: number): Promise<any> {
    const apiURL = `/user/` + id;
    return await this.apiService.DeleteSingleAsync<any>(apiURL, Constants.ssoApi);
  }
}
