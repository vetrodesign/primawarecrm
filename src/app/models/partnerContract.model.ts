import { BaseDomain } from "./base-domain.model";
import { PartnerContractXSalePriceList } from "./partnerContractXSalePriceList.model";

export class PartnerContract extends BaseDomain {
    id: number;
    created: Date;
    isActive: boolean;

    partnerTypeId: number;
    partnerId: number;
    partnerLocationId: number;
    discountGridId: number;
    salePriceListId: number;
    ownerContractNumber: number;
    contractNumber: string;
    departmentId: number;
    paymentInstrumentId: number;
    paymentTermId: number;
    saleListTypeId: number;
    validTo: Date;
    credit: number;
    contractSupportTypeId: number;
    electronicLink: string;

    partnerName: string;
    isForever: boolean = false;
}
