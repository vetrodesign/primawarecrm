export class PartnerItemState {
    id: number;
    codeId: number;
    code: string;
    name: string;
}