import { TestBed } from '@angular/core/testing';

import { CaenCodeDomainService } from './caen-code-domain.service';

describe('CaenCodeDomainService', () => {
  let service: CaenCodeDomainService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CaenCodeDomainService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
