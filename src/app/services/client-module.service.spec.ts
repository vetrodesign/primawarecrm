import { TestBed } from '@angular/core/testing';

import { ClientModuleService } from './client-module.service';

describe('ClientModuleService', () => {
  let service: ClientModuleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClientModuleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
