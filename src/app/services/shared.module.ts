import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersService } from './user.service';
import { ApiService } from './api.service';
import { AuthService } from './auth.service';
import { JwtHelper } from 'angular2-jwt';
import { HelperService } from './helper.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorInterceptor } from 'app/helpers/error.interceptor';
import { NotificationService } from './notification.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from './translate/translate.module';
import { TranslateService } from './translate/translate.service';
import { TRANSLATION_PROVIDERS } from './translate';
import { RolesService } from './roles.service';
import { CustomersService } from './customers.service';
import { ClientModuleService } from './client-module.service';
import { NotificationsComponent } from 'app/notifications/notifications.component';
import { ClientModuleMenuItemsService } from './client-module-menu-items.service';
import { RoleRightsService } from './role-rights.service';
import { PostService } from './post.service';
import { DepartmentsService } from './departments.service';
import { LanguageService } from './language.service';
import { CountryService } from './country.service';
import { CountyService } from './county.service';
import { PartnerService } from './partner.service';
import { CurrencyRateService } from './currency-rate.service';
import { MeasurementUnitService } from './measurement-unit.service';
import { CountryGroupService } from './country-group.service';
import { CountryGroupXcountryService } from './country-group-xcountry.service';
import { SetLanguageService } from './set-language.service';
import { PaymentInstrumentService } from './payment-instrument.service';
import { SupplierPriceListService } from './supplier-price-list.service';
import { CityService } from './city.service';
import { CaenCodeService } from './caen-code.service';
import { SupplierPriceListItemService } from './supplier-price-list-item.service';
import { PageActionService } from './page-action.service';
import { PartnerLegalFormService } from './partner-legal-form.service';
import { SalesZoneService } from './sales-zone.service';
import { CaenCodeSectionService } from './caen-code-section.service';
import { CaenCodeCategoryService } from './caen-code-category.service';
import { CaenCodeDomainService } from './caen-code-domain.service';
import { CaenCodeSpecializationService } from './caen-code-specialization.service';
import { PartnerLocationContactService } from './partner-location-contact.service';
import { PartnerActivityService } from './partner-activity.service';
import { PartnerActivityAllocationService } from './partner-activity-allocation.service';
import { CaenCodeSpecializationPartnerLocationService } from './caen-code-specialization-partner-location.service';
import { PartnerClientCreditControlService } from './partner-client-credit-control.service';
import { PartnerSupplierCreditControlService } from './partner-supplier-credit-control.service';
import { SalePriceListItemService } from './sale-price-list-item.service';
import { SalePriceListService } from './sale-price-list.service';
import { PartnerLocationSalePriceListService } from './partner-location-sale-price-list.service';
import { PartnerReviewService } from './partner-review.service';
import { SalePriceListProductConventionService } from './sale-price-list-product-convention.service';
import { PartnerFinancialInfoService } from './partner-financial-info.service';
import { PartnerCipCheckService } from './partner-cip-check.service';
import { OrderService } from './order.service';
import { SiteService } from './site.service';
import { ItemService } from './item.service';
import { ManagementService } from './management.service';
import { ItemTypeService } from './item-type.service';
import { DeliveryConditionsService } from './delivery-conditions.service';
import { TagService } from './tag.service';
import { PartnerTagService } from './partner-tag.service';
import { ItemGroupCategoryService } from './item-group-category.service';
import { PartnerLocationItemGroupCategoryService } from './partner-location-item-group-category.service';
import { AuctionService } from './auction.service';
import { AuctionTypeService } from './auction-type.service';
import { AuctionGroupService } from './auction-group.service';
import { AuctionGroupCpvCodeService } from './auction-group-cpv-code.service';
import { ItemGroupCodeService } from './item-group-code.service';
import { ItemGroupCategoryCodeService } from './item-group-category-code.service';
import { PartnerLocationItemGroupCodeService } from './partner-location-item-group-code.service';
import { DailyReportService } from './daily-report.service';
import { SalesTargetService } from './sales-target.service';
import { SalesTargetPostService } from './sales-target-post.service';
import { SalesGroupTargetService } from './sales-group-target.service';
import { DailyReportDetailsService } from './daily-report-details.service';
import { PartnerNameEquivalenceService } from './partner-name-equivalence.service';
import { OccupationService } from './occupation.service';
import { ImportExportDataSalePriceListService } from './import-export-data-sale-price-list.service';
import { PartnerContract } from 'app/models/partnerContract.model';
import { PartnerContractXSalePriceList } from 'app/models/partnerContractXSalePriceList.model';
import { OfficeItemsTurnoverService } from './office-items-turnover.service';
import { MandatoryPromotionTurnoverService } from './mandatory-promotion-turnover.service';
import { SendOfferToPartnerLogService } from './send-offer-to-partner-log.service';
import { TurnoverPreorderService } from './turnover-preorder.service';
import { ItemRelatedService } from './item-related.service';
import { BrandService } from './brand.service';
import { LiquidationOfferTurnoverService } from './liquidation-offer-turnover.service';
import { PartnerWebsiteService } from './partner-website.service';
import { ItemGroupProposedService } from './item-group-proposed.service';
import { SpecialOfferService } from './special-offer.service';
import { PartnerXItemGroupCodeService } from './partner-x-item-group-code.service';
import { PartnerAuthorizationService } from './partner-authorization.service';
import { AuthorizationTypeService } from './authorization-type.service';

@NgModule({
  imports: [ CommonModule, FormsModule, ReactiveFormsModule, TranslateModule],
  declarations: [ NotificationsComponent],
  providers: [UsersService, ApiService, AuthService, DepartmentsService, LanguageService,
     SupplierPriceListItemService, ItemService,
     JwtHelper, HelperService, TranslateService, TRANSLATION_PROVIDERS, RolesService, CustomersService,
     CurrencyRateService, MeasurementUnitService, PaymentInstrumentService, ItemTypeService,
     CountryService, ClientModuleService, NotificationService, ClientModuleMenuItemsService, RoleRightsService,
     PostService, CountyService, PartnerService, PartnerFinancialInfoService, PartnerCipCheckService,
     CountryGroupService, CountryGroupXcountryService, SetLanguageService, SiteService, ManagementService,
     SupplierPriceListService, CityService, CaenCodeService, PageActionService, PartnerLegalFormService,
     SalesZoneService, CaenCodeSectionService, CaenCodeCategoryService, CaenCodeDomainService, AuctionGroupService, ItemRelatedService, BrandService,
     CaenCodeSpecializationService, CaenCodeSpecializationPartnerLocationService, OrderService, AuctionTypeService, OccupationService,
     PartnerLocationContactService, PartnerClientCreditControlService, PartnerSupplierCreditControlService, ItemGroupCategoryCodeService, TurnoverPreorderService,
     PartnerActivityService, PartnerActivityAllocationService, SalePriceListItemService, PartnerLocationItemGroupCategoryService, PartnerLocationItemGroupCodeService,
     SalePriceListService, PartnerLocationSalePriceListService, PartnerReviewService, ItemGroupCategoryService, AuctionGroupCpvCodeService, DailyReportService,
     SalePriceListProductConventionService, DeliveryConditionsService, TagService, PartnerTagService, AuctionService, ItemGroupCodeService, SalesTargetService,
     SalesTargetPostService, SalesGroupTargetService, DailyReportDetailsService, PartnerNameEquivalenceService, ImportExportDataSalePriceListService, PartnerContract,
     PartnerContractXSalePriceList, SendOfferToPartnerLogService, OfficeItemsTurnoverService, MandatoryPromotionTurnoverService, LiquidationOfferTurnoverService, PartnerWebsiteService, ItemGroupProposedService, SpecialOfferService,
     PartnerXItemGroupCodeService, PartnerAuthorizationService, AuthorizationTypeService,
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ],
  exports: [NotificationsComponent]
})
export class SharedModule { }
