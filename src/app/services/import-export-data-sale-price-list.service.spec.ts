import { TestBed } from '@angular/core/testing';

import { ImportExportDataSalePriceListService } from './import-export-data-sale-price-list.service';

describe('ImportExportDataSalePriceListService', () => {
  let service: ImportExportDataSalePriceListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImportExportDataSalePriceListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
