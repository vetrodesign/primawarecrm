export class CustomCode {
    public id: number;
    public cnKey: string;
    public cn: string;
    public nameRO: string;
    public nameEN: string;
    public dashes: string;
    public nbdashes: string;
    public su: string;
    public upd: string;

    public getPropertiesArray(): string[] {
        return ['cnKey', 'cn', 'nameRO', 'nameEN', 'dashes', 'nbdashes', 'su', 'upd'];
    }
}