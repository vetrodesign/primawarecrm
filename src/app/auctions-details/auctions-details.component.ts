import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AuctionReviewsComponent } from 'app/auction-reviews/auction-reviews.component';
import { AuctionParticipationEnum } from 'app/enums/auctionFilterDayEnum';
import { AuctionResultEnum } from 'app/enums/auctionResultTypeEnum';
import { AuctionStatusEnum } from 'app/enums/auctionStatusTypeEnum';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { AuctionXCpvCode } from 'app/models/auction-cpv-code.model';
import { AuctionReviews } from 'app/models/auction-review.model';
import { CpvCode } from 'app/models/cpvcode.model';
import { ImportAuction } from 'app/models/importauction.model';
import { AuctionCpvCodeService } from 'app/services/auction-cpv-code.service';
import { AuctionReviewsService } from 'app/services/auction-reviews.service';
import { AuctionService } from 'app/services/auction.service';
import { AuthService } from 'app/services/auth.service';
import { NotificationService } from 'app/services/notification.service';
import { TranslateService } from 'app/services/translate';
import { DxValidatorComponent } from 'devextreme-angular';

@Component({
  selector: 'app-auctions-details',
  templateUrl: './auctions-details.component.html',
  styleUrls: ['./auctions-details.component.css']
})
export class AuctionsDetailsComponent implements OnInit {
  @Input() selectedAuction: ImportAuction;
  @Input() auctionTypes: any;
  @Input() auctionGroups: any;

  @Input() auctionParticipationTypes: any;
  @Input() auctionStatusTypes: any;
  @Input() auctionResultTypes: any;

  @Input() auctionGroupCpvCodes: any;
  
  @Input() Auctions: any;
  @Input() cpvCodes: any;
  @Input() salePosts: any;
  @Input() allPosts: any;
  @Input() users: any;

  @Output() visible: EventEmitter<any> = new EventEmitter<any>();

  auction: ImportAuction;
  isOnSave: boolean;
  loaded: boolean;
  isToAnalyze: boolean = true;
  intervalPopup: boolean;

  observationPopup: boolean = false;
  observationByCancel: boolean = false;
  observationByDisqualified: boolean = false;

  intervalFrom: number;
  intervalTo: number;

  isTabActive: string;
  cpvText: string;
  detailsTabs: any[] = [];
  observation: string;
  lotsNr: any;

  validationPattern: any = /^[-,0-9]+$/;

  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;
  @ViewChild('secondValidationGroup') secondValidationGroup: DxValidatorComponent;
  @ViewChild('thirdValidationGroup') thirdValidationGroup: DxValidatorComponent;
  @ViewChild('intervalValidationGroup') intervalValidationGroup: DxValidatorComponent;
  @ViewChild('auctionReviews') auctionReviews: AuctionReviewsComponent;
  
  constructor(private authService: AuthService, private auctionService: AuctionService, private notificationService: NotificationService,
    private auctionCpvCodeService: AuctionCpvCodeService, private translationService: TranslateService, private auctionReviewsService: AuctionReviewsService) {
      this.isTabActive = this.authService.getCustomerPageTitleTheme(); 
     }

  ngOnInit(): void {
    this.detailsTabs = [{ 'id': 1, 'text': 'General', 'isActive': true },
    { 'id': 2, 'text': 'Observatii', 'isActive': false }
    ];
  }

  async loadData() {
    setTimeout(() => {
      document.getElementById("title")?.scrollIntoView({behavior: 'smooth'});
    }, 200);
   
    this.auction = new ImportAuction();
    this.lotsNr = 0;

    if (this.selectedAuction) {
      this.isOnSave = false;
      this.auction = this.selectedAuction;

      this.auction.isWithoutDocuments = this.auction.isWithoutDocuments ? this.auction.isWithoutDocuments : false;
      this.auction.hasRequestedPrices = this.auction.hasRequestedPrices ? this.auction.hasRequestedPrices : false;
      this.auction.hasReceivedPrices = this.auction.hasReceivedPrices ? this.auction.hasReceivedPrices : false;

      this.cpvText = null;
      
    } else {

      this.auction.isWithoutDocuments = false;
      this.auction.hasRequestedPrices =  false;
      this.auction.hasReceivedPrices =  false;
      this.auction.isCanceled = false;
      this.isOnSave = true;
    }
  }

  toggleDetailsTab(tab: any) {
    this.detailsTabs.find(x => x.isActive).isActive = false;
    tab.isActive = true;

    if (tab.id == 2) {
      this.auctionReviews.loadData();
    }
  }

    // /*User has admin role or superior post*/
    // public isUserAdminOrHasSuperiorPost(): boolean  {
    //   if (this.authService.isUserAdmin() || this.auction.auctionTypeId == 1) {
    //     return true;
    //   } else {
    //     let post = this.allPosts.find(x => x.id === this.users.find(x => x.id === Number(this.authService.getUserId()))?.postId);
    //     if (post && (post.isDepartmentSuperior || post.isGeneralManager || post.isLeadershipPost || post.isLocationSuperior || post.isOfficeSuperior)) {
    //       return true;
    //     } else {
    //       return false;
    //     }
    //   }
    // }

  mapTexCPV() {
    if (this.cpvText && this.cpvText.length > 0) {
      this.auction.cpvCodeIds = [];
      this.cpvText.split(')').forEach(i => {
        let cpv = i.substring(i.indexOf("(CPV:") + 6);

        if (cpv) {
          let findCpv = this.cpvCodes.store.find(x => x.code.includes(cpv));
          if (findCpv) {
            this.auction.cpvCodeIds.push(findCpv.id);
          }
        }
      });

    this.auctionGroups.forEach(x => {
      x.countCPVs = 0
      x.cpvCodes = this.auctionGroupCpvCodes.filter(y => y.auctionGroupId === x.id).map(z => z.cpvCodeId);
    });

    this.auction.cpvCodeIds.forEach(x => {
      this.auctionGroups.forEach(y => {
        if (y.cpvCodes.filter(z => z === x).length > 0) {
          y.countCPVs++;
        }
      });
    }); 

    let items = this.auctionGroups.filter(z => z.countCPVs > 0);
    let group = (items && items.length > 0) ? items.reduce((max, i) => max.countCPVs > i.countCPVs ? max : i) : null;
    if (group) {
      this.auction.auctionGroupId = group.id;
    }
    }
  }

  cpvCodeChangeEvent(e) {
    if (e && e.value && e.value.length > 0) {

      this.auctionGroups.forEach(x => {
        x.countCPVs = 0
        x.cpvCodes = this.auctionGroupCpvCodes.filter(y => y.auctionGroupId === x.id).map(z => z.cpvCodeId);
      });

      this.auction.cpvCodeIds.forEach(x => {
        this.auctionGroups.forEach(y => {
          if (y.cpvCodes.filter(z => z === x).length > 0) {
            y.countCPVs++;
          }
        });
      }); 
  
      let items = this.auctionGroups.filter(z => z.countCPVs > 0);
      let group = (items && items.length > 0) ? items.reduce((max, i) => max.countCPVs > i.countCPVs ? max : i) : null;
      if (group) {
        this.auction.auctionGroupId = group.id;
      }
    }
  }

  backToAuctions() {
    this.auction = null;
    this.visible.emit(false);

    this.toggleDetailsTab(this.detailsTabs[0]);
  }

  async generateServerFolder() {
    if (this.auction && this.auction.id &&  this.auction.participationTypeId === 1) {
      await this.auctionService.createAuctionFolderAsync(this.auction.id);
    }
  }

  async saveData() {
    if (this.validationGroup.instance.validate().isValid && this.thirdValidationGroup.instance.validate().isValid) {
      let item = new ImportAuction();
      item = this.auction;
      item.customerId = Number(this.authService.getUserCustomerId());
      if (item) {
        await this.auctionService.createAuctionAsync(item).then(r => {
          if (r) {

          this.generateServerFolder();  

           // Create AuctionXCpvCode
          let createAuctionCpvCodeIds: AuctionXCpvCode[] = [];
          item.cpvCodeIds?.forEach(id => {
            const c = new AuctionXCpvCode();
            c.auctionId = Number(r.id);
            c.cpvCodeId = id;
            createAuctionCpvCodeIds.push(c);
          });
          if (createAuctionCpvCodeIds && createAuctionCpvCodeIds.length > 0) {
            this.auctionCpvCodeService.createMultipleAuctionCpvCodeAsync(createAuctionCpvCodeIds);
          }

            this.notificationService.alert('top', 'center', 'Licitatii - Datele au fost inserate cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Licitatii - Datele nu au fost inserate! A aparut o eroare!',
              NotificationTypeEnum.Red, true)
          }
          this.backToAuctions();
        });
      }
    }
  }

  modelChangeEvent(auction: any) {
    if (auction.participationTypeId === 3) {
      this.auction.lotsNumberTo = null;
      this.lotsNr = 0;
    }

    if (this.auction.result && (auction.status > 5 && auction.status !== 10)) {
      this.changeStatusBasedOnResult(this.auction.result);
    } else {
      if (this.auction.isCanceled) {
        this.auction.status = Number(AuctionStatusEnum.Canceled);
        return;
      }
      if (auction.participationTypeId == 2 || auction.participationTypeId == 3) {
        this.auction.status = Number(AuctionStatusEnum.Rejected);
      }
      if (auction.participationTypeId == 1) {
        this.auction.status = Number(AuctionStatusEnum.Approved);
      }
      if (auction.participationTypeId == 1 && auction.hasRequestedPrices) {
        this.auction.status = Number(AuctionStatusEnum.RequestedPrice);
      }
      if (auction.participationTypeId == 1 && auction.hasRequestedPrices && auction.hasReceivedPrices) {
        this.auction.status = Number(AuctionStatusEnum.Working);
      }
  
      let d = new Date();
      if (auction.participationTypeId == 1 && auction.hasRequestedPrices && auction.hasReceivedPrices && d > new Date(this.auction.endDate)) {
        this.auction.status = Number(AuctionStatusEnum.Deliberation);
      }
    }
  }

  resultChangeEvent(e) {
    if (e && e.value && e.event && !this.auction.isCanceled) {
      this.changeStatusBasedOnResult(e.value);
    }
  }

  auctionCancelEvent(e) {
    if (e && e.value && e.event) {
      this.auction.status = Number(AuctionStatusEnum.Canceled);

      this.observationPopup = true;
      this.observation = null;
      this.observationByCancel = true;
      this.observationByDisqualified = false;
    }
  }

  changeStatusBasedOnResult(resultId: number) {
    if(resultId == Number(AuctionResultEnum.TotalWin)) {
      this.auction.status = Number(AuctionStatusEnum.TotalWin);
    }
    if(resultId == Number(AuctionResultEnum.TotalRejected)) {
      this.auction.status = Number(AuctionStatusEnum.TotalRejected);
    }
    if(resultId == Number(AuctionResultEnum.PartialWin)) {
      this.auction.status = Number(AuctionStatusEnum.PartialWin);
    }
    if(resultId == Number(AuctionResultEnum.Disqualified)) {
      this.auction.status = Number(AuctionStatusEnum.Disqualified);

      this.observationPopup = true;
      this.observation = null;
      this.observationByCancel = false;
      this.observationByDisqualified = true;
    }
  }

  async updateData() {
    if (this.validationGroup.instance.validate().isValid && this.thirdValidationGroup?.instance.validate().isValid) {
      let item = new ImportAuction();
      item = this.auction;
      if (item) {
        this.loaded = true;
        await this.auctionService.updateAuctionAsync(item).then(r => {
          if (r) {

          this.generateServerFolder();  
          // Create AuctionXCpvCode
          const existingAuctionXCpvCodeIds = item.existingCpvCodeIds;
          let deleteAuctionXCpvCode: AuctionXCpvCode[] = [];
          existingAuctionXCpvCodeIds.filter(x => !item.cpvCodeIds.includes(x)).forEach(id => {
            const c = new AuctionXCpvCode();
            c.auctionId = item.id;
            c.cpvCodeId = id;
            deleteAuctionXCpvCode.push(c);
          });
          if (deleteAuctionXCpvCode.length > 0) {
            this.auctionCpvCodeService.deleteAuctionCpvCodeAsync(deleteAuctionXCpvCode);
          }
          let createAuctionXCpvCodeIds: AuctionXCpvCode[] = [];
          item.cpvCodeIds.filter(x => !existingAuctionXCpvCodeIds.includes(x)).forEach(id => {
            const c = new AuctionXCpvCode();
            c.auctionId = Number(item.id);
            c.cpvCodeId = id;
            createAuctionXCpvCodeIds.push(c);
          });
          if (createAuctionXCpvCodeIds.length > 0) {
            this.auctionCpvCodeService.createMultipleAuctionCpvCodeAsync(createAuctionXCpvCodeIds);
          }


          this.notificationService.generalAlert(r, 'Licitatii', 'modificate', this.translationService.getCurrentLang());
          } 
          this.loaded = false;
          this.backToAuctions();
        });
      }
    }
  }

  async saveObservation() {
    if (this.secondValidationGroup.instance.validate().isValid) { 
      let review = new AuctionReviews();
      if (this.observationByCancel) {
        review.code = 'Anulat';
        review.name = 'Anulat';
      }
      if (this.observationByDisqualified) {
        review.code = 'Descalificat';
        review.name = 'Descalificat';
      }
      review.description = this.observation;
      review.auctionId = this.auction.id;
      if (review) {
        await this.auctionReviewsService.CreateAsync(review).then(r => {
         this.observationPopup = false;
        });
      }
    }
  }

  getDisplayAuctionTypeExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  
  getDisplayPostExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  getCpvCodeExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }
  
  lotsNumberChange(item) {
    if (item && item.value) {

      const lots = item.value.toString().split(",");
      this.lotsNr = lots.filter(n => n).length;
    }
  }

  openIntervalPopup() {
    this.intervalPopup = true;
    this.intervalFrom = null;
    this.intervalTo = null;
  }

  generateRange() {
    if (this.intervalValidationGroup.instance.validate().isValid) {

      if (this.intervalFrom >= this.intervalTo) {
        this.notificationService.alert('top', 'center', 'Licitatii - Intervalul nu a fost generat! Prima valoare trebuie sa fie mai mica!',
        NotificationTypeEnum.Red, true)
      } else {
        let result = '';
        for (let i = this.intervalFrom; i <= this.intervalTo; i++) {
          result += i + ',';
        }
        if (!this.auction.lotsNumberTo) {
          this.auction.lotsNumberTo = result;
        } else {
          if (this.auction.lotsNumberTo.slice(-1) === ',') {
            this.auction.lotsNumberTo += result;
          } else {
            this.auction.lotsNumberTo += ',' + result;
          }
        }
        this.intervalPopup = false;
      }
    }
  }
}
