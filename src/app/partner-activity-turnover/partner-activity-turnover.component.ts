import { Component, OnInit, ViewChild, Input, AfterViewInit, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { DxDataGridComponent, DxTooltipComponent, DxValidatorComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { PartnerXItemComment } from 'app/models/clientturnover.model';
import { TurnoverSectionTypeEnum } from 'app/enums/turnoverSectionTypeEnum';
import * as _ from 'lodash';
import { TurnoverCommentComponent } from 'app/turnover-comment/turnover-comment.component';
import { PartnerActivityTurnover } from 'app/models/partneractivityturnover.model';
import { PartnerActivityTurnoverService } from 'app/services/partner-activity-turnover.service';
import { PartnerItemState } from 'app/models/partneritemstate.model';
import { Brand } from 'app/models/brand.model';
import { VipOfferItems } from 'app/models/vip-offer-items.model';
import { on } from 'devextreme/events';
import { VipOfferItemsDetailsService } from 'app/services/vip-offer-items-details.service';
import { PriceCompositionTypeEnum } from 'app/enums/priceCompositionTypeEnum';
import { ItemService } from 'app/services/item.service';
import { ItemRelatedService } from 'app/services/item-related.service';
import { TurnoverPreorder } from 'app/models/turnover-preorder.model';
import { AuthService } from 'app/services/auth.service';
import { TurnoverPreorderService } from 'app/services/turnover-preorder.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';

@Component({
  selector: 'app-partner-activity-turnover',
  templateUrl: './partner-activity-turnover.component.html',
  styleUrls: ['./partner-activity-turnover.component.css']
})
export class PartnerActivityTurnoverComponent implements OnInit, AfterViewInit {
  @Input() partnerActivityTurnoverActions: IPageActions;
  @Input() postId: number;
  @Input() clientId: number;
  @Input() partnerItemState: PartnerItemState[];
  @Input() brands: Brand[];
  @Input() vipOfferItems: VipOfferItems[];
  @Input() items: any[];
  @Input() customerId: number;
  @Input() showCharts: boolean;
  @Input() partners: any;
  @Input() relatedItems: any;

  @Output() foundItemBySearchFilter: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() isComponentShown: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('popupComponent') popupComponent: TurnoverCommentComponent;
  @ViewChild('tooltipItemCode') tooltipItemCode: DxTooltipComponent;
  @ViewChild('stockTooltipItemCode') stockTooltipItemCode: DxTooltipComponent;
  @ViewChild('tooltipItemName') tooltipItemName: DxTooltipComponent;
  @ViewChild('tooltipItemComment') tooltipItemComment: DxTooltipComponent;

  loaded: boolean;
  partnerActivityTurnovers: PartnerActivityTurnover[];
  selectedClientTurnover: PartnerActivityTurnover;
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  groupedText: string;
  timeout: number;  
  tooltipData: any = {};
  displayData: boolean;
  stockTooltipData: any = {};
  previouslyCheckedProperty: string | null = null;
  section: number;
  itemNameTooltipData: any = {};
  itemNameTooltipTimeout: number;
  treeListDataSource: any = [];
  itemCommentTooltipTimeout: number;  
  itemCommentTooltipData: any = {};
  selectedItemComments: any;
  isItemCommentsPopupOpen: boolean;
  currentPageIndex: number;
  lastPageIndex: any = -1;
  autoExpandAll: any;
  expandedRowKeys: number[] = [];

  partnerActivityTurnoverInfo: string;

  constructor(
    private partnerActivityTurnoverService: PartnerActivityTurnoverService, 
    private translationService: TranslateService,
    private itemService: ItemService,
    private vipOfferItemDetailsService: VipOfferItemsDetailsService,
    private notificationService: NotificationService,
    private itemXRelatedService: ItemRelatedService,
    private authenticationService: AuthService,
    private turnoverPreorderService: TurnoverPreorderService) {
      
    this.section = TurnoverSectionTypeEnum.PartnerActivityTurnover; 
    this.groupedText = this.translationService.instant('groupedText');
    this.partnerActivityTurnovers = [];

    this.partnerActivityTurnoverInfo = this.translationService.instant("partnerActivityTurnoverInfo");
  }

  ngOnInit(): void {
    if (this.partnerActivityTurnoverActions)
      this.partnerActivityTurnoverActions.CanAdd = false;
  }

  async ngOnChanges(changes: SimpleChanges): Promise<void> {
    if (changes.items && this.items && this.items.length > 0) {
      await this.getData();
    }
  }

  canUpdate() {
    return this.partnerActivityTurnoverActions ? this.partnerActivityTurnoverActions.CanUpdate : false;
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }

  async getData() {
    if (this.clientId && this.postId) {
      this.treeListDataSource = [];
      this.loaded = true;
      
      await this.partnerActivityTurnoverService.getData(this.clientId, this.postId, this.section).then(async items => {
        this.partnerActivityTurnovers = (items && items.length > 0) ? items : [];
        const currentDate = new Date().setHours(0, 0, 0, 0);

        this.partnerActivityTurnovers.forEach(i => {
          i.itemComments = _.orderBy(i.itemComments, ['created'], ['desc']);

          i.itemComments.forEach(comment => {
            const { partnerItemStateId } = comment;
          
            // Reset all boolean values to false
            comment.isInvoiced = false;
            comment.notUsed = false;
            comment.hasStock = false;
            comment.atOrder = false;
          
            let name = this.partnerItemState?.find(x => x.id == partnerItemStateId)?.name;

            // Update the boolean value based on the partnerItemStateId
            switch (name) {
              case "Facturat":
                comment.isInvoiced = true;
                break;
              case "Nu Foloseste":
                comment.notUsed = true;
                break;
              case "Are Stoc":
                comment.hasStock = true;
                break;
              case "La Comanda":
                comment.atOrder = true;
                break;
              default:
                comment.isInvoiced = false;
                comment.notUsed = false;
                comment.hasStock = false;
                comment.atOrder = false;
                break;
            }
          });

          const todayObjects = i.itemComments.filter(obj => {
            const objDate = new Date(obj.created).setHours(0, 0, 0, 0);
            return objDate === currentDate;
          });
        
          if (todayObjects.length > 0) {
            i.lastComment = todayObjects.reduce((prev, curr) => {
              const prevDate = new Date(prev.created);
              const currDate = new Date(curr.created);
              return currDate > prevDate ? curr : prev;
            });
          } else {
            i.lastComment = new PartnerXItemComment();;
            i.lastComment.isInvoiced = false;
            i.lastComment.notUsed = false;
            i.lastComment.hasStock = false;
            i.lastComment.atOrder = false;
          }

          i.lastComment.shoppingCartQuantity = null;
        });

        let itemGroupIds = this.partnerActivityTurnovers.map(m => m.itemGroupId);
        let itemIds = this.items.filter(f => itemGroupIds.includes(f.itemGroupId)).map(m => m.id);

        await this.partnerActivityTurnoverService.getChildrenData(this.clientId, this.postId, this.section, itemIds).then(result => {
          if (result && result.length > 0) {
            
            result.forEach(i => {
              i.itemComments = _.orderBy(i.itemComments, ['created'], ['desc']);
    
              i.itemComments.forEach(comment => {
                const { partnerItemStateId } = comment;
              
                // Reset all boolean values to false
                comment.isInvoiced = false;
                comment.notUsed = false;
                comment.hasStock = false;
                comment.atOrder = false;
              
                let name = this.partnerItemState?.find(x => x.id == partnerItemStateId)?.name;
    
                // Update the boolean value based on the partnerItemStateId
                switch (name) {
                  case "Facturat":
                    comment.isInvoiced = true;
                    break;
                  case "Nu Foloseste":
                    comment.notUsed = true;
                    break;
                  case "Are Stoc":
                    comment.hasStock = true;
                    break;
                  case "La Comanda":
                    comment.atOrder = true;
                    break;
                  default:
                    comment.isInvoiced = false;
                    comment.notUsed = false;
                    comment.hasStock = false;
                    comment.atOrder = false;
                    break;
                }
              });
    
              const todayObjects = i.itemComments.filter(obj => {
                const objDate = new Date(obj.created).setHours(0, 0, 0, 0);
                return objDate === currentDate;
              });
            
              if (todayObjects.length > 0) {
                i.lastComment = todayObjects.reduce((prev, curr) => {
                  const prevDate = new Date(prev.created);
                  const currDate = new Date(curr.created);
                  return currDate > prevDate ? curr : prev;
                });
              } else {
                i.lastComment = new PartnerXItemComment();;
                i.lastComment.isInvoiced = false;
                i.lastComment.notUsed = false;
                i.lastComment.hasStock = false;
                i.lastComment.atOrder = false;
              }
    
              i.lastComment.shoppingCartQuantity = null;
            });

            let j = 1;
            for (let i=0; i<this.partnerActivityTurnovers.length; i++) {
              let itemGroup = {
                id: j++,
                parentId: null,
                partnerId: this.partnerActivityTurnovers[i].partnerId,
                itemGroupId: this.partnerActivityTurnovers[i].itemGroupId,
                itemCode: this.partnerActivityTurnovers[i].itemGroupCode,
                itemName: this.partnerActivityTurnovers[i].itemGroupName,
                bestRankingItemId: this.partnerActivityTurnovers[i].bestRankingItemId,
                bestRankingItemCode: this.partnerActivityTurnovers[i].bestRankingItemCode,
                bestRankingItemName: this.partnerActivityTurnovers[i].bestRankingItemName,
                listPrice: this.partnerActivityTurnovers[i].listPrice,
                currencyId: this.partnerActivityTurnovers[i].currencyId,
                currencyName: this.partnerActivityTurnovers[i].currencyName,
                itemGroupTurnoverLast3Months: this.partnerActivityTurnovers[i].itemGroupTurnoverLast3Months,
                itemGroupTurnoverTotal: this.partnerActivityTurnovers[i].itemGroupTurnoverTotal,
                l12: this.partnerActivityTurnovers[i].itemGroupL12,
                l11: this.partnerActivityTurnovers[i].itemGroupL11,
                l10: this.partnerActivityTurnovers[i].itemGroupL10,
                l9: this.partnerActivityTurnovers[i].itemGroupL9,
                l8: this.partnerActivityTurnovers[i].itemGroupL8,
                l7: this.partnerActivityTurnovers[i].itemGroupL7,
                l6: this.partnerActivityTurnovers[i].itemGroupL6,
                l5: this.partnerActivityTurnovers[i].itemGroupL5,
                l4: this.partnerActivityTurnovers[i].itemGroupL4,
                l3: this.partnerActivityTurnovers[i].itemGroupL3,
                l2: this.partnerActivityTurnovers[i].itemGroupL2,
                l1: this.partnerActivityTurnovers[i].itemGroupL1,
                lc: this.partnerActivityTurnovers[i].itemGroupLC,
                lastComment: this.partnerActivityTurnovers[i].lastComment,
                itemComments: this.partnerActivityTurnovers[i].itemComments
              }

              let items = result.filter(f => f.itemGroupId === this.partnerActivityTurnovers[i].itemGroupId && f.currentStock);
              if (items && items.length > 0) {
                this.treeListDataSource.push(itemGroup);

                items?.forEach(item => {
  
                  let childItem = {
                    id: j++,
                    parentId: itemGroup.id,
                    ...item
                  }
  
                  this.treeListDataSource.push(childItem);
                })
              }
            }
          } else {
            let j = 1;
            for (let i=0; i<this.partnerActivityTurnovers.length; i++) {
              let itemGroup = {
                id: j++,
                parentId: null,
                partnerId: this.partnerActivityTurnovers[i].partnerId,
                itemGroupId: this.partnerActivityTurnovers[i].itemGroupId,
                itemCode: this.partnerActivityTurnovers[i].itemGroupCode,
                itemName: this.partnerActivityTurnovers[i].itemGroupName,
                bestRankingItemId: this.partnerActivityTurnovers[i].bestRankingItemId,
                bestRankingItemCode: this.partnerActivityTurnovers[i].bestRankingItemCode,
                bestRankingItemName: this.partnerActivityTurnovers[i].bestRankingItemName,
                listPrice: this.partnerActivityTurnovers[i].listPrice,
                currencyId: this.partnerActivityTurnovers[i].currencyId,
                currencyName: this.partnerActivityTurnovers[i].currencyName,
                itemGroupTurnoverLast3Months: this.partnerActivityTurnovers[i].itemGroupTurnoverLast3Months,
                itemGroupTurnoverTotal: this.partnerActivityTurnovers[i].itemGroupTurnoverTotal,
                l12: this.partnerActivityTurnovers[i].itemGroupL12,
                l11: this.partnerActivityTurnovers[i].itemGroupL11,
                l10: this.partnerActivityTurnovers[i].itemGroupL10,
                l9: this.partnerActivityTurnovers[i].itemGroupL9,
                l8: this.partnerActivityTurnovers[i].itemGroupL8,
                l7: this.partnerActivityTurnovers[i].itemGroupL7,
                l6: this.partnerActivityTurnovers[i].itemGroupL6,
                l5: this.partnerActivityTurnovers[i].itemGroupL5,
                l4: this.partnerActivityTurnovers[i].itemGroupL4,
                l3: this.partnerActivityTurnovers[i].itemGroupL3,
                l2: this.partnerActivityTurnovers[i].itemGroupL2,
                l1: this.partnerActivityTurnovers[i].itemGroupL1,
                lc: this.partnerActivityTurnovers[i].itemGroupLC,
                lastComment: this.partnerActivityTurnovers[i].lastComment,
                itemComments: this.partnerActivityTurnovers[i].itemComments
              }

              this.treeListDataSource.push(itemGroup);
            }
          }

          this.isComponentShown.emit(this.partnerActivityTurnovers && this.partnerActivityTurnovers.length > 0);
          this.autoExpandAll = true;
          this.expandedRowKeys = this.treeListDataSource.map(emp => emp.id);
          this.dataGrid.instance.pageIndex(this.currentPageIndex);
          this.loaded = false;
        })
      });
    }
  }

  applyColumnFilter(filterValue, columnName: string) {
    if (this.dataGrid && this.dataGrid.instance) {
    this.dataGrid.instance.option("searchPanel.text", filterValue);

    var found = false;
    if (this.partnerActivityTurnovers && this.partnerActivityTurnovers.length > 0 && this.partnerActivityTurnovers.find(x => x.itemGroupCode.toLowerCase().includes(filterValue?.toLowerCase())) != null) {
      found = true;
    }
    if (this.partnerActivityTurnovers && this.partnerActivityTurnovers.length > 0 && this.partnerActivityTurnovers.find(x => x.itemGroupName.toLowerCase().includes(filterValue?.toLowerCase())) != null) {
      found = true;
    }
    if (found) {
      this.foundItemBySearchFilter.emit(true);

      // setTimeout(() => {
      //   var scrollableContent = document.getElementById('partnerActivityTurnover');
      //   if (scrollableContent) {
      //     scrollableContent.scrollIntoView({ behavior: 'smooth' });
      //   }
      // }, 100);
    } else {
      this.foundItemBySearchFilter.emit(false);
    }
    }
}

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && e.data.lastComment != null && e.data.lastComment.isInvoiced) {
      e.rowElement.style.backgroundColor = '#a1ffa1';
    }
    if (e.rowType === 'data' && e.data && e.data.priceCompositionType == PriceCompositionTypeEnum.SinglePriceList) {
      e.rowElement.style.color = '#4169e1';
    }
    if (e.rowType === 'data' && e.data && e.data.priceCompositionType == PriceCompositionTypeEnum.SinglePriceStock) {
      e.rowElement.style.color = '#ff3729';
    }
    if (e.rowType === 'data' && e.data && e.data.priceCompositionType == PriceCompositionTypeEnum.SinglePriceCraft) {
      e.rowElement.style.color = '#01910d';
    }

    if (e.rowType === 'header') {
      e.rowElement.style.backgroundColor = 'rgb(242, 242, 242)'; 
      e.rowElement.style.color = '#010101';
    }
  }

  getMonth(offset: number): string {
    const currentMonth = new Date().getMonth();
    const monthIndex = (currentMonth - offset + 12) % 12;
    const monthsInRomanian = [
      "Ian", "Feb", "Mar", "Apr", "Mai", "Iun",
      "Iul", "Aug", "Sep", "Oct", "Noi", "Dec"
    ];
    if (offset == 0) {
      return monthsInRomanian[monthIndex] + ' Curent';
    } else {
      return monthsInRomanian[monthIndex];
    }
  }

  public refreshDataGrid() {
    this.getData();
    this.dataGrid.instance.refresh();
  }

  public async openDetails(row: any) {
    this.selectedClientTurnover = row;
    this.selectedClientTurnover.lastComment.bestRankingItemId = row.bestRankingItemId;
    setTimeout(() => {
      this.popupComponent.openPopup();
    }, 0);
  }

  public async onRowUpdated(event: any): Promise<void> {
    if (event && event.data && event.data.lastComment) {
      let comment = event.data.lastComment;

      if (comment.isInvoiced || comment.hasStock || comment.atOrder) {
        this.openDetails(event.data);
        return;
      }
      
      if (comment.notUsed) {
        this.popupComponent.saveComment(event.data.lastComment, event.data.itemId);
      }
    }

  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";

      if (event.dataField === 'lastComment.comment' && event.row && event.row.data) {
        const rowData = event.row.data;
        const allowEditing = rowData?.lastComment?.notUsed ||
                            rowData?.lastComment?.hasStock ||
                            rowData?.lastComment?.isInvoiced ||
                            rowData?.lastComment?.atOrder;
        event.editorOptions.disabled = !allowEditing;
      }

      if (event.dataField === 'lastComment.isInvoiced' && event.row && event.row.data) {
        const rowData = event.row.data;
        const allowEditing = !rowData?.lastComment?.notUsed &&
                            !rowData?.lastComment?.hasStock &&
                            !rowData?.lastComment?.atOrder;
        event.editorOptions.disabled = !allowEditing;
      }

      if (event.dataField === 'lastComment.notUsed' && event.row && event.row.data) {
        const rowData = event.row.data;
        const allowEditing = !rowData?.lastComment?.isInvoiced &&
                            !rowData?.lastComment?.hasStock &&
                            !rowData?.lastComment?.atOrder;
        event.editorOptions.disabled = !allowEditing;
      }

      if (event.dataField === 'lastComment.hasStock' && event.row && event.row.data) {
        const rowData = event.row.data;
        const allowEditing = !rowData?.lastComment?.notUsed &&
                            !rowData?.lastComment?.isInvoiced &&
                            !rowData?.lastComment?.atOrder;
        event.editorOptions.disabled = !allowEditing;
      }

      if (event.dataField === 'lastComment.atOrder' && event.row && event.row.data) {
        const rowData = event.row.data;
        const allowEditing = !rowData?.lastComment?.notUsed &&
                            !rowData?.lastComment?.hasStock &&
                            !rowData?.lastComment?.isInvoiced;
        event.editorOptions.disabled = !allowEditing;
      }
  }

  setItemCodeTooltip(event) {
    if (event && event.rowType === 'data' && event.column.dataField === 'itemCode' && event.data.parentId) {
      on(event.cellElement, 'mouseover', arg => {
        this.timeout = window.setTimeout(async () => {

          this.tooltipData = null;
          let stockPromise = this.vipOfferItemDetailsService.GetTiersForItemAsync(null, event.data.itemId, this.clientId).then(r => {
            if (r && r.length > 0) {
              this.tooltipData = r;
            } else { this.tooltipData = []; }
          });

          Promise.all([stockPromise]).then(r => {this.tooltipItemCode.instance.show(arg.target);})
        }, 1000);
      });

      on(event.cellElement, 'mouseout', arg => {
        if (this.timeout) {
          window.clearTimeout(this.timeout);
        }
        this.tooltipItemCode.instance.hide();
      });
    }
    if (event && event.rowType === 'data' && event.column.dataField === 'currentStock') {
      on(event.cellElement, 'mouseover', arg => {
        this.timeout = window.setTimeout(async () => {

          this.stockTooltipData = null;
          let stockPromise = this.itemService.GetStocksOnSiteAsync(event.data.bestRankingItemId).then(r => {
            if (r && r.length > 0) {
              this.stockTooltipData = r;
            } else { this.stockTooltipData = []; }
          });

          Promise.all([stockPromise]).then(r => {this.stockTooltipItemCode.instance.show(arg.target);})
        }, 1000);
      });

      on(event.cellElement, 'mouseout', arg => {
        if (this.timeout) {
          window.clearTimeout(this.timeout);
        }
        this.stockTooltipItemCode.instance.hide();
      });
    }
  }

  isItemOnVIP(itemId: number) {
    return (this.vipOfferItems.filter(x => x.itemId == itemId).length > 0);
}

  onCellPrepared(e) {
    this.setItemCodeTooltip(e);
    this.setItemNameTooltip(e);
    this.setItemCommentTooltip(e);
    if (e.rowType === 'data') {
      e.cellElement.style.paddingTop = '2px';
      e.cellElement.style.paddingBottom = '1px';
    }

    if (e && e.rowType === 'header' && e.column.dataField === 'itemName') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Va prezint cateva produse pe care le vindem in cantitati mari pe domeniul dumneavoastra!");
      $(cellElement).tooltip({
        placement: 'top'
      });
    }

    if (e && e.rowType === 'header' && e.column.dataField === 'lastComment.shoppingCartQuantity') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Introduceti cantitatea si adaugati in cos. Un comentariu se va crea automat!");
      $(cellElement).tooltip({
        placement: 'top'
      });
    }
  }

  setItemCommentTooltip(event) {
    if (event && event.rowType === 'data' && event.column.dataField === 'lastComment.comment') {
      on(event.cellElement, 'mouseover', arg => {
        this.itemCommentTooltipTimeout = window.setTimeout(async () => {

          this.itemCommentTooltipData = null;
          if (event.data.itemComments && event.data.itemComments.length > 0) {
            let orderedComments = _.orderBy(event.data.itemComments, 'created', ['desc']);
            let lastComment = orderedComments[0].comment;

            this.itemCommentTooltipData = lastComment;
          } else {
            this.itemCommentTooltipData = [];
          }

          this.tooltipItemComment.instance.show(arg.target);
        }, 1000);
      });

      on(event.cellElement, 'mouseout', arg => {
        if (this.itemCommentTooltipTimeout) {
          window.clearTimeout(this.itemCommentTooltipTimeout);
        }
        this.tooltipItemComment.instance.hide();
      });
    }
  }

  priceCurrencyExpr(data) {
    if (!data || !data?.listPrice) {
      return '';
    }
    return data.listPrice.toFixed(2) + ' ' + data.currencyName;
  }

  setItemNameTooltip(event) {
    if (event && event.rowType === 'data' && event.column.dataField === 'itemName') {
      on(event.cellElement, 'mouseover', arg => {
        this.itemNameTooltipTimeout = window.setTimeout(async () => {

          this.itemNameTooltipData = null;
          let itemXRelatedPromise = this.itemXRelatedService.getItemXRelatedByItemIdForTurnoverAsync(this.clientId, event.data.itemId).then(r => {
            if (r && r.length > 0) {
              let icon = '';       
              let iconName = '';     

              this.itemNameTooltipData = r.map(item => {
                switch(item.relatedItemTypeId) {
                  case 1:
                    icon = 'equivalence-icon.png';
                    iconName = 'Similar';
                    break;
                  case 2:
                    icon = 'complementarity-icon.png';
                    iconName = 'Complementar';
                    break;
                  case 3:
                    icon = 'superiority-icon.png';
                    iconName = 'Superior';
                    break;
        
                  case 4:
                    icon = 'inferiority-icon.png';
                    iconName = 'Inferior';
                    break;
        
                  case 5:
                    icon = 'concurrency-icon.png';
                    iconName = 'Concurent';
                    break;
        
                  default:
                    break;
                }

                item.icon = icon;
                item.iconName = iconName;
                item.quantity = null; // shopping cart quantity
                return item;
              })
            } else { this.itemNameTooltipData = []; }
          });

          Promise.all([itemXRelatedPromise]).then(r => {this.tooltipItemName.instance.show(arg.target);})
        }, 1000);
      });

      on(event.cellElement, 'mouseout', arg => {
        if (this.itemNameTooltipTimeout) {
          window.clearTimeout(this.itemNameTooltipTimeout);
        }
        this.tooltipItemName.instance.hide();
      });
    }
  }

  getRelatedItemPrice(data) {
    if (!data)
      return '';

    return data.price.toFixed(2) + ' ' + data.currencyName;
  }

  onHidingRelatedItemsTooltip(e) {
    const isMouseInsidePopover = e.component.content().parentNode.matches(":hover");
    e.cancel = isMouseInsidePopover;
    if (!e.component.content().parentNode.getAttribute("mouselistener")) {
      e.component.content().parentNode.addEventListener("mouseleave", (arg) => {
        e.component.hide();
      });
      e.component.content().parentNode.setAttribute("mouselistener", true);
    }
  }

  addItemToCart(item) {
    if (item.lastComment.shoppingCartQuantity) {
      item.lastComment.isInvoiced = true;
      item.lastComment.shoppingCartQuantity = Number(item.lastComment.shoppingCartQuantity);
      
      this.popupComponent.saveComment(item.lastComment, item.itemId);
    }
  }

  async addItemToCartFromVipTiers(vipIndex: number, itemId: number, quantity: number) {
    let createPreoder = new TurnoverPreorder();
    createPreoder.customerId = Number(this.authenticationService.getUserCustomerId());
    createPreoder.partnerId = this.clientId;
    createPreoder.postId = Number(this.authenticationService.getUserPostId());
    createPreoder.itemId = itemId;
    createPreoder.quantity = quantity;
    createPreoder.priceSource = vipIndex;

    this.loaded = true;
    await this.turnoverPreorderService.createTurnoverPreorderAsync(createPreoder).then(() => {
      
      this.notificationService.alert('top', 'center', 'Produsul a fost adaugat in cos!', NotificationTypeEnum.Green, true);
      this.loaded = false;
    })
  }

  onHidingVipTiersTooltip(e) {
    const isMouseInsidePopover = e.component.content().parentNode.matches(":hover");
    e.cancel = isMouseInsidePopover;
    if (!e.component.content().parentNode.getAttribute("mouselistener")) {
      e.component.content().parentNode.addEventListener("mouseleave", (arg) => {
        e.component.hide();
      });
      e.component.content().parentNode.setAttribute("mouselistener", true);
    }
  }

  hasItemsRelated(itemId: number) {
    return (this.relatedItems.filter(x => x.itemId == itemId).length > 0);
  }

  onEditingStart(event: any) {
    const nonEditableFields = ['lastComment.shoppingCartQuantity', 'lastComment.hasStock', 'lastComment.atOrder'];

    if (nonEditableFields.includes(event.column.dataField) && event.data?.parentId === null) {
      event.cancel = true;
    }
  }

  onHidingItemCommentTooltip(e) {
    const isMouseInsidePopover = e.component.content().parentNode.matches(":hover");
    e.cancel = isMouseInsidePopover;
    if (!e.component.content().parentNode.getAttribute("mouselistener")) {
      e.component.content().parentNode.addEventListener("mouseleave", (arg) => {
        e.component.hide();
      });
      e.component.content().parentNode.setAttribute("mouselistener", true);
    }
  }

  openItemComments(row: any) {
    this.isItemCommentsPopupOpen = true;
    this.selectedItemComments = _.orderBy(row.itemComments, 'created', ['desc']);
    this.selectedItemComments.forEach(i => {
      if (i.isInvoiced) {
        i.status = 'Facturat';
      }
      if (i.hasStock) {
        i.status = 'Are stoc';
      }
      if (i.atOrder) {
        i.status = 'La comanda ';
      }
      if (i.notUsed) {
        i.status = 'Nu folosesc';
      }
    });
  }

  onContentReady(e:any): void {  
    let dataGrid = e.component;  
    let currentPageIndex = dataGrid.pageIndex();  

    if(this.lastPageIndex === -1) {  
      this.lastPageIndex = currentPageIndex;  
    } else {  
      if(this.lastPageIndex !== currentPageIndex) {  
        this.currentPageIndex = currentPageIndex;  
      }  
    }  
  }  
}

