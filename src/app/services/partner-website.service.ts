import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PartnerWebsite } from 'app/models/partner-website.model';
import { Constants } from 'app/constants';
import { WebsiteAdmin } from 'app/models/websites-admin.model';
@Injectable({
  providedIn: 'root'
})
export class PartnerWebsiteService {

  constructor(private apiService: ApiService) { }

  async getAllPartnerWebsitesAsync(): Promise<PartnerWebsite[]> {
    const apiURL = `/partnerwebsite/getall`;
    return await this.apiService.GetAsync<PartnerWebsite[]>(apiURL, Constants.mrkApi);
  }

  async getByPartnerIdAsync(partnerId: number): Promise<PartnerWebsite[]> {
    const apiURL = `/partnerwebsite/getbypartnerid`;
    return await this.apiService.PostAsync<PartnerWebsite[]>(apiURL, partnerId, Constants.mrkApi);
  }

  async getByPartnerIdIncludingInactiveAsync(partnerId: number): Promise<PartnerWebsite[]> {
    const apiURL = `/partnerwebsite/getbypartneridincludinginactive`;
    return await this.apiService.PostAsync<PartnerWebsite[]>(apiURL, partnerId, Constants.mrkApi);
  }

  async getWebsitesAdminAsync(): Promise<WebsiteAdmin[]> {
    const apiURL = `/partnerwebsite/getwebsitesadmin`;
    return await this.apiService.GetAsync<WebsiteAdmin[]>(apiURL, Constants.mrkApi);
  }

  async createPartnerWebsiteAsync(partnerWebsite: PartnerWebsite): Promise<PartnerWebsite> {
    const apiURL = `/partnerwebsite/create`;
    return await this.apiService.PostAsync<PartnerWebsite>(apiURL, partnerWebsite, Constants.mrkApi);
  }

  async updatePartnerWebsiteAsync(partnerWebsite: PartnerWebsite): Promise<PartnerWebsite> {
    const apiURL = `/partnerwebsite/update`;
    return await this.apiService.PutAsync<PartnerWebsite>(apiURL, partnerWebsite, Constants.mrkApi);
  }

  async activateForIdAsync(id: number): Promise<void> {
    const apiURL = `/partnerwebsite/activateforid`;
    return await this.apiService.PostAsync(apiURL, id, Constants.mrkApi);
  }

  async deletePartnerWebsitesAsync(ids: number[]): Promise<any> {
    const apiURL = `/partnerwebsite/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
