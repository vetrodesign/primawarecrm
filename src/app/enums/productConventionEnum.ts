export enum ProductConventionEnum {
    Consumer = 1,
    Distributor = 2,
    Retail = 3
  }

export enum ProductConventionShortEnum {
  C0 = 1,
  D0 = 2,
  R0 = 3
}