import { BaseDomain } from "./base-domain.model";

export class Post extends BaseDomain {
    code: string;
    companyPost: string;
    isLeadershipPost: boolean;
    description: string;
    email: string;
    occupationCodeId: number;
    CORCode: string;
    CORPost: string;
    officeId: number;
    id: number;

    isActive: boolean;

    postDetail: PostDetail = new PostDetail();

    postXPartnerActivityAllocation: any[];
    partnerActivityAllocationIds: any;

    postXGeographicalArea: any[];
    postXGeographicalAreaIds: any;

    postXCategoryGeographicalArea: any[];
    postXCategoryGeographicalAreaIds: any;

    postXItemGroupCategory: any[];
    postXItemGroupCategoryIds: any;

    isOfficeSuperior: boolean;
    isDepartmentSuperior: boolean;
    directSuperiorPostId: number;
    isLocationSuperior: boolean;
    isGeneralManager: boolean;
}

export class PostDetail {
    postId: number;
    emailBackup: string;
    phone: string;
    phoneInterior: string;
    mobilePhone: string;
    whatsApp: string;

    webSite: string;
    skype: string;
    facebook: string;
    youtube: string;

    isActive: boolean;
}