import { BaseDomain } from "./base-domain.model";

export class PartnerStatusHistory extends BaseDomain {
    partnerStatusId: number;
    previousPartnerStatusId: number;
    partnerId: number;
    postId: number;
    userId: number;
}