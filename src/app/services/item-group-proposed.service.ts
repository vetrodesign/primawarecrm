import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ItemGroupProposed } from 'app/models/itemgroupproposed.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})

export class ItemGroupProposedService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<any[]> {
    const apiURL = `/itemgroupproposed/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL,Constants.mrkApi);
  }

  async getByCustomerId(): Promise<any[]> {
    const apiURL = `/itemgroupproposed/getByCustomerId`;
    return await this.apiService.GetAsync<any[]>(apiURL,Constants.mrkApi);
  }

  async getByItemIdAsync(id: number): Promise<any[]> {
    const apiURL = `/itemgroupproposed/getById`;
    return await this.apiService.PostAsync<any[]>(apiURL, id,Constants.mrkApi);
  }

  async createAsync(i: ItemGroupProposed): Promise<any> {
    const apiURL = `/itemgroupproposed/create`;
    return await this.apiService.PostAsync<any>(apiURL, i,Constants.mrkApi);
  }

  async updateAsync(i: ItemGroupProposed): Promise<any> {
    const apiURL = `/itemgroupproposed/update`;
    return await this.apiService.PutAsync<any>(apiURL, i,Constants.mrkApi);
  }

  async deleteAsync(id: number): Promise<any> {
    const apiURL = `/itemgroupproposed/delete`;
    return await this.apiService.PostAsync<any>(apiURL, id,Constants.mrkApi);
  }
}
