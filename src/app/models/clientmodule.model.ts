export class ClientModule {
    public id: number;
    public description: string;
    public title: string;
    public redirectUri: string;
    public lastUpdate: string;
    public imgSrc?: string;
    public class?: string;
    public icon?: string;
    public orderNr?: number;
}
