import { TestBed } from '@angular/core/testing';

import { SalesZoneService } from './sales-zone.service';

describe('PartnerActivityTypeService', () => {
  let service: SalesZoneService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalesZoneService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
