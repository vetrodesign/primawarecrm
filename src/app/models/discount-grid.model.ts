import { BaseDomain } from "./base-domain.model";

export class DiscountGrid extends BaseDomain {
    public customerId: number;
    public code: string;
    public name: string;
    public description: string;

    public productConventionId: number;
    public dataType: number;
    public benefitType: number;

    public currencyId: number;
    public measurementUnitId: number;

    public validFrom: number;
    public validTo: number;

    public isActive: boolean;
}
