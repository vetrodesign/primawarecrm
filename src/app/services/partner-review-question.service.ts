

import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { PartnerReviewQuestion } from 'app/models/partner-review-question.model';


@Injectable({
  providedIn: 'root'
})
export class PartnerReviewQuestionService {
  constructor(private apiService: ApiService) { }

  async getAllPartnerReviewQuestionsAsync(): Promise<PartnerReviewQuestion[]> {
    const apiURL = `/partnerreviewquestion/GetAll`;
    return await this.apiService.GetAsync<PartnerReviewQuestion[]>(apiURL, Constants.mrkApi);
  }

  async createPartnerReviewQuestionAsync(p: PartnerReviewQuestion): Promise<any> {
    const apiURL = `/partnerreviewquestion/create`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async updatePartnerReviewQuestionAsync(p: PartnerReviewQuestion): Promise<any> {
    const apiURL = `/partnerreviewquestion/update`;
    return await this.apiService.PutAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async deletePartnerReviewQuestionAsync(ids: number[]): Promise<any> {
    const apiURL = `/partnerreviewquestion/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
