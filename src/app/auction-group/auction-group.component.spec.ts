import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionGroupComponent } from './auction-group.component';

describe('AuctionGroupComponent', () => {
  let component: AuctionGroupComponent;
  let fixture: ComponentFixture<AuctionGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuctionGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
