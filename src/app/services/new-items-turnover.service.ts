import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class NewItemsTurnoverService {
  constructor(private apiService: ApiService) { }

  async getData(partnerId: number, postId: number, section: number): Promise<any> {
    let filter = {'partnerId': partnerId, 'postId': postId, 'section': section}
    const apiURL = `/Partner/GetPartnerNewItemsTurnover`;
    return await this.apiService.PostAsync<any>(apiURL, filter, Constants.mrkApi);
  }

  async getChildrenData(partnerId: number, postId: number, section: number, itemIds: number[]): Promise<any> {
    let filter = {'partnerId': partnerId, 'postId': postId, 'section': section, 'itemIds': itemIds}
    const apiURL = `/Partner/GetPartnerNewItemsTurnoverChildrenData`;
    return await this.apiService.PostAsync<any>(apiURL, filter,  Constants.mrkApi);
  }
}
