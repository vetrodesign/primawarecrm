import { TestBed } from '@angular/core/testing';

import { AgentOtherItemTurnoverService } from './agent-other-item-turnover.service';

describe('AgentOtherItemTurnoverService', () => {
  let service: AgentOtherItemTurnoverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AgentOtherItemTurnoverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
