export class AuctionSearchFilter {
    public auctionGroupId: any;
    public auctionTypeId: any;
    public cpvCodeIds: any;

    public acquisitionNumber: string;
    public acquisitionName: string;
    public acquisitionerName: string;

    public country: string;
    public county: string;

    public acquisitionDate: Date;
    public endDate: Date;

    public underEstimatedValue: number;
    public overEstimatedValue: number;
    public status: number;

    public endingDate : number;
    public participationTypeId: number;
    public postId: number;

    public isInTerm: boolean;
    public isExpired: boolean;
    public isToAnalyze: boolean;

    public hideRejected: boolean;
}
