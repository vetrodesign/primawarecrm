import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { IPageActions } from 'app/models/ipageactions';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { DxDataGridComponent, DxValidatorComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { Constants } from 'app/constants';
import { PageActionService } from 'app/services/page-action.service';
import { ActivatedRoute } from '@angular/router';
import { ClientModuleMenuItemsService } from 'app/services/client-module-menu-items.service';
import { ContactCampaignLocation } from 'app/models/contact-campaign.model';
import { ContactCampaignService } from 'app/services/contact-campaign-management.service';
import { ContactCampaignTypeEnum } from 'app/enums/contactCampaignTypeEnum';
import { ContactCampaignManagementLocationDetailsComponent } from './contact-campaign-management-location-details/contact-campaign-management-location-details.component';
import { HelperService } from 'app/services/helper.service';
import { City } from 'app/models/city.model';
import { CountryService } from 'app/services/country.service';
import { CountyService } from 'app/services/county.service';
import { CityService } from 'app/services/city.service';
import { GeographicalArea } from 'app/models/geographicalarea.model';
import { GeographicalAreaService } from 'app/services/geographical-area.service';
import * as moment from 'moment';
import { ContactCampaignLocationGeoAreGeoEntitiesService } from 'app/services/contact-campaign-location-geo-area-geo-entities.service';
import { v4 as uuidv4 } from 'uuid';
import { ContinentService } from 'app/services/continent.service';
import { Continent } from 'app/models/continent.model';
import { GeographicalEntity } from 'app/enums/geographicalEntityEnum';

@Component({
  selector: 'app-contact-campaign-management-location',
  templateUrl: './contact-campaign-management-location.component.html',
  styleUrls: ['./contact-campaign-management-location.component.css']
})
export class ContactCampaignManagementLocationComponent implements OnInit, AfterViewInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('contactCampaignManagementLocationDetails') contactCampaignManagementLocationDetails: ContactCampaignManagementLocationDetailsComponent;
  @ViewChild('deletePopupValidationGroup') deletePopupValidationGroup: DxValidatorComponent;

  loaded: boolean;
  displayData: boolean;
  actions: IPageActions;

  contactCampaignType: { id: number; name: string }[] = [];

  contactCampaigns: ContactCampaignLocation[];
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  isOwner: boolean = false;
  detailsVisibility: boolean;
  customerId: string;
  items: any;
  itemsGroup: any;
  selectedContactCampaign: ContactCampaignLocation;
  groupedText: string;
  countries: any = [];
  countriesDS: any;
  counties: any = [];
  countiesDS: any;
  cities: City[] = [];
  citiesDS: any;
  continents: Continent[];
  deleteConfirmationPopup: boolean;
  selectedRowsObservationsToStop: string;

  infoButton: string;
  geographicalAreas: GeographicalArea[];
  isGeoAreaPopupOpen: boolean;
  geographicalAreaData: any;
  contactCampaignLocationXGeoAreaId: number;

  constructor(private authenticationService: AuthService,
    private translationService: TranslateService, private contactCampaignService: ContactCampaignService, private pageActionService: PageActionService,
    private notificationService: NotificationService, private route: ActivatedRoute, private clientModuleMenuItemsService: ClientModuleMenuItemsService, private helperService: HelperService,
    private countryService: CountryService, private countyService: CountyService, private cityService: CityService, private geographicalAreaService: GeographicalAreaService,
    private contactCampaignLocationGeoAreaGeoEntities: ContactCampaignLocationGeoAreGeoEntitiesService, private continentService: ContinentService) {
    this.groupedText = this.translationService.instant('groupedText');
    this.getDisplayExprGeographicalAreas = this.getDisplayExprGeographicalAreas.bind(this);
    this.setActions();
    this.contactCampaigns = [];

    
  }

 

  ngOnInit(): void {
    Promise.all([this.getContinents(), this.getCountries(), this.getCounties(), this.getCities(), this.getGeographicalAreas()]).then(() => {
      this.getData();
    })
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }

    if (this.dataGrid) {
      this.dataGrid.instance.columnOption('isActive', 'filterValue', ['1']);
    }
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && !e.data.isActive) {
      e.rowElement.style.backgroundColor = '#ffaf82';
    }
  }

  getData() {
    this.getContactCampaigns();
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }

  async getContactCampaigns() {
    this.contactCampaigns = [];
    this.loaded = true;
    await this.contactCampaignService.getAllByCustomerIdAndTypeAsync(Number(this.authenticationService.getUserCustomerId()), ContactCampaignTypeEnum.Location).then(items => {
      this.contactCampaigns = items;
      items.map(item => {
        item.parentCampaign.canModify = new Date(item.parentCampaign.endDate) < new Date();
        
        const startDate = moment(item.parentCampaign.startDate);
        const endDate = moment(item.parentCampaign.endDate);
        const daysDifference = endDate.diff(startDate, 'days');
        item.parentCampaign.numberOfDays = daysDifference;

        item.childCampaign.geographicalAreaId = item.childCampaign.geographicalAreasIds && item.childCampaign.geographicalAreasIds[0];
        return item;
      })
      this.loaded = false;
    }).catch(err => this.loaded = false)
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.contactCampaignManagement).then(result => {
      this.actions = result;
    });
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.deleteConfirmationPopup = true;
  }

  public refreshDataGrid() {
    this.getContactCampaigns();
    this.dataGrid.instance.refresh();
  }

  public deleteRecords(data: any) {
    if (this.deletePopupValidationGroup.instance.validate().isValid) {
      this.contactCampaignService.deleteMultipleAsync(this.selectedRows.map(x => x.parentCampaign.id), this.selectedRowsObservationsToStop).then(response => {
        if (response) {
          this.notificationService.alert('top', 'center', 'Campanii Contactare - Datele au fost sterse cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Campanii Contactare - Datele nu au fost sterse!', NotificationTypeEnum.Red, true)
        }
        this.deleteConfirmationPopup = false;
        this.refreshDataGrid();
      });
    }
  }

  public async openDetails(row: any) {
    this.selectedContactCampaign = row;
    this.loaded = true;
    setTimeout(async () => {
      await this.contactCampaignManagementLocationDetails.loadData().then(function () {
        this.detailsVisibility = true;
        this.loaded = false;
      }.bind(this));
    }, 0);
  }

  changeVisible() {
    this.detailsVisibility = false;
    this.refreshDataGrid();
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  async getGeographicalAreas() {
    if (this.isOwner) {
      await this.geographicalAreaService.getAllGAreaAsync().then(items => {
        this.geographicalAreas = items ? items : [];
      });
    }
    else {
      await this.geographicalAreaService.getAllGAreaByCustomerIdAsync().then(items => {
        this.geographicalAreas = items ? items : [];
      });
    }
  }

  async getContinents() {
    await this.continentService.getAllContinentAsync().then(items => {
      this.continents = items;
    });
  }

  async getCountries() {
    await this.countryService.getAllSmallCountriesAsync().then(items => {
      this.countries = items;
      this.countriesDS = {
        paginate: true,
        pageSize: 15,
        store: this.countries
      };
    });
  }

  async getCounties() {
    await this.countyService.getAllSmallCountiesAsync().then(items => {
      this.counties = items;
      this.countiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.counties
      };
    });
  }

  async getCities() {
    await this.cityService.getAllCitiesSmallAsync().then(items => {
      this.cities = items;
      this.citiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.cities
      };
    });
  }

  async onGeoAreaClick(data: any) {
    this.loaded = true;
    this.contactCampaignLocationXGeoAreaId = data.childCampaign.contactCampaignLocationXGeoAreaId;
    
    await this.contactCampaignService.getGeographicalAreaEntitiesDataAsync(data.childCampaign.geographicalAreaId).then(async geoEntitiesData => {
      this.geographicalAreaData = this.flattenTreeData(geoEntitiesData);
      
      await this.contactCampaignLocationGeoAreaGeoEntities.getByContactCampaignLocationXGeoAreIdAsync(data.childCampaign.contactCampaignLocationXGeoAreaId)
        .then(cclXGeoAreaData => {
          this.geographicalAreaData.forEach(item => {
            
            let dateData;
            if (item.type === GeographicalEntity.Continent) {
              dateData = cclXGeoAreaData.find(d => d.continentId === parseInt(item.id.split('-')[1]) && !d.countryId && !d.countyId);
            } else if (item.type === GeographicalEntity.Country) {
              dateData = cclXGeoAreaData.find(d => d.countryId === parseInt(item.id.split('-')[1]) && !d.countyId);
            } else if (item.type === GeographicalEntity.County) {
              dateData = cclXGeoAreaData.find(d => d.countyId === parseInt(item.id.split('-')[1]));
            }

            if (dateData) {
              item.startDate = dateData.startDate;
              item.endDate = dateData.endDate;

              if (item.startDate && item.endDate) {
                const startDate = moment(item.startDate);
                const endDate = moment(item.endDate);
                const daysDifference = endDate.diff(startDate, 'days');
                item.noOfDays = daysDifference;
              }
            }
          });
        });
  
      this.loaded = false;
      this.isGeoAreaPopupOpen = true;
    });
  }

  flattenTreeData(data: any[]): any[] {
    let flattened: any[] = [];
  
    data.forEach(continent => {

      let continentDetails = this.continents.find(f => f.id === continent.continentId);
      flattened.push({
        id: `Continent-${continent.continentId}`,
        name: `${continentDetails.name}`,
        type: GeographicalEntity.Continent,
        startDate: null,
        endDate: null,
        noOfDays: null
      });
  
      continent.countries.forEach(country => {
        
        let countryDetails = this.countries.find(f => f.id === country.countryId);
        flattened.push({
          id: `Country-${country.countryId}`,
          parentId: `Continent-${continent.continentId}`, 
          name: `${countryDetails.name}`,
          type: GeographicalEntity.Country,
          startDate: null,
          endDate: null,
          noOfDays: null
        });
  
        country.counties.forEach(county => {

          let countyDetails = this.counties.find(f => f.id === county.countyId);
          flattened.push({
            id: `County-${county.countyId}`,
            parentId: `Country-${country.countryId}`,
            name: `${countyDetails.name}`,
            type: GeographicalEntity.County,
            startDate: null,
            endDate: null,
            noOfDays: null
          });
        });
      });
    });
  
    return flattened;
  }

  async onSaveGeoAreaData() {
    const modifiedData = this.geographicalAreaData
      .filter(item => item.startDate && item.endDate)
      .map(item => {
        let entity: any = {
          ContactCampaignLocationXGeoAreaId: this.contactCampaignLocationXGeoAreaId,
          StartDate: item.startDate,
          EndDate: item.endDate,
          Type: item.type
        };

        if (item.type === GeographicalEntity.Continent) {
          entity.ContinentId = parseInt(item.id.split('-')[1]);
          entity.CountryId = null;
          entity.CountyId = null;
  
        } else if (item.type === GeographicalEntity.Country) {
          entity.CountryId = parseInt(item.id.split('-')[1]);
          entity.ContinentId = item.parentId ? parseInt(item.parentId.split('-')[1]) : null;
          entity.CountyId = null;
  
        } else if (item.type === GeographicalEntity.County) {
          entity.CountyId = parseInt(item.id.split('-')[1]);

          let country = this.geographicalAreaData.find(f => f.id === item.parentId);
          let continent = this.geographicalAreaData.find(f => f.id === country?.parentId);

          entity.CountryId = country ? parseInt(country.id.split('-')[1]) : null;
          entity.ContinentId = continent ? parseInt(continent.id.split('-')[1]) : null;
        }

        return entity;
      });
      
    if (modifiedData.length === 0) {
      this.notificationService.alert('top', 'center', 'Configurati cel putin 1 continent/tara/judet!', NotificationTypeEnum.Red, true);
      return;
    }

    this.loaded = true;
    await this.contactCampaignLocationGeoAreaGeoEntities.createMultipleAsync(modifiedData).then(() => {
      this.notificationService.alert('top', 'center', 'Datele au fost salvate cu succes!', NotificationTypeEnum.Green, true);
      this.loaded = false;
    })
  }

  getDisplayExprGeographicalAreas(data: any) {
    if (data && this.geographicalAreas) {
      let item = this.geographicalAreas.find(f => f.id === data);

      if (item) {
        return item.name + ' - ' + item.description;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  customDateFormat = {
    type: 'custom',
    formatter: (date: Date) => this.formatDate(date)
  };

  formatDate(date: Date): string {
    if (!date) return '';
    
    const options: Intl.DateTimeFormatOptions = { day: 'numeric', month: 'short' };
    return new Intl.DateTimeFormat('en-GB', options).format(date);
  }

  onGeoAreaRowUpdated(e: any) {
    if (e && e.data) {
      const rowIndex = e.data.id;
      const row = this.geographicalAreaData.find(item => item.id === rowIndex);

      if (row) {
        if (row.startDate && row.endDate) {        
          let startDate = moment(row.startDate);
          let endDate = moment(row.endDate); 

          if (startDate.isAfter(endDate)) {
            this.notificationService.alert('top', 'center', 'Data de inceput trebuie sa fie mai mica decat data de sfarsit!', NotificationTypeEnum.Red, true);

            row.startDate = null;
            row.endDate = null;
          }

          const overlappingRow = this.geographicalAreaData.filter(f => f.startDate && f.endDate).some(item => {
            if (item.id === row.id) return false;
            
            const existingStartDate = moment(item.startDate);
            const existingEndDate = moment(item.endDate);
    
            return (startDate.isBefore(existingEndDate) && endDate.isAfter(existingStartDate));
          });
    
          if (overlappingRow) {
            this.notificationService.alert('top', 'center', 'Intervalul configurat se suprapune cu alt interval!', NotificationTypeEnum.Red, true);
  
            row.startDate = null;
            row.endDate = null;
          }
        }
      }
    }
  }

  clearRowData(data: any) {
    data.startDate = null;
    data.endDate = null;
  }
}
