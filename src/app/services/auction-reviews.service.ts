import { Injectable } from '@angular/core';
import { AuctionReviews } from 'app/models/auction-review.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class AuctionReviewsService {

  constructor(private apiService: ApiService) { }

  async GetAllAsync(): Promise<any[]> {
    const apiURL = `/AuctionReviews/GetAll`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.mrkApi);
  }

  async GetByAuctionIdAsync(auctionId: number): Promise<any[]> {
    const apiURL = `/AuctionReviews/GetByAuctionId?auctionId=` + auctionId;
    return await this.apiService.GetAsync<any>(apiURL, Constants.mrkApi);
  }

  async CreateAsync(i: AuctionReviews): Promise<any> {
    const apiURL = `/AuctionReviews/Create`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }

  async DeleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/AuctionReviews/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
