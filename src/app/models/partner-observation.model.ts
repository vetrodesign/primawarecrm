import { PartnerObservationSourceTypeEnum } from "app/enums/partnerObservationSourceType";
import { BaseDomain } from "./base-domain.model";

export class PartnerObservation extends BaseDomain {
    salesAgentUserId: number;
    partnerId: number;
    partnerContactPersonId: number;
    description: string;
    postId: number;
    companyPost: string;
    isActive: boolean;
    source: PartnerObservationSourceTypeEnum;
}
