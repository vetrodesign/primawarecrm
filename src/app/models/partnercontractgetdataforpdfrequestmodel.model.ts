export class PartnerContractGetDataForPDFRequestModel {
    contractId: number;
    paymentTerm: string;
}
