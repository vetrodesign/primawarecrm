import { TestBed } from '@angular/core/testing';

import { PartnerCipCheckService } from './partner-cip-check.service';

describe('PartnerCipCheckService', () => {
  let service: PartnerCipCheckService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerCipCheckService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
