import { TestBed } from '@angular/core/testing';

import { ClientTurnoverService } from './client-turnover.service';

describe('ClientTurnoverService', () => {
  let service: ClientTurnoverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClientTurnoverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
