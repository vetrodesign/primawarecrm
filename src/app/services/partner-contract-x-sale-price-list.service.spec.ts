import { TestBed } from '@angular/core/testing';

import { PartnerContractXSalePriceListService } from './partner-contract-x-sale-price-list.service';

describe('PartnerContractXSalePriceListService', () => {
  let service: PartnerContractXSalePriceListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerContractXSalePriceListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
