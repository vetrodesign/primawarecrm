import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'app/services/auth.service';
import { PageActionService } from 'app/services/page-action.service';
import { TranslateService } from 'app/services/translate';

@Component({
  selector: 'app-set-data',
  templateUrl: './set-data.component.html',
  styleUrls: ['./set-data.component.css']
})
export class SetDataComponent implements OnInit {

  constructor(route: ActivatedRoute, private router: Router,
    private authService: AuthService, private translationService: TranslateService, private pageActionService: PageActionService) {
    this.authService.setClientModuleId(route.snapshot.queryParamMap.get('clientModuleId'));
    if (this.authService.getToken() && this.authService.getToken() !== route.snapshot.queryParamMap.get('token')) {
      this.authService.removeToken();
    }
    if (!this.authService.getToken()) {
      this.authService.setToken(route.snapshot.queryParamMap.get('token'));
    }
    const lang = this.translationService.getLangs().find(l => l.code === route.snapshot.queryParamMap.get('lang'));
    if (lang) {
      this.translationService.use(lang);
    }
    this.pageActionService.ensureRoleActionsCache();
    this.router.navigate(['/']);
  }

  ngOnInit(): void {
  }

}
