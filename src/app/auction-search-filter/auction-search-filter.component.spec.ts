import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionSearchFilterComponent } from './auction-search-filter.component';

describe('AuctionSearchFilterComponent', () => {
  let component: AuctionSearchFilterComponent;
  let fixture: ComponentFixture<AuctionSearchFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuctionSearchFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionSearchFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
