import { Component, OnInit, ViewChild, Input, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { DxDataGridComponent, DxTooltipComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { TurnoverSectionTypeEnum } from 'app/enums/turnoverSectionTypeEnum';
import * as _ from 'lodash';
import { TurnoverCommentComponent } from 'app/turnover-comment/turnover-comment.component';
import { PartnerXItemComment } from 'app/models/clientturnover.model';
import { PartnerItemState } from 'app/models/partneritemstate.model';
import { Brand } from 'app/models/brand.model';
import { VipOfferItems } from 'app/models/vip-offer-items.model';
import { on } from 'devextreme/events';
import { VipOfferItemsDetailsService } from 'app/services/vip-offer-items-details.service';
import { PriceCompositionTypeEnum } from 'app/enums/priceCompositionTypeEnum';
import { ItemService } from 'app/services/item.service';
import { ItemRelatedService } from 'app/services/item-related.service';
import { TurnoverPreorderService } from 'app/services/turnover-preorder.service';
import { AuthService } from 'app/services/auth.service';
import { TurnoverPreorder } from 'app/models/turnover-preorder.model';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { OfficeDrinksTurnover } from 'app/models/officedrinksturnover.model';
import { OfficeDrinksTurnoverService } from 'app/services/office-drinks-turnover.service';

@Component({
  selector: 'app-office-drinks-turnover',
  templateUrl: './office-drinks-turnover.component.html',
  styleUrls: ['./office-drinks-turnover.component.css']
})
export class OfficeDrinksTurnoverComponent implements OnInit, AfterViewInit {
  @Input() officeDrinksTurnoverActions: IPageActions;
  @Input() postId: number;
  @Input() clientId: number;
  @Input() partnerItemState: PartnerItemState[];
  @Input() brands: Brand[];
  @Input() vipOfferItems: VipOfferItems[];
  @Input() items: any[];
  @Input() customerId: number;
  @Input() showCharts: boolean;
  @Input() partners: any;
  @Input() relatedItems: any;

  @Output() foundItemBySearchFilter: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() isComponentShown: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('popupComponent') popupComponent: TurnoverCommentComponent;
  @ViewChild('tooltipItemCode') tooltipItemCode: DxTooltipComponent;
  @ViewChild('stockTooltipItemCode') stockTooltipItemCode: DxTooltipComponent;
  @ViewChild('tooltipItemName') tooltipItemName: DxTooltipComponent;
  @ViewChild('tooltipItemComment') tooltipItemComment: DxTooltipComponent;
  
  loaded: boolean;
  officeDrinksTurnover: OfficeDrinksTurnover[];
  officeDrinksTurnoverFirst10: OfficeDrinksTurnover[];
  selectedOfficeDrinksTurnover: OfficeDrinksTurnover;
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  groupedText: string;
  displayData: boolean;
  timeout: number;  
  tooltipData: any = {};
  previouslyCheckedProperty: string | null = null;
  section: number;
  stockTooltipData: any = {};
  officeDrinksTurnoverInfo: string;
  itemNameTooltipData: any = {};
  itemNameTooltipTimeout: number;
  itemCommentTooltipTimeout: number;  
  itemCommentTooltipData: any = {};
  selectedItemComments: any;
  isItemCommentsPopupOpen: boolean;

  constructor(
    private officeDrinksTurnoversService: OfficeDrinksTurnoverService, 
    private translationService: TranslateService,
    private itemService: ItemService,
    private vipOfferItemDetailsService: VipOfferItemsDetailsService,
    private notificationService: NotificationService,
    private itemXRelatedService: ItemRelatedService,
    private authenticationService: AuthService,
    private turnoverPreorderService: TurnoverPreorderService) {
      
    this.section = TurnoverSectionTypeEnum.OfficeDrinksTurnover; 
    this.groupedText = this.translationService.instant('groupedText');
    this.officeDrinksTurnover = [];

    this.officeDrinksTurnoverInfo = this.translationService.instant("officeDrinksTurnoverInfo");
  }

  ngOnInit(): void {
    if (this.officeDrinksTurnoverActions)
      this.officeDrinksTurnoverActions.CanAdd = false;
  }

  canUpdate() {
    return this.officeDrinksTurnoverActions ? this.officeDrinksTurnoverActions.CanUpdate : false;
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }

  async getData() {
    if (this.clientId && this.postId) {
      this.loaded = true;
      await this.officeDrinksTurnoversService.getData(this.clientId, this.postId, this.section).then(items => {
        this.officeDrinksTurnover = (items && items.length > 0) ? items : [];
        this.officeDrinksTurnoverFirst10 = (items && items.length > 0) ? items.slice(0, 10) : [];
        const currentDate = new Date().setHours(0, 0, 0, 0);

        this.officeDrinksTurnover.forEach(i => {
          i.itemComments = _.orderBy(i.itemComments, ['created'], ['desc']);

          i.itemComments.forEach(comment => {
            const { partnerItemStateId } = comment;
          
            // Reset all boolean values to false
            comment.isInvoiced = false;
            comment.notUsed = false;
            comment.hasStock = false;
            comment.atOrder = false;
          
            let name = this.partnerItemState?.find(x => x.id == partnerItemStateId)?.name;

            // Update the boolean value based on the partnerItemStateId
            switch (name) {
              case "Facturat":
                comment.isInvoiced = true;
                break;
              case "Nu Foloseste":
                comment.notUsed = true;
                break;
              case "Are Stoc":
                comment.hasStock = true;
                break;
              case "La Comanda":
                comment.atOrder = true;
                break;
              default:
                comment.isInvoiced = false;
                comment.notUsed = false;
                comment.hasStock = false;
                comment.atOrder = false;
                break;
            }
          });

          const todayObjects = i.itemComments.filter(obj => {
            const objDate = new Date(obj.created).setHours(0, 0, 0, 0);
            return objDate === currentDate;
          });
        
          if (todayObjects.length > 0) {
            i.lastComment = todayObjects.reduce((prev, curr) => {
              const prevDate = new Date(prev.created);
              const currDate = new Date(curr.created);
              return currDate > prevDate ? curr : prev;
            });
          } else {
            i.lastComment = new PartnerXItemComment();;
            i.lastComment.isInvoiced = false;
            i.lastComment.notUsed = false;
            i.lastComment.hasStock = false;
            i.lastComment.atOrder = false;
          }

          i.lastComment.shoppingCartQuantity = null;
        });

        this.isComponentShown.emit(this.officeDrinksTurnover && this.officeDrinksTurnover.length > 0);
        this.loaded = false;
      }); 
    }
  }

  applyColumnFilter(filterValue, columnName: string) {
    if (this.dataGrid && this.dataGrid.instance) {
      this.dataGrid.instance.option("searchPanel.text", filterValue);

      var found = false;
      if (this.officeDrinksTurnover && this.officeDrinksTurnover.length > 0 && this.officeDrinksTurnover.find(x => x.itemCode.toLowerCase().includes(filterValue?.toLowerCase())) != null) {
        found = true;
      }
      if (this.officeDrinksTurnover && this.officeDrinksTurnover.length > 0 && this.officeDrinksTurnover.find(x => x.itemName.toLowerCase().includes(filterValue?.toLowerCase())) != null) {
        found = true;
      }
      if (found) {
        this.foundItemBySearchFilter.emit(true);

        // setTimeout(() => {
        //   var scrollableContent = document.getElementById('officeDrinksTurnover');
        //   if (scrollableContent) {
        //     scrollableContent.scrollIntoView({ behavior: 'smooth' });
        //   }
        // }, 100);
      } else {
        this.foundItemBySearchFilter.emit(false);
      }
    }
}

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && e.data.lastComment != null && e.data.lastComment.isInvoiced) {
      e.rowElement.style.backgroundColor = '#a1ffa1';
    }
    if (e.rowType === 'data' && e.data && e.data.priceCompositionType == PriceCompositionTypeEnum.SinglePriceList) {
      e.rowElement.style.color = '#4169e1';
    }
    if (e.rowType === 'data' && e.data && e.data.priceCompositionType == PriceCompositionTypeEnum.SinglePriceStock) {
      e.rowElement.style.color = '#ff3729';
    }
    if (e.rowType === 'data' && e.data && e.data.priceCompositionType == PriceCompositionTypeEnum.SinglePriceCraft) {
      e.rowElement.style.color = '#01910d';
    }
  }

  getMonth(offset: number): string {
    const currentMonth = new Date().getMonth();
    const monthIndex = (currentMonth - offset + 12) % 12;
    const monthsInRomanian = [
      "Ian", "Feb", "Mar", "Apr", "Mai", "Iun",
      "Iul", "Aug", "Sep", "Oct", "Noi", "Dec"
    ];
    if (offset == 0) {
      return monthsInRomanian[monthIndex] + ' Curent';
    } else {
      return monthsInRomanian[monthIndex];
    }
  }

  public refreshDataGrid() {
    this.getData();
    this.dataGrid.instance.refresh();
  }

  public async openDetails(row: any) {
    this.selectedOfficeDrinksTurnover = row;
    setTimeout(() => {
      this.popupComponent.openPopup();
    }, 0);
  }

  public async onRowUpdated(event: any): Promise<void> {
    if (event && event.data && event.data.lastComment) {
      let comment = event.data.lastComment;

      if (comment.isInvoiced || comment.hasStock || comment.atOrder) {
        this.openDetails(event.data);
        return;
      }
      
      if (comment.notUsed) {
        this.popupComponent.saveComment(event.data.lastComment, event.data.itemId);
      }
    }

  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";

      if (event.dataField === 'lastComment.comment' && event.row && event.row.data) {
        const rowData = event.row.data;
        const allowEditing = rowData?.lastComment?.notUsed ||
                            rowData?.lastComment?.hasStock ||
                            rowData?.lastComment?.isInvoiced ||
                            rowData?.lastComment?.atOrder;
        event.editorOptions.disabled = !allowEditing;
      }

      if (event.dataField === 'lastComment.isInvoiced' && event.row && event.row.data) {
        const rowData = event.row.data;
        const allowEditing = !rowData?.lastComment?.notUsed &&
                            !rowData?.lastComment?.hasStock &&
                            !rowData?.lastComment?.atOrder;
        event.editorOptions.disabled = !allowEditing;
      }

      if (event.dataField === 'lastComment.notUsed' && event.row && event.row.data) {
        const rowData = event.row.data;
        const allowEditing = !rowData?.lastComment?.isInvoiced &&
                            !rowData?.lastComment?.hasStock &&
                            !rowData?.lastComment?.atOrder;
        event.editorOptions.disabled = !allowEditing;
      }

      if (event.dataField === 'lastComment.hasStock' && event.row && event.row.data) {
        const rowData = event.row.data;
        const allowEditing = !rowData?.lastComment?.notUsed &&
                            !rowData?.lastComment?.isInvoiced &&
                            !rowData?.lastComment?.atOrder;
        event.editorOptions.disabled = !allowEditing;
      }

      if (event.dataField === 'lastComment.atOrder' && event.row && event.row.data) {
        const rowData = event.row.data;
        const allowEditing = !rowData?.lastComment?.notUsed &&
                            !rowData?.lastComment?.hasStock &&
                            !rowData?.lastComment?.isInvoiced;
        event.editorOptions.disabled = !allowEditing;
      }
  }

  isItemOnVIP(itemId: number) {
    return (this.vipOfferItems.filter(x => x.itemId == itemId).length > 0);
  }

  onCellPrepared(e) {
    this.setItemCodeTooltip(e);
    this.setItemNameTooltip(e);
    this.setItemCommentTooltip(e);
    if (e.rowType === 'data') {
      e.cellElement.style.paddingTop = '2px';
      e.cellElement.style.paddingBottom = '1px';
    }

    if (e && e.rowType === 'header' && e.column.dataField === 'itemName') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Doriti sa va adaug si o cafea? Ce anume beti la Birou si nu gasiti la noi?");
      $(cellElement).tooltip({
        placement: 'top'
      });
    }

    if (e && e.rowType === 'header' && e.column.dataField === 'lastComment.shoppingCartQuantity') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Introduceti cantitatea si adaugati in cos. Un comentariu se va crea automat!");
      $(cellElement).tooltip({
        placement: 'top'
      });
    }
  }

  setItemCommentTooltip(event) {
    if (event && event.rowType === 'data' && event.column.dataField === 'lastComment.comment') {
      on(event.cellElement, 'mouseover', arg => {
        this.itemCommentTooltipTimeout = window.setTimeout(async () => {

          this.itemCommentTooltipData = null;
          if (event.data.itemComments && event.data.itemComments.length > 0) {
            let orderedComments = _.orderBy(event.data.itemComments, 'created', ['desc']);
            let lastComment = orderedComments[0].comment;

            this.itemCommentTooltipData = lastComment;
          } else {
            this.itemCommentTooltipData = [];
          }

          this.tooltipItemComment.instance.show(arg.target);
        }, 1000);
      });

      on(event.cellElement, 'mouseout', arg => {
        if (this.itemCommentTooltipTimeout) {
          window.clearTimeout(this.itemCommentTooltipTimeout);
        }
        this.tooltipItemComment.instance.hide();
      });
    }
  }

  setItemCodeTooltip(event) {
    if (event && event.rowType === 'data' && event.column.dataField === 'itemCode') {
      on(event.cellElement, 'mouseover', arg => {
        this.timeout = window.setTimeout(async () => {

          this.tooltipData = null;
          let stockPromise = this.vipOfferItemDetailsService.GetTiersForItemAsync(null, event.data.itemId, this.clientId).then(r => {
            if (r && r.length > 0) {
              this.tooltipData = r;
            } else { this.tooltipData = []; }
          });

          Promise.all([stockPromise]).then(r => {this.tooltipItemCode.instance.show(arg.target);})
        }, 1000);
      });

      on(event.cellElement, 'mouseout', arg => {
        if (this.timeout) {
          window.clearTimeout(this.timeout);
        }
        this.tooltipItemCode.instance.hide();
      });
    }
    if (event && event.rowType === 'data' && event.column.dataField === 'currentStock') {
      on(event.cellElement, 'mouseover', arg => {
        this.timeout = window.setTimeout(async () => {

          this.stockTooltipData = null;
          let stockPromise = this.itemService.GetStocksOnSiteAsync(event.data.itemId).then(r => {
            if (r && r.length > 0) {
              this.stockTooltipData = r;
            } else { this.stockTooltipData = []; }
          });

          Promise.all([stockPromise]).then(r => {this.stockTooltipItemCode.instance.show(arg.target);})
        }, 1000);
      });

      on(event.cellElement, 'mouseout', arg => {
        if (this.timeout) {
          window.clearTimeout(this.timeout);
        }
        this.stockTooltipItemCode.instance.hide();
      });
    }
  }

  priceCurrencyExpr(data) {
    if (!data || !data?.listPrice) {
      return '';
    }
    return data.listPrice.toFixed(2) + ' ' + data.currencyName;
  }

  setItemNameTooltip(event) {
    if (event && event.rowType === 'data' && event.column.dataField === 'itemName') {
      on(event.cellElement, 'mouseover', arg => {
        this.itemNameTooltipTimeout = window.setTimeout(async () => {

          this.itemNameTooltipData = null;
          let itemXRelatedPromise = this.itemXRelatedService.getItemXRelatedByItemIdForTurnoverAsync(this.clientId, event.data.itemId).then(r => {
            if (r && r.length > 0) {
              let icon = '';       
              let iconName = '';     

              this.itemNameTooltipData = r.map(item => {
                switch(item.relatedItemTypeId) {
                  case 1:
                    icon = 'equivalence-icon.png';
                    iconName = 'Similar';
                    break;
                  case 2:
                    icon = 'complementarity-icon.png';
                    iconName = 'Complementar';
                    break;
                  case 3:
                    icon = 'superiority-icon.png';
                    iconName = 'Superior';
                    break;
        
                  case 4:
                    icon = 'inferiority-icon.png';
                    iconName = 'Inferior';
                    break;
        
                  case 5:
                    icon = 'concurrency-icon.png';
                    iconName = 'Concurent';
                    break;
        
                  default:
                    break;
                }

                item.icon = icon;
                item.iconName = iconName;
                item.quantity = null; // shopping cart quantity
                return item;
              })
            } else { this.itemNameTooltipData = []; }
          });

          Promise.all([itemXRelatedPromise]).then(r => {this.tooltipItemName.instance.show(arg.target);})
        }, 1000);
      });

      on(event.cellElement, 'mouseout', arg => {
        if (this.itemNameTooltipTimeout) {
          window.clearTimeout(this.itemNameTooltipTimeout);
        }
        this.tooltipItemName.instance.hide();
      });
    }
  }

  getRelatedItemPrice(data) {
    if (!data)
      return '';

    return data.price.toFixed(2) + ' ' + data.currencyName;
  }

  onHidingRelatedItemsTooltip(e) {
    const isMouseInsidePopover = e.component.content().parentNode.matches(":hover");
    e.cancel = isMouseInsidePopover;
    if (!e.component.content().parentNode.getAttribute("mouselistener")) {
      e.component.content().parentNode.addEventListener("mouseleave", (arg) => {
        e.component.hide();
      });
      e.component.content().parentNode.setAttribute("mouselistener", true);
    }
  }

  hasItemsRelated(itemId: number) {
    return (this.relatedItems.filter(x => x.itemId == itemId).length > 0);
  }

  async addRelatedItemToCart(itemRelated: any) {
    let createPreoder = new TurnoverPreorder();
    createPreoder.customerId = Number(this.authenticationService.getUserCustomerId());
    createPreoder.partnerId = this.clientId;
    createPreoder.postId = Number(this.authenticationService.getUserPostId());
    createPreoder.itemId = itemRelated.itemId;
    createPreoder.quantity = itemRelated.quantity;
    createPreoder.price = itemRelated.price;

    this.loaded = true;
    await this.turnoverPreorderService.createTurnoverPreorderAsync(createPreoder).then(() => {
      
      this.notificationService.alert('top', 'center', 'Produsul a fost adaugat in cos!', NotificationTypeEnum.Green, true)
      this.loaded = false;
    })
  }

  addItemToCart(item) {
    if (item.lastComment.shoppingCartQuantity) {
      item.lastComment.isInvoiced = true;
      item.lastComment.shoppingCartQuantity = Number(item.lastComment.shoppingCartQuantity);
      
      this.popupComponent.saveComment(item.lastComment, item.itemId);
    }
  }

  async addItemToCartFromVipTiers(vipIndex: number, itemId: number, quantity: number) {
    let createPreoder = new TurnoverPreorder();
    createPreoder.customerId = Number(this.authenticationService.getUserCustomerId());
    createPreoder.partnerId = this.clientId;
    createPreoder.postId = Number(this.authenticationService.getUserPostId());
    createPreoder.itemId = itemId;
    createPreoder.quantity = quantity;
    createPreoder.priceSource = vipIndex;

    this.loaded = true;
    await this.turnoverPreorderService.createTurnoverPreorderAsync(createPreoder).then(() => {
      
      this.notificationService.alert('top', 'center', 'Produsul a fost adaugat in cos!', NotificationTypeEnum.Green, true);
      this.loaded = false;
    })
  }

  onHidingVipTiersTooltip(e) {
    const isMouseInsidePopover = e.component.content().parentNode.matches(":hover");
    e.cancel = isMouseInsidePopover;
    if (!e.component.content().parentNode.getAttribute("mouselistener")) {
      e.component.content().parentNode.addEventListener("mouseleave", (arg) => {
        e.component.hide();
      });
      e.component.content().parentNode.setAttribute("mouselistener", true);
    }
  }

  onHidingItemCommentTooltip(e) {
    const isMouseInsidePopover = e.component.content().parentNode.matches(":hover");
    e.cancel = isMouseInsidePopover;
    if (!e.component.content().parentNode.getAttribute("mouselistener")) {
      e.component.content().parentNode.addEventListener("mouseleave", (arg) => {
        e.component.hide();
      });
      e.component.content().parentNode.setAttribute("mouselistener", true);
    }
  }

  openItemComments(row: any) {
    this.isItemCommentsPopupOpen = true;
    this.selectedItemComments = _.orderBy(row.itemComments, 'created', ['desc']);
    this.selectedItemComments.forEach(i => {
      if (i.isInvoiced) {
        i.status = 'Facturat';
      }
      if (i.hasStock) {
        i.status = 'Are stoc';
      }
      if (i.atOrder) {
        i.status = 'La comanda ';
      }
      if (i.notUsed) {
        i.status = 'Nu folosesc';
      }
    });
  }
}
