export enum PriceCompositionTypeEnum {
  SinglePriceList = 1,
  DiscountGridPrice = 2,
  SinglePriceStock = 4,
  SinglePriceCraft = 5
}

export enum SalePriceListCompositionTypeEnum {
  SinglePrice = 1,
  DiscountGridPrice = 2,
  Special = 3
}

