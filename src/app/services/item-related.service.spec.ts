import { TestBed } from '@angular/core/testing';

import { ItemRelatedService } from './item-related.service';

describe('ItemRelatedService', () => {
  let service: ItemRelatedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemRelatedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
