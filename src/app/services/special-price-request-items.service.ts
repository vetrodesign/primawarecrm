import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { PriceApprovedTooltipDataRequest, SpecialPriceRequestItem, SpecialPriceRequestItemData, UpdateAndValidateSpecialPriceRequestItems } from 'app/models/special-price-request.model';

@Injectable({
  providedIn: 'root'
})
export class SpecialPriceRequestItemsService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<SpecialPriceRequestItem[]> {
    const apiURL = `/SpecialPriceRequestItems/GetAll`;
    return await this.apiService.GetAsync<SpecialPriceRequestItem[]>(apiURL, Constants.mrkApi);
  }

  async getAllByPartnerIdAsync(partnerId: number, shouldGetHistory?: boolean): Promise<SpecialPriceRequestItem[]> {
    const apiURL = `/SpecialPriceRequestItems/GetByPartnerId/`;
    return await this.apiService.PostAsync<any>(apiURL, { partnerId, shouldGetHistory }, Constants.mrkApi);
  }

  async getPriceApprovedTooltipData(request: PriceApprovedTooltipDataRequest): Promise<any> {
    const apiURL = `/SpecialPriceRequestItems/GetPriceApprovedTooltipData`;
    return await this.apiService.PostAsync<any>(apiURL, request, Constants.mrkApi);
  }

  async getItemPrices(itemId: number): Promise<any> {
    const apiURL = `/SpecialPriceRequestItems/getItemPrices`;
    return await this.apiService.PostAsync<any>(apiURL, itemId, Constants.mrkApi);
  }

  async createAsync(m: SpecialPriceRequestItem): Promise<any> {
    const apiURL = `/SpecialPriceRequestItems/create`;
    return await this.apiService.PostAsync<any>(apiURL, m, Constants.mrkApi);
  }

  async createMultipleAsync(m: SpecialPriceRequestItemData): Promise<any> {
    const apiURL = `/SpecialPriceRequestItems/CreateMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, m, Constants.mrkApi);
  }

  async updateAsync(m: UpdateAndValidateSpecialPriceRequestItems): Promise<any> {
    const apiURL = `/SpecialPriceRequestItems/update`;
    return await this.apiService.PutAsync<any>(apiURL, m, Constants.mrkApi);
  }
}
