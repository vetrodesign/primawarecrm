import { TestBed } from '@angular/core/testing';

import { SupplierAcceptedService } from './supplier-accepted.service';

describe('SupplierAcceptedService', () => {
  let service: SupplierAcceptedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SupplierAcceptedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
