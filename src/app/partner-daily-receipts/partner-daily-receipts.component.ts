import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { PartnerDailyReceipts } from 'app/models/partner-daily-receipts.model';
import { HelperService } from 'app/services/helper.service';
import { PartnerDailyReceiptsService } from 'app/services/partner-daily-receipts.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent } from 'devextreme-angular';
import * as moment from 'moment';

@Component({
  selector: 'app-partner-daily-receipts',
  templateUrl: './partner-daily-receipts.component.html',
  styleUrls: ['./partner-daily-receipts.component.css']
})
export class PartnerDailyReceiptsComponent implements OnInit, AfterViewInit {
  partnerDailyReceipts: PartnerDailyReceipts[] = [];

  actions: IPageActions;
  groupedText: string;
  loaded: boolean = false;
  lastFileUpdate: string;

  partnerDailyReceiptsInfo: string;

  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;

  constructor(
    private translationService: TranslateService,
    private partnerDailyReceiptsService: PartnerDailyReceiptsService,
    private helperService: HelperService
  ) {
    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();
    this.getData();
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  getData() {
    this.loaded = true;
    Promise.all([this.getAllPartnerDailyReceipts()]).then(r => {
      this.loaded = false;
    });
  }

  private async getAllPartnerDailyReceipts() {
    await this.partnerDailyReceiptsService.getAllAsync().then(items => {
      if (items && items.length > 0) {
        this.partnerDailyReceipts = items;
        this.partnerDailyReceipts.map(m => m.processingDate = moment(new Date(m.processingDate)).format('DD.MM.YYYY').toString());
        this.lastFileUpdate = this.partnerDailyReceipts[0].fileLastWriteTime 
          && this.helperService.padNumber(new Date(this.partnerDailyReceipts[0].fileLastWriteTime).getHours()) + ":" + this.helperService.padNumber(new Date(this.partnerDailyReceipts[0].fileLastWriteTime).getMinutes());
      }
    });
  }

  setActions() {
    this.actions = { CanView: true, CanAdd: false, CanUpdate: false, CanDelete: false, 
    CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
  }

  refreshDataGrid() {
    this.getData();
    this.dataGrid.instance.refresh();
  }

  onRowPrepared(e) {
    if (e.rowType === "data") {
      if (e.data.currency === "EUR") {
        e.rowElement.style.color = "#0bc904";
      }

      if (e.data.currency === "BGN") {
        e.rowElement.style.color = "#c404d9";
      }

      if (e.data.currency === "HUF") {
        e.rowElement.style.color = "#1991fa";
      }
    }
  }
}
