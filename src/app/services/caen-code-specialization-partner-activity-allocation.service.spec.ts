import { TestBed } from '@angular/core/testing';

import { CaenCodeSpecializationPartnerActivityAllocationService } from './caen-code-specialization-partner-activity-allocation.service';

describe('CaenCodeSpecializationPartnerActivityAllocationService', () => {
  let service: CaenCodeSpecializationPartnerActivityAllocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CaenCodeSpecializationPartnerActivityAllocationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
