import { Component, OnInit, AfterViewInit, ViewChild, Input } from "@angular/core";
import { NotificationTypeEnum } from "app/enums/notificationTypeEnum";
import { GridToolbarComponent } from "app/helpers/grid-toolbar/grid-toolbar.component";
import { IPageActions } from "app/models/ipageactions";
import { AuthService } from "app/services/auth.service";
import { NotificationService } from "app/services/notification.service";
import { TranslateService } from "app/services/translate";
import { DxDataGridComponent } from "devextreme-angular";
import { AuctionReviews } from "app/models/auction-review.model";
import { AuctionReviewsService } from "app/services/auction-reviews.service";
import { ImportAuction } from "app/models/importauction.model";

@Component({
  selector: 'app-auction-reviews',
  templateUrl: './auction-reviews.component.html',
  styleUrls: ['./auction-reviews.component.css']
})
export class AuctionReviewsComponent implements OnInit, AfterViewInit {
  @ViewChild('auctionReviewDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('auctionReviewGridToolbar') gridToolbar: GridToolbarComponent;

  @Input() selectedAuction: ImportAuction;
  @Input() users: any;

  displayData: boolean;
  loaded: boolean;
  actions: IPageActions;
  auctionReviews: AuctionReviews[] = [];
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  groupedText: string;
  isTabActive: string;
  isOwner: boolean;
  constructor(
    private auctionReviewsService: AuctionReviewsService,
    private translationService: TranslateService,
    private notificationService: NotificationService,
    private authService: AuthService) {
    this.isOwner = this.authService.isUserOwner();
    this.groupedText = this.translationService.instant('groupedText');
    this.isTabActive = this.authService.getCustomerPageTitleTheme();
    this.setActions();
  }

  ngOnInit(): void {
  }

  async getData() {
    this.loaded = true;
    if (this.isOwner) {
      await this.auctionReviewsService.GetAllAsync().then(items => {
        this.auctionReviews = (items && items.length >  0) ? items : [];
        this.loaded = false;
      });
    }
    else {
      await this.auctionReviewsService.GetByAuctionIdAsync(this.selectedAuction.id).then(items => {
        this.auctionReviews = (items && items.length >  0) ? items : [];
        this.loaded = false;
      });
    }
    this.dataGrid.dataSource = this.auctionReviews;
  }

  loadData() {
    this.getData();
    this.setGridInstance();
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }


  ngAfterViewInit() {
    this.setGridInstance();
  }

  setGridInstance() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  setActions() {
    this.actions = {
      CanView: true,
      CanAdd: true,
      CanUpdate: false,
      CanDelete: false,
      CanPrint: true,
      CanExport: true,
      CanImport: true,
      CanDuplicate: true
    };
  }
  public add() {
    this.dataGrid.instance.addRow();
  }

  public refreshDataGrid() {
    this.getData();
    this.dataGrid.instance.refresh();
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public async openDetails(row: any) {
    this.dataGrid.instance.editRow(row.rowIndex);
    this.rowIndex = row.rowIndex;
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  deleteRecords(e: any) {

  }

  public async onRowInserting(event: any): Promise<void> {
    let item = new AuctionReviews();
    item = event.data;
    item.auctionId = this.selectedAuction.id;
    if (item) {
      await this.auctionReviewsService.CreateAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Observatie Licitatie - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Observatie Licitatie - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshDataGrid();
      });
    }
  }

  getDisplayUserExpr(item) {
    if (!item) {
      return '';
    }
    return item.firstName + ' ' + item.lastName;
  }
}


