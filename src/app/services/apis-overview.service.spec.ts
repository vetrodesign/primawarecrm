import { TestBed } from '@angular/core/testing';

import { ApisOverviewService } from './apis-overview.service';

describe('ApisOverviewService', () => {
  let service: ApisOverviewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApisOverviewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
