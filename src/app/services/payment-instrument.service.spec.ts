import { TestBed } from '@angular/core/testing';

import { PaymentInstrumentService } from './payment-instrument.service';

describe('PaymentInstrumentService', () => {
  let service: PaymentInstrumentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaymentInstrumentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
