export enum EndureTransportEnum {
    Client = 1,
    Vetro = 2
}

export enum OrderTypeEnum {
    MonthlyOrder = 1,
    QuantityOrder = 2
}

export enum DeliveryTypeEnum {
    Centralized = 1,
    AtEachSite = 2
}