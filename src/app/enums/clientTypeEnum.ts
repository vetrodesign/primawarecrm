export enum ClientTypeEnum {
  None = 0,
  Active = 1,
  Possible = 2,
}

export enum ActiveClientTypeEnum {
  Lost = 1,
  New = 2
}