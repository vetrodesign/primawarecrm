import { Component, OnInit, ViewChild, Input, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { PartnerCallReminderService } from 'app/services/partner-call-reminder.service';
import { PartnerCallReminder } from 'app/models/partnercallreminder.model';
import * as moment from 'moment';
import * as _ from 'lodash';
import { PartnerLocationContact } from 'app/models/partnerlocationcontact.model';
import { PartnerTurnoverContactService } from 'app/services/partner-turnover-contact.service';
import { PartnerLocationContactService } from 'app/services/partner-location-contact.service';
import { PartnerTurnoverContact } from 'app/models/partnerturnovercontact.model';

@Component({
  selector: 'app-partner-call-reminder',
  templateUrl: './partner-call-reminder.component.html',
  styleUrls: ['./partner-call-reminder.component.css']
})
export class PartnerCallReminderComponent implements OnInit, AfterViewInit {
  @Input() partnerCallReminderActions: IPageActions;
  @Input() postId: number;
  @Input() clientId: number;
  @Input() customerId: number;
  @Input() showCharts: boolean;
  @Output() partnerLocationContactIdChange: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;

  loaded: boolean;
  actions: IPageActions;
  partnerCallReminders: PartnerCallReminder[];
  locationContactPersons: PartnerLocationContact[];
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  groupedText: string;
  currentDate: Date = new Date();
  callReminderDate: Date;
  locationContactId: number;
  displayData: boolean;
  isOnSave: number;
  isOnSaveContact: number;
  initialCallReminderDate: Date;
  initialLocationContactId: number;
  partnerTurnoverContact: any;

  constructor(
    private partnerCallRemindersService: PartnerCallReminderService, 
    private partnerLocationContactService: PartnerLocationContactService,
    private partnerTurnoverContactService: PartnerTurnoverContactService,
    private translationService: TranslateService,
    private notificationService: NotificationService) {
      
    this.groupedText = this.translationService.instant('groupedText');
    this.partnerCallReminders = [];
  }

  ngOnInit(): void {
  }

  canUpdate() {
    return this.partnerCallReminderActions ? this.partnerCallReminderActions.CanUpdate : false;
  }

  canDelete() {
    return false;
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }


  async getData() {
    if (this.clientId && this.postId) {
      this.loaded = true;
       Promise.all([this.getPartnerContact(), this.getPartnerCallReminders(), this.getPartnerTurnoverContact()]).then(x => {
      this.loaded = false;
    });
    }
  }

  async getPartnerCallReminders(): Promise<void> {
    await this.partnerCallRemindersService.getByPartnerAndPostIdAsync(this.clientId, this.postId).then(items => {
      if (items && items.length > 0) {
        this.partnerCallReminders = items;
        this.partnerCallReminders.forEach(i => {
          i.startDate = moment(i.dateAndTimeToRecall).add(1, 'hour').toDate();
          i.endDate = moment(i.dateAndTimeToRecall).add(2, 'hour').toDate();
          i.text = 'De Revenit';
        });
        this.callReminderDate =_.maxBy(this.partnerCallReminders, 'dateAndTimeToRecall')?.dateAndTimeToRecall;
        this.initialCallReminderDate = _.maxBy(this.partnerCallReminders, 'dateAndTimeToRecall')?.dateAndTimeToRecall;
        this.isOnSave = _.maxBy(this.partnerCallReminders, 'dateAndTimeToRecall')?.id;
      } else {
        this.partnerCallReminders = [];
        this.callReminderDate = null;
        this.initialCallReminderDate = null;
        this.isOnSave = null;
      }
    });
  }

  async getPartnerTurnoverContact(): Promise<void> {
    await this.partnerTurnoverContactService.getByPartnerAndPostIdAsync(this.clientId, this.postId).then(item => {
      if (item) {
        this.partnerTurnoverContact = item;
        this.locationContactId = item.partnerLocationContactId;
        this.initialLocationContactId = item.partnerLocationContactId;
        this.isOnSaveContact = item.id;
      } else {
        this.locationContactId = null;
        this.initialLocationContactId = null;
        this.isOnSaveContact = null;
      }

      this.partnerLocationContactIdChange.emit(this.locationContactId);
    });
  }

  async getPartnerContact(): Promise<void> {
    await this.partnerLocationContactService.getPartnerLocationContactByPartnerID(this.clientId).then(items => {
      if (items && items.length > 0) {
        this.locationContactPersons = items;
      } else {
        this.locationContactPersons = null;
      }
    });
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  public add() {
    this.dataGrid.instance.addRow();
  }

  public refreshDataGrid() {
    this.getData();
    this.dataGrid.instance.refresh();
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
}

  public deleteRecords(data: any) {
    this.partnerCallRemindersService.delete(this.selectedRows.map(x => x.id)).then(response => {
      if (!response) {
        this.notificationService.alert('top', 'center', 'De revenit clinent - Datele au fost sterse cu succes!',
         NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'De revenit clinent - Datele nu au fost sterse!',
         NotificationTypeEnum.Red, true)
      }
      this.refreshDataGrid();
    });
  }

  public async openDetails(row: any) {
    this.dataGrid.instance.editRow(row.rowIndex);
    this.rowIndex = row.rowIndex;
  }

  public async onRowUpdated(event: any): Promise<void> {
    let item = new PartnerCallReminder();
    item = this.partnerCallReminders.find(g => g.id === event.key.id);
    if (item) {
      await this.partnerCallRemindersService.update(item).then(r => {
        this.notificationService.alert('top', 'center', 'De revenit clinent - Datele au fost modificate cu succes!',
        NotificationTypeEnum.Green, true)
        this.refreshDataGrid();
      }).catch(ex => {
        this.notificationService.alert('top', 'center', 'De revenit clinent - Datele nu au fost modificate! A aparut o eroare!',
        NotificationTypeEnum.Red, true)
      });
    }
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async onRowInserting(event: any): Promise<void> {
    let item = new PartnerCallReminder();
    item = event.data;
    item.postId = this.postId;
    item.partnerId = this.clientId;
    item.dateAndTimeToRecall = moment(item.dateAndTimeToRecall).add(3, 'hours').toDate();
    if (item) {
      await this.partnerCallRemindersService.create(item).then(r => {
        this.notificationService.alert('top', 'center', 'De revenit clinent - Datele au fost inserate cu succes!',
        NotificationTypeEnum.Green, true)
        this.refreshDataGrid();
      }).catch(ex =>{
        this.notificationService.alert('top', 'center', 'De revenit clinent - Datele nu au fost inserate! A aparut o eroare!',
        NotificationTypeEnum.Red, true)
      });
    }
  }

  onCellPrepared(e) {
    if (e.rowType === 'data') {
      e.cellElement.style.paddingTop = '2px';
      e.cellElement.style.paddingBottom = '1px';
    }
  }

  getDisplayExprContact(item) {
    if (!item) {
        return '';
      }
      return  item.phoneNumber + '  ' + item.firstName + '  ' + item.lastName ;
  }

  async updateCallReminder(event: any) {
    if (event && event.value) {
      if (new Date(this.initialCallReminderDate).getTime() === new Date(event.value).getTime()) {
        return;
      }

      if (this.isOnSave == null || this.isOnSave == undefined) {
        //create
        let item = new PartnerCallReminder();
        item.startDate = moment(this.callReminderDate).add(1, 'hour').toDate();
        item.endDate = moment(this.callReminderDate).add(2, 'hour').toDate();
        item.text = 'De Revenit';
        item.postId = this.postId;
        item.partnerId = this.clientId;
        item.dateAndTimeToRecall = moment(this.callReminderDate).add(3, 'hours').toDate();
        if (item) {
          await this.partnerCallRemindersService.create(item).then(r => {
            this.notificationService.alert('top', 'center', 'De revenit clinent - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
            this.getData();
          }).catch(ex =>{
            this.notificationService.alert('top', 'center', 'De revenit clinent - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
          });
        }
      } else {
        //update
        let item = new PartnerCallReminder();
        item = this.partnerCallReminders.find(g => g.id === this.isOnSave);
        item.dateAndTimeToRecall = moment(this.callReminderDate).add(3, 'hours').toDate();
        if (item) {
          await this.partnerCallRemindersService.update(item).then(r => {
            this.notificationService.alert('top', 'center', 'De revenit clinent - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
            this.getData();
          }).catch(ex => {
            this.notificationService.alert('top', 'center', 'De revenit clinent - Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
          });
        }
      }
    }
  }

  async updateContactPerson(event: any) {
    if (event && event.value) {
      if (this.initialLocationContactId === event.value) {
        return;
      }

    if (this.isOnSaveContact == null || this.isOnSaveContact == undefined) {
        //create
        let item = new PartnerTurnoverContact();
        item.customerId = this.customerId;
        item.postId = this.postId;
        item.partnerId = this.clientId;
        item.partnerLocationContactId = this.locationContactId;
        item.dateAndTimeOfContact = new Date();
        if (item) {
          await this.partnerTurnoverContactService.createAsync(item).then(r => {
            this.notificationService.alert('top', 'center', 'Persoana de contact - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
            this.getData();
          }).catch(ex =>{
            this.notificationService.alert('top', 'center', 'Persoana de contact  - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
          });
        }
      } else {
        //update 
        await this.partnerTurnoverContactService.updateAsync({ ...this.partnerTurnoverContact, partnerLocationContactId: this.locationContactId }).then(r => {
          this.notificationService.alert('top', 'center', 'Persoana de contact - Datele au fost modificate cu succes!',
          NotificationTypeEnum.Green, true)
          this.getData();
        }).catch(ex => {
          this.notificationService.alert('top', 'center', 'Persoana de contact - Datele nu au fost modificate! A aparut o eroare!',
          NotificationTypeEnum.Red, true)
        });   
      }
    }
  }
}

