import { TestBed } from '@angular/core/testing';

import { PartnerReviewService } from './partner-review.service';

describe('PartnerReviewService', () => {
  let service: PartnerReviewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerReviewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
