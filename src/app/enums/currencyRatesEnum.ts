export enum CurrencyTypeEnum {
    RON = 1,
    EUR = 2,
    USD = 3,
    GBP = 4,
    CNY = 5,
    BGN = 19,
    HUF = 61
}

export enum RateTypeEnum {
    BCR = 1,
    BNR = 2
}