import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Constants } from 'app/constants';
import { IPageActions } from 'app/models/ipageactions';
import { PageActionService } from 'app/services/page-action.service';
import { AuthService } from 'app/services/auth.service';
import { UsersService } from 'app/services/user.service';
import { PartnerCallReminderComponent } from 'app/partner-call-reminder/partner-call-reminder.component';
import { ClientTurnoverComponent } from 'app/client-turnover/client-turnover.component';
import { PartnerActivityTurnoverComponent } from 'app/partner-activity-turnover/partner-activity-turnover.component';
import { NewItemsTurnoverComponent } from 'app/new-items-turnover/new-items-turnover.component';
import { YearlyTurnoverComponent } from 'app/yearly-turnover/yearly-turnover.component';
import { PartnerLocationContactService } from 'app/services/partner-location-contact.service';
import { PartnerLocationContact } from 'app/models/partnerlocationcontact.model';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { ClientTurnoverService } from 'app/services/client-turnover.service';
import { SendOfferToPartnerLogService } from 'app/services/send-offer-to-partner-log.service';
import { OfficeItemsTurnoverComponent } from 'app/office-items-turnover/office-items-turnover.component';
import { MandatoryPromotionTurnoverComponent } from 'app/mandatory-promotion-turnover/mandatory-promotion-turnover.component';
import { PartnerService } from 'app/services/partner.service';
import { PartnerItemState } from 'app/models/partneritemstate.model';
import { BrandService } from 'app/services/brand.service';
import { Brand } from 'app/models/brand.model';
import { ItemService } from 'app/services/item.service';
import { LiquidationOfferTurnoverComponent } from 'app/liquidation-offer-turnover/liquidation-offer-turnover.component';
import { ItemGroupProposed } from 'app/models/itemgroupproposed.model';
import { ItemGroupProposedService } from 'app/services/item-group-proposed.service';
import { DxDataGridComponent, DxTooltipComponent, DxValidatorComponent } from 'devextreme-angular';
import { AgentOtherItemTurnoverComponent } from 'app/agent-other-item-turnover/agent-other-item-turnover.component';
import { PartnerXItemComment } from 'app/models/clientturnover.model';
import { TurnoverSectionTypeEnum } from 'app/enums/turnoverSectionTypeEnum';
import { PartnerSuggestionComponent } from 'app/partner-suggestion/partner-suggestion.component';
import { VipOfferItemsService } from 'app/services/vip-offer-items.service';
import { VipOfferItems } from 'app/models/vip-offer-items.model';
import { environment } from 'environments/environment';
import { ActivatedRoute } from '@angular/router';
import { PartnerReviewService } from 'app/services/partner-review.service';
import { PartnerSearchFilter } from 'app/models/partnerSearchFilter.model';
import { PartnerFinancialInfoService } from 'app/services/partner-financial-info.service';
import { DiscountGrid } from 'app/models/discount-grid.model';
import { DiscountGridDetailsService } from 'app/services/discount-grid-details.service';
import { SalePriceList } from 'app/models/salepricelist.model';
import { TurnoverPreorderService } from 'app/services/turnover-preorder.service';
import { TurnoverPreorder } from 'app/models/turnover-preorder.model';
import { Management } from 'app/models/management.model';
import { ManagementService } from 'app/services/management.service';
import { on } from 'devextreme/events';
import { PartnerLocation } from 'app/models/partnerlocation.model';
import { DaysOfWeek } from 'app/enums/dayOfTheWeekEnum';
import { TranslateService } from 'app/services/translate';
import { PartnerLocationService } from 'app/services/partner-location.service';
import { PartnerLocationScheduleComponent } from 'app/partner-location-schedule/partner-location-schedule.component';
import * as moment from 'moment';
import { SiteService } from 'app/services/site.service';
import { Site } from 'app/models/site.model';
import { DiscountGridService } from 'app/services/discount-grid.service';
import { SalePriceListService } from 'app/services/sale-price-list.service';
import { PartnerContactPreferences } from 'app/models/partner-contact-preferences.model';
import { PartnerContactPreferencesService } from 'app/services/partner-contact-preferences.service';
import { DeliveryOfferPreferences } from 'app/enums/deliveryOfferPreferencesEnum';
import * as _ from 'lodash';
import { PartnerSuggestionStateEnum } from 'app/enums/partnerSuggestionState.enum';
import { HelperService } from 'app/services/helper.service';
import { VipOfferItemsDetailsService } from 'app/services/vip-offer-items-details.service';
import { ProductConventionEnum } from 'app/enums/productConventionEnum';
import { ItemRelatedService } from 'app/services/item-related.service';
import { TurnoverPreorderPriceSourceEnum } from 'app/enums/turnoverPreorderPriceSourceEnum';
import { OfficeDrinksTurnoverComponent } from 'app/office-drinks-turnover/office-drinks-turnover.component';
import { CountyService } from 'app/services/county.service';
import { AgentOtherItemTurnoverService } from 'app/services/agent-other-item-turnover.service';
import { AgentOtherItemTurnover } from 'app/models/agent-other-item-turnover.model';
import { SendOfferToPartnerLog } from 'app/models/sendOfferToPartnerLog.model';
import { ItemGroupService } from 'app/services/item-group.service';

@Component({
  selector: 'app-turnover-details',
  templateUrl: './turnover-details.component.html',
  styleUrls: ['./turnover-details.component.css']
})
export class TurnoverDetailsComponent implements OnInit {
  @Input() clientIdPartnerDetails: number;
  @Input() hideTarlyTurnoverTable: boolean = true;
  @Input() showYearlyTurnover: boolean = true;
  @Input() showPartnerCallReminder: boolean = true;
  @Input() showClientTurnover: boolean = true;
  @Input() showPartnerActivityTurnover: boolean = true;
  @Input() showNewItemsTurnover: boolean = true;
  @Input() showMandatoryPromotionTurnover: boolean = true;
  @Input() showLiquidationOfferTurnover: boolean = true;
  @Input() showOfficeDrinksTurnover: boolean = true;
  @Input() showOfficeItemsTurnover: boolean = true;
  @Input() showAgentOtherItemTurnover: boolean = true;

  @Input() showNewProductBtn: boolean = true;
  @Input() showOtherProductBtn: boolean = true;
  @Input() partners: any;
  @Input() isSpecialPriceRequest: boolean;
  @Input() isTurnoverPage: boolean = true;


  @ViewChild('liquidationOfferTurnoverComponent') liquidationOfferTurnoverComponent: LiquidationOfferTurnoverComponent;
  @ViewChild('yearlyTurnoverComponent') yearlyTurnoverComponent: YearlyTurnoverComponent;
  @ViewChild('partnerCallReminderComponent') partnerCallReminderComponent: PartnerCallReminderComponent;
  @ViewChild('partnerSuggestionComponent') partnerSuggestionComponent: PartnerSuggestionComponent;
  @ViewChild('clientTurnoverComponent') clientTurnoverComponent: ClientTurnoverComponent;
  @ViewChild('partnerActivityTurnoverComponent') partnerActivityTurnoverComponent: PartnerActivityTurnoverComponent;
  @ViewChild('newItemsTurnoverComponent') newItemsTurnoverComponent: NewItemsTurnoverComponent;
  @ViewChild('officeDrinksTurnoverComponent') officeDrinksTurnoverComponent: OfficeDrinksTurnoverComponent;
  @ViewChild('officeItemsTurnoverComponent') officeItemsTurnoverComponent: OfficeItemsTurnoverComponent;
  @ViewChild('agentOtherItemsTurnoverComponent') agentOtherItemsTurnoverComponent: AgentOtherItemTurnoverComponent;
  @ViewChild('mandatoryPromotionTurnoverComponent') mandatoryPromotionTurnoverComponent: MandatoryPromotionTurnoverComponent;

  @ViewChild('stockTooltipItemCode') stockTooltipItemCode: DxTooltipComponent;

  loadedPartnerCallReminder: boolean;
  loadedClientTurnover: boolean;
  loadedPartnerActivityTurnover: boolean;
  loadedNewItemsTurnover: boolean;

  offerPopupVisible: boolean;
  proposedItemGroupPopup: boolean;

  showCharts: boolean;
  selectedOfferType: any;
  offerTypes: any[] = [];
  brands: Brand[] = [];
  vipOfferItems: VipOfferItems[] = [];
  items: any[] = [];
  itemsDS: any;
  locationContactPersons: PartnerLocationContact[];
  selectedItemGroupProposed: ItemGroupProposed = new ItemGroupProposed();
  partnerItemState: PartnerItemState[];
  selectedLocationContactPerson: any;

  actions: IPageActions;
  clientTurnoverActions: IPageActions;
  partnerActivityTurnoverActions: IPageActions;
  newItemsTurnoverActions: IPageActions;
  officeDrinksTurnoverActions: IPageActions;
  officeItemsTurnoverActions: IPageActions;
  agentOtherItemsTurnoverActions: IPageActions;
  mandatoryPromotionTurnoverActions: IPageActions;
  liquidationOfferTurnoverActions: IPageActions;

  postId: number;
  customerId: number;
  lastSendOfferToPartnerDate: any;
  emailForTest: string;
  
  searchCode: string = null;
  searchName: string = null;
  searchCodeOrName: string = null;
  addOtherItemPopup: boolean;
  selectedItemId: number = null;
  searchText: string = null;
  selectedComment: PartnerXItemComment = new PartnerXItemComment();
  loaded: Boolean;
  discountName: string;
  supplierDiscountName: string;
  showPartnerSuggestion: boolean;
  partnerId: number;
  templateVisible: boolean;
  tableHtml: string;
  yearlyTurnOvers: any[];
  numericTurnOvers: any[];
  currentMonthValue: number;
  previousMonthValue: number;
  sumOfLastThreeMonths: number;
  productConventionGrid: DiscountGrid;
  salePriceLists: SalePriceList[] = [];
  orderForTheAmount: string;
  selectedManagementId: number;
  selectedSiteId: number;
  turnoverPreorders: TurnoverPreorder[] = [];
  managements: Management[] = [];
  basketTotal: any;
  basketPopup: boolean;
  timeout: number;
  stockTooltipData: any = {};
  filteredManagements: Management[] = [];
  showPartnerSchedule: boolean;
  implicitPartnerLocation: PartnerLocation = new PartnerLocation();
  daysOfWeek: { value: number, label: string }[];
  deliveryOfferPreferences: { value: number, label: string }[];
  partnerContactPreferences: PartnerContactPreferences = new PartnerContactPreferences();
  partnerSugestionPopup: boolean;
  sites: Site[] = [];
  discountGrids: DiscountGrid[] = [];
  clientId: number;
  supplierYearlyTurnOvers: any[];
  supplierNumericTurnOvers: any[];
  supplierCurrentMonthValue: number;
  supplierPreviousMonthValue: number;
  supplierSumOfLastThreeMonths: number;
  isSelectedPartnerChanged: boolean;
  selectedTurnoverPartnerLocationContactId: number;
  turnoverPreordersSelectedRows: any[];
  turnoverPreordersSelectedRowIndex = -1;
  isVipOfferPopupOpen: boolean;
  vipOfferData: any;
  showVipOfferClientProducts: boolean;
  vipOfferButtonText = 'Vezi toate';
  partnerProductConvention: string;
  isDedicatedOfferPopupOpen: boolean;
  dedicatedOfferData: string;
  isDedicatedOfferInvalidCodesPopupOpen: boolean;
  dedicatedOfferInvalidCodes: any;
  isDailyWhatsappOfferPopupOpen: boolean;
  modifyOfferEmailPopupStates: { [key: string]: boolean } = {};
  excelFileName: { [key: string]: string } = {};
  emailSubject: { [key: string]: string } = {};
  emailBody: { [key: string]: string } = {};
  relatedItems: any;
  originalTurnoverPreorders: TurnoverPreorder[] = [];
  isBasketVipQuantityPopupOpen: boolean;
  vipTurnoverPreorderIdSmallerQuantity: number;
  isShoppingCartPopupOpen: boolean;
  localTime: { dayOfWeek: string; formattedTime: string } | null = null;
  currentGmtOffset: number;
  implicitPartnerLocationCountryId: number;
  isCallReminderPopupOpen: boolean;
  isFoundItemLiquidationOffer: boolean;
  isFoundItemMandatoryPromotion: boolean;
  isFoundItemClientTurnover: boolean;
  isFoundItemPartnerActivity: boolean;
  isFoundItemNewItems: boolean;
  isFoundItemOfficeDrinks: boolean;
  isFoundItemOfficeItems: boolean;
  isFoundItemAgentOtherItem: boolean;
  proposedAgentOtherItemTurnover: AgentOtherItemTurnover[];
  proposedItemId: number;
  isSendOfferToPartnerTooltipVisible: boolean;
  sendOfferToPartnerLogTooltipData: string;
  isSendOfferToPartnerTooltipVisiblePartnerPage: boolean;
  sendOfferToPartnerLogTooltipDataPartnerPage: string;
  itemGroups: any;
  isOwner: boolean;
  partnersDS: any;
  isOrderAmountCommunicatedToClient: boolean = false;
  alreadyCommunicatedOrderForTheAmount: string;
  shouldDisplayLiquidationOffer: boolean;
  shouldDisplayMandatoryPromotion: boolean;
  shouldDisplayClientTurnover: boolean;
  shouldDisplayPartnerActivity: boolean;
  shouldDisplayNewItems: boolean;
  shouldDisplayOfficeDrinks: boolean;
  shouldDisplayOfficeItems: boolean;
  shouldDisplayAgentOtherItem: boolean;

  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;
  @ViewChild('secondValidationGroup') secondValidationGroup: DxValidatorComponent;
  @ViewChild('secondDataGrid') secondDataGrid: DxDataGridComponent;
  @ViewChild('partnerLocationScheduleComponent') partnerLocationScheduleComponent: PartnerLocationScheduleComponent;
  @ViewChild('partnerScheduleValidationGroup') partnerScheduleValidationGroup: DxValidatorComponent;
  

  constructor(private pageActionService: PageActionService, private authService: AuthService, private userService: UsersService,
     private partnerLocationContactService: PartnerLocationContactService,
     private partnerService: PartnerService,
     private brandService: BrandService,
     private vipOfferItemsService: VipOfferItemsService,
     private itemService: ItemService,
     private notificationService: NotificationService,
     private clientTurnoversService: ClientTurnoverService, 
     private sendOfferToPartnerLogService: SendOfferToPartnerLogService,
     private itemGroupProposedService: ItemGroupProposedService,
     private route: ActivatedRoute,
     private partnersReviewsService: PartnerReviewService,
     private partnerFinancialInfoService: PartnerFinancialInfoService,
     private discountGridDetailsService: DiscountGridDetailsService,
     private turnoverPreorderService: TurnoverPreorderService,
     private managementService: ManagementService,
     private translationService: TranslateService,
     private partnerLocationService: PartnerLocationService,
     private partnerContactPreferencesService: PartnerContactPreferencesService,
     private siteService: SiteService,
     private discountGridService: DiscountGridService,
     private salePriceListService: SalePriceListService,
     private helperService: HelperService,
     private vipOfferItemDetailsService: VipOfferItemsDetailsService,
     private itemsRelatedService: ItemRelatedService,
     private countyService: CountyService,
     private itemGroupService: ItemGroupService) {
    this.customerId = Number(this.authService.getUserCustomerId());
    this.yearlyTurnOvers = [];
    this.numericTurnOvers = [];

    this.supplierYearlyTurnOvers = [];
    this.supplierNumericTurnOvers = [];
   }

  async ngOnInit(): Promise<void> {
    this.partners = {
      paginate: true,
      pageSize: 15,
      store: []
    };

    if (this.route.snapshot.queryParamMap.get('clientId') !== undefined && this.route.snapshot.queryParamMap.get('clientId') !== null) {
      const clientId = Number(this.route.snapshot.queryParamMap.get('clientId'));
      this.clientId = this.clientIdPartnerDetails || clientId;
      
      this.partnersReviewsService.getPartnerReviewsByPartnerID(clientId).then(reviews => {
        
        if (reviews && reviews.length > 0) {
          let orderedReviews = _.orderBy(reviews, ['created'], ['desc']);
          let orderedReviewsWithState = orderedReviews.filter(f => f.partnerLocationContactId !== null);
          let lastReview = (orderedReviewsWithState && orderedReviewsWithState.length > 0) ? orderedReviewsWithState[0] : orderedReviews[0];

          switch (lastReview.partnerSuggestionState) {
            case PartnerSuggestionStateEnum.Save:
              // check if there is at least 1 review created in the last 6 months
              const sixMonthsAgo = new Date();
              sixMonthsAgo.setMonth(sixMonthsAgo.getMonth() - 6);
              this.showPartnerSuggestion = !(new Date(lastReview.created) > sixMonthsAgo);

              break;
            case PartnerSuggestionStateEnum.NotInterested:
              // partner suggestion popup shows after 1 month again
              const oneMonthAgo = new Date();
              oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1);
              this.showPartnerSuggestion = !(new Date(lastReview.created) > oneMonthAgo);

              break;

            case PartnerSuggestionStateEnum.Postpone:
              // partner suggestion popup shows every time turnover page is opened
              this.showPartnerSuggestion = true;

              break;

            default:
              this.showPartnerSuggestion = true;

              break;
          }
        } else {
          this.showPartnerSuggestion = true;
        }
      });
    }

    if (!this.clientId) {
      if (this.route.snapshot.queryParamMap.get('id') !== undefined && this.route.snapshot.queryParamMap.get('id') !== null) {
        this.clientId = Number(this.route.snapshot.queryParamMap.get('id'));
      }
    }

    this.initializeSendOfferToPartnerData(this.clientId);

    if (this.route.snapshot.queryParamMap.get('isSpecialPriceRequest') !== undefined && this.route.snapshot.queryParamMap.get('isSpecialPriceRequest') !== null) {
      this.isSpecialPriceRequest = Boolean(this.route.snapshot.queryParamMap.get('isSpecialPriceRequest'));
    }

    await this.getImplicitPartnerLocation();
    this.computeLocalTime();
    setInterval(() => { this.computeLocalTime() }, 60000); 

    this.isOwner = this.authService.isUserOwner();
    this.setActions();
    Promise.all([this.getUserPostId(), this.getPartnerItemState(), this.getBrands(), this.getItems(), this.getVipOfferItems(), this.getSites(), this.getManagements(), this.getDiscountGrids(),
      this.getSalePriceLists(), this.getItemsRelated(), this.getItemsGroups()
    ]).then(x => {
      this.getYearlyTurnover(); this.getSupplierYearlyTurnover();
      this.getNumericTurnover(); this.getSupplierNumericTurnover();
    })
  }

  initializeSendOfferToPartnerData(clientId: number) {
    if (clientId) {
      this.partnerService.getPartnerByPartnerIdsAsync([clientId]).then(items => {
        this.partners.store.push(...items.filter(x => !this.partners.store.map(y => y.id).includes(x.id)));
        this.partnerId = items[0].id;
  
        this.offerTypes = [
          { id: 1, text: 'Oferta generala' },
          { id: 2, text: 'Oferta Produse Cumparate Frecvent' },
          { id: 3, text: 'Oferta Produse de Interes' },
          { id: 4, text: 'Oferta Speciala - lichidare, PMV' },
          { id: 5, text: 'Oferta Dedicata' },
          { id: 6, text: 'Oferte Zilnice pentru Whatsapp' },
          { id: 7, text: 'Oferta Produse Cos + Complementare + Doriti' }
        ];
    
        this.selectedOfferType = this.offerTypes[0];
  
        const currentDate = moment().format("DD.MM.YYYY");
  
        this.offerTypes.forEach(offer => {
          switch (offer.id) {
            case 1:
              this.excelFileName[offer.id] = `${this.partners.store[0].name} Oferta Generala ${currentDate}`;
              this.emailSubject[offer.id] = `Oferta Generala ${currentDate}`;
              this.emailBody[offer.id] = `Buna ziua! Aceasta este oferta cu toate produsele pe care le puteti achizitiona pentru data: ${currentDate}`;
              break;
            case 2:
              this.excelFileName[offer.id] = `${this.partners.store[0].name} Oferta Produse Cumparate Frecvent ${currentDate}`;
              this.emailSubject[offer.id] = `Oferta Produse Cumparate Frecvent ${currentDate}`;
              this.emailBody[offer.id] = `Buna ziua! Aceasta este oferta cu produsele cumparate frecvent pentru data: ${currentDate}`;
              break;
            case 3:
              this.excelFileName[offer.id] = `${this.partners.store[0].name} Oferta Produse de Interes ${currentDate}`;
              this.emailSubject[offer.id] = `Oferta Produse de Interes ${currentDate}`;
              this.emailBody[offer.id] = `Buna ziua! Aceasta este oferta cu produsele de interes pe care le puteti achizitiona de la noi pentru data: ${currentDate}`;
              break;
            case 4:
              this.excelFileName[offer.id] = `${this.partners.store[0].name} Oferta Speciala - lichidare, PMV ${currentDate}`;
              this.emailSubject[offer.id] = `Oferta Speciala - lichidare, PMV ${currentDate}`;
              this.emailBody[offer.id] = `Buna ziua! Aceasta este oferta speciala - lichidare, PMV pentru data: ${currentDate}`;
              break;
            case 5:
              this.excelFileName[offer.id] = `${this.partners.store[0].name} Oferta Dedicata ${currentDate}`;
              this.emailSubject[offer.id] = `Oferta Dedicata ${currentDate}`;
              this.emailBody[offer.id] = `Buna ziua! Aceasta este oferta dedicata pentru data: ${currentDate}`;
      
              break;
            case 7:
              this.excelFileName[offer.id] = `${this.partners.store[0].name} Oferta Produse Cos + Complementare + Doriti ${currentDate}`;
              this.emailSubject[offer.id] = `Oferta Produse Cos + Complementare + Doriti ${currentDate}`;
              this.emailBody[offer.id] = `Buna ziua! Aceasta este Oferta Produse Cos + Complementare + Doriti pentru data: ${currentDate}`;
  
              break;
            default:
              break;
          }
        })
      });
    }
  }

  async getItemsGroups(): Promise<any> {
    if (this.isOwner) {
      await this.itemGroupService.getAllItemGroupsAsync().then(items => {
        this.itemGroups = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    } else {
      await this.itemGroupService.getItemGroupsAsyncByID().then(items => {
        this.itemGroups = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    }
  }

  async getImplicitPartnerLocation(): Promise<void> {
    if (this.clientId) {
      await this.partnerLocationService.getPartnerLocationsIncludingInactiveByPartnerID(this.clientId).then(async locations => {
        this.implicitPartnerLocation = (locations && locations.length > 0) ? (locations.find(f => f.isImplicit) || locations.find(f => f.isActive)) : new PartnerLocation();
  
        if (this.implicitPartnerLocation && this.implicitPartnerLocation.countyId) {
          this.implicitPartnerLocationCountryId = this.implicitPartnerLocation.countryId;
  
          await this.countyService.getByIdAsync(this.implicitPartnerLocation.countyId).then(response => {
            this.currentGmtOffset = response ? response.gmtOffset : null;
          })
        }
      });
    }
  }

  async getItemsRelated(): Promise<void> {
    await this.itemsRelatedService.getAll().then(items => {
      this.relatedItems = (items && items.length > 0) ? items : [];
    });
  }

  async getPartnerContact(): Promise<void> {
    if (!this.clientId) {
      if (this.route.snapshot.queryParamMap.get('id') !== undefined && this.route.snapshot.queryParamMap.get('id') !== null) {
        this.clientId = Number(this.route.snapshot.queryParamMap.get('id'));
      }
    }

    await this.partnerLocationContactService.getPartnerLocationContactByPartnerID(this.clientId).then(items => {
      if (items && items.length > 0) {
        this.locationContactPersons = items.filter(f => f.email);
      } else {
        this.locationContactPersons = null;
      }
    });
  }

  async getPartnerItemState(): Promise<void> {
    await this.partnerService.getPartnerItemStateAsync().then(items => {
      this.partnerItemState = (items && items.length > 0) ? items : [];
      this.partnerItemState.forEach(i => { i.codeId = Number(i.code); });
    });
  }

  async getBrands(): Promise<void> {
    await this.brandService.getByCustomerIdAsync().then(items => {
      this.brands = (items && items.length > 0) ? items : [];
    });
  }

  async getVipOfferItems(): Promise<void> {
    await this.vipOfferItemsService.getAllValidAsync().then(items => {
      this.vipOfferItems = (items && items.length > 0) ? items.filter(x => x.isActive == true) : [];
    });
  }

  async getItems(): Promise<void> {
    await this.itemService.getAllItemsAsync().then(items => {
      if (items && items.length > 0) {
        this.items = items;
        this.itemsDS = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      } else {
        this.items = [];
        this.itemsDS = null;
      }
    });
  }
  
  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.clientTurnoverActions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.partnerActivityTurnoverActions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.newItemsTurnoverActions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.officeDrinksTurnoverActions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.officeItemsTurnoverActions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.agentOtherItemsTurnoverActions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.mandatoryPromotionTurnoverActions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.liquidationOfferTurnoverActions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    
    this.pageActionService.getRoleActionsByPagePath(Constants.turnOver).then(result => {
      this.actions = result;

      this.clientTurnoverActions = result;
      this.clientTurnoverActions.CanAdd = false;
      this.clientTurnoverActions.CanDelete = false;


      this.partnerActivityTurnoverActions = result;
      this.partnerActivityTurnoverActions.CanAdd = false;
      this.partnerActivityTurnoverActions.CanDelete = false;

      this.newItemsTurnoverActions = result;
      this.newItemsTurnoverActions.CanAdd = false;
      this.newItemsTurnoverActions.CanDelete = false;

      this.officeDrinksTurnoverActions = result;
      this.officeDrinksTurnoverActions.CanAdd = false;
      this.officeDrinksTurnoverActions.CanDelete = false;

      this.officeItemsTurnoverActions = result;
      this.officeItemsTurnoverActions.CanAdd = false;
      this.officeItemsTurnoverActions.CanDelete = false;

      this.mandatoryPromotionTurnoverActions = result;
      this.mandatoryPromotionTurnoverActions.CanAdd = false;
      this.mandatoryPromotionTurnoverActions.CanDelete = false;

      this.liquidationOfferTurnoverActions = result;
      this.liquidationOfferTurnoverActions.CanAdd = false;
      this.liquidationOfferTurnoverActions.CanDelete = false;

      this.agentOtherItemsTurnoverActions = result;
      this.agentOtherItemsTurnoverActions.CanAdd = false;
      this.agentOtherItemsTurnoverActions.CanDelete = false;
    });
  }

  async addItemGroupProposed() {
    this.loaded = true;
    await this.partnerService.getAllPartnersSmallAsync().then(partners => {
      this.partnersDS = {
        paginate: true,
        pageSize: 15,
        store: partners
      }

      this.loaded = false;
      this.selectedItemGroupProposed = new ItemGroupProposed();
      this.proposedItemGroupPopup = true;
    });
  }

  async saveProposedItemGroup() {
    if (this.selectedItemGroupProposed && this.validationGroup.instance.validate().isValid) {
      this.selectedItemGroupProposed.clientId = this.clientId;
      this.selectedItemGroupProposed.currencyId = 1;
      await this.itemGroupProposedService.createAsync(this.selectedItemGroupProposed).then(result => {
        if (result) {
          this.notificationService.alert('top', 'center', 'Rulaj - Produs XXXX nou adaugat!',
          NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Rulaj - Produs XXXX nu a fost adaugat! A aparut o eroare!',
          NotificationTypeEnum.Red, true)
        }
        this.proposedItemGroupPopup = false;
      })
    }
  } 

  getUserPostId() {
    this.userService.getByUsernameOrEmail(this.authService.getUserUserName(), null).then(user => {
      if (user && user.length > 0 ) {
        if (user[0] && user[0].postId) {
          this.postId = user[0].postId;
        }
      }
    });
  }

  async loadData() {
    if (this.clientId && this.postId) {
      if (this.showYearlyTurnover) {
        this.yearlyTurnoverComponent.getData();
     }
      if (this.showPartnerCallReminder) {
         this.partnerCallReminderComponent.getData();
      }
      if (this.showClientTurnover) {
        this.clientTurnoverComponent?.getData();
      }
      if (this.showPartnerActivityTurnover) {
        //this.partnerActivityTurnoverComponent.getData();
        this.items = [...this.items]; 
      }
      if (this.showNewItemsTurnover) {
        //this.newItemsTurnoverComponent.getData();
        this.items = [...this.items];
      }
      if (this.showOfficeDrinksTurnover) {
        this.officeDrinksTurnoverComponent?.getData();
      }
      if (this.showOfficeItemsTurnover) {
        this.officeItemsTurnoverComponent?.getData();
      }
      if (this.showAgentOtherItemTurnover) {
        this.agentOtherItemsTurnoverComponent?.getData();
      }
      if (this.showMandatoryPromotionTurnover) {
        this.mandatoryPromotionTurnoverComponent?.getData();
      }
      if (this.showLiquidationOfferTurnover) {
        this.liquidationOfferTurnoverComponent?.getData();
      }
      this.getLastSendOfferToPartnerLog();
      if (this.isSelectedPartnerChanged) {
        await this.getDiscountGrids();
        await this.getYearlyTurnover();
      }
    }
  }

  openGeneralOfferPopup() {
    this.getPartnerContact();
    this.emailForTest = null;
    this.offerPopupVisible = true;
  }

  async sendOffer(dedicatedOfferItemIds?: number[]) {
    if (this.selectedLocationContactPerson && this.selectedLocationContactPerson.length > 0) {
      let personContactIds = this.selectedLocationContactPerson.map(m => m.id);

      this.loaded = true;

      await this.clientTurnoversService.sendOfferToPartnerContact(this.clientId, 
        personContactIds, 
        this.emailForTest, 
        this.selectedOfferType.id, 
        this.discountName, 
        this.excelFileName[this.selectedOfferType.id],
        this.emailSubject[this.selectedOfferType.id],
        this.emailBody[this.selectedOfferType.id],
        +(this.authService.getUserPostId()),
        dedicatedOfferItemIds).then(async result => {
        if (result) {
          let sendOfferToPartnerLog = new SendOfferToPartnerLog();
          sendOfferToPartnerLog.customerId = +this.authService.getUserCustomerId();
          sendOfferToPartnerLog.partnerId = this.clientId;
          sendOfferToPartnerLog.date = new Date();
          sendOfferToPartnerLog.offerType = this.selectedOfferType.id;

          await this.sendOfferToPartnerLogService.createAsync(sendOfferToPartnerLog).then(() => {
            this.notificationService.alert('top', 'center', 'Trimite Oferta - Oferta a fost trimisa!', NotificationTypeEnum.Green, true);
          })
        } else {
          this.notificationService.alert('top', 'center', 'Trimite Oferta - Oferta nu a fost trimisa! A aparut o eroare!',
          NotificationTypeEnum.Red, true)
        }

        this.loaded = false;
        this.offerPopupVisible = false;
      })

    } else {
      this.notificationService.alert('top', 'center', 'Trimite Oferta - Persoana de contact selectata nu are un email!',
      NotificationTypeEnum.Red, true)
    }
  }


  getDisplayExprContact(item) {
    if (!item) {
      return '';
    } 

    return item.firstName + ' - ' + item.email + ' - ' + item.phoneNumber;
  }

  getItemDisplayExpr(item) {
    if (!item) {
      return '';
    } 
    return item.code + ' - ' + item.nameRO;
  }

  async getLastSendOfferToPartnerLog() {
    // await this.sendOfferToPartnerLogService.getLastByPartnerIdAsync(this.clientId).then(r => {
    //   if (r) {
    //     this.lastSendOfferToPartnerDate = r[0].date;
    //   }
    // })
  }

  addOtherItem() {
    this.selectedItemId = null;

    this.selectedComment = new PartnerXItemComment();
    this.selectedComment.isInvoiced = false;
    this.selectedComment.hasStock = false;
    this.selectedComment.atOrder = false;
    this.selectedComment.notUsed = false;

    this.addOtherItemPopup = true;

   setTimeout(() => {
    if (this.searchCodeOrName && this.searchCodeOrName.length > 3) {
      this.searchText = this.searchCodeOrName;
    }
  }, 1000);
  }

  checkStockChange(data) {
    if (data && data.hasStock) {
      data.isInvoiced = false;
      data.notUsed = false;
      data.atOrder = false;
      data.hasStockFromMe = true;
      data.callBackDate = null;
    }
  }

  checkInvoicedChange(data) {
    if (data && data.isInvoiced) {
      data.hasStock = false;
      data.notUsed = false;
      data.atOrder = false;
    }
  }

  checkNotUsedChange(data) {
    if (data && data.notUsed) {
      data.isInvoiced = false;
      data.hasStock = false;
      data.atOrder = false;
      data.callBackDate = null;
    }
  }

  checkAtOrderChange(data) {
    if (data && data.atOrder) {
      data.isInvoiced = false;
      data.notUsed = false;
      data.hasStock = false;
      data.callBackDate = null;
    }
  }

  async saveOtherItem(isFromSearchItem?: boolean) {
    if ( this.clientId &&
      this.postId &&
      this.customerId) {
      if (!isFromSearchItem) {
        if (this.secondValidationGroup && !this.secondValidationGroup.instance.validate().isValid) {
          return;
        }
      }

      this.selectedComment.customerId = this.customerId;
      this.selectedComment.partnerId = this.clientId;
      this.selectedComment.postId = this.postId;
      this.selectedComment.section = TurnoverSectionTypeEnum.AgentOtherItemTurnover;

      this.selectedComment.partnerItemStateId = null;

      if (this.selectedComment.isInvoiced) {
        this.selectedComment.partnerItemStateId = this.partnerItemState.find(x => x.name == "Facturat")?.id;
        this.selectedComment.comment = this.selectedComment.comment ? this.selectedComment.comment : '';
      } else  
      if (this.selectedComment.notUsed) {
        this.selectedComment.partnerItemStateId = this.partnerItemState.find(x => x.name == "Nu Foloseste")?.id;
        this.selectedComment.comment = this.selectedComment.comment ? this.selectedComment.comment : '';
        this.selectedComment.callBackDate = null;
      } 
      if (this.selectedComment.hasStock) {
        this.selectedComment.partnerItemStateId = this.partnerItemState.find(x => x.name == "Are Stoc")?.id;
        this.selectedComment.comment = this.selectedComment.comment ? this.selectedComment.comment : '';
      }
      if (this.selectedComment.atOrder) {
        this.selectedComment.partnerItemStateId = this.partnerItemState.find(x => x.name == "La Comanda")?.id;
        this.selectedComment.comment = this.selectedComment.comment ? this.selectedComment.comment : '';
      }

      await this.clientTurnoversService.updatePartnerXItemComment(this.selectedComment).then(r => {
        if (isFromSearchItem) {
          this.notificationService.alert('top', 'center', 'Produsul cautat a fost adaugat in sectiunea Alt Produs deoarece nu a fost gasit in Rulajul acestui client!', NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Rulaj - Datele au fost modificate cu succes!', NotificationTypeEnum.Green, true)
        }

        this.addOtherItemPopup = false;
        this.loadData();
      }).catch(ex => {
        this.addOtherItemPopup = false;
        this.loadData();
      });
    }
  }
 
  async onSearchByItemCodeOrName() {
    this.showLiquidationOfferTurnover = true;
    this.showMandatoryPromotionTurnover = true;
    this.showClientTurnover = true;
    this.showPartnerActivityTurnover = true;
    this.showNewItemsTurnover = true;
    this.showOfficeDrinksTurnover = true;
    this.showOfficeItemsTurnover = true;
    this.showAgentOtherItemTurnover = true;

    if (this.searchCodeOrName) {

      this.clientTurnoverComponent.applyColumnFilter(this.searchCodeOrName, 'itemCode');
      this.partnerActivityTurnoverComponent.applyColumnFilter(this.searchCodeOrName, 'itemCode');
      this.newItemsTurnoverComponent.applyColumnFilter(this.searchCodeOrName, 'itemCode');
      this.officeDrinksTurnoverComponent.applyColumnFilter(this.searchCodeOrName, 'itemCode');
      this.officeItemsTurnoverComponent.applyColumnFilter(this.searchCodeOrName, 'itemCode');
      this.mandatoryPromotionTurnoverComponent.applyColumnFilter(this.searchCodeOrName, 'itemCode');
      this.liquidationOfferTurnoverComponent.applyColumnFilter(this.searchCodeOrName, 'itemCode');
      this.agentOtherItemsTurnoverComponent.applyColumnFilter(this.searchCodeOrName, 'itemCode');

      if (!this.isFoundItemLiquidationOffer) this.showLiquidationOfferTurnover = false;
      if (!this.isFoundItemMandatoryPromotion) this.showMandatoryPromotionTurnover = false;
      if (!this.isFoundItemClientTurnover) this.showClientTurnover = false;
      if (!this.isFoundItemPartnerActivity) this.showPartnerActivityTurnover = false;
      if (!this.isFoundItemNewItems) this.showNewItemsTurnover = false;
      if (!this.isFoundItemOfficeDrinks) this.showOfficeDrinksTurnover = false;
      if (!this.isFoundItemOfficeItems) this.showOfficeItemsTurnover = false;
      if (!this.isFoundItemAgentOtherItem && (this.isFoundItemLiquidationOffer || this.isFoundItemMandatoryPromotion || this.isFoundItemClientTurnover 
        || this.isFoundItemPartnerActivity || this.isFoundItemNewItems || this.isFoundItemOfficeDrinks || this.isFoundItemOfficeItems)) this.showAgentOtherItemTurnover = false;
      
      if (!this.isFoundItemLiquidationOffer &&
        !this.isFoundItemMandatoryPromotion &&
        !this.isFoundItemClientTurnover &&
        !this.isFoundItemPartnerActivity &&
        !this.isFoundItemNewItems &&
        !this.isFoundItemOfficeDrinks &&
        !this.isFoundItemOfficeItems &&
        !this.isFoundItemAgentOtherItem
      ) {
        let item = this.items.find(f => f.code.toLowerCase().includes(this.searchCodeOrName.toLowerCase()) 
          || f.nameRO.toLowerCase().includes(this.searchCodeOrName.toLowerCase()));
        
        if (item) {
          this.selectedComment.itemId = item.id;
          await this.saveOtherItem(true);
          // setTimeout(() => {
          //   var scrollableContent = document.getElementById('agentOtherItemsTurnover');
          //   if (scrollableContent) {
          //     scrollableContent.scrollIntoView({ behavior: 'smooth' });
          //   }
          // }, 100);
        } else {
          this.notificationService.alert('top', 'center', 'Produsul cautat nu exista!', NotificationTypeEnum.Red, true);
        }
      }
    } else {
      // this.clientTurnoverComponent.applyColumnFilter(null, 'itemCode');
      // this.partnerActivityTurnoverComponent.applyColumnFilter(null, 'itemCode');
      // this.newItemsTurnoverComponent.applyColumnFilter(null, 'itemCode');
      // this.officeDrinksTurnoverComponent.applyColumnFilter(null, 'itemCode');
      // this.officeItemsTurnoverComponent.applyColumnFilter(null, 'itemCode');
      // this.mandatoryPromotionTurnoverComponent.applyColumnFilter(null, 'itemCode');
      // this.liquidationOfferTurnoverComponent.applyColumnFilter(null, 'itemCode');
      // this.agentOtherItemsTurnoverComponent.applyColumnFilter(null, 'itemCode');
      this.onClearFiltersClick();
    }
  }

  discountNameValueChange(discountName: string) {
    this.discountName = discountName;
  }

  backToPartner() {
    var url = environment.CRMPrimaware + '/' + Constants.partner + '?id=' + this.clientId;
    window.open(url, '_self');
  }

   onRefresh() {
    this.loadData();
  }

  getDisplayExprPartners(item) {
    if (!item) {
        return '';
      }
      return item.code + ' - ' + item.name;
  }

  onPartnerChanged(e) {
    if (e && e.target && e.target.value && e.target.value.length > 4) {
      let searchFilter = new PartnerSearchFilter();
      searchFilter.name = e.target.value;
      searchFilter.isActive = true;
      this.partnerService.getPartnersByFilter(searchFilter).then(items => {
        this.partners.store.push(...items.filter(x => !this.partners.store.map(y => y.id).includes(x.id)));
      })
    }
  }

  partnerChange(e) {
    if (e && e.value) {

      this.clientId = e.value;   
      this.isSelectedPartnerChanged = true;  
      setTimeout(() => {
        this.loadData();
        //this.setShowButtons();
      }, 100);
    }
  }

  toggleTemplate() {

    this.templateVisible = !this.templateVisible;
    setTimeout(async () => {
      if (document.getElementById("tooltipInfo")) {
        document.getElementById("tooltipInfo").innerHTML = this.tableHtml;
      }
    }, 0);

  }

  getMonthsData(yearData: any) {
    let monthValues = [];
    for (let i = 1; i <= 12; i++) {
      monthValues.push(yearData['l' + i]);
    }
    return monthValues;
  }

  findRequiredValueForNextRange(x, ranges) {
    for (let i = 0; i < ranges.length; i++) {
        const { valueFrom, valueTo } = ranges[i];

        if (x >= valueFrom && x <= valueTo) {
            if (i + 1 < ranges.length) {
                const nextRangeStart = ranges[i + 1].valueFrom;
                const difference = nextRangeStart - x;
                return difference; // Return difference to reach the next range
            } else {
                return -1; // x is in the last range, no next range
            }
        }

        // If x is less than the current range's valueFrom, calculate the difference to get into this range
        if (x < valueFrom) {
            return valueFrom - x;
        }
    }

    return -1;
}

async getDiscountGrids(): Promise<any> {
  if (this.clientId) {
    await this.partnerService.getPartnerProductConventionIdByImplicitLocationAsync(this.clientId).then(async r => {
      if (r && r.productConventionId && r.currencyId) {
        this.partnerProductConvention = this.translationService.instant(ProductConventionEnum[r.productConventionId])
        await this.discountGridService.GetAllAsync().then(grids => {
          this.discountGrids = (grids && grids.length > 0) ? grids : null;
          this.productConventionGrid = this.discountGrids.find(pc => pc.productConventionId == r.productConventionId && pc.currencyId == r.currencyId && pc.benefitType == 1 && pc.dataType == 1 && pc.isActive == true);      
        });
      }
    });  
  }
}

async getSupplierYearlyTurnover(): Promise<any> {
  if (this.clientId && this.postId) {
    this.loaded = true;
    this.supplierDiscountName = "";
    await this.partnerFinancialInfoService.getSupplierDetailsByPartnerIdAsync(this.clientId).then(items => {
      this.supplierYearlyTurnOvers = (items && items.length > 0) ? items : [];
      if (this.supplierYearlyTurnOvers && this.supplierYearlyTurnOvers.length > 0) {

        let data = this.supplierYearlyTurnOvers.find(x => x.year == new Date().getFullYear());
        if (data) {
          let monthValues = this.getMonthsData(data);

          let currentMonthIndex = new Date().getMonth(); // This gives you a 0-based index

          // Requirement 1: obtain the value of the current month
          this.currentMonthValue = Math.ceil(monthValues[currentMonthIndex]);

          // Requirement 2: obtain the value of the previous month from the current month
          if (currentMonthIndex > 0) {
            this.supplierPreviousMonthValue = Math.ceil(monthValues[currentMonthIndex - 1]);
          } else {
            let previousYear = this.supplierYearlyTurnOvers.find(x => x.year == (new Date().getFullYear() - 1));
            let previousYearMonths = this.getMonthsData(previousYear);
            this.supplierPreviousMonthValue = Math.ceil(Number(previousYearMonths[11]));
          }          

          // Requirement 3: obtain the sum of the last 3 months excepting the current month and divide that sum by 3
          this.supplierSumOfLastThreeMonths = 0;
          if (currentMonthIndex >= 3) {
           let x = (Number(monthValues[currentMonthIndex - 1]) + Number(monthValues[currentMonthIndex - 2]) + Number(monthValues[currentMonthIndex - 3])) / 3;
            this.supplierSumOfLastThreeMonths = Math.ceil(Number(x.toFixed(2)));
          } else {
            let previousYear = this.supplierYearlyTurnOvers.find(x => x.year == (new Date().getFullYear() - 1));
            let previousYearMonths = this.getMonthsData(previousYear);
            switch (currentMonthIndex) {
                case 2:  // March, so take January of this year and December, November of last year
                    let x = (Number(monthValues[1]) + Number(previousYearMonths[11]) + Number(previousYearMonths[10])) / 3;
                    this.supplierSumOfLastThreeMonths = Math.ceil(Number(x.toFixed(2)));
                    break;
                case 1:  // February, so take January of this year, December and November of last year
                    let y = (Number(monthValues[0]) + Number(previousYearMonths[11]) + Number(previousYearMonths[10])) / 3;
                    this.supplierSumOfLastThreeMonths = Math.ceil(Number(y.toFixed(2)));
                    break;
                case 0:  // January, so take December, November, and October of last year
                    let z = (Number(previousYearMonths[11]) + Number(previousYearMonths[10]) + Number(previousYearMonths[9])) / 3;
                    this.supplierSumOfLastThreeMonths = Math.ceil(Number(z.toFixed(2)));
                    break;
            }
          }
        }

        const formatter = new Intl.NumberFormat('en-US', {
          minimumFractionDigits: 0,
          maximumFractionDigits: 0,
        });
        this.supplierYearlyTurnOvers.forEach(item => {
          for (let i = 1; i <= 12; i++) {
            let prop = 'l' + i;
            if (item[prop]) {
              let roundedValue = Math.ceil(item[prop]);
              item[prop] = formatter.format(roundedValue);
            }
          }
          let roundedValue = Math.ceil(item['total']);
          item['total'] = formatter.format(roundedValue);
        });

      }
      this.loaded = false;
    });
  }
}


  async getYearlyTurnover(): Promise<any> {
    if (this.clientId && this.postId) {
      this.loaded = true;
      this.discountName = "";
      await this.partnerFinancialInfoService.getDetailsByPartnerIdAsync(this.clientId).then(items => {
        this.yearlyTurnOvers = (items && items.length > 0) ? items : [];
        if (this.yearlyTurnOvers && this.yearlyTurnOvers.length > 0) {

          let data = this.yearlyTurnOvers.find(x => x.year == new Date().getFullYear());
          if (data) {
            let monthValues = this.getMonthsData(data);

            let currentMonthIndex = new Date().getMonth(); // This gives you a 0-based index

            // Requirement 1: obtain the value of the current month
            this.currentMonthValue = Math.ceil(monthValues[currentMonthIndex]);

            // Requirement 2: obtain the value of the previous month from the current month
            if (currentMonthIndex > 0) {
              this.previousMonthValue = Math.ceil(monthValues[currentMonthIndex - 1]);
            } else {
              let previousYear = this.yearlyTurnOvers.find(x => x.year == (new Date().getFullYear() - 1));
              let previousYearMonths = this.getMonthsData(previousYear);
              this.previousMonthValue = Math.ceil(Number(previousYearMonths[11]));
            }          

            // Requirement 3: obtain the sum of the last 3 months excepting the current month and divide that sum by 3
            this.sumOfLastThreeMonths = 0;
            if (currentMonthIndex >= 3) {
             let x = (Number(monthValues[currentMonthIndex - 1]) + Number(monthValues[currentMonthIndex - 2]) + Number(monthValues[currentMonthIndex - 3])) / 3;
              this.sumOfLastThreeMonths = Math.ceil(Number(x.toFixed(2)));
            } else {
              let previousYear = this.yearlyTurnOvers.find(x => x.year == (new Date().getFullYear() - 1));
              let previousYearMonths = this.getMonthsData(previousYear);
              switch (currentMonthIndex) {
                  case 2:  // March, so take January of this year and December, November of last year
                      let x = (Number(monthValues[1]) + Number(previousYearMonths[11]) + Number(previousYearMonths[10])) / 3;
                      this.sumOfLastThreeMonths = Math.ceil(Number(x.toFixed(2)));
                      break;
                  case 1:  // February, so take January of this year, December and November of last year
                      let y = (Number(monthValues[0]) + Number(previousYearMonths[11]) + Number(previousYearMonths[10])) / 3;
                      this.sumOfLastThreeMonths = Math.ceil(Number(y.toFixed(2)));
                      break;
                  case 0:  // January, so take December, November, and October of last year
                      let z = (Number(previousYearMonths[11]) + Number(previousYearMonths[10]) + Number(previousYearMonths[9])) / 3;
                      this.sumOfLastThreeMonths = Math.ceil(Number(z.toFixed(2)));
                      break;
              }
            }
          }
          
          let maxValue = Math.max(this.currentMonthValue, this.previousMonthValue, this.sumOfLastThreeMonths);
          if (this.productConventionGrid) {
             this.discountGridDetailsService.GetByDiscountGridAsync(this.productConventionGrid.id).then(grids => {
              if (grids && grids.length > 0) {
                this.tableHtml = '<table border="1">';
                this.tableHtml += '<thead><tr><th>Grila</th><th>De la</th><th>Pana la</th></tr></thead>';
            
                this.tableHtml += '<tbody>';
                for (const grid of grids) {
                     this.tableHtml += `<tr>
                                          <td style="text-align: left;">${this.salePriceLists.find(ss => ss.id == grid.salePriceListId)?.name}</td>
                                          <td style="text-align: right;">${formatter.format(grid.valueFrom)}</td>
                                          <td style="text-align: right;">${formatter.format(grid.valueTo)}</td>
                                        </tr>`;
                }
                this.tableHtml += '</tbody>';
            
                let item = grids.find(grid => {
                  let lowerConditionMet = (grid.fromConditionType === 1 && maxValue >= grid.valueFrom) || (grid.fromConditionType === 2 && maxValue >= grid.valueFrom);
                  let upperConditionMet = (grid.toConditionType === 1 && maxValue < grid.valueTo) || (grid.toConditionType === 2 && maxValue <= grid.valueTo);
                  return lowerConditionMet && upperConditionMet;
                });
            
                if (item && item.salePriceListId && this.salePriceLists && this.salePriceLists.length > 0) {

                  const index = grids.findIndex(i => i.name === item.name && i.salePriceListId === item.salePriceListId);
                  var valueForNextRange = this.findRequiredValueForNextRange(this.currentMonthValue, grids.slice(index + 1));

                  let nextGrid = grids.slice(index + 1) != null ? grids.slice(index + 1)[0] : null;
                  const matchDiscountFromPriceList = this.salePriceLists.find(ss => ss.id == nextGrid?.salePriceListId)?.name?.match(/-\d+%/);

                  let formattedDiscount = matchDiscountFromPriceList ? matchDiscountFromPriceList[0] : null;
                  this.orderForTheAmount = valueForNextRange != -1 ?`Comanda de ${this.helperService.setNumberWithCommas(this.helperService.roundUp(valueForNextRange))} RON beneficiaza de discount ${formattedDiscount}!` : null;
                  this.alreadyCommunicatedOrderForTheAmount = `Am comunicat: La o comanda de ${this.helperService.setNumberWithCommas(this.helperService.roundUp(valueForNextRange))} lei, va beneficia de discount ${formattedDiscount}`;
                 
                  let s = this.salePriceLists.find(ss => ss.id == item.salePriceListId);
                  if (s) {
                    this.discountName = s.name;
                  }
                }
            } else {
                this.tableHtml = '';
              }
            })
          }

          const formatter = new Intl.NumberFormat('en-US', {
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
          });
          this.yearlyTurnOvers.forEach(item => {
            for (let i = 1; i <= 12; i++) {
              let prop = 'l' + i;
              if (item[prop]) {
                let roundedValue = Math.ceil(item[prop]);
                item[prop] = formatter.format(roundedValue);
              }
            }
            let roundedValue = Math.ceil(item['total']);
            item['total'] = formatter.format(roundedValue);
          });

        }
        this.loaded = false;
      });
    }
  }

  async getManagements(): Promise<any> {
    await this.managementService.getManagementsAsyncByID().then(lists => {
      this.managements = (lists && lists.length > 0) ? lists : null;
    })
  }

  async getTurnoverPreorders() {
    await this.turnoverPreorderService.GetByPartnerIdAsync(this.clientId).then(async items => {
      this.turnoverPreorders = (items && items.length > 0) ? items : [];     
      if (this.turnoverPreorders && this.turnoverPreorders.length > 0) {
        let itemIds = this.turnoverPreorders.map(i => i.itemId);
        await this.itemService.getItemStocks(itemIds).then(r => {
          if (r) {
            this.turnoverPreorders.forEach(i => {
              if (r.hasOwnProperty(i.itemId)) {
                i.totalStock = r[i.itemId];
              } else {
                i.totalStock = 0;
              }
            });
          }

          if (this.partners && this.partners.store && this.partners.store.length > 0) {
            let partnerSiteId = this.partners.store[0].siteId;
            this.selectedSiteId = partnerSiteId && +partnerSiteId;
      
            let partnerManagement = this.managements.find(f => f.siteId === this.selectedSiteId);
            this.selectedManagementId = partnerManagement && partnerManagement.id;
          }

          this.turnoverPreorders = this.turnoverPreorders.map(m => ({ 
            ...m,
            agentQuantity: m.quantity
          }));

          this.onManagementChanged();

          this.originalTurnoverPreorders = _.cloneDeep(this.turnoverPreorders);
        });
      }
    });
  }

  updateBasketTotal() {
    this.basketTotal = 0;
    this.turnoverPreorders.filter(x => x.agentQuantity > 0).forEach(i => {
      this.basketTotal += (i.agentQuantity * i.price);
    })
    this.basketTotal = Math.ceil(this.basketTotal * 100) / 100;
  }

  async onManagementChanged(e?: any) {
    if (this.selectedManagementId && this.selectedSiteId) {
      var itemIds = this.turnoverPreorders.map(x => x.itemId);
      await this.itemService.getItemStocksOnManagements(itemIds, [this.selectedManagementId]).then(r => {
        if (r) {
          this.turnoverPreorders.forEach(i => {
            if (r.hasOwnProperty(i.itemId)) {
              i.managementStock = r[i.itemId];
              i.managementId = this.selectedManagementId;
            } else {
              i.managementStock = 0;
              i.managementId = null;
            }
          });
        }
        this.secondDataGrid.instance.clearSelection();
      });
    }
  }

  async getSites(): Promise<any> {
    await this.siteService.getCustomerLocationsAsyncByID().then(lists => {
      this.sites = (lists && lists.length > 0) ? lists : null;
    })
  }

  async openBasketPopup() {
    this.isShoppingCartPopupOpen = true;
  }

  openSpecialPriceRequest() {
    if (this.partners && this.partners.store && this.partners.store.length > 0) {
      var url = environment.CRMPrimaware + '/' + Constants.specialPriceRequest + '?clientCode=' + this.partners.store[0].code;
      window.open(url, '_blank');
    }
  }

  displayCodeExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  onBasketRowPrepared(e) {
    if (e.rowType === 'data' && e.data && e.data.totalStock < e.data.quantity ) {
      e.rowElement.style.backgroundColor = '#ffdfcf';
    }
    if (e.rowType === 'data' && e.data && e.data.agentQuantity &&  e.data.managementStock && e.data.managementStock > e.data.agentQuantity ) {
      e.rowElement.style.backgroundColor = '#b8ffd3';
    }
    if (e.rowType === 'data' && e.data && e.data.agentQuantity && e.data.managementStock < e.data.agentQuantity ) {
      e.rowElement.style.backgroundColor = '#ff5842';
    }
  }

  async getSalePriceLists(): Promise<any> {
    await this.salePriceListService.getSalePriceListsAsyncByID().then(lists => {
      this.salePriceLists = (lists && lists.length > 0) ? lists : null;
    })
  }

  setItemCodeTooltip(event) {
    if (event && event.rowType === 'data' && event.column.dataField === 'totalStock') {
      on(event.cellElement, 'mouseover', arg => {
        this.timeout = window.setTimeout(async () => {

          this.stockTooltipData = null;
          let stockPromise = this.itemService.GetStocksOnSiteAsync(event.data.itemId).then(r => {
            if (r && r.length > 0) {
              this.stockTooltipData = r;
            } else { this.stockTooltipData = []; }
          });

          Promise.all([stockPromise]).then(r => {this.stockTooltipItemCode.instance.show(arg.target);})
        }, 1000);
      });

      on(event.cellElement, 'mouseout', arg => {
        if (this.timeout) {
          window.clearTimeout(this.timeout);
        }
        this.stockTooltipItemCode.instance.hide();
      });
    }
  }

  onBasketCellPrepared(e) {
    this.setItemCodeTooltip(e);
    if (e.rowType === 'data') {
      e.cellElement.style.paddingTop = '2px';
      e.cellElement.style.paddingBottom = '1px';
    }
  }

  onSiteChanged(e) {
    if (e && e.value) {
      this.filteredManagements = this.managements.filter(x => x.eShop == true && x.siteId == e.value);
    } else {
      this.filteredManagements = [];
    }
  }

  async openPartnerSchedule() {
    if (this.clientId) {
      
      this.daysOfWeek = Object.keys(DaysOfWeek)
      .filter(key => isNaN(Number(key)) && DaysOfWeek[key as keyof typeof DaysOfWeek] < 6)
      .map(key => ({
        value: DaysOfWeek[key as keyof typeof DaysOfWeek],
        label: this.translationService.instant(key)
      }));

      this.deliveryOfferPreferences = Object.keys(DeliveryOfferPreferences)
      .filter(key => isNaN(Number(key)))
      .map(key => ({
        value: DeliveryOfferPreferences[key as keyof typeof DeliveryOfferPreferences],
        label: this.translationService.instant(key)
      }));

      this.showPartnerSchedule = true;

      await this.partnerLocationService.getPartnerLocationsIncludingInactiveByPartnerID(this.clientId).then(async locations => {
        this.implicitPartnerLocation = (locations && locations.length > 0) ? (locations.find(f => f.isImplicit) || locations.find(f => f.isActive)) : new PartnerLocation();

        if (this.implicitPartnerLocation && this.implicitPartnerLocation.id) {
          this.partnerLocationScheduleComponent.getPartnerLocationSchedule(this.implicitPartnerLocation.id);  
        }

        await this.partnerContactPreferencesService.getAllByPartnerIdAsync(this.clientId).then(resp => {
          if (resp && resp.length > 0) {
            this.partnerContactPreferences = resp[0];
          }
        })
      });
    }
  }

  customizeDateBoxButtons(e) {
    let items = e.component.option("toolbarItems");
    items.shift();
    e.component.option("toolbarItems", items);
  }

  async onSavePartnerContactAndPreferences() {
    if (this.partnerScheduleValidationGroup.instance.validate().isValid) {
      await this.partnerLocationScheduleComponent.saveData();   

      this.partnerContactPreferences.partnerId = this.clientId;
      this.partnerContactPreferences.fromTime = this.partnerContactPreferences.fromTime ? moment(this.partnerContactPreferences.fromTime, "HH:mm").format("HH:mm") : null;
      this.partnerContactPreferences.toTime = this.partnerContactPreferences.toTime ? moment(this.partnerContactPreferences.toTime, "HH:mm").format("HH:mm") : null;

      await this.partnerContactPreferencesService.upsertAsync(this.partnerContactPreferences).then(resp => {
        if (resp && !resp.isError) {
          this.notificationService.alert('top', 'center', 'Preferinte Contact Partener - Datele au fost salvate cu succes!', NotificationTypeEnum.Green, true);
          this.showPartnerSchedule = false;
        } else {
          this.notificationService.alert('top', 'center', 'Preferinte Contact Partener - Datele nu au fost salvate! A aparut o eroare!', NotificationTypeEnum.Red, true)
        }
      })
    }
  }

  getDisplayExprSite(item) {
    if (!item) {
       return '';
     }
     return item.code + ' - ' + item.name;
  }

  getDisplayExprManagement(item) {
    if (!item) {
       return '';
     }
     return item.code + ' - ' + item.name;
  }

  async getNumericTurnover(): Promise<any> { 
    if (this.clientId && this.postId) { 
      await this.partnerFinancialInfoService.getNumericDetailsByPartnerIdAsync(this.clientId).then(items => { 
        this.numericTurnOvers = (items && items.length > 0) ? items : [];
      } );
    }
  }

  async getSupplierNumericTurnover(): Promise<any> { 
    if (this.clientId && this.postId) { 
      await this.partnerFinancialInfoService.getSupplierNumericDetailsByPartnerIdAsync(this.clientId).then(items => { 
        this.supplierNumericTurnOvers = (items && items.length > 0) ? items : [];
      } );
    }
  }

  getMarginTop(): string {
    return (this.clientId && this.partnerSugestionPopup) ? '10%' : '0';
  }

  partnerLocationContactIdChange(partnerLocationContactId: number) {
    this.selectedTurnoverPartnerLocationContactId = partnerLocationContactId;
  }

  hidePartnerSuggestionPopupChange(event: any) {
    this.showPartnerSuggestion = !event;
  }

  createOrder() {
    var selectedRows = this.turnoverPreorders.filter(x => x.agentQuantity > 0);
    if (selectedRows && selectedRows.length > 0 && this.selectedManagementId) {
      let isQuantityOk = true;
      selectedRows.forEach(sr => {
        if (sr.agentQuantity > sr.managementStock) {
          isQuantityOk = false;
        }
      });

      if (!isQuantityOk) {
        this.notificationService.alert('top', 'center', 'Comanda nu a fost creata! Ati selectat o cantitate mai mare ca stocul disponibil pe gestiune!', NotificationTypeEnum.Red, true);
        return;
      }

      const filteredRows = selectedRows.map(row => ({
        id: row.id,
        itemId: row.itemId,
        partnerId: row.partnerId,
        mngId: row.managementId,
        quantity: row.agentQuantity,
        price: row.price
      }));

      const encodedData = encodeURIComponent(JSON.stringify(filteredRows));
      const targetUrl =  environment.CRMPrimaware +  `/order?data=${encodedData}`;
      window.open(targetUrl, '_blank'); 

    } else {
      this.notificationService.alert('top', 'center', 'Comanda nu a fost creata! Nu ati selectat unul sau mai multe produse sau o gestiune!',
        NotificationTypeEnum.Red, true)
    }
  }

  onTurnoverPreordersSelectionChanged(data: any) {
    this.turnoverPreordersSelectedRows = data.selectedRowsData;
    this.turnoverPreordersSelectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  async deleteTurnoverPreorderRow(data) {
    if (data.data) {
      await this.turnoverPreorderService.deleteTurnoverPreorderAsync([data.data.id]).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Datele au fost sterse cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost sterse! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        };
        this.getTurnoverPreorders();
      });
    }
  }

  async onRowUpdatedTurnoverPreorders(e) {
    if (e && e.data) {
      let item = e.data;
      
      if (item.priceSource && item.priceSource !== TurnoverPreorderPriceSourceEnum.PriceComposition) {
        let originalItem = this.originalTurnoverPreorders.find(f => f.id === item.id);

        if (item.quantity < originalItem.quantity) {
          this.vipTurnoverPreorderIdSmallerQuantity = item.id;
          this.isBasketVipQuantityPopupOpen = true;
        }
      }
    }

    if (e && e.data && e.data.agentQuantity) {
      this.updateBasketTotal();
    }
    else {
      let item = new TurnoverPreorder();
      item = e.data;
  
      if (item) {
        await this.turnoverPreorderService.updateTurnoverPreorderAsync(item).then(r => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Datele au fost modificate cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Datele nu au fost modificate! A aparut o eroare!',
              NotificationTypeEnum.Red, true)
          };
          this.getTurnoverPreorders();
        });
      }
    }
  }

  async onSaveBasketVipQuantityWithPriceComposition() {
    let item = this.turnoverPreorders.find(f => f.id === this.vipTurnoverPreorderIdSmallerQuantity);

    if (item) {
      item.priceSource = TurnoverPreorderPriceSourceEnum.PriceComposition;

      this.loaded = true;
      await this.turnoverPreorderService.updateTurnoverPreorderAsync(item).then(async r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        };

        await this.getTurnoverPreorders().then(x => {
          this.isBasketVipQuantityPopupOpen = false;
          this.loaded = false;
        })
      });
    }
  }

  onCloseBasketVipQuantity() {
    let modifiedItem = this.turnoverPreorders.find(f => f.id === this.vipTurnoverPreorderIdSmallerQuantity);
    let originalItem = this.originalTurnoverPreorders.find(f => f.id === this.vipTurnoverPreorderIdSmallerQuantity);
    modifiedItem.quantity = originalItem.quantity;
    
    this.isBasketVipQuantityPopupOpen = false;
  }

  async openVipOffer(isOnlyForClient: boolean) {
    this.loaded = true;
    this.showVipOfferClientProducts = isOnlyForClient;
    await this.vipOfferItemDetailsService.GetTiersForClientAsync(null, null, this.clientId, isOnlyForClient).then(r => {
      if (r && r.length > 0) {
        this.vipOfferData = _.orderBy(r, ['tiers.length'], ['desc']);

        this.vipOfferData.forEach(vipOffer => {
          let itemGroup = this.itemGroups.store.find(f => f.id === vipOffer.itemGroupId);

          if (itemGroup) {
            vipOffer.orderNumber = itemGroup.orderNumber;
          }
        });

        this.vipOfferData = _.orderBy(this.vipOfferData, ['orderNumber', (item) => item.itemName.replace(/^[*_.\s]+/, '').toLowerCase()], ['asc', 'asc']);
        this.isVipOfferPopupOpen = true;
      } else {
        this.vipOfferData = [];
        this.isVipOfferPopupOpen = true;
      }

      this.loaded = false;
    });
  }

  async switchVipOfferProducts() {
    this.showVipOfferClientProducts = !this.showVipOfferClientProducts;
    this.vipOfferButtonText = this.showVipOfferClientProducts ? 'Vezi toate' : 'Vezi de interes';

    await this.openVipOffer(this.showVipOfferClientProducts);
  }

  async onOfferTypeValueChanged(event: any) {
    if (event && event.value && event.value.id === 5) {
      this.isDedicatedOfferPopupOpen = true;
    }

    this.isDailyWhatsappOfferPopupOpen = false;
    if (event && event.value && event.value.id === 6) {
      this.isDailyWhatsappOfferPopupOpen = true;
    }
  }

  async onSaveDedicatedOffer() {
    const { validCodes, invalidCodes } = this.validateCodes(this.dedicatedOfferData);

    if (invalidCodes.length > 0) {
      this.dedicatedOfferInvalidCodes = invalidCodes.join(', ');
      this.isDedicatedOfferInvalidCodesPopupOpen = true;
    } else {
      await this.processOffer(validCodes);
    }
  }

  async onSaveDedicatedOfferWithInvalidCodes() {
    const { validCodes } = this.validateCodes(this.dedicatedOfferData);
    await this.processOffer(validCodes);
  }

  validateCodes(inputData: string): { validCodes: string[], invalidCodes: string[] } {
    const auxProductCodes = inputData.split(/[\s,;.:?!()\[\]{}|]+/);
    const productCodes = auxProductCodes.filter(code => code.trim() !== "");
    
    let validCodes: string[] = [];
    let invalidCodes: string[] = [];

    productCodes.forEach(code => {
      const existingItem = this.items.find(item => item.code === code);

      if (existingItem) {
        validCodes.push(code);
      } else {
        const substringCode = code.substring(0, 4);
        const matchingItems = this.items.filter(item => item.code.startsWith(substringCode));

        if (!matchingItems || matchingItems.length === 0) {
          invalidCodes.push(code);
        } else {
          matchingItems.forEach(item => validCodes.push(item.code));
        }
      }
    });

    return { validCodes, invalidCodes };
  }

  async processOffer(validCodes: string[]) {
    const itemIds = this.items.filter(f => validCodes.includes(f.code)).map(m => m.id);
    await this.sendOffer(itemIds);
  }

  closeDedicatedOfferInvalidCodesPopup() {
    this.isDedicatedOfferInvalidCodesPopupOpen = false;
  }

  async onViewOfferClick(offer: any) {
    this.loaded = true;

    await this.clientTurnoversService.generateOfferToPartnerContact(this.clientId, 
      this.selectedLocationContactPerson?.id, 
      this.emailForTest, offer.id, 
      this.discountName, offer.text, 
      this.excelFileName[this.selectedOfferType.id],
      this.emailSubject[this.selectedOfferType.id],
      this.emailBody[this.selectedOfferType.id],
      []).then(result => {  
        if (result && !result.success) {
          this.notificationService.alert('top', 'center', 'A aparut o eroare! Verificati daca partenerul are alocate listele de pret conform ofertei selectate!', NotificationTypeEnum.Red, true);
        }

        this.loaded = false;
      })
  }
  
  onModifyEmailClick(offer: any) {
    this.modifyOfferEmailPopupStates[offer.id] = true;
  }

  isShoppingCartPopupOpenOutputChange(event: any) {
    this.isShoppingCartPopupOpen = event;
  }

  computeLocalTime() {
    const nowGMT = new Date(Date.UTC(
      new Date().getUTCFullYear(),
      new Date().getUTCMonth(),
      new Date().getUTCDate(),
      new Date().getUTCHours(),
      new Date().getUTCMinutes(),
      new Date().getUTCSeconds(),
      new Date().getUTCMilliseconds()
    ));

    const utcTime = new Date(nowGMT).getTime();
    const localTime = new Date(utcTime + this.currentGmtOffset * 1000);

    const [datePart, timePart] = localTime.toISOString().split('T');
    const [year, month, day] = datePart.split('-');
    const [hour, minute] = timePart.split(':');

    const dayFormatter = new Intl.DateTimeFormat('ro-RO', { weekday: 'short' });
    const dayOfWeek = dayFormatter.format(new Date(`${year}-${month}-${day}`));

    const timeFormatter = new Intl.DateTimeFormat('ro-RO', { hour: '2-digit', minute: '2-digit' });
    const formattedTime = timeFormatter.format(new Date(`${year}-${month}-${day}T${hour}:${minute}:00`));

    this.localTime = { dayOfWeek, formattedTime };
  }

  onClearFiltersClick() {
    this.searchCodeOrName = null;

    setTimeout(() => { this.showLiquidationOfferTurnover = this.shouldDisplayLiquidationOffer; });
    setTimeout(() => { this.showMandatoryPromotionTurnover = this.shouldDisplayMandatoryPromotion; });
    setTimeout(() => { this.showClientTurnover = this.shouldDisplayClientTurnover; });
    setTimeout(() => { this.showPartnerActivityTurnover = this.shouldDisplayPartnerActivity; });
    setTimeout(() => { this.showNewItemsTurnover = this.shouldDisplayNewItems; });
    setTimeout(() => { this.showOfficeDrinksTurnover = this.shouldDisplayOfficeDrinks; });
    setTimeout(() => { this.showOfficeItemsTurnover = this.shouldDisplayOfficeItems; });
    setTimeout(() => { this.showAgentOtherItemTurnover = this.shouldDisplayAgentOtherItem; });

    this.liquidationOfferTurnoverComponent?.dataGrid.instance.option("searchPanel.text", "");
    this.mandatoryPromotionTurnoverComponent?.dataGrid.instance.option("searchPanel.text", "");
    this.clientTurnoverComponent?.dataGrid.instance.option("searchPanel.text", "");
    this.partnerActivityTurnoverComponent?.dataGrid.instance.option("searchPanel.text", "");
    this.newItemsTurnoverComponent?.dataGrid.instance.option("searchPanel.text", "");
    this.officeDrinksTurnoverComponent?.dataGrid.instance.option("searchPanel.text", "");
    this.officeItemsTurnoverComponent?.dataGrid.instance.option("searchPanel.text", "");
    this.agentOtherItemsTurnoverComponent?.dataGrid.instance.option("searchPanel.text", "");

    this.onRefresh();
  }

  onCallReminderClick() {
    this.isCallReminderPopupOpen = true;
  }

  foundItemBySearchFilterLiquidationOffer(event: any) {
    this.isFoundItemLiquidationOffer = event;
  }

  foundItemBySearchFilterMandatoryPromotion(event: any) {
    this.isFoundItemMandatoryPromotion = event;
  }

  foundItemBySearchFilterClientTurnover(event: any) {
    this.isFoundItemClientTurnover = event;
  }

  foundItemBySearchFilterPartnerActivity(event: any) {
    this.isFoundItemPartnerActivity = event;
  }

  foundItemBySearchFilterNewItems(event: any) {
    this.isFoundItemNewItems = event;
  }

  foundItemBySearchFilterOfficeDrinks(event: any) {
    this.isFoundItemOfficeDrinks = event;
  }

  foundItemBySearchFilterOfficeItems(event: any) {
    this.isFoundItemOfficeItems = event;
  }

  foundItemBySearchFilterAgentOtherItem(event: any) {
    this.isFoundItemAgentOtherItem = event;
  }

  getMonth(offset: number): string {
    const currentMonth = new Date().getMonth();
    const monthIndex = (currentMonth - offset + 12) % 12;
    const monthsInRomanian = [
      "Ian", "Feb", "Mar", "Apr", "Mai", "Iun",
      "Iul", "Aug", "Sep", "Oct", "Noi", "Dec"
    ];
    if (offset == 0) {
      return monthsInRomanian[monthIndex] + ' Curent';
    } else {
      return monthsInRomanian[monthIndex];
    }
  }

  priceCurrencyExpr(data) {
    if (!data || !data?.listPrice) {
      return '';
    }
    return data.listPrice.toFixed(2) + ' ' + data.currencyName;
  }

  async getSendOfferToPartnerLogTooltipData() {
    this.sendOfferToPartnerLogTooltipData = 'Se incarca...'; 
    this.isSendOfferToPartnerTooltipVisible = true;

    await this.sendOfferToPartnerLogService.getLastByPartnerIdAsync(this.clientId).then(resp => {
      if (resp) {
        let relevantOfferName = this.offerTypes.find(f => f.id === resp.offerType)?.text ?? '';
        let relevantOfferLastDate = moment(resp.date).format('DD.MM.YYYY');

        this.sendOfferToPartnerLogTooltipData = `Ultima Oferta trimisa: ${relevantOfferName} / ${relevantOfferLastDate}`;
      } else {
        this.sendOfferToPartnerLogTooltipData = '';
      }
    })
  }

  hideSendOfferToPartnerLogTooltipData(): void {
    this.isSendOfferToPartnerTooltipVisible = false;
  }

  async getSendOfferToPartnerLogTooltipDataPartnerPage() {
    this.sendOfferToPartnerLogTooltipDataPartnerPage = 'Se incarca...'; 
    this.isSendOfferToPartnerTooltipVisiblePartnerPage = true;

    await this.sendOfferToPartnerLogService.getLastByPartnerIdAsync(this.clientId).then(resp => {
      if (resp) {
        let relevantOfferName = this.offerTypes.find(f => f.id === resp.offerType)?.text ?? '';
        let relevantOfferLastDate = moment(resp.date).format('DD.MM.YYYY');

        this.sendOfferToPartnerLogTooltipDataPartnerPage = `Ultima Oferta trimisa: ${relevantOfferName} / ${relevantOfferLastDate}`;
      } else {
        this.sendOfferToPartnerLogTooltipDataPartnerPage = '';
      }
    })
  }

  hideSendOfferToPartnerLogTooltipDataPartnerPage(): void {
    this.isSendOfferToPartnerTooltipVisiblePartnerPage = false;
  }

  async addItemToCartFromVipOffer(vipIndex: number, itemId: number, quantity: number) {
    let createPreoder = new TurnoverPreorder();
    createPreoder.customerId = Number(this.authService.getUserCustomerId());
    createPreoder.partnerId = this.clientId;
    createPreoder.postId = Number(this.authService.getUserPostId());
    createPreoder.itemId = itemId;
    createPreoder.quantity = quantity;
    createPreoder.priceSource = vipIndex;

    this.loaded = true;
    await this.turnoverPreorderService.createTurnoverPreorderAsync(createPreoder).then(() => {
      
      this.notificationService.alert('top', 'center', 'Produsul a fost adaugat in cos!', NotificationTypeEnum.Green, true);
      this.loaded = false;
    })
  }

  getDisplayExprBrands(item) {
    if (!item) {
        return '';
      }

    return item.code;;
  }

  onOrderAmountCheckBoxChange(event) {
    if (event) {
      this.isOrderAmountCommunicatedToClient = event.value;
    }
  }

  isComponentShownLiquidationOffer(event: any) {
    this.showLiquidationOfferTurnover = event;
    this.shouldDisplayLiquidationOffer = event;
  }

  isComponentShownMandatoryPromotion(event: any) {
    this.showMandatoryPromotionTurnover = event;
    this.shouldDisplayMandatoryPromotion = event;
  }

  isComponentShownClientTurnover(event: any) {
    this.showClientTurnover = event;
    this.shouldDisplayClientTurnover = event;
  }

  isComponentShownPartnerActivity(event: any) {
    this.showPartnerActivityTurnover = event;
    this.shouldDisplayPartnerActivity = event;
  }

  isComponentShownNewItems(event: any) {
    this.showNewItemsTurnover = event;
    this.shouldDisplayNewItems = event;
  }

  isComponentShownOfficeDrinks(event: any) {
    this.showOfficeDrinksTurnover = event;
    this.shouldDisplayOfficeDrinks = event;
  }

  isComponentShownOfficeItems(event: any) {
    this.showOfficeItemsTurnover = event;
    this.shouldDisplayOfficeItems = event;
  }

  isComponentShownAgentOtherItem(event: any) {
    this.showAgentOtherItemTurnover = event;
    this.shouldDisplayAgentOtherItem = event;
  }
}
