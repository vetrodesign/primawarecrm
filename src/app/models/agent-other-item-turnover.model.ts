import { BaseDomain } from "./base-domain.model";
import { PartnerXItemComment } from "./clientturnover.model";

export class AgentOtherItemTurnover extends BaseDomain {
  itemId: number;
  itemCode: string;
  itemName: string;
  listPrice: number;
  currentStock: number;
  cantBaxArticol: number;

  l12: number;
  l11: number;
  l10: number;
  l9: number;
  l8: number;
  l7: number;
  l6: number;
  l5: number;
  l4: number;
  l3: number;
  l2: number;
  l1: number;
  lc: number;

  lastComment: PartnerXItemComment;
  itemComments: PartnerXItemComment[];
}