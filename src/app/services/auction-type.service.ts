import { Injectable } from '@angular/core';
import { AuctionType } from 'app/models/auction-type.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class AuctionTypeService {

  constructor(private apiService: ApiService) { }

  async getAllAuctionTypesAsync(): Promise<AuctionType[]> {
    const apiURL =  `/auctiontype/getAll`;
    return await this.apiService.GetAsync<AuctionType[]>(apiURL, Constants.mrkApi);
  }

  async getAllAuctionTypesByCustomerIdAsync(): Promise<AuctionType[]> {
    const apiURL = `/auctiontype/getByCustomerId`;
    return await this.apiService.GetAsync<AuctionType[]>(apiURL, Constants.mrkApi);
  }

  async createAuctionTypeAsync(auction: AuctionType): Promise<any> {
    const apiURL =  `/auctiontype/create`;
    return await this.apiService.PostAsync<any>(apiURL, auction, Constants.mrkApi);
  }

  async updateAuctionTypeAsync(auction: AuctionType): Promise<any> {
    const apiURL =  `/auctiontype/update`;
    return await this.apiService.PutAsync<any>(apiURL, auction, Constants.mrkApi);
  }

  async deleteAuctionTypesAsync(ids: number[]): Promise<any> {
    const apiURL = `/auctiontype/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
