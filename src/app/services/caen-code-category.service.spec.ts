import { TestBed } from '@angular/core/testing';

import { CaenCodeCategoryService } from './caen-code-category.service';

describe('CaenCodeCategoryService', () => {
  let service: CaenCodeCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CaenCodeCategoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
