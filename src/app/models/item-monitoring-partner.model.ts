import { BaseDomain } from "./base-domain.model";

export class ItemMonitoringPartnerModel extends BaseDomain {
    partnerId: number;
    baseURL: string;
    isImplemented: boolean;

    isActive: boolean;

    // FE only
    partnerName: string;
    totalProductsCount: number;
    lowerPriceProductsCount: number;
    higherPriceProductsCount: number;
    noStockCount: number;
    noStockCountMine: number;
}