import { BaseDomain } from "./base-domain.model";

export class Tag extends BaseDomain {
    code: string;
    name: string;
    description: string;

    isActive?: boolean;
    customerId: number;
}
