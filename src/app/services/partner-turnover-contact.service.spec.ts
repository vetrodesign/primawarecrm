import { TestBed } from '@angular/core/testing';

import { PartnerTurnoverContactService } from './partner-turnover-contact.service';

describe('PartnerTurnoverContactService', () => {
  let service: PartnerTurnoverContactService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerTurnoverContactService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
