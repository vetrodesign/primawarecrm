import { Injectable } from '@angular/core';
import { ItemPriceParam } from 'app/models/item.model';
import { SalePriceListItem } from 'app/models/salepricelistitem.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class SalePriceListItemService {

  constructor(private apiService: ApiService) { }

  async getSalePriceListItemsAsyncByID(id): Promise<any[]> {
    const apiURL = `/salepricelistitem/getbysalepricelistid/` + id;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getSalePriceListItemsAsyncByIDs(ids : number[]): Promise<any[]> {
    const apiURL = `/salepricelistitem/GetBySalePriceListIds/`;
    return await this.apiService.PostAsync<any[]>(apiURL, ids, Constants.mrkApi);
  }

  async createSalePriceListItemAsync(salePriceListItem: SalePriceListItem): Promise<any> {
    const apiURL = `/salepricelistitem/create`;
    return await this.apiService.PostAsync<any>(apiURL, salePriceListItem, Constants.mrkApi);
  }

  async updateSalePriceListItemAsync(salePriceListItem: SalePriceListItem): Promise<any> {
    const apiURL = `/salepricelistitem/update`;
    return await this.apiService.PutAsync<any>(apiURL, salePriceListItem, Constants.mrkApi);
  }

  async deleteSalePriceListItemsAsync(ids: number[]): Promise<any> {
    const apiURL = `/salepricelistitem/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }

  async getItemPriceAndRate(itemId: number, partnerId: number, salePriceListId: number): Promise<any> {
    let p = new ItemPriceParam();
    p.itemId = itemId;
    p.partnerId = partnerId;
    p.salePriceListId = salePriceListId;

    const apiURL = `/salepricelistitem/GetItemPriceAndRate`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }
}
