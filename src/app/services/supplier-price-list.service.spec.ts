import { TestBed } from '@angular/core/testing';

import { SupplierPriceListService } from './supplier-price-list.service';

describe('SupplierPriceListService', () => {
  let service: SupplierPriceListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SupplierPriceListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
