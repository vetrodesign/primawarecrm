import { TestBed } from '@angular/core/testing';

import { VipOfferItemsDetailsService } from './vip-offer-items-details.service';

describe('VipOfferItemsDetailsService', () => {
  let service: VipOfferItemsDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VipOfferItemsDetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
