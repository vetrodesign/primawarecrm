import { Component, OnInit, AfterViewInit, ViewChild } from "@angular/core";
import { NotificationTypeEnum } from "app/enums/notificationTypeEnum";
import { GridToolbarComponent } from "app/helpers/grid-toolbar/grid-toolbar.component";
import { IPageActions } from "app/models/ipageactions";
import { AuthService } from "app/services/auth.service";
import { NotificationService } from "app/services/notification.service";
import { AuctionService } from "app/services/auction.service";
import { TranslateService } from "app/services/translate";
import { DxDataGridComponent, DxPopupComponent } from "devextreme-angular";
import { ImportAuction } from "app/models/importauction.model";
import { AuctionTypeComponent } from "app/auction-type/auction-type.component";
import { AuctionType } from "app/models/auction-type.model";
import { AuctionTypeService } from "app/services/auction-type.service";
import { ImportAuctionsComponent } from "app/import-auctions/import-auctions.component";
import { AuctionGroupComponent } from "app/auction-group/auction-group.component";
import { AuctionGroup } from "app/models/auction-group.model";
import { AuctionGroupService } from "app/services/auction-group.service";
import { CpvCodeService } from "app/services/cpv-code.service";
import { AuctionSearchFilter } from "app/models/auctionSearchFilter.model";
import { PostService } from "app/services/post.service";
import { DepartmentsService } from "app/services/departments.service";
import { UsersService } from "app/services/user.service";
import { DepartmentTypeEnum } from "app/enums/departmentTypeEnum";
import { AuctionsDetailsComponent } from "app/auctions-details/auctions-details.component";
import { AuctionCpvCodeService } from "app/services/auction-cpv-code.service";
import { AuctionParticipationEnum } from "app/enums/auctionFilterDayEnum";
import { AuctionGroupCpvCodeService } from "app/services/auction-group-cpv-code.service";
import { AuctionGroupXCpvCode } from "app/models/auction-group-cpv-code.model";
import { AuctionResultEnum } from "app/enums/auctionResultTypeEnum";
import { AuctionStatusEnum } from "app/enums/auctionStatusTypeEnum";
import * as moment from 'moment';
import { CountyService } from "app/services/county.service";
import { CountryService } from "app/services/country.service";
import { AuctionPostsService } from "app/services/auction-posts.service";
import { AuctionPosts } from "app/models/auction-posts.model";
import { ActivatedRoute } from "@angular/router";
import { ClientModuleMenuItemsService } from "app/services/client-module-menu-items.service";

@Component({
  selector: 'app-auctions',
  templateUrl: './auctions.component.html',
  styleUrls: ['./auctions.component.css']
})
export class AuctionsComponent implements OnInit, AfterViewInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;

  @ViewChild('auctionType') auctionType: AuctionTypeComponent;
  @ViewChild('auctionGroup') auctionGroup: AuctionGroupComponent;
  @ViewChild('auctionImport') auctionImport: ImportAuctionsComponent;
  @ViewChild('auctionsDetails') auctionsDetails: AuctionsDetailsComponent;
  @ViewChild('popOver') popup: DxPopupComponent;

  displayData: boolean;
  loaded: boolean;
  actions: IPageActions;

  auctions: ImportAuction[] = [];
  auctionTypes: AuctionType[] = [];
  auctionGroups: AuctionGroup[] = [];
  auctionGroupCpvCodes: AuctionGroupXCpvCode[] = [];

  cpvCodes: any;
  departments: any[];
  posts: any[];
  allPosts: any[];
  users: any[];
  tabs: any[];

  counties: any[] = [];
  countries: any[] = [];
  postData: any[] = [];
  countriesDS: any;
  countiesDS: any;

  selectedRows: any[];
  selectedAuction: any;
  selectedRowIndex = -1;
  rowIndex: any;
  groupedText: string;
  isTabActive: string;
  auctionSearchFilter: AuctionSearchFilter;

  postIds: number[];
  userPostId: number;
  initialPostIds: any;

  auctionParticipationTypes: { id: number; name: string }[] = [];
  auctionResultTypes: { id: number; name: string }[] = [];
  auctionStatusTypes: { id: number; name: string }[] = [];

  isOwner: boolean;
  detailsVisibility: boolean;

  infoButton: string;

  constructor(
    private auctionService: AuctionService,
    private auctionCpvCodeService: AuctionCpvCodeService,
    private auctionPostsService: AuctionPostsService,
    private auctionTypeService: AuctionTypeService,
    private auctionGroupCpvCodeService: AuctionGroupCpvCodeService,
    private cpvCodeService: CpvCodeService,
    private userService: UsersService,
    private countyService: CountyService,
    private countryService: CountryService,
    private departmentsService: DepartmentsService,
    private auctionGroupService: AuctionGroupService,
    private translationService: TranslateService,
    private notificationService: NotificationService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private clientModuleMenuItemsService: ClientModuleMenuItemsService) {
    this.isOwner = this.authService.isUserOwner();
    this.groupedText = this.translationService.instant('groupedText');
    this.isTabActive = this.authService.getCustomerPageTitleTheme();

    this.onCellHoverChanged = this.onCellHoverChanged.bind(this);

    this.setActions();
    this.getPostData()
    this.getDictionaryData();

    

    for (let n in AuctionParticipationEnum) {
      if (typeof AuctionParticipationEnum[n] === 'number') {
        this.auctionParticipationTypes.push({
          id: <any>AuctionParticipationEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }

    for (let n in AuctionResultEnum) {
      if (typeof AuctionResultEnum[n] === 'number') {
        this.auctionResultTypes.push({
          id: <any>AuctionResultEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }

    for (let n in AuctionStatusEnum) {
      if (typeof AuctionStatusEnum[n] === 'number') {
        this.auctionStatusTypes.push({
          id: <any>AuctionStatusEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }

    this.tabs = [{ 'id': 1, 'text': 'Administrare', 'isActive': true },
    { 'id': 2, 'text': 'Tip Licitatie', 'isActive': false },
    { 'id': 3, 'text': 'Grup CPV', 'isActive': false },
    { 'id': 4, 'text': 'Posturi Alocate', 'isActive': false },
    { 'id': 5, 'text': 'Import', 'isActive': false }
    ];
  }

 

  ngOnInit(): void {
  }

  async getCounties() {
    await this.countyService.getAllCountysAsync().then(items => {
      this.counties = (items && items.length > 0) ? items : [];
      this.countiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.counties
      };
    });
  }

  async getCountries() {
    await this.countryService.getAllCountrysAsync().then(items => {
      this.countries = (items && items.length > 0) ? items : [];
      this.countriesDS = {
        paginate: true,
        pageSize: 15,
        store: this.countries
      };
    });
  }

  toggleMode(tab: any) {
    this.tabs.find(x => x.isActive).isActive = false;
    tab.isActive = true;

    if (tab.id == 1) {
      setTimeout(() => {
        this.setGridInstance();
      }, 100);
      this.loadData();
    }

    if (tab.id == 2) {
      this.auctionType.loadData();
    }

    if (tab.id == 3) {
      this.auctionGroup.loadData();
    }

    if (tab.id == 4) {

    }
  }

  async loadData() {
    this.loaded = true;
    Promise.all([this.getAuctionTypes(), this.getAuctionGroups(), this.getAuctionGroupCpvCode(),]).then(x => {
      this.loaded = false
    });
  }

  async getDictionaryData() {
    this.loaded = true;
    Promise.all([this.getCpvCodes(), this.getUsers(), this.getAuctionPosts(), this.getDepartments(), this.getCountries(), this.getCounties()]).then(x => {
      this.setPost();
      this.loadData();
    });
  }

  setPost() {
    this.auctionSearchFilter = new AuctionSearchFilter();
    let user = this.users.find(x => x.userName === this.authService.getUserUserName());
    if (user && user.postId && this.postIds.filter(x => x == user.postId).length > 0) {
      this.userPostId = user.postId;
    } else {
      this.userPostId = 0;
    }
  }

  async getAuctions() {
    if (this.isOwner) {
      await this.auctionService.getAllAuctionsAsync().then(items => {
        if (items && items.length > 0) {
          this.auctions = items;
          this.loadAuctionCpvCode();
        } else {
          this.auctions = [];
        }
      });
    }
    else {
      await this.auctionService.getAuctionsByFilter(this.auctionSearchFilter).then(items => {
        if (items && items.length > 0) {
          this.auctions = items;
          this.loadAuctionCpvCode();
        } else {
          this.auctions = [];
        }
      });
    }
  }

  async getAuctionGroupCpvCode() {
    await this.auctionGroupCpvCodeService.getAllAuctionGroupCpvCodes().then(items => {
      if (items && items.length > 0) {
        this.auctionGroupCpvCodes = items;
      }
    });
  }

  async getPostData() {
    await this.auctionService.getAuctionsPostStatistics().then(items => {
      if (items && items.length > 0) {
        this.postData = items;
      }
    });
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && e.data.endingDate === 1) {
      e.rowElement.style.backgroundColor = '#ffaf82';
    }
    if (e.rowType === 'data' && e.data && e.data.endingDate === 2) {
      e.rowElement.style.backgroundColor = '#abd5ff';
    }
    if (e.rowType === 'data' && e.data && e.data.endingDate === 3) {
      e.rowElement.style.backgroundColor = '#c3ffba';
    }
  }

  loadAuctionCpvCode() {
    this.auctionCpvCodeService.getAllAuctionCpvCodes().then(items => {
      if (items) {
        this.auctions.forEach(u => {
          const i = items.filter(x => x.auctionId === u.id)
            .map(x => x.cpvCodeId);
          if (i) {
            u.existingCpvCodeIds = i ? i : [];
            u.cpvCodeIds = i ? i : [];
          }
        });
      }
    });
  }

  async getAuctionTypes() {
    this.auctionTypes = [];
    if (this.isOwner) {
      await this.auctionTypeService.getAllAuctionTypesAsync().then(items => {
        this.auctionTypes = (items && items.length > 0) ? items : [];
      });
    }
    else {
      await this.auctionTypeService.getAllAuctionTypesByCustomerIdAsync().then(items => {
        this.auctionTypes = (items && items.length > 0) ? items : [];
      });
    }
  }

  async getAuctionPosts() {
    await this.auctionPostsService.getAllAsync().then(items => {
      this.postIds = (items && items.length > 0) ? items.map(x => x.postId) : [];
      this.initialPostIds = (items && items.length > 0) ? items : [];
    });
  }

  async savePostsAllocation() {
    if (this.postIds.length > 0) {
      // Create AuctionPosts
      this.loaded = true;
      const existingIds = this.initialPostIds;
      let deleteData: AuctionPosts[] = [];
      existingIds.filter(x => !this.postIds.includes(x)).forEach(i => {
        const c = new AuctionPosts();
        c.postId = i.postId;
        c.id = i.id;
        deleteData.push(c);
      });
      if (deleteData.length > 0) {
        this.auctionPostsService.deleteAsync(deleteData);
      }
      let createIds: AuctionPosts[] = [];
      this.postIds.filter(x => !existingIds.includes(x)).forEach(id => {
        const c = new AuctionPosts();
        c.postId = id;
        c.customerId = Number(this.authService.getUserCustomerId());
        createIds.push(c);
      });
      if (createIds.length > 0) {
        this.auctionPostsService.createMultipleAsync(createIds).then(x => {
          this.getAuctionPosts();
          this.notificationService.alert('top', 'center', 'Alocare Posturi - Datele au fost salvate cu succes!',
            NotificationTypeEnum.Green, true)
          this.loaded = false;
        });
      }
    }
  }

  async getAuctionGroups() {
    this.auctionGroups = [];
    if (this.isOwner) {
      await this.auctionGroupService.getAllAuctionGroupsAsync().then(items => {
        this.auctionGroups = (items && items.length > 0) ? items : [];
      });
    }
    else {
      await this.auctionGroupService.getAllAuctionGroupsByCustomerIdAsync().then(items => {
        this.auctionGroups = (items && items.length > 0) ? items : [];
      });
    }
  }

  async getDepartments() {
    this.departments = [];
    if (this.isOwner) {
      await this.departmentsService.getAllDepartmentsAsync().then(items => {
        this.departments = items;
        this.setSalesPosts();
      });
    } else {
      await this.departmentsService.getDepartmentsAsyncByID().then(items => {
        this.departments = items;
        this.setSalesPosts();
      });
    }
  }

  async getUsers() {
    this.users = [];
    if (this.isOwner) {
      await this.userService.getUsersAsync().then(items => {
        this.users = items;
      });
    } else {
      await this.userService.getUserAsyncByID().then(items => {
        this.users = items;
      });
    }
  }

  setSalesPosts() {
    if (this.departments && this.departments.length > 0) {
      const off = this.departments.map(x => x.offices).reduce((x, y) => x.concat(y));
      if (off && off.some(x => x.posts.length > 0)) {
        this.allPosts = off.map(x => x.posts)
          .reduce((x, y) => x.concat(y));
      }

      const salesDeps = this.departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Sales);
      if (salesDeps && salesDeps.length > 0 && salesDeps[0].offices) {
        const off = salesDeps.map(x => x.offices).reduce((x, y) => x.concat());
        if (off && off.some(x => x.posts.length > 0)) {
          this.posts = off.map(x => x.posts)
            .reduce((x, y) => x.concat(y));
        }
      }
    }
  }

  async getCpvCodes() {
    this.cpvCodes = [];
    await this.cpvCodeService.getAllCpvCodesAsync().then(items => {
      if (items && items.length > 0) {
        this.cpvCodes = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      }
    });
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }


  ngAfterViewInit() {
    this.setGridInstance();
  }

  setGridInstance() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  setActions() {
    this.actions = {
      CanView: true,
      CanAdd: true,
      CanUpdate: true,
      CanDelete: false,
      CanPrint: true,
      CanExport: true,
      CanImport: true,
      CanDuplicate: true
    };
  }

  canDelete() {
    return this.actions.CanDelete;
  }

  openFolder(data) {
    if (data && data.data) {
      window.open('filebrowser:///' + data.data.folderUrl);
    }
  }

  async searchEvent(e: any) {
    this.auctionSearchFilter = e
    this.loaded = true;
    await this.getAuctions().then(x => this.loaded = false);
  }

  refreshEvent(e: any) {
    this.getAuctionTypes();
    this.getAuctionGroups();
  }

  public add() {
    this.dataGrid.instance.addRow();
  }

  public refreshDataGrid() {
    this.getAuctions();
    this.dataGrid.instance.refresh();
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public openLink(data) {
    if (data && data.data && data.data.url) {
      window.open(data.data.url, '_blank').focus();
    }
  }

  public deleteRecords(data: any) {
    this.auctionService.deleteAuctionsAsync(this.selectedRows.map(x => x.id)).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Licitatii - Datele au fost sterse cu succes!',
          NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Licitatii - Datele nu au fost sterse!',
          NotificationTypeEnum.Red, true)
      }
      this.refreshDataGrid();
    });
  }

  public async openDetails(row: any) {
    this.selectedAuction = row;
    setTimeout(() => {
      this.auctionsDetails.loadData().then(function () {
        this.detailsVisibility = true;
      }.bind(this));
    }, 0);
  }

  changeVisible() {
    this.detailsVisibility = false;
    this.getAuctions();
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  getDisplayExprNumber(item) {
    if (!item) {
      return '';
    }
    let c = this.auctionGroups.find(x => x.id === item.auctionGroupId)?.code;
    return item.id + (c ? ` (${c})` : '');
  }

  getDisplayExprCountry(item) {
    if (!item) {
      return '';
    }
    if (this.countriesDS && this.countiesDS) {
      let countryName = this.countriesDS.store.find(x => x.id === item.countryId)?.name;
      let countyName = this.countiesDS.store.find(x => x.id === item.countyId)?.name;
      if (countryName && countyName) {
        return countryName + ' - ' + countyName;
      } else {
        return '';
      }
    }
  }

  getLotsExpr(item) {
    if (!item) {
      return '';
    }

    const lotsNr = item.lotsNumberTo?.toString().split(",").filter(n => n).length;
    return (lotsNr && item.lotsNumberFrom) ? (lotsNr + ' / ' + item.lotsNumberFrom) : '';
  }

  getEstimatedValueExpr(item) {
    if (!item) {
      return '';
    }
    return item.estimatedValue ? (Number(item.estimatedValue.toFixed(0)).toLocaleString('en-US') + ' RON') : '';
  }

  getDisplayAuctionTypeExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  postNameExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  getDisplayPostExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  getPostName(postId) {
    if (!postId) {
      return '';
    }
    if (this.allPosts && this.allPosts.length > 0) {
      let p = this.allPosts.find(x => x.id == postId);
      return p ? (p.code + ' - ' + p.companyPost) : '';
    }
  }

  onRowUpdated(e) {
    if (e && e.data) {
      let auction = e.data;
      if (auction.participationTypeId) {

        if (auction.result && auction.status < 5) {
          auction.result = null;
          this.notificationService.alert('top', 'center', 'Licitatii - Operatiunea nu s-a finalizat, deoarece licitatia nu este in stare de deliberare!',
            NotificationTypeEnum.Red, true)
        }

        if (auction.result && (auction.status >= 5 && auction.status !== 10)) {
          if (auction.result == Number(AuctionResultEnum.TotalWin)) {
            auction.status = Number(AuctionStatusEnum.TotalWin);
          }
          if (auction.result == Number(AuctionResultEnum.TotalRejected)) {
            auction.status = Number(AuctionStatusEnum.TotalRejected);
          }
          if (auction.result == Number(AuctionResultEnum.PartialWin)) {
            auction.status = Number(AuctionStatusEnum.PartialWin);
          }
          if (auction.result == Number(AuctionResultEnum.Disqualified)) {
            auction.status = Number(AuctionStatusEnum.Disqualified);
          }

        } else {
          if (auction.isCanceled) {
            auction.status = Number(AuctionStatusEnum.Canceled);
            return;
          }
          if (auction.participationTypeId == 2 || auction.participationTypeId == 3) {
            auction.status = Number(AuctionStatusEnum.Rejected);
          }
          if (auction.participationTypeId == 1) {
            auction.status = Number(AuctionStatusEnum.Approved);
          }
          if (auction.participationTypeId == 1 && auction.hasRequestedPrices) {
            auction.status = Number(AuctionStatusEnum.RequestedPrice);
          }
          if (auction.participationTypeId == 1 && auction.hasRequestedPrices && auction.hasReceivedPrices) {
            auction.status = Number(AuctionStatusEnum.Working);
          }

          let d = new Date();
          if (auction.participationTypeId == 1 && auction.hasRequestedPrices && auction.hasReceivedPrices && d > new Date(auction.endDate)) {
            auction.status = Number(AuctionStatusEnum.Deliberation);
          }
        }

        let item = new ImportAuction();
        item = auction;
        if (item && item.id) {
          this.loaded = true;
          this.auctionService.updateAuctionAsync(item).then(r => {
            if (r) {
              this.notificationService.alert('top', 'center', 'Licitatii - Datele au fost modificate cu succes!',
                NotificationTypeEnum.Green, true)
            }
            this.refreshGridSimple();
            this.loaded = false;
          });
        }

      } else {
        this.notificationService.alert('top', 'center', 'Licitatii - Operatiunea nu s-a finalizat, deoarece licitatia nu are proprietatea de Tip Participare selectata!',
          NotificationTypeEnum.Red, true)
        this.refreshGridSimple();
      }
    }
  }

  async refreshGridSimple() {
    await this.auctionService.getAuctionsByFilter(this.auctionSearchFilter).then(items => {
      if (items && items.length > 0) {
        this.auctions = items;
      } else {
        this.auctions = [];
      }
    });
    this.dataGrid.instance.refresh();
  }

  getWithNoStatus() {
    if (this.auctions && this.auctions.length > 0) {
      return this.auctions.filter(x => !x.status).length;
    }
  }



  onCellHoverChanged(e: any) {
    if (e.rowType === "data") {
      if (e.column.dataField === "id") {
        let ag = this.auctionGroups.find(x => x.id === e.data.auctionGroupId);
        if (ag) {
          this.popup.instance.option("contentTemplate",
            function (contentElement) {

              $("<div/>")
                .append('<div>' + ag.code + ' - ' + ag.name + '</div>')
                .appendTo(contentElement);
            });
          this.popup.instance.option("target", e.cellElement);
          this.popup.instance.show();
        }
      }

      if (e.column.dataField === "postId") {
        let u = this.users.find(x => x.postId === e.data.postId);
        let user = 'Utilizator: ' + u?.userName + ' - ' + u?.firstName + ' ' + u?.lastName + ' - ' + u?.email;

        this.popup.instance.option("contentTemplate",
          function (contentElement) {

            $("<div/>")
              .append('<div>' + user + '</div>')
              .appendTo(contentElement);
          });
        this.popup.instance.option("target", e.cellElement);
        this.popup.instance.show();
      }

      if (e.column.dataField === "acquisitionNumber") {
        let at = this.auctionTypes.find(x => x.id === e.data.auctionTypeId);

        if (at) {
          let uc = this.users.find(x => x.id === e.data.createdBy);
          let userNameCreated = uc?.userName + ' - ' + uc?.firstName + ' ' + uc?.lastName;

          let u = this.users.find(x => x.id === e.data.modifiedBy);
          let userNameModified = u?.userName + ' - ' + u?.firstName + ' ' + u?.lastName;

          let c = moment(e.data.created).format('DD-MM-YYYY HH:mm');
          let d = moment(e.data.modified).format('DD-MM-YYYY HH:mm');

          this.popup.instance.option("contentTemplate",
            function (contentElement) {
              $("<div/>")
                .append('<div>' + at.code + ' - ' + at.name + '</div><br>' + '<div>' + 'Data creare: ' + c + ' de ' + userNameCreated + '</div>' + '<div>' + 'Ultima modificare: ' + d + ' de ' + userNameModified + '</div>')
                .appendTo(contentElement);
            });
          this.popup.instance.option("target", e.cellElement);
          this.popup.instance.show();
        }
      }
    }
  }
}

