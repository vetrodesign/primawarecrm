import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent implements OnInit, OnChanges {
@Input() visible: boolean = true;
@Input() zone: string;

public hidden: boolean = false;
indicatorUrl: string = "https://js.devexpress.com/Content/data/loadingIcons/rolling.svg";
  constructor() { }

  ngOnInit(): void {
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.visible) {
      if (!changes.visible.currentValue) {
        setTimeout(() => {
          this.hidden = true;
        }, 300);
      } else {
        this.hidden = false;
      }
    }
  }

  onShown() {
  }

  onHidden() { 
  }
}