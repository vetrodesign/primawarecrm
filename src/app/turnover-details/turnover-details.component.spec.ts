import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurnoverDetailsComponent } from './turnover-details.component';

describe('TurnoverDetailsComponent', () => {
  let component: TurnoverDetailsComponent;
  let fixture: ComponentFixture<TurnoverDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurnoverDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurnoverDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
