import { BaseDomain } from "./base-domain.model";

export class PartnerXItemGroupCode extends BaseDomain {
    partnerId: number;
    itemGroupCodeId: number;
}