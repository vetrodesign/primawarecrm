import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurnoverCommentComponent } from './turnover-comment.component';

describe('TurnoverCommentComponent', () => {
  let component: TurnoverCommentComponent;
  let fixture: ComponentFixture<TurnoverCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurnoverCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurnoverCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
