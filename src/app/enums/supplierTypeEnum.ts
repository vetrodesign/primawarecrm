export enum SupplierTypeEnum {
  None = 0,
  Active = 1,
  Possible = 2,
  Inactive = 3
}