import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { CpvCode } from 'app/models/cpvcode.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class CpvCodeService {

  constructor(private apiService: ApiService) { }

  async getAllCpvCodesAsync(): Promise<any[]> {
    const apiURL = `/cpvcode/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL,Constants.commonApi);
  }

  async createCpvCodeAsync(cpvCode: CpvCode): Promise<any> {
    const apiURL = `/cpvcode/create`;
    return await this.apiService.PostAsync<any>(apiURL, cpvCode,Constants.commonApi);
  }

  async updateCpvCodeAsync(cpvCode: CpvCode): Promise<any> {
    const apiURL = `/cpvcode/update`;
    return await this.apiService.PutAsync<any>(apiURL, cpvCode,Constants.commonApi);
  }

  async deleteCpvCodeAsync(ids: number[]): Promise<any> {
    const apiURL = `/cpvcode/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids,Constants.commonApi);
  }
}
