export enum TurnoverPreorderPriceSourceEnum {
    VIP1 = 1,
    VIP2 = 2,
    VIP3 = 3,
    VIP4 = 4,
    VIP5 = 5,
    PriceComposition = 6
}