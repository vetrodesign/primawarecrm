import { TestBed } from '@angular/core/testing';

import { OfficeItemsTurnoverService } from './office-items-turnover.service';

describe('OfficeItemsTurnoverService', () => {
  let service: OfficeItemsTurnoverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OfficeItemsTurnoverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
