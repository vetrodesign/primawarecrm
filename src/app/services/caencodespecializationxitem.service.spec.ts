import { TestBed } from '@angular/core/testing';

import { CaenCodeSpecializationXItemService } from './caencodespecializationxitem.service';

describe('CaencodespecializationxitemService', () => {
  let service: CaenCodeSpecializationXItemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CaenCodeSpecializationXItemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
