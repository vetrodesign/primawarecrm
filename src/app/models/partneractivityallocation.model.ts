import { BaseDomain } from "./base-domain.model";

export class PartnerActivityAllocation extends BaseDomain {
    partnerActivityId: number;
    partnerActivityClassification: string;
    isLegal: boolean;
    isIndividual: boolean;
    productConventionId: number;
    saleZoneCode: string;

    caenCodeSpecializationIds: any;
    existingCaenCodeIds: any;
}