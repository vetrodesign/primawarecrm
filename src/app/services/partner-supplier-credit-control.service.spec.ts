import { TestBed } from '@angular/core/testing';

import { PartnerSupplierCreditControlService } from './partner-supplier-credit-control.service';

describe('PartnerSupplierCreditControlService', () => {
  let service: PartnerSupplierCreditControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerSupplierCreditControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
