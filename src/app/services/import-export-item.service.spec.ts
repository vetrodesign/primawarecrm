import { TestBed } from '@angular/core/testing';

import { ImportExportItemService } from './import-export-item.service';

describe('ImportExportItemService', () => {
  let service: ImportExportItemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImportExportItemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
