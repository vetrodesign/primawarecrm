import { Injectable } from '@angular/core';
import { AuctionSearchFilter } from 'app/models/auctionSearchFilter.model';
import { ImportAuction } from 'app/models/importauction.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';


@Injectable({
  providedIn: 'root'
})
export class AuctionService {

  constructor(private apiService: ApiService) { }

  async getAllAuctionsAsync(): Promise<ImportAuction[]> {
    const apiURL =  `/auction/getAll`;
    return await this.apiService.GetAsync<ImportAuction[]>(apiURL, Constants.mrkApi);
  }

  async getAllAuctionsByCustomerIdAsync(): Promise<ImportAuction[]> {
    const apiURL =  `/auction/GetByCustomerId`;
    return await this.apiService.GetAsync<ImportAuction[]>(apiURL, Constants.mrkApi);
  }

  async getAuctionsByFilter(auctionsSearchFilter: AuctionSearchFilter): Promise<any> {
    const apiURL =  `/auction/getAuctionsByFilter`;
    return await this.apiService.PostAsync<any>(apiURL, auctionsSearchFilter, Constants.mrkApi);
  }

  async getAuctionsPostStatistics(): Promise<any> {
    const apiURL =  `/auction/GetAuctionsPostStatistics`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.mrkApi);
  }

  async createAuctionAsync(auction: ImportAuction): Promise<any> {
    const apiURL =  `/auction/create`;
    return await this.apiService.PostAsync<any>(apiURL, auction, Constants.mrkApi);
  }

  async createMultipleAuctionAsync(auctions: ImportAuction[]): Promise<any> {
    const apiURL =  `/auction/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, auctions, Constants.mrkApi);
  }

  async validateAuctionsAsync(auctions: ImportAuction[]): Promise<any> {
    const apiURL =  `/auction/validateAuctionsPartners`;
    return await this.apiService.PostAsync<any>(apiURL, auctions, Constants.mrkApi);
  }

  async createAuctionFolderAsync(auctionId: number): Promise<any> {
    const apiURL =  `/auction/createAuctionFolder`;
    return await this.apiService.PostAsync<any>(apiURL, auctionId, Constants.mrkApi);
  }

  async updateAuctionAsync(auction: ImportAuction): Promise<any> {
    const apiURL =  `/auction/update`;
    return await this.apiService.PutAsync<any>(apiURL, auction, Constants.mrkApi);
  }

  async deleteAuctionsAsync(ids: number[]): Promise<any> {
    const apiURL =  `/auction/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
