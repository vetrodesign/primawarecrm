import { TestBed } from '@angular/core/testing';

import { SalesTargetPostService } from './sales-target-post.service';

describe('SalesTargetPostService', () => {
  let service: SalesTargetPostService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalesTargetPostService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
