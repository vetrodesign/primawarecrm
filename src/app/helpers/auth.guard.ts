import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { Constants } from '../constants';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    coreRoutesNames = Constants;
    constructor(
        private router: Router,
        private authenticationService: AuthService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;

        if (currentUser && (state.url === '/' + Constants.language || state.url === '/' + Constants.country
        || state.url === '/' + Constants.county || state.url === '/' + Constants.cpvcode || state.url === '/' + Constants.tax
        || state.url === '/' + Constants.customcode
        || state.url === '/' + Constants.countryGroup
        || state.url === '/' + Constants.paymentInstrument || state.url === '/' + Constants.city
        || state.url === '/' + Constants.logs) &&
            !this.authenticationService.isUserOwner()) {
            this.router.navigate(['/']);
        }

        // if (currentUser && (route.queryParams['clientId'])) {
        //     const href = Constants.crm + (route.queryParams['returnUrl'] ? route.queryParams['returnUrl'] : '');
        //     window.location.href = href;
        // }

        if (currentUser) {
            // logged in so return true
            return true;
        }
        // not logged in so redirect to login page with the return url
        return true;
    }
}