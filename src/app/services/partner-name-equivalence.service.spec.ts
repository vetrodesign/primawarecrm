import { TestBed } from '@angular/core/testing';

import { PartnerNameEquivalenceService } from './partner-name-equivalence.service';

describe('PartnerNameEquivalenceService', () => {
  let service: PartnerNameEquivalenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerNameEquivalenceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
