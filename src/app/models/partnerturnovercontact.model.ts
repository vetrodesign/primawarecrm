import { BaseDomain } from "./base-domain.model";

export class PartnerTurnoverContact extends BaseDomain {
    partnerId: number;
    postId: number;
    partnerLocationContactId: number;
    dateAndTimeOfContact : Date;
}