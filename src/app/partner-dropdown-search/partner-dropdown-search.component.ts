import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Partner } from 'app/models/partner.model';
import { PartnerSearchFilter } from 'app/models/partnerSearchFilter.model';
import { AuthService } from 'app/services/auth.service';
import { PartnerService } from 'app/services/partner.service';
import { TranslateService } from 'app/services/translate';
import { DxValidatorComponent } from 'devextreme-angular';

@Component({
  selector: 'app-partner-dropdown-search',
  templateUrl: './partner-dropdown-search.component.html',
  styleUrls: ['./partner-dropdown-search.component.css']
})
export class PartnerDropdownSearchComponent implements OnInit, OnChanges {
  @Input() partnerLabelText: string;
  @Input() titleText: string;
  @Input() dropdownLengthNr: string;
  @Input() isClient: boolean = false;
  @Input() isSupplier: boolean = false;
  @Input() isDisabledOnUpdate: boolean = false;
  @Input() isRequired: boolean = false;

  @Input() partnerId: number;
  @Output() partnerChange = new EventEmitter<any>();
  @Output() selectedPartnerChange = new EventEmitter<string>();
  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;
  
  partnerCode: string;
  partnerName: string;
  colMd: string;
  partnerFiscalCode: string;
  themeColor: string = 'btn-success';
  partnerText: string;
  partnerDS: any
  loaded: boolean = false;

  constructor(private translationService: TranslateService, private partnerService: PartnerService, private authService: AuthService) {
    if (this.authService.getCustomerGridToolbarTheme()) {
      this.themeColor = 'btn-' + this.authService.getCustomerGridToolbarTheme();
     }
   }

  ngOnInit(): void {
  }

  onPartnerChange(e: any) {
    if (e && e.value) {
      this.partnerChange.emit(e.value);
      this.selectedPartnerChange.emit(this.partnerDS?.store.find(x => x.id === e.value)?.name);
    }
  }

  partnerDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  async ngOnChanges(changes: SimpleChanges) {
    if (changes.partnerLabelText) {
      this.partnerText = this.partnerLabelText ? this.partnerLabelText : this.translationService.instant('partner.partner')
    }
    if (changes.dropdownLengthNr) {
      this.colMd = this.dropdownLengthNr ? (`col-md-${this.dropdownLengthNr} col-lg-${this.dropdownLengthNr}`) : 'col-md-2 col-lg-2';
    }
    if (changes.partnerId && this.partnerId) {
      await this.partnerService.getByPartnerId(this.partnerId).then(partner => {
        if (partner) {
          let store = [];
          store.push(partner);
          this.partnerDS = {
            paginate: true,
            pageSize: 15,
            store: store
          };
        }
      })
    }
  }

  searchPartner() {
    if (this.partnerCode || this.partnerName || this.partnerFiscalCode) {
      let searchFilter = new PartnerSearchFilter();
      searchFilter.name = this.partnerName ? this.partnerName : null;
      searchFilter.code = this.partnerCode ? this.partnerCode : null;
      searchFilter.fiscalCode = this.partnerFiscalCode ? this.partnerFiscalCode : null;
      
      searchFilter.partnerType = [];
      if (this.isClient) {
        searchFilter.partnerType.push(1);
      }
      if (this.isSupplier) {
        searchFilter.partnerType.push(2);
      }
      searchFilter.isActive = true;
      this.loaded = true;
      this.partnerService.getPartnersByFilter(searchFilter).then(partners => {
        if (partners && partners.length) {
          this.partnerDS = {
            paginate: true,
            pageSize: 15,
            store: partners
          };
        }
        this.loaded = false;
      });
    }
  }

  validate() {
    if (this.isRequired) {
      if (this.validationGroup.instance.validate().isValid) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }
}



