import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { PartnerAuthorization } from 'app/models/partner-authorization.model';

@Injectable({
  providedIn: 'root'
})

export class PartnerAuthorizationService {
  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<PartnerAuthorization[]> {
    const apiURL = `/partnerauthorization/getAll`;
    return await this.apiService.GetAsync<PartnerAuthorization[]>(apiURL, Constants.mrkApi);
  }

  async getByCustomerIdAndPartnerIdAsync(partnerId): Promise<PartnerAuthorization[]> {
    const apiURL = `/partnerauthorization/GetByCustomerIdAndPartnerId`;
    return await this.apiService.PostAsync<any>(apiURL, partnerId, Constants.mrkApi);
  }

  async createAsync(ec: PartnerAuthorization): Promise<any> {
    const apiURL = `/partnerauthorization/create`;
    return await this.apiService.PostAsync<any>(apiURL, ec, Constants.mrkApi);
  }

  async updateAsync(ec: PartnerAuthorization): Promise<any> {
    const apiURL = `/partnerauthorization/update`;
    return await this.apiService.PutAsync<any>(apiURL, ec, Constants.mrkApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/partnerauthorization/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
