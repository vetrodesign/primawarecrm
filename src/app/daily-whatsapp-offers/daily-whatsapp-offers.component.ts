import { Component, Input, OnInit } from '@angular/core';
import { NotificationService } from 'app/services/notification.service';
import { ClientTurnoverService } from 'app/services/client-turnover.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { DailyWhatsappOffersEmail } from 'app/models/clientturnover.model';
import { AuthService } from 'app/services/auth.service';

declare global {
  interface ClipboardItem {
    new (data: { [mimeType: string]: Blob }): ClipboardItem;
  }
}

@Component({
  selector: 'app-daily-whatsapp-offers',
  templateUrl: './daily-whatsapp-offers.component.html',
  styleUrls: ['./daily-whatsapp-offers.component.css']
})
export class DailyWhatsappOffersComponent implements OnInit {
  @Input() clientId: number;
  @Input() selectedLocationContactPerson: any;
  @Input() email: any;

  loaded: boolean;
  dailyWhatsappOffers: any;
  isDailyWhatsappOfferPopupOpen: boolean;
  isImageEnlarged = false;
  enlargedImage: string | null = null;
  isEmailConfigurationPopupOpen: boolean;
  emailSubject: string;
  emailBody: string;

  constructor(
    private clientTurnoversService: ClientTurnoverService,
    private notificationService: NotificationService,
    private authService: AuthService) {
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.loaded = true;
    Promise.all([this.getDailyWhatsappOffers()]).then(() => {
      this.loaded = false;
    })
  }

  async getDailyWhatsappOffers() {
    await this.clientTurnoversService.getDailyWhatsappOffers(this.clientId).then((items) => {
      this.dailyWhatsappOffers = items ? items.map(m => ({...m, isSelected: false})) : [];
      this.isDailyWhatsappOfferPopupOpen = true;
    })
  }

  enlargeImage(imageBase64: string) {
    this.enlargedImage = imageBase64;
    this.isImageEnlarged = true;
  }

  async onSaveDailyWhatsappOffers() {
    let selectedOffers = this.dailyWhatsappOffers.filter(f => f.isSelected);

    if (!selectedOffers || selectedOffers.length === 0) {
      this.notificationService.alert('top', 'center', 'Selectati cel putin o oferta!', NotificationTypeEnum.Red, true);
      return;
    }

    if (selectedOffers && selectedOffers.length > 3) {
      this.notificationService.alert('top', 'center', 'Selectati cel mult 3 oferte!', NotificationTypeEnum.Red, true);
      return;
    }

    this.isEmailConfigurationPopupOpen = true;
  }

  copySelectedImage(base64: any): void {
    const blob = this.base64ToBlob(base64, 'image/png');
    
    const ClipboardItemConstructor = window as any;
    const clipboardItem = new ClipboardItemConstructor.ClipboardItem({
      'image/png': blob
    });
  
    (navigator.clipboard as any).write([clipboardItem])
      .then(() => this.notificationService.alert('top', 'center', 'Imaginea a fost copiata cu succes!', NotificationTypeEnum.Green, true))
      .catch((err) => this.notificationService.alert('top', 'center', 'Imaginea nu a fost copiata! A aparut o eroare!', NotificationTypeEnum.Red, true));
  }
  
  base64ToBlob(base64: string, mimeType: string): Blob {
    const byteString = atob(base64.split(',')[1]);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const uint8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      uint8Array[i] = byteString.charCodeAt(i);
    }

    return new Blob([arrayBuffer], { type: mimeType });
  }

  async sendEmail() {
    if (!this.selectedLocationContactPerson) {
      this.notificationService.alert('top', 'center', 'Selectati persoana de contact!', NotificationTypeEnum.Red, true);
      return;
    }

    if (this.selectedLocationContactPerson && !this.selectedLocationContactPerson.email) {
      this.notificationService.alert('top', 'center', 'Persoana de contact selectata nu are email!', NotificationTypeEnum.Red, true);
      return;
    }

    let selectedOffers = this.dailyWhatsappOffers.filter(f => f.isSelected);

    let request = new DailyWhatsappOffersEmail();
    request.selectedOffers = selectedOffers;
    request.emailSubject = this.emailSubject;
    request.emailBody = this.emailBody;
    request.personContactId = this.selectedLocationContactPerson.id;
    request.email = this.email;
    request.postId = +(this.authService.getUserPostId());

    this.loaded = true;
    await this.clientTurnoversService.sendDailyWhatsappOffers(request).then((response) => {
      this.notificationService.alert('top', 'center', 'Oferta a fost trimisa cu succes!', NotificationTypeEnum.Green, true);
      this.loaded = false;
    })
  }
}

