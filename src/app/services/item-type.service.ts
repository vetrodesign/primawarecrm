import { Injectable } from '@angular/core';
import { ItemType } from 'app/models/itemtype.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemTypeService {
  constructor(private apiService: ApiService) { }

  async getAllItemTypesAsync(): Promise<ItemType[]> {
    const apiURL = `/itemtype/getall`;
    return await this.apiService.GetAsync<ItemType[]>(apiURL,Constants.mrkApi);
  }

  async getItemTypesByCustomerId(): Promise<ItemType[]> {
    const apiURL = `/itemtype/getbycustomerid/`;
    return await this.apiService.GetAsync<ItemType[]>(apiURL,Constants.mrkApi);
  }
}
