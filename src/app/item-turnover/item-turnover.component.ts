import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { City } from 'app/models/city.model';
import { Country } from 'app/models/country.model';
import { County } from 'app/models/county.model';
import { Department } from 'app/models/department.model';
import { Post } from 'app/models/post.model';
import { TurnoverFilter, TurnoverHistoryFilter } from 'app/models/item-turnover-filter.model';
import { TurnoverItem, TurnoverMonths, TurnoverMonthsRow } from 'app/models/turnover-item.model';
import { TurnoverPartner } from 'app/models/turnover-partner.model';
import { TurnoverSearchFilter } from 'app/models/turnover-search-filter.model';
import { AuthService } from 'app/services/auth.service';
import { CityService } from 'app/services/city.service';
import { CountryService } from 'app/services/country.service';
import { CountyService } from 'app/services/county.service';
import { DepartmentsService } from 'app/services/departments.service';
import { ItemTurnoverService } from 'app/services/item-turnover.service';
import { ItemService } from 'app/services/item.service';
import { PostService } from 'app/services/post.service';
import * as moment from 'moment';
import { ItemSeoPlanningService } from 'app/services/item-seo-planning.service';
import { SpecialPriceRequestItemsService } from 'app/services/special-price-request-items.service';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-item-turnover',
  templateUrl: './item-turnover.component.html',
  styleUrls: ['./item-turnover.component.css']
})
export class ItemTurnoverComponent implements OnInit {
  @Input() filterItemCode: string;
  @Output() displayItemTurnoverPopup: EventEmitter<any> = new EventEmitter<any>();

  loaded: boolean;
  isOwner: boolean;
  countries: Country[] = [];
  countriesDS: any;
  counties: County[] = [];
  countiesDS: any;
  cities: City[] = [];
  citiesDS: any;
  turnoverSearchFilter: TurnoverSearchFilter;
  items: any;
  posts: Post[] = [];
  departments: Department[] = [];
  turnOverItems: TurnoverItem[] = [];
  turnoverMonths: TurnoverMonthsRow[] = [];
  turnoverSuppliers: TurnoverPartner[] = [];
  currentSupplierIndex: number = 0;
  currentSupplierDisplayIndex: number = 0;
  monthsToDisplay: Date[];
  isItemTurnoverHistoryOpened: boolean;
  itemTurnoverOperationName: string;
  turnoverHistoryItemId: number;
  turnoverHistoryOperationId: number;
  isDataLoading: boolean;
  itemName: string;
  itemCode: string;
  pricesIconTooltip: string;
  isPricesTooltipVisible = {};
  seoIconTooltip: any;
  isSeoTooltipVisible = {};
  isOrdersTooltipVisible = {};

  constructor(private itemsService: ItemService, private itemTurnoverService: ItemTurnoverService, private postService: PostService, private cityService: CityService, private countyService: CountyService,
    private countryService: CountryService, private departmentsService: DepartmentsService, private authenticationService: AuthService, private route: ActivatedRoute, private decimalPipe: DecimalPipe,
    private specialPriceRequestItemsService: SpecialPriceRequestItemsService, private itemSeoPlanningService: ItemSeoPlanningService) {
    this.authenticationService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authenticationService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  loadData() {
    this.getData();
  }

  getData() {
    this.loaded = true;
    this.displayItemTurnoverPopup.emit(false);
    Promise.all([this.getItems(), this.getPosts(),
    this.getCities(), this.getDepartments(), this.getCounties(), this.getCountries()]).then(async x => {

      if (this.filterItemCode) {
        let filter: any = {};
        filter.code = this.filterItemCode;
        await this.searchEvent(filter);
      }

      this.displayItemTurnoverPopup.emit(true);
      this.loaded = false
    });
  }

  async getItems(): Promise<any> {
    if (this.isOwner) {
      await this.itemsService.getAllItemsAsync().then(items => {
        this.items = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    } else {
      await this.itemsService.getItemssAsyncByID().then(items => {
        this.items = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    }
  }

  async getPosts() {
    if (this.isOwner) {
      await this.postService.getAllPostsAsync().then(items => {
        this.posts = items;
      });
    } else {
      await this.postService.getPostsByCustomerId().then(items => {
        this.posts = items;
      });
    }
  }

  async getDepartments() {
    if (this.isOwner) {
      await this.departmentsService.getAllDepartmentsAsync().then(items => {
        this.departments = items;
      });
    } else {
      await this.departmentsService.getDepartmentsAsyncByID().then(items => {
        this.departments = items;
      });
    }
  }

  async getCountries() {
    await this.countryService.getAllCountrysAsync().then(items => {
      this.countries = items;
      this.countriesDS = {
        paginate: true,
        pageSize: 15,
        store: this.cities
      };
    });
  }

  async getCities() {
    await this.cityService.getAllCitiesSmallAsync().then(items => {
      this.cities = items;
      this.citiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.cities
      };
    });
  }

  async getCounties() {
    await this.countyService.getAllCountysAsync().then(items => {
      this.counties = items;
      this.countiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.counties
      };
    });
  }

  async searchEvent(e: any) {
    this.turnoverSearchFilter = e
    this.loaded = true;
    //send SearchFilter as parameter to the BE
    await this.getTurnOver().then(x => this.loaded = false);
  }

  async getTurnOver() {
    let filter = new TurnoverFilter();

    if (this.turnoverSearchFilter) {
      if (this.turnoverSearchFilter.citiesIds && this.turnoverSearchFilter.citiesIds.length > 0) {
        filter.cityIds = this.turnoverSearchFilter.citiesIds;
      }

      if (this.turnoverSearchFilter.countiesIds && this.turnoverSearchFilter.countiesIds.length > 0) {
        filter.countyIds = this.turnoverSearchFilter.countiesIds;
      }

      if (this.turnoverSearchFilter.countriesIds && this.turnoverSearchFilter.countriesIds.length > 0) {
        filter.countryIds = this.turnoverSearchFilter.countriesIds;
      }

      if (this.turnoverSearchFilter.itemIds && this.turnoverSearchFilter.itemIds.length > 0) {
        filter.itemIds = this.turnoverSearchFilter.itemIds;
      }

      if (this.turnoverSearchFilter.partnerId) {
        filter.supplierIds = [this.turnoverSearchFilter.partnerId];
      }

      if (this.turnoverSearchFilter.postIds) {
        filter.supplierAgentIds = this.turnoverSearchFilter.postIds;
      }

      if (this.turnoverSearchFilter.code) {
        filter.itemCodes = [this.turnoverSearchFilter.code];
      }
    }

    await this.itemTurnoverService.getItemTurnoverSuppliers(filter).then(items => {
      if (items && items.length > 0) {
        this.turnoverSuppliers = items;
        this.setCurrentSupplierIndex(0);
      } else {
        this.turnoverSuppliers = [];
        this.setCurrentSupplierIndex(0);
      }
    });
  }

  getItem(item) {
    if (item && this.items.length > 0) {
      let c = this.items.find(x => x.id === item[0].itemId);
      return c ? c.code + ' - ' + c.nameRO : '';
    } else {
      return '';
    }
  }

  refreshEvent(e: any) {
    this.loaded = true;
    this.getData();
    this.getTurnOver().then(x => this.loaded = false);
  }

  async setCurrentSupplierIndex(index: number) {
    this.loaded = true;
    this.turnOverItems = [];
    this.currentSupplierIndex = index;
    this.currentSupplierDisplayIndex = this.currentSupplierIndex + 1;

    let filter = new TurnoverFilter();

    if (this.turnoverSearchFilter) {
      if (this.turnoverSearchFilter.itemIds && this.turnoverSearchFilter.itemIds.length > 0) {
        filter.itemIds = this.turnoverSearchFilter.itemIds;
      }

      filter.supplierIds = [this.turnoverSuppliers[index].supplierId];

      if (this.turnoverSearchFilter.code) {
        filter.itemCodes = [this.turnoverSearchFilter.code];
      }
    }

    await this.itemTurnoverService.getTurnoverItemsBySupplierIds(filter).then(items => {
      this.turnOverItems = items;
      this.loaded = false;
      this.calculateTurnoverMonths();
    });
  }

  calculateTurnoverMonths() {
    this.turnoverMonths = [];
    this.monthsToDisplay = [];
    let currentMonth = moment();
    let firstMonth = moment().add(-13, 'months');

    while (currentMonth >= firstMonth) {
      this.monthsToDisplay.push(currentMonth.toDate());
      currentMonth.add(-1, 'months');
    }

    this.monthsToDisplay = this.monthsToDisplay.sort((a, b) => (a.getTime() - b.getTime()));

    let dataFlatten = this.turnOverItems.map(x => x.itemDetails.map(y => ({ itemId: x.itemId, ...y }))).reduce((a, b) => { return a.concat(b); }, []);

    let distinctOperations = dataFlatten.reduce((prev, { itemId, turnoverOperationId, turnoverOperationName }) =>
      prev.some(x => x.itemId === itemId && x.turnoverOperationId === turnoverOperationId) ?
        prev : [...prev, { itemId, turnoverOperationId, turnoverOperationName }], []);

    this.turnoverMonths = distinctOperations.map(x => {
      var row = new TurnoverMonthsRow();
      var totalQuantity = 0;
      
      row.itemId = x.itemId;
      row.turnoverOperationName = x.turnoverOperationName;
      row.turnoverOperationId = x.turnoverOperationId;
      row.monthsData = this.monthsToDisplay.map(y => {
        var month = new TurnoverMonths();
        month.month = y;

        var itemMonthData = dataFlatten.filter(a => a.turnoverOperationId === x.turnoverOperationId && a.itemId === x.itemId
          && a.year === y.getFullYear() && a.month === (y.getMonth() + 1));

        if (itemMonthData.length > 0) {
          month.amount = itemMonthData.reduce((a, b) => { return a + b.amount }, 0);
          month.quantity = itemMonthData.reduce((a, b) => { return a + b.quantity }, 0);
          totalQuantity+= month.quantity;
        }

        return month;
      }).sort((a, b) => (a.month.getTime() - b.month.getTime()));

      row.Total = totalQuantity;

      return row;
    });

    this.turnoverMonths.forEach(async element => {
      let filter = new TurnoverHistoryFilter();
      filter.itemId = element.itemId;
      filter.itemTurnoverOperationId = element.turnoverOperationId;

      await this.itemTurnoverService.getCountItemTurnoverDetailXPartner(filter).then(result => {
        element.NoOfPartners = result;
      })
    })
  }

  previousSupplier() {
    this.setCurrentSupplierIndex(this.currentSupplierIndex - 1);
  }

  nextSupplier() {
    this.setCurrentSupplierIndex(this.currentSupplierIndex + 1);
  }

  setSupplier(e: any) {
    let element = (e.event.target as HTMLInputElement);
    const newValue = element.value;

    if (parseInt(newValue) > 0 && parseInt(newValue) <= this.turnoverSuppliers.length) {
      this.setCurrentSupplierIndex(parseInt(newValue) - 1);
    }
    else {
      this.currentSupplierDisplayIndex = this.currentSupplierIndex + 1;
      element.value = (this.currentSupplierIndex + 1).toString();
    }
  }

  getHttpLink(url: string) {
    if (url === undefined || url == null)
      return '';
    else if (url.startsWith('http'))
      return url;
    else
      return 'https://'.concat(url);
  }

  filterItemIdHasTurnoverMonths(itemId: number) {
    return this.turnoverMonths.filter(x => x.itemId === itemId).length > 0;
  }

  filterTurnoverMonthsByItemId(itemId: number) {
    return this.turnoverMonths.filter(x => x.itemId === itemId).sort((a, b) => (a.turnoverOperationId - b.turnoverOperationId));
  }

  onTurnoverOperationClick(rowData: any) {
    if (rowData) {
      this.itemCode = this.items.store.find(f => f.id === rowData.itemId)?.code;
      this.itemName = this.items.store.find(f => f.id === rowData.itemId)?.nameRO;
      this.itemTurnoverOperationName = rowData.turnoverOperationName;
      this.turnoverHistoryItemId = rowData.itemId;
      this.turnoverHistoryOperationId = rowData.turnoverOperationId;
      this.isItemTurnoverHistoryOpened = true;
    }
  }

  displayItemTurnoverHistoryPopupChange(event: any) {
    event ? this.isDataLoading = true : this.isDataLoading = false;
  }

  async getItemPrices(itemId: number) {
    this.pricesIconTooltip = 'Se incarca...'; 
    this.isPricesTooltipVisible[itemId] = true;

    await this.specialPriceRequestItemsService.getItemPrices(itemId).then(resp => {
      if (resp) {
        const cmp = this.decimalPipe.transform(resp.cmp, '1.2-2') || '0.00';
        const d0 = this.decimalPipe.transform(resp.d0, '1.2-2') || '0.00';
        const c0 = this.decimalPipe.transform(resp.c0, '1.2-2') || '0.00';
        const r0 = this.decimalPipe.transform(resp.r0, '1.2-2') || '0.00';

        this.pricesIconTooltip = `CMP: ${cmp} \n D0: ${d0} \n C0: ${c0} \n R0: ${r0}`;
      } else {
        this.pricesIconTooltip = '';
      }
    })
  }

  hidePricesTooltip(itemId: number): void {
    this.isPricesTooltipVisible[itemId] = false;
  }

  async getItemSeo(itemId: number) {
    this.seoIconTooltip = []; 
    this.isSeoTooltipVisible[itemId] = true;
    
    await this.itemSeoPlanningService.getByIdsAsync([itemId]).then(resp => {
      if (resp && resp.length > 0) {  
        let response = resp[0];

        this.seoIconTooltip = [{
          roValue: !!response.roValue,
          enValue: !!response.enValue,
          deValue: !!response.deValue,
          frValue: !!response.frValue,
          huValue: !!response.huValue,
          bgValue: !!response.bgValue,
          photos: !!response.photos,
          p360: !!response.p360,
          film: !!response.film
        }];
      } else {
        this.seoIconTooltip = [];
      }
    })
  }

  hideSeoTooltip(itemId: number): void {
    this.isSeoTooltipVisible[itemId] = false;
  }

  showOrdersTooltip(itemId: number): void {
    this.isOrdersTooltipVisible[itemId] = true;
  }

  hideOrdersTooltip(itemId: number): void {
    this.isOrdersTooltipVisible[itemId] = false;
  }
}
