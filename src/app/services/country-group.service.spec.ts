import { TestBed } from '@angular/core/testing';

import { CountryGroupService } from './country-group.service';

describe('CountryGroupService', () => {
  let service: CountryGroupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CountryGroupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
