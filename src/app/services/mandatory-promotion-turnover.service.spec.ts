import { TestBed } from '@angular/core/testing';

import { MandatoryPromotionTurnoverService } from './mandatory-promotion-turnover.service';

describe('MandatoryPromotionTurnoverService', () => {
  let service: MandatoryPromotionTurnoverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MandatoryPromotionTurnoverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
