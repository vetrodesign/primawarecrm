import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { ContactCampaignLocationGeoAreGeoEntities } from 'app/models/contact-campaign-location-geo-area-geo-entities.model';

@Injectable({
  providedIn: 'root'
})
export class ContactCampaignLocationGeoAreGeoEntitiesService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<ContactCampaignLocationGeoAreGeoEntities[]> {
    const apiURL = `/ContactCampaignLocationXGeoAreasXGeoEntities/GetAll`;
    return await this.apiService.GetAsync<ContactCampaignLocationGeoAreGeoEntities[]>(apiURL, Constants.mrkApi);
  }

  async getByContactCampaignLocationXGeoAreIdAsync(contactCampaignLocationGeoAreaId: number): Promise<ContactCampaignLocationGeoAreGeoEntities[]> {
    const apiURL = `/ContactCampaignLocationXGeoAreasXGeoEntities/GetByContactCampaignLocationXGeoAreId/` + contactCampaignLocationGeoAreaId;
    return await this.apiService.GetAsync<ContactCampaignLocationGeoAreGeoEntities[]>(apiURL, Constants.mrkApi);
  }

  async createAsync(m: ContactCampaignLocationGeoAreGeoEntities): Promise<any> {
    const apiURL = `/ContactCampaignLocationXGeoAreasXGeoEntities/create`;
    return await this.apiService.PostAsync<any>(apiURL, m, Constants.mrkApi);
  }

  async createMultipleAsync(m: ContactCampaignLocationGeoAreGeoEntities[]): Promise<any> {
    const apiURL = `/ContactCampaignLocationXGeoAreasXGeoEntities/CreateMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, m, Constants.mrkApi);
  }

  async deleteMultipleAsync(m: Number[]): Promise<any> {
    const apiURL = `/ContactCampaignLocationXGeoAreasXGeoEntities/deleteMultiple`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, m, Constants.mrkApi);
  }
}
