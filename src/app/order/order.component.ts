import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { AuthService } from 'app/services/auth.service';
import { NotificationService } from 'app/services/notification.service';
import { DxDataGridComponent } from 'devextreme-angular';
import * as _ from 'lodash';
import { Order } from 'app/models/order.model';
import { Partner } from 'app/models/partner.model';
import { OrderDetailsComponent } from 'app/order-details/order-details.component';
import { PartnerService } from 'app/services/partner.service';
import { OrderService } from 'app/services/order.service';
import { PageActionService } from 'app/services/page-action.service';
import { Constants } from 'app/constants';
import { IPageActions } from 'app/models/ipageactions';
import { OrderStatusTypeEnum } from 'app/enums/orderStatusTypeEnum';
import { DepartmentTypeEnum } from 'app/enums/departmentTypeEnum';
import { DepartmentsService } from 'app/services/departments.service';
import { Site } from 'app/models/site.model';
import { SiteService } from 'app/services/site.service';
import { BaseCurrency } from 'app/models/base-currency.model';
import { CurrencyRateService } from 'app/services/currency-rate.service';
import { MeasurementUnit } from 'app/models/measurementunit.model';
import { MeasurementUnitService } from 'app/services/measurement-unit.service';
import { PaymentInstrumentService } from 'app/services/payment-instrument.service';
import { PaymentInstrument } from 'app/models/paymentinstrument.model';
import { ItemService } from 'app/services/item.service';
import { OrderTypeEnum } from 'app/enums/orderTypeEnum';
import { ManagementService } from 'app/services/management.service';
import { Management } from 'app/models/management.model';
import { ItemTypeService } from 'app/services/item-type.service';
import { ItemType } from 'app/models/itemtype.model';
import { UsersService } from 'app/services/user.service';
import { User } from 'app/models/user.model';
import { DeliveryConditionsService } from 'app/services/delivery-conditions.service';
import { DeliveryConditions } from 'app/models/deliveryConditions.model';
import { ERPSyncSource } from 'app/enums/ERPSyncSourceEnum';
import { OrderSearchFilterVM } from 'app/models/orderSearchFilter.model';
import { CityService } from 'app/services/city.service';
import { City } from 'app/models/city.model';
import { CountyService } from 'app/services/county.service';
import { County } from 'app/models/county.model';
import { ActivatedRoute } from '@angular/router';
import { ClientModuleMenuItemsService } from 'app/services/client-module-menu-items.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit, AfterViewInit {
  orders: Order[] = [];
  partners: Partner[] = [];

  actions: IPageActions;
  loaded: boolean;
  selectedRows: any;
  templates: any[]
  detailsVisibility: boolean;
  selectedOrder: Order;
  isOwner: boolean;
  isUserAdmin: boolean;
  departments: any[] = [];
  sites: Site[] = [];
  users: User[] = [];
  items: any;
  salesPosts: any[] = [];
  posts: any[] = [];

  cities: City[] = [];
  citiesDS: any;

  counties: County[] = [];
  countiesDS: any;

  deliveryConditions: DeliveryConditions[] = []
  currencies: BaseCurrency[] = [];
  measurementUnits: MeasurementUnit[] = [];
  paymentInstruments: PaymentInstrument[] = [];
  managements: Management[] = [];
  itemTypes: ItemType[] = [];
  orderSearchFilter: any;
  userPostId: number;

  infoButton: string;

  orderTypes: { id: number; name: string }[] = [];
  orderStatusTypes: { id: number; name: string }[] = [];
  erpSyncSource: { id: number; name: string }[] = [];
  @ViewChild('orderDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('orderToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('orderDetails') orderDetails: OrderDetailsComponent;

  constructor(private authService: AuthService, private partnerService: PartnerService, private orderService: OrderService, private cityService: CityService, private countyService: CountyService,
    private notificationService: NotificationService, private pageActionService: PageActionService, private departmentsService: DepartmentsService,
    private siteService: SiteService, private currencyService: CurrencyRateService, private measurementUnitsService: MeasurementUnitService,
    private paymentInstrumentService: PaymentInstrumentService, private itemsService: ItemService, private managementService: ManagementService,
    private itemTypeService: ItemTypeService, private userService: UsersService, private deliveryConditionsService: DeliveryConditionsService,
    private route: ActivatedRoute,
    private clientModuleMenuItemsService: ClientModuleMenuItemsService) {
    this.setActions();
    this.authService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });

    for (let n in OrderTypeEnum) {
      if (typeof OrderTypeEnum[n] === 'number') {
        this.orderTypes.push({
          id: <any>OrderTypeEnum[n],
          name: n
        });
      }
    }

    for (let n in OrderStatusTypeEnum) {
      if (typeof OrderStatusTypeEnum[n] === 'number') {
        this.orderStatusTypes.push({
          id: <any>OrderStatusTypeEnum[n],
          name: n
        });
      }
    }

    for (let n in ERPSyncSource) {
      if (typeof ERPSyncSource[n] === 'number') {
        this.erpSyncSource.push({
          id: <any>ERPSyncSource[n],
          name: n
        });
      }
    }

    this.partners = [];
    this.orders = [];
    this.getOrders();
    this.getData();

    
  }

 

  ngOnInit(): void {
    if (this.route.snapshot.queryParamMap.get('data') !== undefined && this.route.snapshot.queryParamMap.get('data') !== null) {
      const encodedData = this.route.snapshot.queryParamMap.get('data');
      if (encodedData) {
        try {
          if (JSON.parse(decodeURIComponent(encodedData)) != null) {
            this.getData().then(r => {
              this.orderDetails.loadData();
              this.detailsVisibility = true;
              this.orderDetails.loadNewOrderFromTurnover(JSON.parse(decodeURIComponent(encodedData)));
            });
          }
        } catch (e) {
          console.error('Failed to parse data:', e);
        }
      }
    }
  }

  async getData(): Promise<any> {
    this.loaded = true;
    return Promise.all([this.getSites(), this.getBaseCurrency(), this.getDepartments(), this.getItems(), this.getMeasurementUnits(),
    this.getPaymentInstrument(), this.getManagements(), this.getItemTypes(), this.getUsers(), this.getDeliveryConditions(), this.getCities(), this.getCounties()]).then(x => {
      this.setSalesPosts();
      this.loaded = false
    });
  }

  async searchEvent(e: any) {
    this.orderSearchFilter = e ? e : new OrderSearchFilterVM();
    this.loaded = true;
    await this.getOrders().then(x => this.loaded = false);
  }

  async getBaseCurrency(): Promise<any> {
    await this.currencyService.getBaseCurrency().then(x => {
      this.currencies = x;
    });
  }

  async getCities() {
    await this.cityService.getAllCitiesSmallAsync().then(items => {
      this.cities = items;
      this.citiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.cities
      };
    });
  }

  async getCounties() {
    await this.countyService.getAllCountysAsync().then(items => {
      this.counties = items;
      this.countiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.counties
      };
    });
  }

  canExport() {
    return this.actions.CanExport;
  }

  async getUsers(): Promise<any> {
    await this.userService.getUserAsyncByID().then(x => {
      this.users = x;
    });
  }

  setPost() {
    let user = this.users.find(x => x.userName === this.authService.getUserUserName());
    if (user && user.postId && this.posts.map(x => x.id).filter(x => x == user.postId).length > 0) {
      this.userPostId = user.postId;
    } else {
      this.userPostId = 0;
    }
    this.isUserAdmin = this.isUserAdminOrHasSuperiorPost();
  }


  public isUserAdminOrHasSuperiorPost(): boolean {
    if (this.authService.isUserAdmin()) {
      return true;
    } else {
      if (this.posts && this.posts.length > 0 && this.users && this.users.length > 0) {
        let post = this.posts?.find(x => x.id === this.users.find(x => x.id === Number(this.authService.getUserId()))?.postId);
        if (post && (post.isDepartmentSuperior || post.isGeneralManager || post.isLeadershipPost)) {
          return true;
        } else {
          return false;
        }
      }
      return false;
    }
  }

  async getSites(): Promise<any> {
    if (this.isOwner) {
      await this.siteService.getAllCustomerLocationsAsync().then(items => {
        this.sites = items;
      });
    } else {
      await this.siteService.getCustomerLocationsAsyncByID().then(items => {
        this.sites = items;
      });
    }
  }

  async getItemTypes(): Promise<any> {
    if (this.isOwner) {
      await this.itemTypeService.getAllItemTypesAsync().then(items => {
        this.itemTypes = items;
      });
    } else {
      await this.itemTypeService.getItemTypesByCustomerId().then(items => {
        this.itemTypes = items;
      });
    }
  }

  async getDeliveryConditions(): Promise<any> {
    await this.deliveryConditionsService.getAllDeliveryConditionsAsync().then(items => {
      if (items && items.length > 0) {
        this.deliveryConditions = items;
      } else {
        this.deliveryConditions = [];
      }
    });
  }

  async getManagements(): Promise<any> {
    await this.managementService.getManagementsBySiteID().then(m => {
      this.managements = m;
    });
  }

  async getDepartments(): Promise<any> {
    if (this.isOwner) {
      await this.departmentsService.getAllDepartmentsAsync().then(items => {
        this.departments = items;
      });
    } else {
      await this.departmentsService.getDepartmentsAsyncByID().then(items => {
        this.departments = items;
      });
    }
  }

  async getItems(): Promise<any> {
    if (this.isOwner) {
      await this.itemsService.getAllItemsAsync().then(items => {
        this.items = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    } else {
      await this.itemsService.getItemssAsyncByID().then(items => {
        this.items = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    }
  }

  async getMeasurementUnits(): Promise<any> {
    if (this.isOwner) {
      this.measurementUnitsService.getAllMUAsync().then(ums => {
        this.measurementUnits = ums;
      });
    } else {
      this.measurementUnitsService.getAllMUByCustomerIdAsync().then(ums => {
        this.measurementUnits = ums;
      });
    }
  }

  async getPaymentInstrument(): Promise<any> {
    if (this.isOwner) {
      await this.paymentInstrumentService.getAllPaymentInstrumentsAsync().then(items => {
        this.paymentInstruments = items;
      });
    }
    else {
      await this.paymentInstrumentService.getAllPaymentInstrumentsByCustomerIdAsync().then(items => {
        this.paymentInstruments = items;
      });
    }
  }

  setSalesPosts() {
    const salesDeps = this.departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Sales);
    if (salesDeps && salesDeps.length > 0 && salesDeps[0].offices) {
      const off = salesDeps.map(x => x.offices).reduce((x, y) => x.concat(y));
      if (off && off.some(x => x.posts.length > 0)) {
        this.salesPosts = off.map(x => x.posts)
          .reduce((x, y) => x.concat(y));
      }
    }

    if (this.departments && this.departments.length > 0) {
      const off = this.departments.map(x => x.offices).reduce((x, y) => x.concat(y));
      if (off && off.some(x => x.posts.length > 0)) {
        this.posts = off.map(x => x.posts)
          .reduce((x, y) => x.concat(y));
        this.setPost();
      }
    }
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.order).then(result => {
      this.actions = result;
    });
  }
  ngAfterViewInit() {
    this.setGridInstances();
  }

  refreshEvent(e: any) {
    this.loaded = true;
    this.getData();
    this.getOrders().then(x => this.loaded = false);
  }

  async getOrders() {
    if (this.orderSearchFilter == null || this.orderSearchFilter == undefined) {
      this.orderSearchFilter = new OrderSearchFilterVM();
    }
    if ((this.orderSearchFilter?.isLegal && this.orderSearchFilter?.isIndividual) || (!this.orderSearchFilter?.isLegal && !this.orderSearchFilter?.isIndividual)) {
      this.orderSearchFilter.partnerType = null;
    } else if (this.orderSearchFilter.isLegal) {
      this.orderSearchFilter.partnerType = 1;
    } else if (this.orderSearchFilter.isIndividual) {
      this.orderSearchFilter.partnerType = 2;
    }

    await this.orderService.getByFilter(this.orderSearchFilter).then(t => {
      if (t) {
        this.orders = t;
      }
    })
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && e.data.status == 2) {
      e.rowElement.style.backgroundColor = '#ffc187';
    }
    if (e.rowType === 'data' && e.data && e.data.status == 3) {
      e.rowElement.style.backgroundColor = '#fffea6';
    }
    if (e.rowType === 'data' && e.data && e.data.status == 4) {
      e.rowElement.style.backgroundColor = '#b0ffce';
    }

  }

  refreshDataGrid() {
    this.getOrders();
  }

  setGridInstances() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  public async openDetails(row: any) {
    this.selectedOrder = row;
    setTimeout(() => {
      this.orderDetails.loadData().then(function () {
        this.detailsVisibility = true;
      }.bind(this));
    }, 0);
  }

  changeVisible() {
    this.detailsVisibility = false;
    this.loaded = true;
    this.getOrders().then(x => this.loaded = false);
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public deleteRecords(data: any) {
    this.loaded = true;
    this.orderService.deleteAsync(this.selectedRows.map(x => x.id)).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Datele au fost sterse cu succes!', NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Datele nu au fost sterse!', NotificationTypeEnum.Red, true)
      }
      this.loaded = false;
      this.getOrders();
    });
  }


  getDisplayExprPostsName(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }
}

