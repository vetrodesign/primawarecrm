import { TestBed } from '@angular/core/testing';

import { CaenCodeSpecializationService } from './caen-code-specialization.service';

describe('CaenCodeSpecializationService', () => {
  let service: CaenCodeSpecializationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CaenCodeSpecializationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
