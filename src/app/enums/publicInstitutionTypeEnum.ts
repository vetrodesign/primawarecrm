export enum PublicInstitutionEnum {
    TownHall = 1,
    Prefecture = 2,
    CountyCouncil= 3,
    DeconcentratedDirections = 4,
    CentralInstitutions = 5,
    HealthInstitutions = 6,
    InternationalMissions = 7,
    NationalMissions = 8,
    NGO = 9,
    EducationalUnits = 10
  }