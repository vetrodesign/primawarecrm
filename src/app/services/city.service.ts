import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { City } from 'app/models/city.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class CityService {
  constructor(private apiService: ApiService) { }

  async getAllCitiesAsync(): Promise<City[]> {
    const apiURL = `/city/getall`;
    return await this.apiService.GetAsync<City[]>(apiURL, Constants.commonApi);
  }

  async getAllCitiesSmallAsync(): Promise<City[]> {
    const apiURL = `/city/getallsmall`;
    return await this.apiService.GetAsync<City[]>(apiURL, Constants.commonApi);
  }

  async getByCountyAndCountryID(countyId: number, countryId: number): Promise<City[]> {
    const apiURL = `/city/GetByCountyAndCountry/`;
    const city = new City();
    city.countryId = countryId;
    city.countyId = countyId;
    return await this.apiService.PostAsync<City[]>(apiURL, city, Constants.commonApi);
  }

  async createCityAsync(city: City): Promise<any> {
    const apiURL = `/city/create`;
    return await this.apiService.PostAsync<any>(apiURL, city, Constants.commonApi);
  }

  async updateCityAsync(city: City): Promise<any> {
    const apiURL = `/city/update`;
    return await this.apiService.PutAsync<any>(apiURL, city, Constants.commonApi);
  }

  async deleteCitiesAsync(ids: number[]): Promise<any> {
    const apiURL = `/city/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
