import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class OfficeDrinksTurnoverService {
  constructor(private apiService: ApiService) { }

  async getData(partnerId: number, postId: number, section: number): Promise<any> {
    let filter = {'partnerId': partnerId, 'postId': postId, 'section': section}
    const apiURL = `/Partner/GetOfficeDrinksTurnover`;
    return await this.apiService.PostAsync<any>(apiURL, filter, Constants.mrkApi);
  }
}
