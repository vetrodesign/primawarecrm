export class CpvCode {
    public id: number;
    public code: string;

    public nameBG: string;
    public nameCS: string;
    public nameDA: string;
    public nameDE: string;
    public nameEL: string;
    public nameEN: string;
    public nameES: string;
    public nameET: string;
    public nameFI: string;
    public nameFR: string;
    public nameGA: string;
    public nameHR: string;
    public nameHU: string;
    public nameIT: string;
    public nameLT: string;
    public nameLV: string;
    public nameMT: string;
    public nameNL: string;
    public namePL: string;
    public namePT: string;
    public nameRO: string;
    public nameSK: string;
    public nameSL: string;
    public nameSV: string;

    public getPropertiesArray(): string[] {
        return ['code', 'nameBG', 'nameEN', 'nameRO'];
    }
}
