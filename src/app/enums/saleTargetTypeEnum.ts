export enum SaleTargetTypeEnum {
    IntroducedClientsTarget = 1,
    PotentialClientsContactedTarget = 2,
    ActiveClientsContactedTarget = 3
}

export enum TargetTypeEnum {
    Currency = 1,
    Numeric = 2,
    Text = 3,
    Date = 4,
    Percentage = 5,
    Time = 6,
    Custom = 7
}