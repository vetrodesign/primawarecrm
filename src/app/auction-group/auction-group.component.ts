import { Component, OnInit, AfterViewInit, ViewChild, Input } from "@angular/core";
import { NotificationTypeEnum } from "app/enums/notificationTypeEnum";
import { GridToolbarComponent } from "app/helpers/grid-toolbar/grid-toolbar.component";
import { IPageActions } from "app/models/ipageactions";
import { AuthService } from "app/services/auth.service";
import { NotificationService } from "app/services/notification.service";
import { TranslateService } from "app/services/translate";
import { DxDataGridComponent } from "devextreme-angular";
import { AuctionGroup } from "app/models/auction-group.model";
import { AuctionGroupService } from "app/services/auction-group.service";
import { AuctionGroupXCpvCode } from "app/models/auction-group-cpv-code.model";
import { AuctionGroupCpvCodeService } from "app/services/auction-group-cpv-code.service";

@Component({
  selector: 'app-auction-group',
  templateUrl: './auction-group.component.html',
  styleUrls: ['./auction-group.component.css']
})
export class AuctionGroupComponent implements OnInit, AfterViewInit {
  @Input() cpvCodes: any;
  @Input() posts: any;

  @ViewChild('auctionGroupDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('auctionGroupGridToolbar') gridToolbar: GridToolbarComponent;

  displayData: boolean;
  loaded: boolean;
  actions: IPageActions;
  auctionGroups: AuctionGroup[] = [];
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  groupedText: string;
  isTabActive: string;
  isOwner: boolean;
  constructor(
    private auctionGroupService: AuctionGroupService,
    private translationService: TranslateService,
    private notificationService: NotificationService,
    private auctionGroupCpvCodeService: AuctionGroupCpvCodeService,
    private authService: AuthService) {
    this.isOwner = this.authService.isUserOwner();
    this.groupedText = this.translationService.instant('groupedText');
    this.isTabActive = this.authService.getCustomerPageTitleTheme();
    this.setActions();
  }

  ngOnInit(): void {
  }

  async getData() {
    if (this.isOwner) {
      await this.auctionGroupService.getAllAuctionGroupsAsync().then(items => {
        this.auctionGroups = (items && items.length >  0) ? items : [];
      });
    }
    else {
      await this.auctionGroupService.getAllAuctionGroupsByCustomerIdAsync().then(items => {
        this.auctionGroups = (items && items.length >  0) ? items : [];
      });
    }
    this.dataGrid.dataSource = this.auctionGroups;
  }

  

  loadData() {
    this.loaded = true;
    this.getData().then(y => { this.loadAuctionGroupCpvCode(); this.loaded = false; });
    this.setGridInstance();
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }


  ngAfterViewInit() {
    this.setGridInstance();
  }

  setGridInstance() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  setActions() {
    this.actions = {
      CanView: true,
      CanAdd: true,
      CanUpdate: true,
      CanDelete: true,
      CanPrint: true,
      CanExport: true,
      CanImport: true,
      CanDuplicate: true
    };
  }

  loadAuctionGroupCpvCode() {
    this.auctionGroupCpvCodeService.getAllAuctionGroupCpvCodes().then(items => {
      if (items) {
        this.auctionGroups.forEach(u => {
          const i = items.filter(x => x.auctionGroupId === u.id)
            .map(x => x.cpvCodeId);
          if (i) {
            u.existingCpvCodeIds = i ? i : [];
            u.cpvCodeIds = i ? i : [];
          }
        });
      }
    });
  }

  public add() {
    this.dataGrid.instance.addRow();
  }

  public refreshDataGrid() {
    this.loadData();
    this.dataGrid.instance.refresh();
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public deleteRecords(data: any) {
    this.auctionGroupService.deleteAuctionGroupsAsync(this.selectedRows.map(x => x.id)).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Grup Licitatie - Datele au fost sterse cu succes!',
          NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Grup Licitatie - Datele nu au fost sterse!',
          NotificationTypeEnum.Red, true)
      }
      this.refreshDataGrid();
    });
  }

  public async openDetails(row: any) {
    this.dataGrid.instance.editRow(row.rowIndex);
    this.rowIndex = row.rowIndex;
  }

  public async onRowUpdated(event: any): Promise<void> {
    let item = new AuctionGroup();
    item = this.auctionGroups.find(g => g.id === event.key.id);

    if (item) {
      await this.auctionGroupService.updateAuctionGroupAsync(item).then(r => {
        if (r) {

          // Create AuctionGroupXCpvCode
          const existingAuctionGroupXCpvCodeIds = item.existingCpvCodeIds;
          let deleteAuctionGroupXCpvCode: AuctionGroupXCpvCode[] = [];
          existingAuctionGroupXCpvCodeIds.filter(x => !item.cpvCodeIds.includes(x)).forEach(id => {
            const c = new AuctionGroupXCpvCode();
            c.auctionGroupId = item.id;
            c.cpvCodeId = id;
            deleteAuctionGroupXCpvCode.push(c);
          });
          if (deleteAuctionGroupXCpvCode.length > 0) {
            this.auctionGroupCpvCodeService.deleteAuctionGroupCpvCodeAsync(deleteAuctionGroupXCpvCode);
          }
          let createAuctionGroupXCpvCodeIds: AuctionGroupXCpvCode[] = [];
          item.cpvCodeIds.filter(x => !existingAuctionGroupXCpvCodeIds.includes(x)).forEach(id => {
            const c = new AuctionGroupXCpvCode();
            c.auctionGroupId = Number(item.id);
            c.cpvCodeId = id;
            createAuctionGroupXCpvCodeIds.push(c);
          });
          if (createAuctionGroupXCpvCodeIds.length > 0) {
            this.auctionGroupCpvCodeService.createMultipleAuctionGroupCpvCodeAsync(createAuctionGroupXCpvCodeIds);
          }

          this.notificationService.alert('top', 'center', 'Grup Licitatii - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Grup Licitatii - Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        };

        this.refreshDataGrid();
      });
    }
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async onRowInserting(event: any): Promise<void> {
    let item = new AuctionGroup();
    item = event.data;
    item.customerId = Number(this.authService.getUserCustomerId());

    if (item) {
      await this.auctionGroupService.createAuctionGroupAsync(item).then(r => {
        if (r) {

          // Create AuctionGroupXCpvCode
          let createAuctionGroupCpvCodeIds: AuctionGroupXCpvCode[] = [];
          item.cpvCodeIds?.forEach(id => {
            const c = new AuctionGroupXCpvCode();
            c.auctionGroupId = Number(r.id);
            c.cpvCodeId = id;
            createAuctionGroupCpvCodeIds.push(c);
          });
          if (createAuctionGroupCpvCodeIds && createAuctionGroupCpvCodeIds.length > 0) {
            this.auctionGroupCpvCodeService.createMultipleAuctionGroupCpvCodeAsync(createAuctionGroupCpvCodeIds);
          }

          this.notificationService.alert('top', 'center', 'Grup Licitatii - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Grup Licitatii - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshDataGrid();
      });
    }
  }

  calculateFilterExpression(filterValue, selectedFilterOperation, target) {
    if (target === 'search' && typeof (filterValue) === 'string') {
      return [(this as any).dataField, 'contains', filterValue];
    }
    return function (data) {
      return (data.cpvCodeIds || []).indexOf(filterValue) !== -1
    }
  }

  cellTemplate(container, options) {
    const noBreakSpace = '\u00A0',
      text = (options.value || []).map(element => {
        return options.column.lookup.calculateCellValue(element);
      }).join(', ');
    container.textContent = text || noBreakSpace;
    container.title = text;
  }

  getCpvCodeExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  getPostDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }
}


