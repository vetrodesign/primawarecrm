export class OrderSearchFilterVM {
    startDate: Date;
    endDate: Date;
    status: number;
    siteId: number;
    number: string;
    partnerIds: any;
    isActive: boolean = true;
    salesAgentId: number;
    itemIds: any;
    confirmed: any;

    isLegal: boolean;
    isIndividual: boolean;
    partnerType: number;
}