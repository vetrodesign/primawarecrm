import { TestBed } from '@angular/core/testing';

import { MessagingAppService } from './messaging-app.service';

describe('MessagingAppService', () => {
  let service: MessagingAppService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MessagingAppService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
