import { Injectable } from '@angular/core';
import { GeographicalArea } from 'app/models/geographicalarea.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class GeographicalAreaService {

  constructor(private apiService: ApiService) { }

  async getAllGAreaAsync(): Promise<GeographicalArea[]> {
    const apiURL = `/GeographicalArea/getall`;
    return await this.apiService.GetAsync<GeographicalArea[]>(apiURL,  Constants.commonApi);
  }

  async getAllGAreaByCustomerIdAsync(): Promise<GeographicalArea[]> {
    const apiURL = `/GeographicalArea/getallbycustomer`;
    return await this.apiService.GetAsync<GeographicalArea[]>(apiURL, Constants.commonApi);
  }

  async createGAreaAsync(mu: GeographicalArea): Promise<any> {
    const apiURL = `/GeographicalArea/create`;
    return await this.apiService.PostAsync<any>(apiURL, mu, Constants.commonApi);
  }

  async updateGAreaAsync(mu: GeographicalArea): Promise<any> {
    const apiURL = `/GeographicalArea/update`;
    return await this.apiService.PutAsync<any>(apiURL, mu, Constants.commonApi);
  }

  async deleteGAreaAsync(ids: number[]): Promise<any> {
    const apiURL = `/GeographicalArea/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
