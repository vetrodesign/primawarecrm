export enum DeliveryOfferPreferences  {
    Email = 1,
    Whatsapp = 2,
    SMS = 3,
    Other = 4
};