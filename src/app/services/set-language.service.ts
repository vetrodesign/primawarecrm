import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { environment } from 'environments/environment';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class SetLanguageService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  setLanguage(lang: string): Promise<any> {
    const basePath = environment.useApiGateway ? environment.apiGatewayUrl + environment.apiPaths[Constants.commonApi] : environment[Constants.commonApi];
    return this.http.post<any>(`${basePath}/common/SetLanguage`,
    JSON.stringify(lang) , { headers: this.getHeaders() }).toPromise().then(t => {
      });
  }

  private getHeaders(): HttpHeaders {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Cache-Control', 'no-cache');
    headers = headers.append('Pragma', 'no-cache');
    headers = headers.append('Expires', 'Sat, 01 Jan 2000 00:00:00 GMT');
    headers = headers.append('If-Modified-Since', '0');
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', 'Bearer ' + this.authService.getToken());
    let copyHeader = headers;
    return copyHeader;
  }
}
