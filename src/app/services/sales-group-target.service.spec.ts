import { TestBed } from '@angular/core/testing';

import { SalesGroupTargetService } from './sales-group-target.service';

describe('SalesGroupTargetService', () => {
  let service: SalesGroupTargetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalesGroupTargetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
