import { Injectable } from '@angular/core';
import { CaenCodeSpecializationXPartnerActivityAllocation } from 'app/models/caencodespecializationxpartneractivityallocation.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class CaenCodeSpecializationPartnerActivityAllocationService {

  constructor(private apiService: ApiService) { }

  async getAllCaenCodeSpecializationPartnerActivityAllocationAsync(): Promise<any[]> {
    const apiURL = `/CaenCodeSpecializationXPartnerActivityAllocation/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.commonApi);
  }

  async createCaenCodeSpecializationPartnerActivityAllocationAsync(p: CaenCodeSpecializationXPartnerActivityAllocation): Promise<any> {
    const apiURL = `/CaenCodeSpecializationXPartnerActivityAllocation/create`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.commonApi);
  }

  async createMultipleCaenCodeSpecializationPartnerActivityAllocationAsync(p: CaenCodeSpecializationXPartnerActivityAllocation[]): Promise<any> {
    const apiURL = `/CaenCodeSpecializationXPartnerActivityAllocation/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.commonApi);
  }

  async updateCaenCodeSpecializationPartnerActivityAllocationAsync(p: CaenCodeSpecializationXPartnerActivityAllocation): Promise<any> {
    const apiURL = `/CaenCodeSpecializationXPartnerActivityAllocation/update`;
    return await this.apiService.PutAsync<any>(apiURL, p, Constants.commonApi);
  }

  async deleteMultipleCaenCodeSpecializationPartnerActivityAllocationsAsync(p: CaenCodeSpecializationXPartnerActivityAllocation[]): Promise<any> {
    const apiURL = `/CaenCodeSpecializationXPartnerActivityAllocation/deleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.commonApi);
  }
}
