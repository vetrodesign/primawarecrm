export enum ExportImportItemTypeEnum {
    Reference = 1,
    Divided = 2,
    Grouped = 3
  }