export enum ContactCampaignTypeEnum {
    HappyBirthDay = 1,
    Location = 2,

    // not contact campaigns but needed on the grid
    CallReminder = 3
}