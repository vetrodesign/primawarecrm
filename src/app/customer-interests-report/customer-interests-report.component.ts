import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DepartmentTypeEnum } from 'app/enums/departmentTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { CaenCodeSpecializationXPartnerLocation } from 'app/models/caencodespecializationxpartner.model';
import { CustomerInterestsReport } from 'app/models/customer-interests-report.model';
import { DailyReportFullDetails } from 'app/models/daily-report.model';
import { IPageActions } from 'app/models/ipageactions';
import { Item } from 'app/models/item.model';
import { Occupation } from 'app/models/occupation.model';
import { PartnerObservation } from 'app/models/partner-observation.model';
import { Partner } from 'app/models/partner.model';
import { PartnerSearchFilter } from 'app/models/partnerSearchFilter.model';
import { PartnerLocationContact } from 'app/models/partnerlocationcontact.model';
import { Post } from 'app/models/post.model';
import { AuthService } from 'app/services/auth.service';
import { CaenCodeSpecializationPartnerLocationService } from 'app/services/caen-code-specialization-partner-location.service';
import { CaenCodeSpecializationXItemService } from 'app/services/caencodespecializationxitem.service';
import { DepartmentsService } from 'app/services/departments.service';
import { ItemService } from 'app/services/item.service';
import { OccupationService } from 'app/services/occupation.service';
import { PartnerLocationContactService } from 'app/services/partner-location-contact.service';
import { PartnerLocationService } from 'app/services/partner-location.service';
import { PartnerObservationService } from 'app/services/partner-observation.service';
import { PartnerService } from 'app/services/partner.service';
import { SalesTargetPostService } from 'app/services/sales-target-post.service';
import { TranslateService } from 'app/services/translate';
import { UsersService } from 'app/services/user.service';
import { DxDataGridComponent, DxValidatorComponent } from 'devextreme-angular';
import { Clipboard } from '@angular/cdk/clipboard';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import 'bootstrap';
import { Constants } from 'app/constants';
import { ActivatedRoute } from '@angular/router';
import { ClientModuleMenuItemsService } from 'app/services/client-module-menu-items.service';

@Component({
  selector: 'app-customer-interests-report',
  templateUrl: './customer-interests-report.component.html',
  styleUrls: ['./customer-interests-report.component.css']
})
export class CustomerInterestsReportComponent implements OnInit {
  @ViewChild('customerInterestsDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('customerInterestsGridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;

  displayData: boolean;
  popupVisible: boolean;
  loaded: boolean;

  actions: IPageActions;
  salesTargetAllocations: any[] = [];
  allPosts: any[] = []
  posts: any[] = []
  departments: any[] = [];
  occupations: Occupation[] = [];
  specializations: CaenCodeSpecializationXPartnerLocation[] = [];
  mappedSpecializations: CaenCodeSpecializationXPartnerLocation[] = [];
  users: any[] = []
  items: any;
  isOwner: boolean;
  selectedRows: any[];
  selectedRowIndex = -1;
  groupedText: string;

  searchedPostId: any;
  searchedItemId: number;
  searchedSGF: boolean = false;
  searchedCDA: boolean = false;
  searchedSZO: boolean = false;
  searchedALT: boolean = false;

  selectedPostId: number;
  selectedValid: boolean;

  isUserAdminOrHasSuperiorPostValue: boolean;

  customerInterestsReports: CustomerInterestsReport[] = [];

  currentForViewRowSpecializations: { identifier: string, value: string[] }[] = [];
  currentForViewRowData: any;
  showSpecializationsPopup: boolean;

  @ViewChild('popupContent', { static: false }) popupContent: ElementRef;

  textSGF: string = '';
  textCDA: string = '';
  textSZO: string = '';
  textALT: string = '';

  infoButton: string;

  constructor(
    private authService: AuthService,
    private userService: UsersService,
    private translationService: TranslateService,
    private notificationService: NotificationService,
    private departmentsService: DepartmentsService,
    private partnerService: PartnerService,
    private partnerLocationService: PartnerLocationService,
    private caenCodeSpecializationPartnerLocationService: CaenCodeSpecializationPartnerLocationService,
    private itemService: ItemService,
    private caenCodeSpecializationXItemService: CaenCodeSpecializationXItemService,
    private partnerObservationService: PartnerObservationService,
    private partnerLocationContactService: PartnerLocationContactService,
    private occupationService: OccupationService,
    private clipboard: Clipboard,
    private route: ActivatedRoute,
    private clientModuleMenuItemsService: ClientModuleMenuItemsService
  ) {
    this.isOwner = this.authService.isUserOwner();
    this.groupedText = this.translationService.instant('groupedText');

    this.setActions();

    this.textSGF = Constants.textSGF;
    this.textCDA = Constants.textCDA;
    this.textSZO = Constants.textSZO;
    this.textALT = Constants.textALT;

    
  }

 

  ngOnInit(): void {
    this.getData();
    this.isUserAdminOrHasSuperiorPostValue = this.isUserAdminOrHasSuperiorPost();
  }

  getData() {
    this.loaded = true;
    Promise.all([this.getItems(), this.getUsers(), this.getDepartments(), this.getOccupations()]).then(async x => {
      this.loaded = false;
    });
  }

  async getUsers() {
    if (this.isOwner) {
      await this.userService.getUsersAsync().then(items => {
        this.users = items;
      });
    } else {
      await this.userService.getUserAsyncByID().then(items => {
        this.users = items;
      });
    }
  }

  async getOccupations() {
    await this.occupationService.getAllOccupationsAsync().then(occupations => {
      this.occupations = occupations;
    })
  }

  /*User has admin role or superior post*/
  public isUserAdminOrHasSuperiorPost(): boolean {
    if (this.authService.isUserAdmin()) {
      return true;
    } else {
      if (this.allPosts && this.allPosts.length > 0 && this.users && this.users.length > 0) {
        let post = this.allPosts?.find(x => x.id === this.users.find(x => x.id === Number(this.authService.getUserId()))?.postId);
        if (post && (post.isDepartmentSuperior || post.isGeneralManager || post.isLeadershipPost)) {
          return true;
        } else {
          return false;
        }
      }
      return false;
    }
  }

  openDetails(e) { }

  setActions() {
    this.actions = {
      CanView: true,
      CanAdd: false,
      CanUpdate: true,
      CanDelete: false,
      CanPrint: true,
      CanExport: true,
      CanImport: false,
      CanDuplicate: false
    };
  }

  async getDepartments() {
    this.departments = [];
    if (this.isOwner) {
      await this.departmentsService.getAllDepartmentsAsync().then(items => {
        this.departments = items;
        this.setSalesPosts();
      });
    } else {
      await this.departmentsService.getDepartmentsAsyncByID().then(items => {
        this.departments = items;
        this.setSalesPosts();
      });
    }
  }

  setSalesPosts() {
    if (this.departments && this.departments.length > 0) {
      const off = this.departments.map(x => x.offices).reduce((x, y) => x.concat(y));
      if (off && off.some(x => x.posts.length > 0)) {
        this.allPosts = off.map(x => x.posts)
          .reduce((x, y) => x.concat(y));

        if (!this.isUserAdminOrHasSuperiorPostValue) {
          this.searchedPostId = this.allPosts.find(x => x.id === this.users.find(x => x.id === Number(this.authService.getUserId()))?.postId)?.id;
        }
      }

      const salesDeps = this.departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Sales);
      if (salesDeps && salesDeps.length > 0 && salesDeps[0].offices) {
        const off = salesDeps.map(x => x.offices).reduce((x, y) => x.concat(y), []);
        if (off && off.some(x => x.posts.length > 0)) {
          this.posts = off.map(x => x.posts)
            .reduce((x, y) => x.concat(y));
        }
      }
    }
  }

  async getItems(): Promise<any> {
    if (this.isOwner) {
      await this.itemService.getAllItemsAsync().then(items => {
        this.items = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    } else {
      await this.itemService.getItemssAsyncByID().then(items => {
        this.items = {
          paginate: true,
          pageSize: 15,
          store: items
        };
      });
    }
  }

  async searchData() {
    if (this.validationGroup.instance.validate().isValid) {
      this.customerInterestsReports = [];
      this.loaded = true;

      if ((this.searchedSGF || this.searchedCDA || this.searchedSZO || this.searchedALT) === false) {
        this.notificationService.alert('top', 'center', 'Trebuie bifata cel putin o casuta (SGF/CDA/SZO/ALT) pentru a incepe cautarea!', NotificationTypeEnum.Red, true);
        this.loaded = false;
        return;
      }

      if (this.searchedItemId) {
        this.caenCodeSpecializationXItemService.getAllByItemId(this.searchedItemId).then(spxs => {
          if (spxs && spxs.length > 0) {
            const usedSpxs = spxs.filter(spx => {
              const matchesSGF: boolean = (spx.sgf && this.searchedSGF) === true;
              const matchesCDA: boolean = (spx.cda && this.searchedCDA) === true;
              const matchesSZO: boolean = (spx.szo && this.searchedSZO) === true;
              const matchesALT: boolean = (spx.alt && this.searchedALT) === true;

              return matchesSGF || matchesCDA || matchesSZO || matchesALT;
            });

            if (((this.searchedSGF || this.searchedCDA || this.searchedSZO || this.searchedALT) === true) && (usedSpxs && usedSpxs.length === 0)) {
              this.notificationService.alert('top', 'center', 'Nu exista specializare pe produs pentru bifa/bifele selectata.', NotificationTypeEnum.Red, true);
              this.loaded = false;
              return;
            }

            if (this.searchedPostId) {
              const partnerSearchFilter = new PartnerSearchFilter();

              const postIds: number[] = [];
              postIds.push(this.searchedPostId);
              const caenCodeSpecializationIds: number[] = usedSpxs.map(fs => fs.caenCodeSpecializationId);

              partnerSearchFilter.postIds = postIds;
              partnerSearchFilter.caenSpecializationsIds = caenCodeSpecializationIds;
              partnerSearchFilter.isActive = true;

              this.partnerService.getPartnersByFilter(partnerSearchFilter).then(partners => {
                if (partners && partners.length === 0) {
                  this.notificationService.alert('top', 'center', 'Nu s-a gasit nici un partener.', NotificationTypeEnum.Red, true);
                  this.loaded = false;
                  return;
                }

                if (partners && partners.length > 0) {
                  const partnerIds: number[] = partners.map(p => p.id);

                  this.partnerLocationService.getPartnerLocatonsByPartnerIds(partnerIds).then(pls => {
                    this.caenCodeSpecializationPartnerLocationService.getAllCaenCodeSpecializationPartnerLocationByPartnerLocationsAsync(pls.map(p => p.id)).then(sxpls => {
                      const mappedSpecializations = sxpls.filter(sxpl => usedSpxs.some(spx => spx.caenCodeSpecializationId === sxpl.caenCodeSpecializationId));
                      this.mappedSpecializations = mappedSpecializations;
                      this.specializations = mappedSpecializations;
                      const mappedPartnerLocations = pls.filter(pl => mappedSpecializations.some(is => is.partnerLocationId === pl.id));
                      const mappedPartners = partners.filter(p => mappedPartnerLocations.some(mpl => mpl.partnerId === p.id));

                      if (mappedPartners && mappedPartners.length === 0) {
                        this.notificationService.alert('top', 'center', 'Nu s-a gasit nici un partener.', NotificationTypeEnum.Red, true);
                        this.loaded = false;
                        return;
                      }

                      this.partnerObservationService.getByPartnerIDs(mappedPartners.map(mp => mp.id)).then(observations => {
                        this.partnerLocationContactService.getPartnerLocationContactByPartnerLocationIDs(mappedPartnerLocations.filter(mpl => mpl.isImplicit).map(mpl => mpl.id)).then(plcs => {
                          mappedPartners.forEach(i => {
                            const partnerContact: PartnerLocationContact = plcs.find(plc => plc.partnerId === i.id);
                            const partnerObservations: PartnerObservation[] = observations.filter(o => o.partnerId === i.id);
                            const partnerSpecializations = mappedSpecializations.filter(ms => ms.partnerLocationId === (mappedPartnerLocations.find(mpl => mpl.partnerId === i.id).id));
                            const partnerName: string = i.name;
                            const partnerContactName: string = partnerContact ? partnerContact.firstName + " " + partnerContact.lastName : '';
                            const partnerContactOccupation: number = partnerContact ? partnerContact.position : 0;
                            const partnerContactEmail: string = partnerContact ? partnerContact.email : '';
                            const partnerContactPhone: string = partnerContact ? partnerContact.phoneNumber : '';
                            const partnerContactLastObservationDescription: string = (partnerObservations && partnerObservations.length > 0) ? partnerObservations.reduce((max, current) => {
                              return current.id > max.id ? current : max;
                            }).description : "";
                            const specializationIds: number[] = partnerSpecializations.map(ps => ps.caenCodeSpecializationId);
                            const filteredSpecializations = usedSpxs.filter(fs => partnerSpecializations.some(ms => ms.caenCodeSpecializationId === fs.caenCodeSpecializationId))
                            const sgf: boolean = filteredSpecializations.some(fs => (fs.sgf === true)) && this.searchedSGF;
                            const cda: boolean = filteredSpecializations.some(fs => (fs.cda === true)) && this.searchedCDA;
                            const szo: boolean = filteredSpecializations.some(fs => (fs.szo === true)) && this.searchedSZO;
                            const alt: boolean = filteredSpecializations.some(fs => (fs.alt === true)) && this.searchedALT;

                            const customerInterestsReport = new CustomerInterestsReport();
                            customerInterestsReport.partnerName = partnerName;
                            customerInterestsReport.partnerContactName = partnerContactName;
                            customerInterestsReport.partnerContactOccupation = partnerContactOccupation;
                            customerInterestsReport.partnerContactEmail = partnerContactEmail;
                            customerInterestsReport.partnerContactPhone = partnerContactPhone;
                            customerInterestsReport.partnerLastObservationDescription = partnerContactLastObservationDescription;
                            customerInterestsReport.specializationIds = specializationIds;
                            customerInterestsReport.sgf = sgf;
                            customerInterestsReport.cda = cda;
                            customerInterestsReport.szo = szo;
                            customerInterestsReport.alt = alt;

                            this.customerInterestsReports.push(customerInterestsReport);
                          });

                          this.loaded = false;
                        });
                      });
                    })
                  });
                }
              });
            }
          } else {
            this.notificationService.alert('top', 'center', 'Nu exista specializari pentru produsul selectat.', NotificationTypeEnum.Red, true);
            this.loaded = false;
            return;
          }
        });
      }
    }
  }

  setGridInstance() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  loadData() {
    this.getData();
    this.setGridInstance();
  }

  public refreshDataGrid() {
    this.loadData();
    this.dataGrid.instance.refresh();
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }

  deleteRecords(e) {
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  calculateFilterExpression(filterValue, selectedFilterOperation, target) {
    if (target === 'search' && typeof (filterValue) === 'string') {
      return [(this as any).dataField, 'contains', filterValue];
    }
    return function (data) {
      return (data.specializationIds || []).indexOf(filterValue) !== -1
    }
  }

  cellTemplate(container, options) {
    const noBreakSpace = '\u00A0',
      text = (options.value || []).map(element => {
        return options.column.lookup.calculateCellValue(element);
      }).join(', ');
    container.textContent = text || noBreakSpace;
    container.title = text;
  }

  onCellPrepared(e: any) {
    if (e) {
      if (e.rowType == "header") {
        const cellElement = e.cellElement;

        if (e.cellElement.ariaLabel === "Column SGF") {
          cellElement.setAttribute("title", this.textSGF);
          $(cellElement).tooltip({
            placement: 'bottom'
          });
        }

        if (e.cellElement.ariaLabel === "Column CDA") {
          cellElement.setAttribute("title", this.textCDA);
          $(cellElement).tooltip({
            placement: 'bottom'
          });
        }

        if (e.cellElement.ariaLabel === "Column SZO") {
          cellElement.setAttribute("title", this.textSZO);
          $(cellElement).tooltip({
            placement: 'bottom'
          });
        }

        if (e.cellElement.ariaLabel === "Column ALT") {
          cellElement.setAttribute("title", this.textALT);
          $(cellElement).tooltip({
            placement: 'bottom'
          });
        }

      }
    }
  }

  public async viewExternalApp(row: any) {
    this.showSpecializationsPopup = true;
    this.currentForViewRowSpecializations = this.getFilteredSpecializationsForPopup(row);
    this.currentForViewRowData = row.data;
  }

  private getFilteredSpecializationsForPopup(row: any): { identifier: string, value: string[] }[] {
    const rowSpecializationIds: number[] = row.data.specializationIds;

    const filteredSpecializationsForPopup: { identifier: string, value: string[] }[] = [];

    const sgf: string = "SGF";
    const cda: string = "CDA";
    const szo: string = "SZO";
    const alt: string = "ALT";

    const rowSGF: boolean = row.data.sgf;
    const rowCDA: boolean = row.data.cda;
    const rowSZO: boolean = row.data.szo;
    const rowALT: boolean = row.data.alt;

    if ((this.searchedSGF) && rowSGF) {
      filteredSpecializationsForPopup.push(
        { identifier: sgf, value: [] }
      );
    }
    if ((this.searchedCDA) && rowCDA) {
      filteredSpecializationsForPopup.push(
        { identifier: cda, value: [] }
      );
    }
    if ((this.searchedSZO) && rowSZO) {
      filteredSpecializationsForPopup.push(
        { identifier: szo, value: [] }
      );
    }
    if ((this.searchedALT) && rowALT) {
      filteredSpecializationsForPopup.push(
        { identifier: alt, value: [] }
      );
    }

    const filteredSpecializations = this.mappedSpecializations.filter(ms => rowSpecializationIds.includes(ms.caenCodeSpecializationId));

    rowSpecializationIds.forEach(i => {
      const specializationById = filteredSpecializations.find(fs => fs.caenCodeSpecializationId === i);

      if ((this.searchedSGF === true) && rowSGF) {
        filteredSpecializationsForPopup.find(ffp => ffp.identifier === sgf).value.push(specializationById.specializationCode + " - " + specializationById.specializationDescription);
      }

      if ((this.searchedCDA === true) && rowCDA) {
        filteredSpecializationsForPopup.find(ffp => ffp.identifier === cda).value.push(specializationById.specializationCode + " - " + specializationById.specializationDescription);
      }

      if ((this.searchedSZO === true) && rowSZO) {
        filteredSpecializationsForPopup.find(ffp => ffp.identifier === szo).value.push(specializationById.specializationCode + " - " + specializationById.specializationDescription);
      }

      if ((this.searchedALT === true) && rowALT) {
        filteredSpecializationsForPopup.find(ffp => ffp.identifier === alt).value.push(specializationById.specializationCode + " - " + specializationById.specializationDescription);
      }
    });

    return filteredSpecializationsForPopup;
  }

  copySpecializationsFromPopup(popupContent: ElementRef) {
    const popupContentText = popupContent.nativeElement.innerText;
    this.clipboard.copy(popupContentText);
  }

  okSpecializationsPopup() {
    this.showSpecializationsPopup = false;
  }

  getPostDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  getItemDisplayExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  getDisplayExprOccupation(item) {
    if (!item) {
      return '';
    }
    return item.name;
  }

  getDisplayExprSpecialization(item) {
    if (!item) {
      return '';
    }
    return item.specializationCode + " - " + item.specializationDescription;
  }
}
