import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { PartnerXItemGroupCode } from "app/models/partnerxitemgroupcode.model";
import { Constants } from "app/constants";

@Injectable({
    providedIn: 'root'
})
export class PartnerXItemGroupCodeService {
    constructor(private apiService: ApiService) { }
    async getByPartnerId(partnerId: number): Promise<PartnerXItemGroupCode[]> { 
        const apiURL = `/partnerxitemgroupcode/getbypartner?partnerId=${partnerId}`;
        return await this.apiService.GetAsync<PartnerXItemGroupCode[]>(apiURL, Constants.mrkApi);
    }
    async create(groupCodeIds: number[], partnerId) { 
        let filter = {'groupCodeIds': groupCodeIds, 'partnerId': partnerId}
        const apiURL = `/partnerxitemgroupcode/create`;
        return await this.apiService.PostAsync<any>(apiURL, filter, Constants.mrkApi);
    }
    async update(groupCodeIds: number[], partnerId) { 
        let filter = {'groupCodeIds': groupCodeIds, 'partnerId': partnerId}
        const apiURL = `/partnerxitemgroupcode/update`;
        return await this.apiService.PostAsync<any>(apiURL, filter, Constants.mrkApi);
    }
}