export class SupplierPriceListXPartnerLocation {
    public id: number;
    public supplierPriceListId: number;
    public partnerLocationId: number;
}