import { Component, OnInit, AfterViewInit, ViewChild } from "@angular/core";
import { NotificationTypeEnum } from "app/enums/notificationTypeEnum";
import { GridToolbarComponent } from "app/helpers/grid-toolbar/grid-toolbar.component";
import { IPageActions } from "app/models/ipageactions";
import { AuthService } from "app/services/auth.service";
import { NotificationService } from "app/services/notification.service";
import { TranslateService } from "app/services/translate";
import { DxDataGridComponent } from "devextreme-angular";
import { AuctionType } from "app/models/auction-type.model";
import { AuctionTypeService } from "app/services/auction-type.service";

@Component({
  selector: 'app-auction-type',
  templateUrl: './auction-type.component.html',
  styleUrls: ['./auction-type.component.css']
})
export class AuctionTypeComponent implements OnInit, AfterViewInit {
  @ViewChild('auctionTypeDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('auctionTypeGridToolbar') gridToolbar: GridToolbarComponent;

  displayData: boolean;
  loaded: boolean;
  actions: IPageActions;
  auctionTypes: AuctionType[] = [];
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  groupedText: string;
  isTabActive: string;
  isOwner: boolean;
  constructor(
    private auctionTypesService: AuctionTypeService,
    private translationService: TranslateService,
    private notificationService: NotificationService,
    private authService: AuthService) {
    this.isOwner = this.authService.isUserOwner();
    this.groupedText = this.translationService.instant('groupedText');
    this.isTabActive = this.authService.getCustomerPageTitleTheme();
    this.setActions();
  }

  ngOnInit(): void {
  }

  async getData() {
    this.loaded = true;
    if (this.isOwner) {
      await this.auctionTypesService.getAllAuctionTypesAsync().then(items => {
        this.auctionTypes = (items && items.length >  0) ? items : [];
        this.loaded = false;
      });
    }
    else {
      await this.auctionTypesService.getAllAuctionTypesByCustomerIdAsync().then(items => {
        this.auctionTypes = (items && items.length >  0) ? items : [];
        this.loaded = false;
      });
    }
    this.dataGrid.dataSource = this.auctionTypes;
  }

  loadData() {
    this.getData();
    this.setGridInstance();
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }


  ngAfterViewInit() {
    this.setGridInstance();
  }

  setGridInstance() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  setActions() {
    this.actions = {
      CanView: true,
      CanAdd: true,
      CanUpdate: true,
      CanDelete: true,
      CanPrint: true,
      CanExport: true,
      CanImport: true,
      CanDuplicate: true
    };
  }
  public add() {
    this.dataGrid.instance.addRow();
  }

  public refreshDataGrid() {
    this.getData();
    this.dataGrid.instance.refresh();
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public deleteRecords(data: any) {
    this.auctionTypesService.deleteAuctionTypesAsync(this.selectedRows.map(x => x.id)).then(response => {
      if (response) {
        this.notificationService.alert('top', 'center', 'Tip Licitatie - Datele au fost sterse cu succes!',
          NotificationTypeEnum.Green, true)
      } else {
        this.notificationService.alert('top', 'center', 'Tip Licitatie - Datele nu au fost sterse!',
          NotificationTypeEnum.Red, true)
      }
      this.refreshDataGrid();
    });
  }

  public async openDetails(row: any) {
    this.dataGrid.instance.editRow(row.rowIndex);
    this.rowIndex = row.rowIndex;
  }

  public async onRowUpdated(event: any): Promise<void> {
    let item = new AuctionType();
    item = this.auctionTypes.find(g => g.id === event.key.id);
    if (item) {
      await this.auctionTypesService.updateAuctionTypeAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Tip Licitatii - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Tip Licitatii - Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        };

        this.refreshDataGrid();
      });
    }
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async onRowInserting(event: any): Promise<void> {
    let item = new AuctionType();
    item = event.data;
    item.customerId = Number(this.authService.getUserCustomerId());
    if (item) {
      await this.auctionTypesService.createAuctionTypeAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Tip Licitatii - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Tip Licitatii - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshDataGrid();
      });
    }
  }
}


