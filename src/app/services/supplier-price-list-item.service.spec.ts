import { TestBed } from '@angular/core/testing';

import { SupplierPriceListItemService } from './supplier-price-list-item.service';

describe('SupplierPriceListItemService', () => {
  let service: SupplierPriceListItemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SupplierPriceListItemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
