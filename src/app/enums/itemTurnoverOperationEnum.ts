export enum ItemTurnoverOperation {
    Buy = 1,
    InStock = 2,
    NoMovement = 3,
    OutStock = 4,
    SaleConsumer = 5,
    SaleDistributor = 6
}