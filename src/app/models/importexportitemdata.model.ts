export class ImportExportItemData {
    public id: any;
    public referenceNumber :string;
    public gtinCode :string;
    public name :string;
    public description :string;
    public termsOfDelivery :string;
    public paymentTerms :string;
    public price :number;
    public measurmentUnit :string;
    public inStock :boolean;
    public isExclusive: boolean;
    public site :string;
    public cpv :string;

    public itemId: number;
    public itemType: number;
    public itemMeasurmentUnit: number;
    public itemPriceDividingFactor: number;
    public itemKeywords: string;
    public itemParentId: number;
    public cpvId: number;
    public isActive: boolean;
}