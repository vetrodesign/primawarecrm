import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { TurnoverPreorder } from 'app/models/turnover-preorder.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class TurnoverPreorderService {

  constructor(private apiService: ApiService) { }

  async getTurnoverPreorderAsync(): Promise<TurnoverPreorder[]> {
    const apiURL = `/TurnoverPreorder/getall`;
    return await this.apiService.GetAsync<TurnoverPreorder[]>(apiURL, Constants.mrkApi);
  }

  async GetByPartnerIdAsync(partnerId): Promise<TurnoverPreorder[]> {
    const apiURL = `/TurnoverPreorder/GetByPartnerId`;
    return await this.apiService.PostAsync<TurnoverPreorder[]>(apiURL, partnerId, Constants.mrkApi);
  }

  async getItemsByManagementIdAsync(managementIds: number[]): Promise<any> {
    const apiURL = `/TurnoverPreorder/GetItemsByManagementId`;
    return await this.apiService.PostAsync<any>(apiURL, managementIds, Constants.mrkApi);
  }

  async createTurnoverPreorderAsync(t: TurnoverPreorder): Promise<any> {
    const apiURL = `/TurnoverPreorder/create`;
    return await this.apiService.PostAsync<any>(apiURL, t, Constants.mrkApi);
  }

  async createMultipleTurnoverPreorderAsync(t: TurnoverPreorder[]): Promise<any> {
    const apiURL = `/TurnoverPreorder/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, t, Constants.mrkApi);
  }

  async updateTurnoverPreorderAsync(t: TurnoverPreorder): Promise<any> {
    const apiURL = `/TurnoverPreorder/update`;
    return await this.apiService.PutAsync<any>(apiURL, t, Constants.mrkApi);
  }

  async deleteTurnoverPreorderAsync(ids: number[]): Promise<any> {
    const apiURL = `/TurnoverPreorder/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
