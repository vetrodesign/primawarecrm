import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesTargetAllocationComponent } from './sales-target-allocation.component';

describe('SalesTargetAllocationComponent', () => {
  let component: SalesTargetAllocationComponent;
  let fixture: ComponentFixture<SalesTargetAllocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesTargetAllocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesTargetAllocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
