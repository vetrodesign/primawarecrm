import { Injectable } from '@angular/core';
import { DiscountGrid } from 'app/models/discount-grid.model';
import { ApiService } from './api.service';
import { DiscountGridsByProductConventionIdAndCurrencyIdRequestModel } from 'app/models/discountGridsByProductConventionIdAndCurrencyIdRequestModel.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class DiscountGridService {

  constructor(private apiService: ApiService) { }

  async GetAllAsync(): Promise<any[]> {
    const apiURL = `/discountGrid/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }
  async GetByCustomerAsync(): Promise<any[]> {
    const apiURL = `/discountGrid/GetByCustomerId`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async GetDiscountGridsByProductConventionIdAndCurrencyId(requestModel: DiscountGridsByProductConventionIdAndCurrencyIdRequestModel): Promise<DiscountGrid[]> {
    const apiURL = `/discountGrid/GetDiscountGridsByProductConventionIdAndCurrencyId`;
    return await this.apiService.PostAsync<DiscountGrid[]>(apiURL, requestModel, Constants.mrkApi);
  }

  async CreateAsync(dg: DiscountGrid): Promise<any> {
    const apiURL = `/discountGrid/create`;
    return await this.apiService.PostAsync<any>(apiURL, dg, Constants.mrkApi);
  }

  async UpdateAsync(dg: DiscountGrid): Promise<any> {
    const apiURL = `/discountGrid/update`;
    return await this.apiService.PutAsync<any>(apiURL, dg, Constants.mrkApi);
  }

  async DeleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/discountGrid/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
