export enum AuctionFilterDayEnum {
    Today = 1,
    InOneDay = 2,
    InTwoDays = 3
  }

  export enum AuctionParticipationEnum {
    Yes = 1,
    No = 2,
    NoButToBeAnalyzed = 3
  }
