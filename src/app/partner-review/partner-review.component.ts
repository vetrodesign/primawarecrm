import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { PartnerSuggestionStateEnum } from 'app/enums/partnerSuggestionState.enum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { Occupation } from 'app/models/occupation.model';
import { Partner } from 'app/models/partner.model';
import { PartnerLocationContact } from 'app/models/partnerlocationcontact.model';
import { PartnerReview } from 'app/models/partnerreview.model';
import { Post } from 'app/models/post.model';
import { User } from 'app/models/user.model';
import { AuthService } from 'app/services/auth.service';
import { NotificationService } from 'app/services/notification.service';
import { OccupationService } from 'app/services/occupation.service';
import { PartnerLocationContactService } from 'app/services/partner-location-contact.service';
import { PartnerReviewService } from 'app/services/partner-review.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
  selector: 'app-partner-review',
  templateUrl: './partner-review.component.html',
  styleUrls: ['./partner-review.component.css']
})
export class PartnerReviewComponent implements OnInit, OnChanges {
  @Input() partner: Partner;
  @Input() posts: any;
  @Input() users: User[];
  @Input() actions: any;

  @ViewChild('partnerReviewsDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('reviewGridToolbar') gridToolbar: GridToolbarComponent;

  partnerReviews: PartnerReview[] = [];
  userPost: Post;
  selectedPartnerReview: PartnerReview;
  selectedRows: any[];
  selectedRowIndex = -1;
  groupedText: string;

  partnerReviewInfo: string;
  partnerContacts: PartnerLocationContact[] = [];
  occupations: Occupation[] = [];

  constructor(private partnerReviewsService: PartnerReviewService, private notificationService: NotificationService,
    private authService: AuthService, private translationService: TranslateService, private partnerLocationContactService: PartnerLocationContactService,
    private occupationService: OccupationService) {
    this.setActions();
      this.groupedText = this.translationService.instant('groupedText');

      this.getDisplayExprCreatedBy = this.getDisplayExprCreatedBy.bind(this);
      this.getDisplayExprPartnerContacts = this.getDisplayExprPartnerContacts.bind(this);
    this.partnerReviewInfo = this.translationService.instant("partnerReviewInfo"); 
   }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.partner) {
      this.getReviews();
      this.getPartnerLocationContacts();
    }
    if (changes.posts) {
      if (this.posts && this.posts.length && this.users && this.users.length) {
        let user = this.users.find(x => x.userName === this.authService.getUserUserName());
        if (user) {
          this.userPost = this.posts.find(x => Number(x.id) === user.postId);
        }
      }
     }
  }

  async getData() {
   if (this.partner && this.partner.id) {
    await this.partnerReviewsService.getPartnerReviewsByPartnerID(this.partner.id).then(async reviews => {
      this.partnerReviews = (reviews && reviews.length > 0) ? reviews.filter(f => f.description) : reviews;

      this.partnerReviews.forEach(review => {
        // Split the description using '|||'
        const parts = review.description.split(' ||| ');
      
        // If more than 5 parts, store the whole description in response1
        if (parts.length > 5) {
          review.response1 = review.description;
        } else {
          // Populate response1 to response5 dynamically
          for (let i = 0; i < 5; i++) {
            review[`response${i + 1}`] = parts[i] || ''; // Fill with empty string if undefined
          }
        }
      });
      

      await this.partnerLocationContactService.getPartnerLocationContactIncludingInactiveByPartnerID(this.partner.id).then(async contacts => {
        if (contacts) {
          this.partnerContacts = contacts;

          await this.occupationService.getAllOccupationsAsync().then(async items => {
            if (items && items.length > 0) {
              this.occupations = items;
            }
          });
        } else {
          this.partnerContacts = [];
        }
      });

    });
   }
  }

  async getReviews() {
    if (this.partner && this.partner.id) {
      this.partnerReviewsService.getPartnerReviewsByPartnerID(this.partner.id).then(reviews => {
        this.partnerReviews = (reviews && reviews.length > 0) ? reviews.filter(f => f.description) : reviews;
        this.partnerReviews.forEach(review => {
          // Split the description using '|||'
          const parts = review.description.split(' ||| ');
        
          // If more than 5 parts, store the whole description in response1
          if (parts.length > 5) {
            review.response1 = review.description;
          } else {
            // Populate response1 to response5 dynamically
            for (let i = 0; i < 5; i++) {
              review[`response${i + 1}`] = parts[i] || ''; // Fill with empty string if undefined
            }
          }
        });
      });
     }
  }

  public refreshReviews() {
    this.getData().then(() => {
      this.dataGrid.instance.refresh();
    })
  }

  canExport() {
    return this.actions.CanExport;
  }

  public add() {
    this.dataGrid.instance.addRow();
  }

  public async onRowReviewInserting(event: any): Promise<void> {
    let item = new PartnerReview();
    item = event.data;
    item.partnerId = Number(this.partner.id);
    item.postId = this.userPost.id;
    item.partnerSuggestionState = PartnerSuggestionStateEnum.Save;
  
    if (item) {
      await this.partnerReviewsService.createPartnerReviewAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Contact Locatie Partener - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshReviews();
      });
    }
  }

  setActions() {
    this.actions = {
      CanView: true,
      CanAdd: true,
      CanUpdate: false,
      CanDelete: false,
      CanPrint: true,
      CanExport: true,
      CanImport: false,
      CanDuplicate: false
    };
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  selectionReviewChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  getDisplayExprCreatedBy(value: any) {
    if (value && this.users && this.posts) {
      let user = this.users.find(f => f.id === value.id);
      if (user) {
        let post = this.posts.find(f => f.id === user.postId);

        if (post) {
          return `${post.companyPost}(${user.firstName} ${user.lastName})`;
        } 
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  async getPartnerLocationContacts() {
    this.partnerContacts = [];
    
    await this.occupationService.getAllOccupationsAsync().then(async items => {
      if (items && items.length > 0) {
        this.occupations = items;

        if (this.partner && this.partner.id) {
          await this.partnerLocationContactService.getPartnerLocationContactIncludingInactiveByPartnerID(this.partner.id).then(contacts => {
            if (contacts) {
              this.partnerContacts = contacts;
            } else {
              this.partnerContacts = [];
            }
          });
        }
      } else {
        this.occupations = [];
      }
    });
  }

  getDisplayExprPartnerContacts(value:any) {
    if (value && this.occupations) {
      let occupation = this.occupations.find(f => f.id === value.position);

      if (occupation) {
        return `${value.firstName} ${value.lastName}(${occupation.code})`;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }
}
