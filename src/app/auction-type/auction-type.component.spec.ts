import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionTypeComponent } from './auction-type.component';

describe('AuctionTypeComponent', () => {
  let component: AuctionTypeComponent;
  let fixture: ComponentFixture<AuctionTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuctionTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
