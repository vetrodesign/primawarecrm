export class RoleRights {
    public id: number;
    public roleId: number;
    public clientModuleId: number;
    public menuItemId: number;

    public title: string;

    public canView: boolean;
    public canAdd: boolean;
    public canUpdate: boolean;
    public canDelete: boolean;
    public canPrint: boolean;
    public canExport: boolean;
    public canImport: boolean;
}
