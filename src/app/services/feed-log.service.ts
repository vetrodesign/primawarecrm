import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { FeedLog } from 'app/models/feed-log.model';

@Injectable({
  providedIn: 'root'
})
export class FeedLogService {
  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<FeedLog[]> {
    const apiURL = `/FeedLog/getall`;
    return await this.apiService.GetAsync<FeedLog[]>(apiURL, Constants.mrkApi);
  }

  async getByCustomerAsync(): Promise<FeedLog[]> {
    const apiURL = `/FeedLog/getbycustomer`;
    return await this.apiService.GetAsync<FeedLog[]>(apiURL, Constants.mrkApi);
  }

  async getByPartnerIdAsync(partnerId: number): Promise<FeedLog[]> {
    const apiURL = `/FeedLog/GetByPartnerId/` + partnerId;
    return await this.apiService.GetAsync<FeedLog[]>(apiURL, Constants.mrkApi);
  }

  async createAsync(pif: FeedLog): Promise<any> {
    const apiURL = `/FeedLog/create`;
    return await this.apiService.PostAsync<any>(apiURL, pif, Constants.mrkApi);
  }

  async updateAsync(pif: FeedLog): Promise<any> {
    const apiURL = `/FeedLog/update`;
    return await this.apiService.PutAsync<any>(apiURL, pif, Constants.mrkApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/FeedLog/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
