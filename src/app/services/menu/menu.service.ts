import { Injectable } from '@angular/core';
import { AuthService } from '../auth.service';
import { BehaviorSubject } from 'rxjs';
import { ClientModuleService } from '../client-module.service';
import { ClientModuleMenuItemsService } from '../client-module-menu-items.service';
import { RoleRightsService } from '../role-rights.service';
import { ClientModule } from 'app/models/clientmodule.model';
import { Constants } from 'app/constants';
import { RolesService } from '../roles.service';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  clientModule: ClientModule;
  public subject: BehaviorSubject<string[]>;
  public menuItems: BehaviorSubject<RouteInfo[]>;
  constructor(private authenticationService: AuthService,
    private clientModuleService: ClientModuleService,
    private roleService: RolesService,
    private clientModuleMenuItemService: ClientModuleMenuItemsService,
    private roleRightsService: RoleRightsService) {
    this.subject = new BehaviorSubject<string[]>(null);
    this.menuItems = new BehaviorSubject<RouteInfo[]>([]);
  }

  public getMenuItems() {
    const clientModuleMenuItems = [];
    this.clientModuleService.getClientModuleAsyncByID(this.authenticationService.getClientModuleId()).then(r => {
      if (r) {
        const isAdminOrOwner = this.authenticationService.isUserAdmin() || this.authenticationService.isUserOwner();
        this.clientModule = r;
        this.clientModuleMenuItemService.getClientModuleMenuItemsByModuleIDAsync(r.id, true).then((items) => {
          items.reverse();
          let roles = [];
          const promises = [];
          this.roleService.getRolesAsyncByUserIdAndSiteId(this.authenticationService.getUserId()).then(ur => {
             roles = ur;
             roles.forEach(role => {
              promises.push(this.roleRightsService.getAllRolesRightsByRoleAndModuleAsync(Number(role), r.id).then(rolesRights => {
                items.forEach((i) => {
                  if (!isAdminOrOwner) {
                    const right = rolesRights.find(x => x.clientModuleMenuItemId === i.id);
                    i.hidden = (i.hidden === undefined) ? (right ? !right.canView : true) :
                      (i.hidden ? (right ? !right.canView : true) : false);
                  };
                });
              }));
            });
          });
          Promise.all(promises).then(values => {
            items.forEach((i) => {
              if (i.subMenuId !== 0) {
                const menuItem = items.find(x => x.id === i.subMenuId);
                if (menuItem) {
                  if (menuItem.submenu) {
                    i.padding = 10;
                    menuItem.submenu.unshift(i);
                  } else {
                    menuItem.submenu = [];
                    i.padding = 10;
                    menuItem.submenu.unshift(i);
                  }
                }
              }
            });
            items.reverse();
            items.forEach((i) => {
              if (i.subMenuId === 0) {
                clientModuleMenuItems.push(i);
              }
            })
            this.menuItems.next(clientModuleMenuItems);
          });
        });
      }
    });
  }
}

export declare interface RouteInfo {
  id: number;
  path?: string;
  title: string;
  icon: string;
  class: string;
  hidden?: boolean;
  padding?: number;
  submenu?: RouteInfo[];
  subMenuId?: number;
}
