import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { RouteInfo } from 'app/services/menu/menu.service';
import { SubmenuColours } from 'app/services/submenu-colors.service';

@Component({
  selector: 'app-sidebar-nav-item',
  templateUrl: './sidebar-nav-item.component.html',
  styleUrls: ['./sidebar-nav-item.component.css'],
})
export class SidebarNavItemComponent implements OnInit {
  @Input() sideNavItem!: RouteInfo;
  @Input() isActive!: boolean;
  expanded: boolean;
  colour: string;
  bold: any;

  constructor(private submenuColours: SubmenuColours) { }

  ngOnInit(): void {
    this.setStyle();
  }

  private setStyle() {
    if (this.sideNavItem.subMenuId != 0) {
      this.bold = 'bold';
      this.colour = this.submenuColours.getColour(this.sideNavItem.id);
    }
  }

}
