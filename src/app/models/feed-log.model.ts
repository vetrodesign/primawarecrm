import { BaseDomain } from "./base-domain.model";

export class FeedLog extends BaseDomain {
    id: number;
    feedId: number;
    requestTimestamp: number;
    feedType: number;
    partnerId: number;
    isSuccess: boolean;
    isBlocked: boolean;
    blockMessage: string;

    partnerName: String;
}