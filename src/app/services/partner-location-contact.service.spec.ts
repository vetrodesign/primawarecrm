import { TestBed } from '@angular/core/testing';

import { PartnerLocationContactService } from './partner-location-contact.service';

describe('PartnerLocationContactService', () => {
  let service: PartnerLocationContactService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerLocationContactService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
