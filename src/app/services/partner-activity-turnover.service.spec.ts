import { TestBed } from '@angular/core/testing';

import { PartnerActivityTurnoverService } from './partner-activity-turnover.service';

describe('PartnerActivityTurnoverService', () => {
  let service: PartnerActivityTurnoverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerActivityTurnoverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
