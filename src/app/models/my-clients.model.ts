export class MyClientsFilter {
    isMonthlyTurnover: boolean;
    isYearlyTurnover: boolean;
    year: number;
    turnoverType: number;
    postId: number;
    inactivityValue: number; 
    personType: number;
}