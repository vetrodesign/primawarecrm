import { TestBed } from '@angular/core/testing';

import { CpvCodeService } from './cpv-code.service';

describe('CpvCodeService', () => {
  let service: CpvCodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CpvCodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
