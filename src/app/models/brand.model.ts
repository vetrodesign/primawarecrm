import { BaseDomain } from "./base-domain.model";

export class Brand extends BaseDomain {
    public code: string;
    public partnerId: number;
    public isActive: boolean;

    detailsId: number;
    brandId: number;
    languageId: number;
    name: string;

    detailsRowVersion: any;
}