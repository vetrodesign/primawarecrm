import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MandatoryPromotionTurnoverComponent } from './mandatory-promotion-turnover.component';

describe('MandatoryPromotionTurnoverComponent', () => {
  let component: MandatoryPromotionTurnoverComponent;
  let fixture: ComponentFixture<MandatoryPromotionTurnoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MandatoryPromotionTurnoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MandatoryPromotionTurnoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
