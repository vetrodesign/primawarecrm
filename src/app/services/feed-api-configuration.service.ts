import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { FeedApiConfiguration, FeedApiXPartnerIp } from 'app/models/feed-api-configuration.model';

@Injectable({
  providedIn: 'root'
})
export class FeedApiConfigurationService {
  constructor(private apiService: ApiService) { }


  // FeedApiConfiguration
  async getAllAsync(): Promise<FeedApiConfiguration[]> {
    const apiURL = `/FeedApiConfiguration/getall`;
    return await this.apiService.GetAsync<FeedApiConfiguration[]>(apiURL, Constants.mrkApi);
  }

  async getByCustomerAsync(): Promise<FeedApiConfiguration[]> {
    const apiURL = `/FeedApiConfiguration/getall`;
    return await this.apiService.GetAsync<FeedApiConfiguration[]>(apiURL, Constants.mrkApi);
  }

  async getByPartnerIdAsync(partnerId: number): Promise<FeedApiConfiguration[]> {
    const apiURL = `/FeedApiConfiguration/GetByPartnerId/` + partnerId;
    return await this.apiService.GetAsync<FeedApiConfiguration[]>(apiURL, Constants.mrkApi);
  }

  async createAsync(pif: FeedApiConfiguration): Promise<any> {
    const apiURL = `/FeedApiConfiguration/create`;
    return await this.apiService.PostAsync<any>(apiURL, pif, Constants.mrkApi);
  }

  async updateAsync(pif: FeedApiConfiguration): Promise<any> {
    const apiURL = `/FeedApiConfiguration/update`;
    return await this.apiService.PutAsync<any>(apiURL, pif, Constants.mrkApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/FeedApiConfiguration/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }

  // FeedApiXPartnerIp
  async getAllFeedApiXPartnerIpAsync(): Promise<FeedApiXPartnerIp[]> {
    const apiURL = `/FeedApiXPartnerIp/getall`;
    return await this.apiService.GetAsync<FeedApiXPartnerIp[]>(apiURL, Constants.mrkApi);
  }

  async getByCustomerFeedApiXPartnerIpAsync(): Promise<FeedApiXPartnerIp[]> {
    const apiURL = `/FeedApiXPartnerIp/getbycustomer`;
    return await this.apiService.GetAsync<FeedApiXPartnerIp[]>(apiURL, Constants.mrkApi);
  }

  async getByFeedApiConfigurationIdAsync(configurationId: number): Promise<FeedApiXPartnerIp[]> {
    const apiURL = `/FeedApiXPartnerIp/GetByFeedApiConfigurationId/` + configurationId;
    return await this.apiService.GetAsync<FeedApiXPartnerIp[]>(apiURL, Constants.mrkApi);
  }

  async createFeedApiXPartnerIpAsync(pif: FeedApiXPartnerIp): Promise<any> {
    const apiURL = `/FeedApiXPartnerIp/create`;
    return await this.apiService.PostAsync<any>(apiURL, pif, Constants.mrkApi);
  }

  async syncMultipleAsync(feeds: FeedApiXPartnerIp[]): Promise<any> {
    const apiURL = `/FeedApiXPartnerIp/syncMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, feeds, Constants.mrkApi);
  }

  async updateFeedApiXPartnerIpAsync(pif: FeedApiXPartnerIp): Promise<any> {
    const apiURL = `/FeedApiXPartnerIp/update`;
    return await this.apiService.PutAsync<any>(apiURL, pif, Constants.mrkApi);
  }

  async deleteFeedApiXPartnerIpAsync(ids: number[]): Promise<any> {
    const apiURL = `/FeedApiXPartnerIp/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
