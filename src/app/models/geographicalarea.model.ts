import { BaseDomain } from "./base-domain.model";

export class GeographicalArea extends BaseDomain {
    public name: string;
    public description: string;
}
