export class Country {
    public id: number;
    public name: string;
    public code: string;
    public phonePrefix: string;
    public firstLanguageId: number;
    public secondLanguageId: number;
    public currencyId: number;
    public offerCurrencyId: number;

    public isEUMember: boolean;
    public isNatoMember: boolean;
    public IsIndependent: boolean;

    public alphaThreeCode: string;
    public numericCode: number;
    public ISOCode: string;
    public continentId: number;
}

export class SmallCountry {
    public id: number;
    public name: string;
    public code: string;
    public firstLanguageId: number;
}
