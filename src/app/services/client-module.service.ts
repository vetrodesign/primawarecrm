import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ClientModule } from 'app/models/clientmodule.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ClientModuleService {

  constructor(private apiService: ApiService) { }

  async getAllClientModulesAsync(): Promise<ClientModule[]> {
    const apiURL = `/clientModule/getall`;
    return await this.apiService.GetAsync<ClientModule[]>(apiURL, Constants.ssoApi);
  }

  async getClientModuleAsyncByID(moduleId): Promise<ClientModule> {
    const apiURL = `/clientModule/GetById/` + moduleId;
    return await this.apiService.GetAsync<ClientModule>(apiURL, Constants.ssoApi);
  }

  async createClientModuleAsync(customer: ClientModule): Promise<any> {
    const apiURL = `/clientModule`;
    return await this.apiService.PostAsync<any>(apiURL, customer, Constants.ssoApi);
  }

  async updateClientModuleAsync(customer: ClientModule): Promise<any> {
    const apiURL = `/clientModule/` + customer.id;
    return await this.apiService.PutAsync<any>(apiURL, customer, Constants.ssoApi);
  }

  async deleteClientModuleAsync(id: number): Promise<any> {
    const apiURL = `/clientModule/` + id;
    return await this.apiService.DeleteSingleAsync<any>(apiURL, Constants.ssoApi);
  }
}
