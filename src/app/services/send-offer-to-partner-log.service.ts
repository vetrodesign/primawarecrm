import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { SendOfferToPartnerLog } from 'app/models/sendOfferToPartnerLog.model';
import { Constants } from 'app/constants';


@Injectable({
  providedIn: 'root'
})
export class SendOfferToPartnerLogService {

  constructor(private apiService: ApiService) { }

  async getLastByPartnerIdAsync(partnerId: number): Promise<SendOfferToPartnerLog> {
    const apiURL = `/SendOfferToPartnerLog/GetLastByPartnerId`;
    return await this.apiService.PostAsync<SendOfferToPartnerLog>(apiURL, partnerId,  Constants.mrkApi);
  }

  async createAsync(s: SendOfferToPartnerLog): Promise<any> {
    const apiURL = `/SendOfferToPartnerLog/create`;
    return await this.apiService.PostAsync<any>(apiURL, s, Constants.mrkApi);
  }
}
