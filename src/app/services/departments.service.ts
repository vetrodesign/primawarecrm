import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Department } from 'app/models/department.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class DepartmentsService {

  constructor(private apiService: ApiService) { }

  async getAllDepartmentsAsync(): Promise<any[]> {
    const apiURL = `/department/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.ssoApi);
  }

  async getDepartmentsAsyncByID(): Promise<any[]> {
    const apiURL = `/department/getbycustomerid`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.ssoApi);
  }

  async getByDepartmentIdAsync(departmentId: number): Promise<Department> {
    const apiURL = '/department/getbydepartmentid';
    return await this.apiService.PostAsync<any>(apiURL, departmentId, Constants.ssoApi);
  }
}
