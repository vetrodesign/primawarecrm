export class DiscountGridsByProductConventionIdAndCurrencyIdRequestModel {
    productConventionId: number;
    currencyId: number;
}