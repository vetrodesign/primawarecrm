import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GeocoderResult, GeocoderStatus } from '@agm/core';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GoogleGeocodingService {

  private apiUrl: string = 'https://maps.googleapis.com/maps/api/geocode/';
  private format: string = 'json';

  constructor(private http: HttpClient) { }

  private formatRequestUrl(address: string): string {
    return `${this.apiUrl}${this.format}?address=${address}&key=${environment.googleApiKey}`;
  }

  async getGeolocationsFromAddress(address: string) {
    const result = await this.http.get(this.formatRequestUrl(address))
    .toPromise()
    .catch(e => { this.logException('getGeolocationFromAddress', e); });

    return <GeocodingAPIResult>result;
  }

  private logException(actionType: string, e: any): void {
    console.log(`a aparut o eroare la ${actionType}`, e);
  }
}

export class GeocodingAPIResult {
  results: GeocoderResult[];
  status: GeocoderStatus;
}