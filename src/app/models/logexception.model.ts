export class LogException {
    public ActionType: string;
    public Exception: string;
    public User: string;
    public Customer: string;
    public Created: Date;
    public Body: string;
    public Performance: string;
}
