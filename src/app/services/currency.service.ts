import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  constructor(private apiService: ApiService) { }

  async getBaseCurrency(): Promise<any> {
    const apiURL = `/currency/getallbasecurrencies`;
    return await this.apiService.GetAsync<any[]>(apiURL,Constants.commonApi);

  }

  async GetLast3YearsCurrencyRates(): Promise<any> {
    const apiURL = `/currency/GetLast3YearsCurrencyRates`;
    return await this.apiService.GetAsync<any[]>(apiURL,Constants.commonApi);
  }
}
