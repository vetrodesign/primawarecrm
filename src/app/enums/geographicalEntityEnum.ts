export enum GeographicalEntity {
    Continent = 1,
    Country = 2,
    County = 3,
    City = 4
}