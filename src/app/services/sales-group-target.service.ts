import { Injectable } from '@angular/core';
import { SalesGroupTarget } from 'app/models/salegrouptarget.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class SalesGroupTargetService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<SalesGroupTarget[]> {
    const apiURL = `/SalesGroupTarget/getall`;
    return await this.apiService.GetAsync<SalesGroupTarget[]>(apiURL, Constants.mrkApi);
  }

  async getByCustomerIdAsync(): Promise<SalesGroupTarget[]> {
    const apiURL = `/SalesGroupTarget/GetByCustomerId`;
    return await this.apiService.GetAsync<SalesGroupTarget[]>(apiURL, Constants.mrkApi);
  }

  async createAsync(st: SalesGroupTarget): Promise<any> {
    const apiURL = `/SalesGroupTarget/create`;
    return await this.apiService.PostAsync<any>(apiURL, st, Constants.mrkApi);
  }

  async updateAsync(st: SalesGroupTarget): Promise<any> {
    const apiURL = `/SalesGroupTarget/update`;
    return await this.apiService.PutAsync<any>(apiURL, st, Constants.mrkApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/SalesGroupTarget/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
