import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerReviewQuestionComponent } from './partner-review-question.component';

describe('PartnerReviewQuestionComponent', () => {
  let component: PartnerReviewQuestionComponent;
  let fixture: ComponentFixture<PartnerReviewQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerReviewQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerReviewQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
