import { Component, OnInit, ViewChild, Input, AfterViewInit, EventEmitter, Output, SimpleChanges } from '@angular/core';
import { DxDataGridComponent, DxTooltipComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import * as _ from 'lodash';
import { PartnerFinancialInfoService } from 'app/services/partner-financial-info.service';
import { DiscountGridService } from 'app/services/discount-grid.service';
import { DiscountGrid } from 'app/models/discount-grid.model';
import { DiscountGridDetailsService } from 'app/services/discount-grid-details.service';
import { SalePriceListService } from 'app/services/sale-price-list.service';
import { SalePriceList } from 'app/models/salepricelist.model';
import { PartnerService } from 'app/services/partner.service';
import { TurnoverPreorderService } from 'app/services/turnover-preorder.service';
import { TurnoverPreorder } from 'app/models/turnover-preorder.model';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { environment } from 'environments/environment';
import { Constants } from 'app/constants';
import { ItemService } from 'app/services/item.service';
import { on } from 'devextreme/events';
import { SiteService } from 'app/services/site.service';
import { ManagementService } from 'app/services/management.service';
import { Site } from 'app/models/site.model';
import { Management } from 'app/models/management.model';
import { AuthService } from 'app/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { ClientModuleMenuItemsService } from 'app/services/client-module-menu-items.service';
import { HelperService } from 'app/services/helper.service';

@Component({
  selector: 'app-yearly-turnover',
  templateUrl: './yearly-turnover.component.html',
  styleUrls: ['./yearly-turnover.component.css']
})
export class YearlyTurnoverComponent implements OnInit, AfterViewInit {
  @Input() yearlyTurnoverActions: IPageActions;
  @Input() hideTable: boolean;
  @Input() postId: number;
  @Input() clientId: number;
  @Input() customerId: number;
  @Input() showCharts: boolean;
  @Input() items: any;
  @Input() itemsDS: any;
  @Input() partners: any;
  @Input() isSpecialPriceRequest: boolean;
  @Input() isTurnoverPage: boolean = true;

  @Output() discountNameValueChange: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('secondDataGrid') secondDataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('stockTooltipItemCode') stockTooltipItemCode: DxTooltipComponent;

  @ViewChild('supplierYearlyTurnOversDataGrid') supplierYearlyTurnOversDataGrid: DxDataGridComponent;
  @ViewChild('supplierNumericTurnOversDataGrid') supplierNumericTurnOversDataGrid: DxDataGridComponent;

  loaded: boolean;
  templateVisible: boolean;
  infoTemplateVisible: boolean;

  actions: IPageActions;

  yearlyTurnOvers: any[];
  numericTurnOvers: any[];

  supplierYearlyTurnOvers: any[];
  supplierNumericTurnOvers: any[];

  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  groupedText: string;
  currentDate: Date = new Date();

  currentMonthValue: number;
  previousMonthValue: number;
  sumOfLastThreeMonths: number;

  supplierCurrentMonthValue: number;
  supplierPreviousMonthValue: number;
  supplierSumOfLastThreeMonths: number;

  discountGrids: DiscountGrid[] = [];
  salePriceLists: SalePriceList[] = [];
  turnoverPreorders: TurnoverPreorder[] = [];
  productConventionGrid: DiscountGrid;
  discountName: string;
  supplierDiscountName: string;
  tableHtml: string

  displayData: boolean;
  basketPopup: boolean;

  timeout: number; 
  stockTooltipData: any = {};

  sites: Site[] = [];
  managements: Management[] = [];
  filteredManagements: Management[] = [];

  selectedManagementId: number;
  selectedSiteId: number;

  orderForTheAmount: string;
  infoButton: string;
  basketTotal: any;
  isClient: boolean;
  isSupplier: boolean;
  isShoppingCartPopupOpen: boolean;
  isOrderAmountCommunicatedToClient: boolean = false;
  alreadyCommunicatedOrderForTheAmount: string;

  constructor(
    private translationService: TranslateService,
    private partnerFinancialInfoService: PartnerFinancialInfoService,
    private partnerService: PartnerService,
    private itemService: ItemService,
    private siteService: SiteService,
    private managementService: ManagementService,
    private discountGridService: DiscountGridService,
    private salePriceListService: SalePriceListService,
    private discountGridDetailsService: DiscountGridDetailsService,
    private turnoverPreorderService: TurnoverPreorderService,
    private notificationService: NotificationService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private clientModuleMenuItemsService: ClientModuleMenuItemsService,
    private helperService: HelperService) {

    
    this.groupedText = this.translationService.instant('groupedText');
    this.yearlyTurnOvers = [];
    this.numericTurnOvers = [];

    this.supplierYearlyTurnOvers = [];
    this.supplierNumericTurnOvers = [];
  }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['partners'] && this.partners && this.partners.store && this.partners.store.length > 0) {
      this.isClient = this.partners.store[0].isClient;
      this.isSupplier = this.partners.store[0].isSupplier;
    }

    if (changes['clientId'] && this.clientId) {
      this.getData();
    }
  }

  canUpdate() {
    return this.yearlyTurnoverActions ? this.yearlyTurnoverActions.CanUpdate : false;
  }

  canDelete() {
    return false;
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }


  async getData() {


    Promise.all([this.getSalePriceLists(), this.getDiscountGrids(), this.getSites(), this.getManagements()]).then(r => {
      this.getYearlyTurnover(); this.getSupplierYearlyTurnover();
      this.getNumericTurnover(); this.getSupplierNumericTurnover();
    });
  }

  async getSites(): Promise<any> {
    await this.siteService.getCustomerLocationsAsyncByID().then(lists => {
      this.sites = (lists && lists.length > 0) ? lists : null;
    })
  }

  async getManagements(): Promise<any> {
    await this.managementService.getManagementsAsyncByID().then(lists => {
      this.managements = (lists && lists.length > 0) ? lists : null;
    })
  }


  async getSalePriceLists(): Promise<any> {
    await this.salePriceListService.getSalePriceListsAsyncByID().then(lists => {
      this.salePriceLists = (lists && lists.length > 0) ? lists : null;
    })
  }

  async getDiscountGrids(): Promise<any> {
    if (this.clientId) {
      await this.partnerService.getPartnerProductConventionIdByImplicitLocationAsync(this.clientId).then(async r => {
        if (r && r.productConventionId && r.currencyId) {
          await this.discountGridService.GetAllAsync().then(grids => {
            this.discountGrids = (grids && grids.length > 0) ? grids : null;
            this.productConventionGrid = this.discountGrids.find(pc => pc.productConventionId == r.productConventionId && pc.currencyId == r.currencyId && pc.benefitType == 1 && pc.dataType == 1 && pc.isActive == true);      
          });
        }
      });  
    }
  }

  async getNumericTurnover(): Promise<any> { 
    if (this.clientId && this.postId) { 
      await this.partnerFinancialInfoService.getNumericDetailsByPartnerIdAsync(this.clientId).then(items => { 
        this.numericTurnOvers = (items && items.length > 0) ? items : [];
      } );
    }
  }

  async getSupplierNumericTurnover(): Promise<any> { 
    if (this.clientId && this.postId) { 
      await this.partnerFinancialInfoService.getSupplierNumericDetailsByPartnerIdAsync(this.clientId).then(items => { 
        this.supplierNumericTurnOvers = (items && items.length > 0) ? items : [];
      } );
    }
  }

  async getYearlyTurnover(): Promise<any> {
    if (this.clientId && this.postId) {
      this.loaded = true;
      this.discountName = "";
      await this.partnerFinancialInfoService.getDetailsByPartnerIdAsync(this.clientId).then(items => {
        this.yearlyTurnOvers = (items && items.length > 0) ? items : [];
        if (this.yearlyTurnOvers && this.yearlyTurnOvers.length > 0) {

          let data = this.yearlyTurnOvers.find(x => x.year == new Date().getFullYear());
          if (data) {
            let monthValues = this.getMonthsData(data);

            let currentMonthIndex = new Date().getMonth(); // This gives you a 0-based index

            // Requirement 1: obtain the value of the current month
            this.currentMonthValue = Math.ceil(monthValues[currentMonthIndex]);

            // Requirement 2: obtain the value of the previous month from the current month
            if (currentMonthIndex > 0) {
              this.previousMonthValue = Math.ceil(monthValues[currentMonthIndex - 1]);
            } else {
              let previousYear = this.yearlyTurnOvers.find(x => x.year == (new Date().getFullYear() - 1));
              let previousYearMonths = this.getMonthsData(previousYear);
              this.previousMonthValue = Math.ceil(Number(previousYearMonths[11]));
            }          

            // Requirement 3: obtain the sum of the last 3 months excepting the current month and divide that sum by 3
            this.sumOfLastThreeMonths = 0;
            if (currentMonthIndex >= 3) {
             let x = (Number(monthValues[currentMonthIndex - 1]) + Number(monthValues[currentMonthIndex - 2]) + Number(monthValues[currentMonthIndex - 3])) / 3;
              this.sumOfLastThreeMonths = Math.ceil(Number(x.toFixed(2)));
            } else {
              let previousYear = this.yearlyTurnOvers.find(x => x.year == (new Date().getFullYear() - 1));
              let previousYearMonths = this.getMonthsData(previousYear);
              switch (currentMonthIndex) {
                  case 2:  // March, so take January of this year and December, November of last year
                      let x = (Number(monthValues[1]) + Number(previousYearMonths[11]) + Number(previousYearMonths[10])) / 3;
                      this.sumOfLastThreeMonths = Math.ceil(Number(x.toFixed(2)));
                      break;
                  case 1:  // February, so take January of this year, December and November of last year
                      let y = (Number(monthValues[0]) + Number(previousYearMonths[11]) + Number(previousYearMonths[10])) / 3;
                      this.sumOfLastThreeMonths = Math.ceil(Number(y.toFixed(2)));
                      break;
                  case 0:  // January, so take December, November, and October of last year
                      let z = (Number(previousYearMonths[11]) + Number(previousYearMonths[10]) + Number(previousYearMonths[9])) / 3;
                      this.sumOfLastThreeMonths = Math.ceil(Number(z.toFixed(2)));
                      break;
              }
            }
          }

          let maxValue = Math.max(this.currentMonthValue, this.previousMonthValue, this.sumOfLastThreeMonths);
          if (this.productConventionGrid) {
             this.discountGridDetailsService.GetByDiscountGridAsync(this.productConventionGrid.id).then(grids => {
              if (grids && grids.length > 0) {
                this.tableHtml = '<table border="1">';
                this.tableHtml += '<thead><tr><th>Grila</th><th>De la</th><th>Pana la</th></tr></thead>';
            
                this.tableHtml += '<tbody>';
                for (const grid of grids) {
                     this.tableHtml += `<tr>
                                          <td style="text-align: left;">${this.salePriceLists.find(ss => ss.id == grid.salePriceListId)?.name}</td>
                                          <td style="text-align: right;">${formatter.format(grid.valueFrom)}</td>
                                          <td style="text-align: right;">${formatter.format(grid.valueTo)}</td>
                                        </tr>`;
                }
                this.tableHtml += '</tbody>';
            
                let item = grids.find(grid => {
                  let lowerConditionMet = (grid.fromConditionType === 1 && maxValue >= grid.valueFrom) || (grid.fromConditionType === 2 && maxValue >= grid.valueFrom);
                  let upperConditionMet = (grid.toConditionType === 1 && maxValue < grid.valueTo) || (grid.toConditionType === 2 && maxValue <= grid.valueTo);
                  return lowerConditionMet && upperConditionMet;
                });
            
                if (item && item.salePriceListId && this.salePriceLists && this.salePriceLists.length > 0) {

                  const index = grids.findIndex(i => i.name === item.name && i.salePriceListId === item.salePriceListId);
                  var valueForNextRange = this.findRequiredValueForNextRange(this.currentMonthValue, grids.slice(index + 1));

                  let nextGrid = grids.slice(index + 1) != null ? grids.slice(index + 1)[0] : null;
                  const matchDiscountFromPriceList = this.salePriceLists.find(ss => ss.id == nextGrid?.salePriceListId)?.name?.match(/-\d+%/);

                  let formattedDiscount = matchDiscountFromPriceList ? matchDiscountFromPriceList[0] : null;
                  this.orderForTheAmount = valueForNextRange != -1 ?`Comanda de ${this.helperService.setNumberWithCommas(this.helperService.roundUp(valueForNextRange))} RON beneficiaza de discount ${formattedDiscount}!` : null;
                  this.alreadyCommunicatedOrderForTheAmount = `Am comunicat: La o comanda de ${this.helperService.setNumberWithCommas(this.helperService.roundUp(valueForNextRange))} lei, va beneficia de discount ${formattedDiscount}`;

                  let s = this.salePriceLists.find(ss => ss.id == item.salePriceListId);
                  if (s) {
                    this.discountName = s.name;
                    this.discountNameValueChange.emit(s.name);
                  }
                }
            } else {
                this.tableHtml = '';
              }
            })
          }

          const formatter = new Intl.NumberFormat('en-US', {
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
          });
          this.yearlyTurnOvers.forEach(item => {
            for (let i = 1; i <= 12; i++) {
              let prop = 'l' + i;
              if (item[prop]) {
                let roundedValue = Math.ceil(item[prop]);
                item[prop] = formatter.format(roundedValue);
              }
            }
            let roundedValue = Math.ceil(item['total']);
            item['total'] = formatter.format(roundedValue);
          });

        }
        this.loaded = false;
      });
    }
  }

  async getSupplierYearlyTurnover(): Promise<any> {
    if (this.clientId && this.postId) {
      this.loaded = true;
      this.supplierDiscountName = "";
      await this.partnerFinancialInfoService.getSupplierDetailsByPartnerIdAsync(this.clientId).then(items => {
        this.supplierYearlyTurnOvers = (items && items.length > 0) ? items : [];
        if (this.supplierYearlyTurnOvers && this.supplierYearlyTurnOvers.length > 0) {

          let data = this.supplierYearlyTurnOvers.find(x => x.year == new Date().getFullYear());
          if (data) {
            let monthValues = this.getMonthsData(data);

            let currentMonthIndex = new Date().getMonth(); // This gives you a 0-based index

            // Requirement 1: obtain the value of the current month
            this.supplierCurrentMonthValue = Math.ceil(monthValues[currentMonthIndex]);

            // Requirement 2: obtain the value of the previous month from the current month
            if (currentMonthIndex > 0) {
              this.supplierPreviousMonthValue = Math.ceil(monthValues[currentMonthIndex - 1]);
            } else {
              let previousYear = this.supplierYearlyTurnOvers.find(x => x.year == (new Date().getFullYear() - 1));
              let previousYearMonths = this.getMonthsData(previousYear);
              this.supplierPreviousMonthValue = Math.ceil(Number(previousYearMonths[11]));
            }          

            // Requirement 3: obtain the sum of the last 3 months excepting the current month and divide that sum by 3
            this.supplierSumOfLastThreeMonths = 0;
            if (currentMonthIndex >= 3) {
             let x = (Number(monthValues[currentMonthIndex - 1]) + Number(monthValues[currentMonthIndex - 2]) + Number(monthValues[currentMonthIndex - 3])) / 3;
              this.supplierSumOfLastThreeMonths = Math.ceil(Number(x.toFixed(2)));
            } else {
              let previousYear = this.supplierYearlyTurnOvers.find(x => x.year == (new Date().getFullYear() - 1));
              let previousYearMonths = this.getMonthsData(previousYear);
              switch (currentMonthIndex) {
                  case 2:  // March, so take January of this year and December, November of last year
                      let x = (Number(monthValues[1]) + Number(previousYearMonths[11]) + Number(previousYearMonths[10])) / 3;
                      this.supplierSumOfLastThreeMonths = Math.ceil(Number(x.toFixed(2)));
                      break;
                  case 1:  // February, so take January of this year, December and November of last year
                      let y = (Number(monthValues[0]) + Number(previousYearMonths[11]) + Number(previousYearMonths[10])) / 3;
                      this.supplierSumOfLastThreeMonths = Math.ceil(Number(y.toFixed(2)));
                      break;
                  case 0:  // January, so take December, November, and October of last year
                      let z = (Number(previousYearMonths[11]) + Number(previousYearMonths[10]) + Number(previousYearMonths[9])) / 3;
                      this.supplierSumOfLastThreeMonths = Math.ceil(Number(z.toFixed(2)));
                      break;
              }
            }
          }

          const formatter = new Intl.NumberFormat('en-US', {
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
          });
          this.supplierYearlyTurnOvers.forEach(item => {
            for (let i = 1; i <= 12; i++) {
              let prop = 'l' + i;
              if (item[prop]) {
                let roundedValue = Math.ceil(item[prop]);
                item[prop] = formatter.format(roundedValue);
              }
            }
            let roundedValue = Math.ceil(item['total']);
            item['total'] = formatter.format(roundedValue);
          });

        }
        this.loaded = false;
      });
    }
  }

  findRequiredValueForNextRange(x, ranges) {
    for (let i = 0; i < ranges.length; i++) {
        const { valueFrom, valueTo } = ranges[i];

        if (x >= valueFrom && x <= valueTo) {
            if (i + 1 < ranges.length) {
                const nextRangeStart = ranges[i + 1].valueFrom;
                const difference = nextRangeStart - x;
                return difference; // Return difference to reach the next range
            } else {
                return -1; // x is in the last range, no next range
            }
        }

        // If x is less than the current range's valueFrom, calculate the difference to get into this range
        if (x < valueFrom) {
            return valueFrom - x;
        }
    }

    return -1;
}

  openInNewTab() {
    var url = environment.CRMPrimaware + '/' + Constants.turnOver + '?clientId=' + this.clientId;
    window.open(url, '_blank');
  }

  getMonthsData(yearData: any) {
    let monthValues = [];
    for (let i = 1; i <= 12; i++) {
      monthValues.push(yearData['l' + i]);
    }
    return monthValues;
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  public add() {
    this.dataGrid.instance.addRow();
  }

  public refreshDataGrid() {
    //this.getData();
    this.dataGrid.instance.refresh();
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
}

  public deleteRecords(data: any) {
   
  }

  public async openDetails(row: any) {
    this.dataGrid.instance.editRow(row.rowIndex);
    this.rowIndex = row.rowIndex;
  }

  displayCodeExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  public async onRowInserting(event: any): Promise<void> {
    
  }

  toggleTemplate() {
    this.templateVisible = !this.templateVisible;
    setTimeout(async () => {
      if (document.getElementById("discountGridsTooltipYearlyTurnoverInfo")) {
        document.getElementById("discountGridsTooltipYearlyTurnoverInfo").innerHTML = this.tableHtml;
      }
    }, 0);

  }

  toggleInfoTemplate() {
    this.infoTemplateVisible = !this.infoTemplateVisible;
    setTimeout(async () => {
      if (document.getElementById("infoTooltipInfo")) {
        document.getElementById("infoTooltipInfo").innerHTML = this.infoButton;
      }
    }, 0);

  }

  async openBasketPopup() {
    this.isShoppingCartPopupOpen = true;
  }

  updateBasketTotal() {
    this.basketTotal = 0;
    this.turnoverPreorders.filter(x => x.agentQuantity > 0).forEach(i => {
      this.basketTotal += (i.agentQuantity * i.price);
    })
    this.basketTotal = Math.ceil(this.basketTotal * 100) / 100;
  }

  async getTurnoverPreorders() {
    await this.turnoverPreorderService.GetByPartnerIdAsync(this.clientId).then(async items => {
      this.turnoverPreorders = (items && items.length > 0) ? items : [];     
      if (this.turnoverPreorders && this.turnoverPreorders.length > 0) {
        let itemIds = this.turnoverPreorders.map(i => i.itemId);
        await this.itemService.getItemStocks(itemIds).then(r => {
          if (r) {
            this.turnoverPreorders.forEach(i => {
              if (r.hasOwnProperty(i.itemId)) {
                i.totalStock = r[i.itemId];
              } else {
                i.totalStock = 0;
              }
            });
          }
        });
      }
    });
  }

  async onRowUpdated(e) {
    if (e && e.data && e.data.agentQuantity) {
      this.updateBasketTotal();
    }
    else {
      let item = new TurnoverPreorder();
      item = e.data;
  
      if (item) {
        await this.turnoverPreorderService.updateTurnoverPreorderAsync(item).then(r => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Datele au fost modificate cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Datele nu au fost modificate! A aparut o eroare!',
              NotificationTypeEnum.Red, true)
          };
          this.getTurnoverPreorders();
        });
      }
    }
  }

  async deleteTurnoverPreorderRow(data) {
    if (data.data) {
      await this.turnoverPreorderService.deleteTurnoverPreorderAsync([data.data.id]).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Datele au fost sterse cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost sterse! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        };
        this.getTurnoverPreorders();
      });
    }
  }

  openSpecialPriceRequest() {
    if (this.partners && this.partners.store && this.partners.store.length > 0) {
      var url = environment.CRMPrimaware + '/' + Constants.specialPriceRequest + '?clientCode=' + this.partners.store[0].code;
      window.open(url, '_blank');
    }
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && e.data.totalStock < e.data.quantity ) {
      e.rowElement.style.backgroundColor = '#ffdfcf';
    }
    if (e.rowType === 'data' && e.data && e.data.agentQuantity &&  e.data.managementStock && e.data.managementStock > e.data.agentQuantity ) {
      e.rowElement.style.backgroundColor = '#b8ffd3';
    }
    if (e.rowType === 'data' && e.data && e.data.agentQuantity && e.data.managementStock < e.data.agentQuantity ) {
      e.rowElement.style.backgroundColor = '#ff5842';
    }
  }

  onCellPrepared(e) {
    this.setItemCodeTooltip(e);
    if (e.rowType === 'data') {
      e.cellElement.style.paddingTop = '2px';
      e.cellElement.style.paddingBottom = '1px';
    }
  }

  setItemCodeTooltip(event) {
    if (event && event.rowType === 'data' && event.column.dataField === 'totalStock') {
      on(event.cellElement, 'mouseover', arg => {
        this.timeout = window.setTimeout(async () => {

          this.stockTooltipData = null;
          let stockPromise = this.itemService.GetStocksOnSiteAsync(event.data.itemId).then(r => {
            if (r && r.length > 0) {
              this.stockTooltipData = r;
            } else { this.stockTooltipData = []; }
          });

          Promise.all([stockPromise]).then(r => {this.stockTooltipItemCode.instance.show(arg.target);})
        }, 1000);
      });

      on(event.cellElement, 'mouseout', arg => {
        if (this.timeout) {
          window.clearTimeout(this.timeout);
        }
        this.stockTooltipItemCode.instance.hide();
      });
    }
  }

  getDisplayExprSite(item) {
    if (!item) {
       return '';
     }
     return item.code + ' - ' + item.name;
  }

  getDisplayExprManagement(item) {
    if (!item) {
       return '';
     }
     return item.code + ' - ' + item.name;
  }

  onSiteChanged(e) {
    if (e && e.value) {
      this.filteredManagements = this.managements.filter(x => x.siteId == e.value);
    } else {
      this.filteredManagements = [];
    }
  }

  async onManagementChanged(e) {
    if (this.selectedManagementId && this.selectedSiteId) {
      var itemIds = this.turnoverPreorders.map(x => x.itemId);
      await this.itemService.getItemStocksOnManagements(itemIds, [this.selectedManagementId]).then(r => {
        if (r) {
          this.turnoverPreorders.forEach(i => {
            if (r.hasOwnProperty(i.itemId)) {
              i.managementStock = r[i.itemId];
              i.managementId = this.selectedManagementId;
            } else {
              i.managementStock = 0;
              i.managementId = null;
            }
          });
        }
        this.secondDataGrid.instance.clearSelection();
      });
    }
  }

  createOrder() {
    var selectedRows = this.turnoverPreorders.filter(x => x.agentQuantity > 0);
    if (selectedRows && selectedRows.length > 0 && this.selectedManagementId) {
      selectedRows.forEach(sr => {
        if (sr.agentQuantity > sr.managementStock) {
          this.notificationService.alert('top', 'center', 'Comanda nu a fost creata! Ati selectat o cantitate mai mare ca stocul disponibil pe gestiune!',
            NotificationTypeEnum.Red, true)
          return;
        }
      });

      const filteredRows = selectedRows.map(row => ({
        id: row.id,
        itemId: row.itemId,
        partnerId: row.partnerId,
        mngId: row.managementId,
        quantity: row.agentQuantity,
        price: row.price
      }));

      const encodedData = encodeURIComponent(JSON.stringify(filteredRows));
      const targetUrl =  environment.CRMPrimaware +  `/order?data=${encodedData}`;
      window.open(targetUrl, '_blank'); 

    } else {
      this.notificationService.alert('top', 'center', 'Comanda nu a fost creata! Nu ati selectat unul sau mai multe produse sau o gestiune!',
        NotificationTypeEnum.Red, true)
    }
  }

  onRowPreparedSupplierNumericTurnovers(e: any) {
    if (e.rowType === 'header') {
      e.rowElement.style.backgroundColor = '#66bb6a'; 
      e.rowElement.style.color = 'white';
    }
  }

  onRowPreparedSupplierYearlyTurnovers(e: any) {
    if (e.rowType === 'header') {
      e.rowElement.style.backgroundColor = '#66bb6a'; 
      e.rowElement.style.color = 'white';
    }
  }

  onRowPreparedClientNumericTurnovers(e: any) {
    if (e.rowType === 'header') {
      e.rowElement.style.backgroundColor = '#26c6da'; 
      e.rowElement.style.color = 'white';
    }
  }

  onRowPreparedClientYearlyTurnovers(e: any) {
    if (e.rowType === 'header') {
      e.rowElement.style.backgroundColor = '#26c6da'; 
      e.rowElement.style.color = 'white';
    }
  }

  isShoppingCartPopupOpenOutputChange(event: any) {
    this.isShoppingCartPopupOpen = event;
  }

  onOrderAmountCheckBoxChange(event) {
    if (event) {
      this.isOrderAmountCommunicatedToClient = event.value;
    }
  }
}
