import { Injectable } from '@angular/core';
import { DeliveryConditions } from 'app/models/deliveryConditions.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class DeliveryConditionsService {

  constructor(private apiService: ApiService) { }

  async getAllDeliveryConditionsAsync(): Promise<any[]> {
    const apiURL = `/deliveryConditions/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.commonApi);
  }
}
