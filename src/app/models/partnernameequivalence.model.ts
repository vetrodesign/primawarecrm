export class PartnerNameEquivalence 
{
    id: number;
    partnerId: number;
    name: string;
}