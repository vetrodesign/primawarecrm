import { Injectable } from '@angular/core';
import { HelperService } from './helper.service';
import { AuthService } from './auth.service';
import { RoleRightsService } from './role-rights.service';

@Injectable({
  providedIn: 'root'
})
export class PageActionService {
  private roleActionsCache: any[] | null = null;

  constructor(private helperService: HelperService, private authService: AuthService, private roleRightService: RoleRightsService) { }

  public async ensureRoleActionsCache() {
    if (!this.roleActionsCache) {
      this.roleActionsCache = await this.roleRightService.getAllRolesRights();
    }
  }

  public async getRoleActionsByPagePath(path: string) {
    if (!this.roleActionsCache) {
      await this.ensureRoleActionsCache();
    }

    if (this.authService.isUserAdmin() || this.authService.isUserOwner()) {
      return {
        CanView: true,
        CanAdd: true,
        CanUpdate: true,
        CanDelete: true,
        CanPrint: true,
        CanExport: true,
        CanImport: true,
        CanDuplicate: true
      };
    }

    const actions = this.roleActionsCache;
    
    if (actions && actions.find(p => p.clientModuleMenuItem?.path === '/' + path && p.clientModuleMenuItem.clientModuleId === Number(this.authService.getClientModuleId()))) {
      const rights = actions.find(p => p.clientModuleMenuItem?.path === '/' + path&& p.clientModuleMenuItem.clientModuleId === Number(this.authService.getClientModuleId()));
      return {
        CanView: rights.canView,
        CanAdd: rights.canAdd,
        CanUpdate: rights.canUpdate,
        CanDelete: rights.canDelete,
        CanPrint: rights.canPrint,
        CanExport: rights.canExport,
        CanImport: rights.canImport,
        CanDuplicate: rights.canDuplicate
      };
    } else {
      return {
        CanView: false,
        CanAdd: false,
        CanUpdate: false,
        CanDelete: false,
        CanPrint: false,
        CanExport: false,
        CanImport: false,
        CanDuplicate: false
      };
    }
  }
}
