import { NotificationTypeEnum } from '../../app/enums/notificationTypeEnum';

export class Notification {
    from: string;
    align: string;
    text: string;
    color?: NotificationTypeEnum;
    addToNotificationArray?: boolean;
  }