import { BaseDomain } from "./base-domain.model";

export class DailyReportDetails extends BaseDomain {
    dailyReportId: number;
    value: number;
    salesTargetXPostId: number;
}
