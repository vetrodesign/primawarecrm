import { BaseDomain } from "./base-domain.model";

export class Order extends BaseDomain {
    number: string;
    date: Date;

    paymentDueDate: Date;
    deliveryDate: Date;

    type: number;
    status: number;

    initialPartnerId: number;
    partnerId: number;
    partnerLocationId: number;
    partnerLocationContactId: number;
    paymentInstrumentId: number;
    salePriceListId: number;
    deliveryConditionId: number;
    managementId: number;
    paymentOwnerPartnerId: number;
    salesAgentId: number;
    deliveryPartnerId: number;
    deliveryPartnerLocationId: number;

    orderItems: OrderItem[];

    total: number;
    partnerCode: string;
    partnerName: string;
    currencyId: number;

    source: number;
    isActive: boolean = false;
    syncERPExternalId: string;
    syncExternalAppId: string;
    syncERPInvoiceId: string;
    erpNumber: string;
}

export class OrderItem extends BaseDomain {
    orderId: number;
    itemId: number;
    remarks: string;
    quantity: number;
    measurementUnitId: number;
    salePriceListId: number;

    /* Without VAT*/
    orderPrice: number;

    /* VAT as percent */
    rate: number;

    /* OrderPrice with VAT included */
    orderPriceWithVAT: number;

    dueDate: Date;
    arrivalDate: Date;
    loadDate: Date;
    lotNumber: string;

    isActive: boolean = false;
}

export class OrderERPSyncOptionsVM {
    orderId: number;
    historyPostName: string;
    historyUserName: string;
}

export class ListPrice {
    listPrice: number;
    rate: number;
    listPriceWithVAT: number;
}

