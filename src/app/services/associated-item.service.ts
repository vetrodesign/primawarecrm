import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';
import { AssociatedItem } from 'app/models/associated-item.model';

@Injectable({
  providedIn: 'root'
})
export class AssociatedItemService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<AssociatedItem[]> {
    const apiURL = `/AssociatedItem/GetAll`;
    return await this.apiService.GetAsync<AssociatedItem[]>(apiURL, Constants.mrkApi);
  }

  async getAllByPartnerIdAsync(customerId): Promise<AssociatedItem[]> {
    const apiURL = `/AssociatedItem/GetByPartnerId/` + customerId;
    return await this.apiService.GetAsync<AssociatedItem[]>(apiURL, Constants.mrkApi);
  }

  async createAsync(m: AssociatedItem): Promise<any> {
    const apiURL = `/AssociatedItem/create`;
    return await this.apiService.PostAsync<any>(apiURL, m, Constants.mrkApi);
  }

  async createMultipleAsync(m: AssociatedItem[]): Promise<any> {
    const apiURL = `/AssociatedItem/CreateMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, m, Constants.mrkApi);
  }

  async deleteMultipleAsync(m: Number[]): Promise<any> {
    const apiURL = `/AssociatedItem/deleteMultiple`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, m, Constants.mrkApi);
  }
}
