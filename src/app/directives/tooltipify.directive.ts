import { Directive, ElementRef, Input, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appTooltipify]'
})
export class TooltipifyDirective implements AfterViewInit {
  @Input() appTooltipifyText: string = 'default';
  @Input() appTooltipifyDirection: 'top' | 'bottom' | 'left' | 'right';

  constructor(private elementRef: ElementRef) { }

  ngAfterViewInit() {
    this.elementRef.nativeElement.setAttribute('title', this.appTooltipifyText ?? 'text not set');

    $(this.elementRef.nativeElement).tooltip({
      placement: this.appTooltipifyDirection ?? 'top'
    });
  }
}
