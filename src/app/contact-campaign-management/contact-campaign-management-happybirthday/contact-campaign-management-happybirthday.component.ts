import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { IPageActions } from 'app/models/ipageactions';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { DxDataGridComponent, DxValidatorComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { Constants } from 'app/constants';
import { PageActionService } from 'app/services/page-action.service';
import { ActivatedRoute } from '@angular/router';
import { ClientModuleMenuItemsService } from 'app/services/client-module-menu-items.service';
import { ContactCampaign, ContactCampaignHappyBirthDay } from 'app/models/contact-campaign.model';
import { ContactCampaignService } from 'app/services/contact-campaign-management.service';
import { ContactCampaignManagementHappyBirthDayDetailsComponent } from './contact-campaign-management-happybirthday-details/contact-campaign-management-happybirthday-details.component';
import { ContactCampaignTypeEnum } from 'app/enums/contactCampaignTypeEnum';


@Component({
  selector: 'app-contact-campaign-management-happybirthday',
  templateUrl: './contact-campaign-management-happybirthday.component.html',
  styleUrls: ['./contact-campaign-management-happybirthday.component.css']
})
export class ContactCampaignManagementHappyBirthdayComponent implements OnInit, AfterViewInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('contactCampaignManagementHappyBirthDayDetails') contactCampaignManagementHappyBirthDayDetails: ContactCampaignManagementHappyBirthDayDetailsComponent;
  @ViewChild('deletePopupValidationGroup') deletePopupValidationGroup: DxValidatorComponent;

  loaded: boolean;
  displayData: boolean;
  actions: IPageActions;

  contactCampaignType: { id: number; name: string }[] = [];

  contactCampaigns: ContactCampaignHappyBirthDay[];
  selectedRows: any[];
  selectedRowIndex = -1;
  rowIndex: any;
  isOwner: boolean = false;
  detailsVisibility: boolean;
  customerId: string;
  items: any;
  itemsGroup: any;
  selectedContactCampaign: ContactCampaignHappyBirthDay;
  groupedText: string;
  deleteConfirmationPopup: boolean;
  selectedRowsObservationsToStop: string;

  infoButton: string;

  constructor(private authenticationService: AuthService,
    private translationService: TranslateService, private contactCampaignService: ContactCampaignService, private pageActionService: PageActionService,
    private notificationService: NotificationService, private route: ActivatedRoute, private clientModuleMenuItemsService: ClientModuleMenuItemsService) {
    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();

    this.contactCampaigns = [];

    
  }

 

  ngOnInit(): void {
    this.getData();
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }

    if (this.dataGrid) {
      this.dataGrid.instance.columnOption('isActive', 'filterValue', ['1']);
    }
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && !e.data.isActive) {
      e.rowElement.style.backgroundColor = '#ffaf82';
    }
  }

  getData() {
    this.getContactCampaigns();
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }

  async getContactCampaigns() {
    this.contactCampaigns = [];
    this.loaded = true;
    
    await this.contactCampaignService.getAllByCustomerIdAndTypeAsync(Number(this.authenticationService.getUserCustomerId()), ContactCampaignTypeEnum.HappyBirthDay).then(items => {
      this.contactCampaigns = items;
      items.map(item => {
        item.parentCampaign.canModify = new Date(item.parentCampaign.endDate) < new Date()

        return item;
      })
      
      this.loaded = false;
    }).catch(err => this.loaded = false)
  }


  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.contactCampaignManagement).then(result => {
      this.actions = result;
    });
  }

  canUpdate() {
    return this.actions.CanUpdate;
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.deleteConfirmationPopup = true;
  }

  public refreshDataGrid() {
    this.getContactCampaigns();
    this.dataGrid.instance.refresh();
  }

  public deleteRecords(data: any) {
    if (this.deletePopupValidationGroup.instance.validate().isValid) {
      this.contactCampaignService.deleteMultipleAsync(this.selectedRows.map(x => x.parentCampaign.id), this.selectedRowsObservationsToStop).then(response => {
        if (response) {
          this.notificationService.alert('top', 'center', 'Campanii Contactare - Datele au fost sterse cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Campanii Contactare - Datele nu au fost sterse!', NotificationTypeEnum.Red, true)
        }
        this.deleteConfirmationPopup = false;
        this.refreshDataGrid();
      });
    }
  }

  public async openDetails(row: any) {
    this.selectedContactCampaign = row;
    this.loaded = true;
    setTimeout(async () => {
      await this.contactCampaignManagementHappyBirthDayDetails.loadData().then(function () {
        this.detailsVisibility = true;
        this.loaded = false;
      }.bind(this));
    }, 0);
  }

  changeVisible() {
    this.detailsVisibility = false;
    this.refreshDataGrid();
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }
}
