import { Injectable } from '@angular/core';
import { CaenCodeSection } from 'app/models/caencodesection.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';


@Injectable({
  providedIn: 'root'
})
export class CaenCodeSectionService {
  constructor(private apiService: ApiService) { }

  async getAllCaenCodeSectionsAsync(): Promise<CaenCodeSection[]> {
    const apiURL = `/CaenCodeSection/getall`;
    return await this.apiService.GetAsync<CaenCodeSection[]>(apiURL, Constants.commonApi);
  }

  async createCaenCodeSectionAsync(caenCodeSection: CaenCodeSection): Promise<any> {
    const apiURL = `/CaenCodeSection/create`;
    return await this.apiService.PostAsync<any>(apiURL, caenCodeSection, Constants.commonApi);
  }

  async updateCaenCodeSectionAsync(caenCodeSection: CaenCodeSection): Promise<any> {
    const apiURL = `/CaenCodeSection/update`;
    return await this.apiService.PutAsync<any>(apiURL, caenCodeSection, Constants.commonApi);
  }

  async deleteCaenCodeSectionAsync(ids: number[]): Promise<any> {
    const apiURL = `/CaenCodeSection/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
