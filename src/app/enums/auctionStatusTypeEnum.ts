export enum AuctionStatusEnum {
    Rejected = 1,
    Approved = 2,
    RequestedPrice = 3,
    Working = 4,
    Deliberation = 5,
    TotalWin = 6,
    PartialWin = 7,
    TotalRejected = 8,
    Disqualified = 9,
    Canceled = 10
  }
