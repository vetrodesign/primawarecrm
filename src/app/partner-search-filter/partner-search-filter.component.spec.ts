import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerSearchFilterComponent } from './partner-search-filter.component';

describe('PartnerSearchFilterComponent', () => {
  let component: PartnerSearchFilterComponent;
  let fixture: ComponentFixture<PartnerSearchFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerSearchFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerSearchFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
