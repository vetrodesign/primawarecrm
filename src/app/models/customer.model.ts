export class Customer {
    id: string;
    name: string;
    cui: string;
}