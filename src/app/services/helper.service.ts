import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import * as CryptoJS from 'crypto-js';
import { environment } from 'environments/environment';
import { TranslateService } from './translate';

declare const require;
const xml2js = require('xml2js');

@Injectable()
export class HelperService {

  constructor(private translationService: TranslateService) { }

  public getBrowserName() {
    const agent = window.navigator.userAgent.toLowerCase()
    switch (true) {
      case agent.indexOf('edge') > -1:
        return 'edge';
      case agent.indexOf('opr') > -1 && !!(<any>window).opr:
        return 'opera';
      case agent.indexOf('chrome') > -1 && !!(<any>window).chrome:
        return 'chrome';
      case agent.indexOf('trident') > -1:
        return 'ie';
      case agent.indexOf('firefox') > -1:
        return 'firefox';
      case agent.indexOf('safari') > -1:
        return 'safari';
      default:
        return 'other';
    }
  }
  public generateRandomString(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  public convertCurrency(value: number, rate: number) {
    if (value && rate) {
      return (value * rate).toFixed(4);
    } else {
      return '';
    }
  }

  public setCookie(name: string, val: any) {
    const date = new Date();
    const value = val;
    // Set it expire in 7 days
    date.setTime(date.getTime() + (7 * 24 * 60 * 60 * 1000));
    // Set it
    document.cookie = name + '=' + value + '; expires=' + date.toUTCString() + '; path=/';
  }

  public getCookie(name: string) {
    const value = '; ' + document.cookie;
    const parts = value.split('; ' + name + '=');
    if (parts.length === 2) {
      return parts.pop().split(';').shift();
    }
  }

  public deleteCookie(name: string) {
    const date = new Date();
    // Set it expire in -1 days
    date.setTime(date.getTime() + (-1 * 24 * 60 * 60 * 1000));
    // Set it
    document.cookie = name + '=; expires=' + date.toUTCString() + '; path=/';
  }

  public async encryptData(msg) {
    var keySize = 256;
    var salt = CryptoJS.lib.WordArray.random(16);
    var key = CryptoJS.PBKDF2(environment.encryptionKey, salt, {
      keySize: keySize / 32,
      iterations: 100
    });
    var iv = CryptoJS.lib.WordArray.random(128 / 8);
    var encrypted = CryptoJS.AES.encrypt(msg, key, {
      iv: iv,
      padding: CryptoJS.pad.Pkcs7,
      mode: CryptoJS.mode.CBC
    });
    var result = CryptoJS.enc.Base64.stringify(salt.concat(iv).concat(encrypted.ciphertext));

    return result;
  }

  public async trimObject(obj): Promise<any> {
    if (obj) {
      Object.keys(obj).map(x => obj[x] = typeof obj[x] == 'string' ? obj[x].trim() : obj[x]);
    }
    return obj;
  }

  public setNumberWithCommas(x: any) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  public roundUp(x: any) {
    return Math.ceil(x);
  }

  async parseXmlToJson(xml) {
    return await xml2js
      .parseStringPromise(xml, { explicitArray: false })
      .then((response) => response.CatalogItems.CatalogItem);
  }

  public parseJsonToXml(string) {
    let json = JSON.stringify(string);

    return xml2js
      .parseStringPromise(json)
      .then((response) => response.CatalogItems.CatalogItem);

  }

  json2xml(data) {
    let count = 0;
    let i = [];

    if (data.length > 1000) {
      data.forEach((d, index) => {
        count++;
        i.push(d);
        if (count === 1000) {
          this.executeDownload(i);
          count = 0;
          i = [];
        }

        if (data.length == index + 1 && data.length % 1000 != 0) {
          this.executeDownload(i);
        }
        
      });
    } else {
      this.executeDownload(data);
    }
  }

  executeDownload(data) {
    let catalogItems = { 'CatalogItems': null };
    let items = [];
    data.forEach(d => {
      let catalogItem = { 'CatalogItem': '' };
      catalogItem.CatalogItem = d;
      items.push(catalogItem);
    });
    catalogItems.CatalogItems = items;

    var xml2js = require('xml2js');
    var builder = new xml2js.Builder();
    var xml = builder.buildObject(catalogItems);
    this.downloadFile('exportImportXML.xml', xml);
  }

  downloadFile(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  getMonthsData(yearData: any) {
    let monthValues = [];
    for (let i = 1; i <= 12; i++) {
      monthValues.push(yearData['l' + i]);
    }
    return monthValues;
  }

  mapEnumToKeyValue(enumObject: any): { id: any, name: string }[] {
    const keyValueArray: { id: any, name: string }[] = [];
  
    for (let key in enumObject) {
      if (typeof enumObject[key] === 'number') {
        keyValueArray.push({
          id: enumObject[key],
          name: this.translationService.instant(key)
        });
      }
    }
  
    return keyValueArray;
  }

  padNumber(number) {
    return number < 10 ? '0' + number : number.toString();
  }

  transformThemeText(input: string): string {
    switch (input) {
      case 'success':
        return 'green';
      case 'warning':
        return 'orange';
      case 'info':
        return 'azure';
      case 'primary':
        return 'purple';
      default:
        return input;
    }
  }

  isExcelValidFileType(file: File): boolean {
    const allowedExtensions = ['.xls', '.xlsx'];
    const fileExtension = file.name.split('.').pop()?.toLowerCase();
    return !allowedExtensions.includes(`.${fileExtension}`);
  }
}
