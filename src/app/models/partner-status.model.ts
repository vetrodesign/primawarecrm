export class PartnerStatus {
    id: number;
    code: string;
    name: string;
}