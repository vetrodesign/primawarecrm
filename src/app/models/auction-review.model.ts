import { BaseDomain } from "./base-domain.model";

export class AuctionReviews extends BaseDomain {
    public auctionId: number;
    public code: string;
    public name: string;
    public description: string;
}