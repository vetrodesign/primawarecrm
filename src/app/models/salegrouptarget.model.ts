import { BaseDomain } from "./base-domain.model";

export class SalesGroupTarget extends BaseDomain {
    code: string;
    name: string;
    description: string;
    isActive: boolean;
}