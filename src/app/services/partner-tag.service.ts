import { Injectable } from '@angular/core';
import { PartnerXTag } from 'app/models/partnerxtag.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})

export class PartnerTagService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<any[]> {
    const apiURL = `/partnerxtag/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getByPartnerIdAsync(partnerId: number): Promise<any[]> {
    const apiURL = `/partnerxtag/getByPartnerId`;
    return await this.apiService.PostAsync<any[]>(apiURL, partnerId, Constants.mrkApi);
  }

  async createMultipleAsync(i: PartnerXTag[]): Promise<any> {
    const apiURL = `/partnerxtag/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }

  async deleteMultipleAsync(i: PartnerXTag[]): Promise<any> {
    const apiURL = `/partnerxtag/deleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }
}
