import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ClientTurnover, DailyWhatsappOffersEmail, PartnerXItemComment } from 'app/models/clientturnover.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ClientTurnoverService {
  constructor(private apiService: ApiService) { }

  async getAll(): Promise<any> {
    const apiURL = `/ClientTurnover/getAll`;
    return await this.apiService.GetAsync<any>(apiURL,  Constants.mrkApi);
  }

  async getData(partnerId: number, postId: number, section: number): Promise<any> {
    let filter = {'partnerId': partnerId, 'postId': postId, 'section': section}
    const apiURL = `/Partner/GetPartnerTurnover`;
    return await this.apiService.PostAsync<any>(apiURL, filter, Constants.mrkApi);
  }

  async createPartnerXItemComment(p: PartnerXItemComment): Promise<any> {
    const apiURL = `/Partner/UpsertPartnerXItemComment`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async updatePartnerXItemComment(p: PartnerXItemComment): Promise<any> {
    const apiURL = `/Partner/UpsertPartnerXItemComment`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async sendOfferToPartnerContact(clientId, personContactIds, email: string, offerType: number, discountName: string, fileName: string, emailSubject: string, emailBody: string, postId: number, dedicatedOfferItemIds?: number[]): Promise<any> {
    let filter = {'partnerId': clientId, 
    'personContactIds': personContactIds, 
    'email': email, 
    'offerType': offerType, 
    'discountName': discountName, 
    'dedicatedOfferItemIds': dedicatedOfferItemIds,
    'fileName': fileName,
    'emailSubject': emailSubject,
    'emailBody': emailBody,
    'postId': postId}

    const apiURL = `/Partner/SendOfferToPartnerContact`;
    return await this.apiService.PostAsync<any>(apiURL, filter, Constants.mrkApi);
  }

  async generateOfferToPartnerContact(clientId, personContactId, email: string, offerType: number, discountName: string, offerName: string, fileName: string, emailSubject: string, emailBody: string, dedicatedOfferItemIds?: number[]) {
    const exportData = {'partnerId': clientId, 
      'personContactId': personContactId, 
      'email': email, 
      'offerType': offerType, 
      'discountName': discountName, 
      'dedicatedOfferItemIds': dedicatedOfferItemIds,
      'fileName': fileName,
      'emailSubject': emailSubject,
      'emailBody': emailBody}

    const apiURL = `/Partner/GenerateOfferToPartnerContact`;

    try {
      const result = await this.apiService.PostBlobAsync(apiURL, exportData, Constants.mrkApi);
      const blob = result as Blob;
      const url = window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = url;
      link.download = fileName + '.xlsx';
      link.click();
      window.URL.revokeObjectURL(url);
  
      return { success: true, message: 'Oferta generata cu succes.' };
    } catch (error) {
      const errorMessage = error?.message || 'A aparut o eroare la generarea ofertei.';
      return { success: false, message: errorMessage };
    }
  }

  async updatePartnerXItemComments(p: PartnerXItemComment[]): Promise<any> {
    const apiURL = `/Partner/UpsertPartnerXItemComments`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }

  async getDailyWhatsappOffers(clientId: number): Promise<any> {
    const apiURL = `/Partner/GetDailyWhatsappOffers`;
    return await this.apiService.PostAsync<any>(apiURL, clientId, Constants.mrkApi);
  }

  async sendDailyWhatsappOffers(request: DailyWhatsappOffersEmail): Promise<any> {
    const apiURL = `/Partner/SendDailyWhatsappOffers`;
    return await this.apiService.PostAsync<any>(apiURL, request, Constants.mrkApi);
  }
}
