import { TestBed } from '@angular/core/testing';

import { VipOfferItemsService } from './vip-offer-items.service';

describe('VipOfferItemsService', () => {
  let service: VipOfferItemsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VipOfferItemsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
