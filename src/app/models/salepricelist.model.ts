import { BaseDomain } from "./base-domain.model";
import { SalePriceListXProductConvention } from "./salepricelistxproductconvention.model";

export class SalePriceList extends BaseDomain {
    name: string;
    code: string;
    currencyId: number;
    type: number;

    availableFrom: Date;
    availableUntil: Date;
    referenceListId: number;

    salePriceListXProductConventionList: SalePriceListXProductConvention[];
    productConventionIds: any[];

    formula: number;
    status: boolean;
    isImplicit: boolean;
    isActive: boolean;
    syncERPExternalId: number;
}
