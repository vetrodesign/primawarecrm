import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerReviewReportComponent } from './partner-review-report.component';

describe('PartnerReviewReportComponent', () => {
  let component: PartnerReviewReportComponent;
  let fixture: ComponentFixture<PartnerReviewReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerReviewReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerReviewReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
