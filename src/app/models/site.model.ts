import { BaseDomain } from "./base-domain.model";

export class Site extends BaseDomain {
    name: string;
    code: string;
    countryId: number;
    countyId: number;
    cityId: number;
    street: string;
    streetNumber: string;
    block: string;
    level: string;
    apartment: string;
    zipCode: string;
    latitude: number;
    longitude: number;
    isHeadquarter: boolean;
    addressTypeId: number;
}