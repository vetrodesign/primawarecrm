import { Component, OnInit, ViewChild, AfterViewInit, Input, OnChanges } from '@angular/core';
import { DxDataGridComponent, DxValidatorComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { ImportComponent } from 'app/import/import.component';
import * as _ from 'lodash';
import { PageActionService } from 'app/services/page-action.service';
import { Constants } from 'app/constants';
import { ImportExportItemService } from 'app/services/import-export-item.service';
import { ImportExportItemData } from 'app/models/importexportitemdata.model';
import { HelperService } from 'app/services/helper.service';
import { ExportAuctionsTypeEnum } from 'app/enums/exportAuctionsTypeEnum';
import { SalePriceList } from 'app/models/salepricelist.model';
import { SalePriceListService } from 'app/services/sale-price-list.service';
import { ImportExportDataSalePriceListService } from 'app/services/import-export-data-sale-price-list.service';
import { ImportExportDataSalePriceList } from 'app/models/importexportdatasalepricelist.model';
import { ExportImportItemTypeEnum } from 'app/enums/exportImportItemTypeEnum';
import { MeasurementUnit } from 'app/models/measurementunit.model';
import { MeasurementUnitService } from 'app/services/measurement-unit.service';
import { CpvCodeService } from 'app/services/cpv-code.service';
import { ItemService } from 'app/services/item.service';
import { SalePriceListItemService } from 'app/services/sale-price-list-item.service';
import { ImportExportGroupItemDataService } from 'app/services/import-export-group-item-data.service';
import { ImportExportGroupItemData } from 'app/models/importexportgroupitemdata.model';

@Component({
  selector: 'app-import-export-item',
  templateUrl: './import-export-item.component.html',
  styleUrls: ['./import-export-item.component.css']
})
export class ImportExportItemComponent implements OnInit, AfterViewInit {
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('importData') importComponent: ImportComponent;

  @ViewChild('validationEditGroup') validationEditGroup: DxValidatorComponent;

  loaded: boolean;
  displayData: boolean;
  showImportBtn: boolean;

  importExportGroupItemData: any[] = [];
  importExportItems: any[] = [];
  copyImportExportItems: any[] = [];
  nonGrupedImportExportItems: any;

  tabs: any[];
  isTabActive: string;
  actions: IPageActions;
  selectedRows: any[];
  selectedRowIndex = -1;
  importedItems: any[];
  salePriceLists: SalePriceList[];
  measurementUnits: MeasurementUnit[] = [];
  cpvCodes: any;

  importExportItemsIds: any;

  selectedRefItem: any;
  selectedItem: any;

  groupedItemsIds: any;
  initialGroupedItemsIds: any;

  exportType: { id: number; name: string }[] = [];
  exportImportItemType: { id: number; name: string }[] = [];

  selectedExportType: number;

  salePriceListIds: number[];
  salePriceListItems: any[] = [];
  initialSalePriceListIds: any;

  isMultipleCreate: boolean;
  multipleItemsIds: any;
  
  editMultipleItemsPopupVisible: boolean;
  multipleItemUpdate: ImportExportItemData;

  createDividedItemPopupVisible: boolean;
  createItemPopupVisible: boolean;

  hideWithoutDivided: boolean;

  items: any;
  rowIndex: any;
  mode: boolean;
  reqKeys: any;
  groupedText: string;

  divideFactor: number;
  divideUM: string;
  constructor(
    private translationService: TranslateService,
    private pageActionService: PageActionService,
    private measurementUnitService: MeasurementUnitService,
    private itemsService: ItemService,
    private cpvCodeService: CpvCodeService,
    private notificationService: NotificationService,
    private salePriceListService: SalePriceListService,
    private salePriceListItemService: SalePriceListItemService,
    private importExportDataSalePriceListService: ImportExportDataSalePriceListService,
    private importExportGroupItemDataService: ImportExportGroupItemDataService,
    private helperService: HelperService,
    private authService: AuthService,
    private importExportItemService: ImportExportItemService) {
    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();
    this.getImportExportGroupItemData();
    this.getImportExportItems();
    this.getData();

    this.itemExpr = this.itemExpr.bind(this);

    this.isTabActive = this.authService.getCustomerPageTitleTheme();
    this.selectedExportType = 1;

    for (let n in ExportAuctionsTypeEnum) {
      if (typeof ExportAuctionsTypeEnum[n] === 'number') {
        this.exportType.push({
          id: <any>ExportAuctionsTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }

    for (let n in ExportImportItemTypeEnum) {
      if (typeof ExportImportItemTypeEnum[n] === 'number') {
        this.exportImportItemType.push({
          id: <any>ExportImportItemTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
  }
   
  ngOnInit(): void {
    this.tabs = [{ 'id': 1, 'text': 'General', 'isActive': true },
    { 'id': 2, 'text': 'Configurare Liste de pret', 'isActive': false },
    { 'id': 3, 'text': 'Import', 'isActive': false },
    ];
  }

  async getData() {
    Promise.all([this.getMUs(), this.getCPVs(), this.getItems(), this.getSalePriceLists(),
      this.getImportExportDataSalePriceLists()]).then(x => {
        this.setItemsPrices(); this.setStockForGrouped();
    });
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && ((e.data.itemId == null && e.data.itemType != 3) || e.data.itemMeasurmentUnit == null || e.data.cpvId == null || e.data.price == null)) {
         e.rowElement.style.backgroundColor = '#ffaf82';
     } 
  }

  async getMUs(): Promise<any> {
    await this.measurementUnitService.getAllMUAsync().then(items => {
      this.measurementUnits = (items && items.length > 0) ? items : [];
    });
  }
  async getCPVs(): Promise<any> {
    await this.cpvCodeService.getAllCpvCodesAsync().then(items => {
      this.cpvCodes = {
        paginate: true,
        pageSize: 15,
        store: items
      };
    });
  }
  async getItems(): Promise<any> {
    await this.itemsService.getItemssAsyncByID().then(items => {
      this.items = {
        paginate: true,
        pageSize: 15,
        store: items
      };
    });
  }

  toggleMode(tab: any) {
    this.tabs.find(x => x.isActive).isActive = false;
    tab.isActive = true;

    if (tab.id == 1) {
      setTimeout(() => {
        this.getImportExportGroupItemData();
        this.getImportExportItems().then(r => {this.setItemsPrices(); this.setStockForGrouped() });
      }, 100);
    }

    if (tab.id == 2) {
    }

    if (tab.id == 3) {
    }
  }

  setStockForGrouped() {
    if(this.importExportItems && this.importExportItems.length > 0 && this.importExportGroupItemData && this.importExportGroupItemData.length > 0) {
      this.importExportItems?.filter(x => x.itemType == 3)?.forEach(x => {
      
        let childrens = this.importExportGroupItemData?.filter(y => y.itemDataId == x.id).map(z => z.itemDataChildId);
        let childrenItems = this.importExportItems?.filter(h => childrens.includes(h.id));
  
        if (childrenItems && childrenItems.length > 0) {
          if (childrenItems.filter(q => q.inStock).length > 0) {
            x.inStock = true;
          } else {
            x.inStock = false;
          }
        }
      });
    }
  }

  async onRowUpdated(e) {
    this.loaded = true;
    let item = new ImportExportItemData();
    item = e.data;

    if (item.isExclusive == true && item.itemType != 2) {
      this.notificationService.alert('top', 'center', 'Oferta generala - Datele nu au fost modificate! Check-ul de exclusiv se pune DOAR pentru articolele de tip divizat, pentru a specifica ca referinta acestui divizat nu se mai exporta!',
      NotificationTypeEnum.Red, true)
      this.refreshDataGrid();
      this.loaded = false;
      return;
    }

    if (item) {
      await this.importExportItemService.updateAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Oferta generala - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Oferta generala - Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        };
        this.refreshDataGrid();
      });
    }
  }

  getDataWithoutPrice() {
    return this.importExportItems.filter(x => (x.itemId == null && x.itemType != 3) || x.itemMeasurmentUnit == null || x.cpvId == null || x.price == null).length;
  }

  async setItemsPrices() {
    if (this.salePriceListIds && this.salePriceListIds.length > 0) {
      this.loaded = true;
      let lists = this.salePriceLists.filter(x => this.salePriceListIds.includes(x.id));
      this.salePriceListItems = [];
      const promises = [];
      lists.forEach(list => {
        if (list.type == 1) {
          promises.push(this.salePriceListItemService.getSalePriceListItemsAsyncByID(list.id).then(listItems => {
            if (listItems) {
              this.salePriceListItems.push(listItems.filter(z => z.isActive == 1).map(x => ({ 'itemId': x.itemId, 'listPrice': x.listPrice, 'isActive': x.isActive })));
            }
          }));
        } else {
          if (list.type == 2 && list.referenceListId) {
            promises.push(this.salePriceListItemService.getSalePriceListItemsAsyncByID(list.referenceListId).then(listItems => {
              if (listItems) {
                let i = listItems.filter(z => z.isActive == 1).map(x => ({ 'itemId': x.itemId, 'listPrice': x.listPrice * list.formula, 'isActive': x.isActive }));
                this.salePriceListItems.push(i);
              }
            }));
          }
        }
      });
      Promise.all(promises).then(values => {  
         let prices = this.salePriceListItems.reduce((x, y) => x.concat(y));
         this.importExportItems.forEach(i => {
          let pr = prices.filter(x => x.itemId === i.itemId && x.isActive == 1);        
           if (i.itemId && pr.length > 0) {
            if (i.itemType == 1) {
              i.price = (Number(Math.min.apply(null,pr.map(i => i.listPrice))).toFixed(2)).toString();
            }
            if (i.itemType == 2) {
              i.price = (Number(Math.min.apply(null,pr.map(i => i.listPrice)) / i.itemPriceDividingFactor).toFixed(2)).toString();
            }
           } else {
            i.price = null;
           }
         });

         this.importExportItems.filter(x => x.itemType == 3).forEach(i => {   
          let groupedItems = this.importExportGroupItemData.filter(x => x.itemDataId == i.id).map(y => y.itemDataChildId);
          i.price = (groupedItems && groupedItems.length > 0) ? this.nonGrupedImportExportItems.store.find(x => x.id == groupedItems[0]).price : null;
         });
         this.dataGrid.instance.refresh();
         this.loaded = false;
      });
    }
  }

  public displayDataEvent(e) {
    this.displayData = e;
  }

   ngAfterViewInit() {
     this.setGridInstance();
   }

  setGridInstance() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  async getImportExportItems() {
    this.loaded = true;
    await this.importExportItemService.getAll().then(items => {
      this.importExportItems = (items && items.length > 0) ? items : [];
      this.copyImportExportItems = (items && items.length > 0) ? items : [];

      if (this.importExportItems && this.importExportItems.length > 0) {
        this.importExportItemsIds = this.importExportItems.filter(y => y.isActive == true).map(x => x.itemId);
      } 

      this.nonGrupedImportExportItems = {
        paginate: true,
        pageSize: 15,
        store: (items && items.length > 0) ? items.filter(x => x.itemType != 3) : []
      };
      this.setGridInstance();
    });
  }

  async getImportExportGroupItemData() {
    await this.importExportGroupItemDataService.getAll().then(items => {
      this.importExportGroupItemData = (items && items.length > 0) ? items : [];
    });
  }

  async getSalePriceLists() {
    await this.salePriceListService.getSalePriceListsAsyncByID().then(items => {
      this.salePriceLists = (items && items.length > 0) ? items.filter(x => x.currencyId == 1 && x.isWithTax == false) : [];
    });
  }

  async getImportExportDataSalePriceLists() {
    await this.importExportDataSalePriceListService.getAll().then(items => {
      this.salePriceListIds = (items && items.length > 0) ? items.map(x => x.salePriceListId) : [];
      this.initialSalePriceListIds = (items && items.length > 0) ? items : [];
    });
  }

  setActions() {
    this.actions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
    this.pageActionService.getRoleActionsByPagePath(Constants.importExportItemData).then(result => {
      this.actions = result;
    });
  }

    canUpdate() {
    return this.actions.CanUpdate;
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
}

  public refreshDataGrid() {
    this.getImportExportGroupItemData();
    this.getImportExportItems().then(r => {this.setItemsPrices(); this.setStockForGrouped(); this.hideWithoutDividedEvent(); });
    if (this.dataGrid) {
      this.dataGrid.instance.refresh();
    }
  }

  public deleteRecords(data: any) {
      this.importExportItemService.deleteAsync(this.selectedRows.map(x => x.id)).then(response => {
        if (response) {
          this.notificationService.alert('top', 'center', 'Datele au fost sterse cu succes!', NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost sterse!', NotificationTypeEnum.Red, true)
        }
        this.refreshDataGrid();
      });
  }

  public async openDetails(data: any) {
    if (data) {
      this.createItemPopupVisible = true;
      this.isMultipleCreate = false;
      this.selectedItem = data;

      if (data.itemType == 3) {
        this.loaded = true;
        this.importExportGroupItemDataService.getByItemDataIdAsync(data.id).then(items => {
          if (items && items.length > 0) {

            this.groupedItemsIds = items.map(x => x.itemDataChildId);
            this.initialGroupedItemsIds = items;

            this.selectedItem.measurmentUnit = this.nonGrupedImportExportItems.store.find(x => x.id === this.groupedItemsIds[0]).measurmentUnit;
          } else {
            this.groupedItemsIds = [];
          }
          this.loaded = false;
        })
      }
    } else {
      this.createItemPopupVisible = true; 
       this.isMultipleCreate = false;
      this.selectedItem = new ImportExportItemData();
      this.selectedItem.itemType = 1;
      this.selectedItem.isActive = true;
      this.selectedItem.inStock = true;

      this.selectedItem.site = 'www.vetro.ro';
      this.selectedItem.termsOfDelivery = 'Preturile: sunt in LEI fara TVA. Preturile se pot schimba fara notificare prealabila, va rugam sa solicitati oferta inainte de comanda. Transportul este GRATUIT! Pentru comenzile peste 200.00 lei fara tva. Pentru localitatile din afara zonei de livrare Nemo Curier, costul suplimentar al transportului se adauga pe factura, 0.80lei/Km.Pentru comenzile sub 200.00 lei se adauga 30.00 Lei cheltuieli de transport.Termenul de Livrare este de maxim 48 de ore.';
      
    }

  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(e: any) {
    if (e.parentType == 'filterRow' && e.editorName == 'dxSelectBox')
      e.editorName = "dxTextBox";
  }



  public async update(): Promise<void> {
    if (this.validationEditGroup.instance.validate().isValid) { 
      let item = new ImportExportItemData();
      item = this.selectedItem;
      item.price = item.price ? item.price : 0;

      if (item.itemId) {
        let i = this.items.store.find(x => x.id === item.itemId);
        item.itemMeasurmentUnit = i.measurementUnitId;
      }

      if (item.itemType == 3 && this.groupedItemsIds.length > 0 && this.groupedItemsIds.length != this.initialGroupedItemsIds.length) {
         // Delete / Create ImportExportGroupItemData

         let i = this.nonGrupedImportExportItems.store.filter(x => this.groupedItemsIds.includes(x.id));
         if (i.some(x => i[0].price != x.price) || i.some(x => i[0].itemType != x.itemType) || i.some(x => i[0].measurmentUnit != x.measurmentUnit)) {
           this.notificationService.alert('top', 'center', 'Datele nu au fost inserate! Articolele selectate trebuie sa fie de tip identic, sa contina pret identic si UM(XML) identic!',
             NotificationTypeEnum.Red, true);
         } else {
          const existingImportExportGroupItemDataIds = this.initialGroupedItemsIds;
          let deleteItems: ImportExportGroupItemData[] = [];
          existingImportExportGroupItemDataIds.filter(x => !this.groupedItemsIds.includes(x.itemDataChildId)).forEach(item => {
            const c = new ImportExportGroupItemData();
            c.itemDataChildId = item.itemDataChildId;
            c.itemDataId = item.itemDataId;
            c.id = item.id;
            deleteItems.push(c);
          });
          if (deleteItems.length > 0) {
            this.importExportGroupItemDataService.deleteMultipleAsync(deleteItems);
          }
 
          let createItems: ImportExportGroupItemData[] = [];
          this.groupedItemsIds.filter(x => !existingImportExportGroupItemDataIds.map(y => y.itemDataChildId).includes(x)).forEach(id => {
            const c = new ImportExportGroupItemData();
            c.itemDataId = Number(item.id);
            c.itemDataChildId = id;
            createItems.push(c);
          });
          if (createItems && createItems.length > 0) {
           this.importExportGroupItemDataService.createMultipleAsync(createItems);
         }
         }
      }
  
      if (item) {
        this.loaded = true;
        await this.importExportItemService.updateAsync(item).then(r => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Datele au fost modificate cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Datele nu au fost modificate! A aparut o eroare!',
              NotificationTypeEnum.Red, true)
          }
          this.createItemPopupVisible = false;
          this.loaded = false;
          this.refreshDataGrid();
        });
    }
    }
  }

  public async save() {
    if (this.validationEditGroup.instance.validate().isValid) {
      //validation - just divided or just reference, same price, same UM(XML)
      if (this.selectedItem.itemType == 3) {
        let i = this.nonGrupedImportExportItems.store.filter(x => this.groupedItemsIds.includes(x.id));
        if (i.some(x => i[0].price != x.price) || i.some(x => i[0].itemType != x.itemType) || i.some(x => i[0].measurmentUnit != x.measurmentUnit)) {
          this.notificationService.alert('top', 'center', 'Datele nu au fost inserate! Articolele selectate trebuie sa fie de tip identic, sa contina pret identic si UM(XML) identic!',
            NotificationTypeEnum.Red, true);
          return;
        }
        if (this.groupedItemsIds.length < 2) {
          this.notificationService.alert('top', 'center', 'Datele nu au fost inserate! In grup trebuie sa fie introduse cel putin 2 inregistrari!',
          NotificationTypeEnum.Red, true);
        return;
        }
      }
      
      let items = [];
      if (this.selectedItem.itemType == 3) {
        this.selectedItem.measurmentUnit = this.nonGrupedImportExportItems.store.find(x => x.id === this.groupedItemsIds[0]).measurmentUnit;
        this.selectedItem.itemMeasurmentUnit = this.nonGrupedImportExportItems.store.find(x => x.id === this.groupedItemsIds[0]).itemMeasurmentUnit;
      }

      if (this.isMultipleCreate && this.multipleItemsIds.length > 0) {
        this.multipleItemsIds.forEach(i => {
          let item = new ImportExportItemData();
          item = _.clone(this.selectedItem);
          item.itemId = i;
          
          let j = this.items.store.find(x => x.id === i);
          item.referenceNumber = j? j.code : null;
          items.push(item);
        });
      } else {
        items.push(this.selectedItem);
      }

      this.loaded = true;
      await this.importExportItemService.createAsync(items).then(r => {
        if (r && r.length > 0) {
          this.notificationService.alert('top', 'center', 'Datele au fost inserate cu succes!', NotificationTypeEnum.Green, true);
          //If is grouped type
          if (this.selectedItem.itemType == 3 && this.groupedItemsIds != null) {
            //create
            let createItemIds: ImportExportGroupItemData[] = [];
            this.groupedItemsIds?.forEach(id => {
              const c = new ImportExportGroupItemData();
              c.itemDataId = r[0];
              c.itemDataChildId = id;
              createItemIds.push(c);
            });
            if (createItemIds && createItemIds.length > 0) {
              this.importExportGroupItemDataService.createMultipleAsync(createItemIds);
            }
          }
          this.refreshDataGrid();
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true);
        }
        this.loaded = false;
        this.createItemPopupVisible = false;
      });
    }
  }

  openFile(event) {
    let input = event.target;
    this.showImportBtn = input.files.length > 0;

    for (var index = 0; index < input.files.length; index++) {
        let reader = new FileReader();
        reader.onload = () => {
            // this 'text' is the content of the file
            var text = reader.result;
            this.importedItems = [];
            this.helperService.parseXmlToJson(text).then(result => {
              if (result && result.length != undefined)
              {
                this.importedItems = result;
              } else {
                this.importedItems.push(result);
              }
            })
        }
        reader.readAsText(input.files[index]);
    };
   }

  saveSalePriceLists() {
    if (this.salePriceListIds.length > 0) {
    // Create ImportExportDataSalePriceList
    this.loaded = true;
    const existingIds = this.initialSalePriceListIds;
    let deleteData: ImportExportDataSalePriceList[] = [];
    existingIds.filter(x => !this.salePriceListIds.includes(x)).forEach(i => {
      const c = new ImportExportDataSalePriceList();
      c.salePriceListId = i.salePriceListId;
      c.id = i.id;
      deleteData.push(c);
    });
    if (deleteData.length > 0) {
      this.importExportDataSalePriceListService.deleteAsync(deleteData.map(x => x.id));
    }
    let createIds: ImportExportDataSalePriceList[] = [];
    this.salePriceListIds.filter(x => !existingIds.includes(x)).forEach(id => {
      const c = new ImportExportDataSalePriceList();
      c.salePriceListId = id;
      createIds.push(c);
    });
    if (createIds.length > 0) {
      this.importExportDataSalePriceListService.createAsync(createIds).then(x => {
        this.getImportExportDataSalePriceLists();
        this.loaded = false;
      });
    }
  }
  }

  import() {
    let items = [];
    this.showImportBtn = false;
    this.importedItems.forEach((i) => {
        let item = new ImportExportItemData();
        item.itemType = 1;

        if (i.CatalogItemCode && i.CatalogItemCode.slice(-1) == '-') {
          item.referenceNumber = i.CatalogItemCode.substring(0, i.CatalogItemCode.length - 1);
        } else {
          item.referenceNumber = i.CatalogItemCode;
        }

        item.gtinCode = null;
        item.name = i.CatalogItemName;
        item.description = i.CatalogItemDescription;
        item.termsOfDelivery = i.CatalogItemDeliveryCondition;
        item.paymentTerms = i.CatalogItemPaymentCondition;
        item.price = i.CatalogItemPrice ? i.CatalogItemPrice : 0;
        item.measurmentUnit = i.MeasureUnit;
        item.inStock = i.InStock == 'true';
        item.isActive = true;
        item.site = i.URLReference;

        item.cpv = i.CpvCode;
        if (i.CpvCode) {
          item.cpvId = this.cpvCodes.store.find(x => x.code == i.CpvCode.substring(0, 10))?.id;
        }

        items.push(item);
    })
    
    if (items && items.length > 0) {
      if (this.importExportItems && this.importExportItems.length > 0) {
        this.importExportItemService.deleteAsync(this.importExportItems.map(x => x.id)).then(response => {
          if (response) {
            this.createRequest(items);
          }
        });
      } else {
        this.createRequest(items);
      }
    }
  }

  createRequest(list: any) {
    let b = [];
    let s = [];

    list.forEach((e, index) => {
      if (s.length < 800) {
        s.push(e);
        if (index === list.length - 1) {
          b.push(s);
        }
      } else {
        b.push(s);
        s = [];   
        s.push(e);
      }
    });

    b.forEach(many => {
      this.loaded = true;
      this.importExportItemService.createAsync(many).then(r => {
       if (r > 0) {
         this.notificationService.alert('top', 'center', `Datele au fost inserate cu succes!`, NotificationTypeEnum.Green, true);
         this.loaded = false;
       } else {
         this.notificationService.alert('top', 'center', 'Datele nu au fost inserate! A aparut o eroare sau codul de articol deja exista salvat!',
           NotificationTypeEnum.Yellow, true);
         this.loaded = false;
       }
     });
    });
  }

  hideWithoutDividedEvent() {
    if (this.hideWithoutDivided == true) {
      this.importExportItems = this.importExportItems.filter( x => x.itemType == 1 && this.importExportItems.find(y => y.itemParentId === x.id) == null);
    } else {
      if (this.hideWithoutDivided == false) {
        this.importExportItems = this.copyImportExportItems;
      }
    }
  }

  export() {
    let items = [];
    let data = [];
    if (this.selectedExportType === 2) {
      if (!this.selectedRows) {
        this.notificationService.alert('top', 'center', 'Nu au fost selectate inregistrari!',
        NotificationTypeEnum.Red, true);
        return;
      } else {
        if (this.selectedRows && this.selectedRows.length > 1000) {
          this.notificationService.alert('top', 'center', 'Obligatoriu sub 1000 de articole selectate!',
          NotificationTypeEnum.Red, true);
          return;
        }
        data = this.selectedRows;
      }
    } else {
      data = this.importExportItems;
    }



    data.forEach((i) => {
      if (i && i.isActive && i.price) {

        //Don't export reference type rows that have divided rows with isExclusive check true
        if (i.itemType == 1 || i.itemType == 2) {
          let z = this.importExportItems.find(x => x.itemParentId === i.id && x.itemType == 2 && x.isExclusive == true);
          let j = this.importExportGroupItemData.filter(x => x.itemDataChildId == i.id);
          if (z != undefined || (j && j.length > 0)) {
            return;
          }
        }

        let item = {'CatalogItemCode':'','CatalogItemName':'','CatalogItemDescription':'','CatalogItemDeliveryCondition':'',
        'CatalogItemPaymentCondition':'','CatalogItemPrice':'','MeasureUnit':'','InStock':'','URLReference':'','CpvCode':''};

        let it = i.itemId ? this.items.store.find(x => x.id == i.itemId) : null;
        //Ref type
        if (i.itemType == 1) {
          let ref = (i.referenceNumber && i.referenceNumber.length == 4) ? i.referenceNumber + '-' : i.referenceNumber;
          if (it && it.code && it.code.length == 4) {
            item.CatalogItemCode = it.code + '-';
          }
          if (it && it.code && it.code.length > 4) {
            item.CatalogItemCode = it.code;
          } else {
            item.CatalogItemCode = ref;
          }
        }
        //Divided Type
        if (i.itemType == 2) {
          let ref = (i.referenceNumber && i.referenceNumber.length == 4) ? i.referenceNumber + '-' : i.referenceNumber;
          if (ref && ref.length > 4) {
            item.CatalogItemCode = ref;
          }
        }
         //Grouped Type
         if (i.itemType == 3) {
          let ref = (i.referenceNumber && i.referenceNumber.length == 4) ? i.referenceNumber + '-' : i.referenceNumber;
          if (ref && ref.length > 4) {
            item.CatalogItemCode = ref;
          }
        }
      
        //item.gtinCode = null;
        item.CatalogItemName = i.itemKeywords ? (i.name + ' ' + i.itemKeywords) : (i.name);
        item.CatalogItemName = item.CatalogItemName.replace("PRIMA", "");
        item.CatalogItemName = item.CatalogItemName.replace("....", "");
        item.CatalogItemName = item.CatalogItemName.replace("...", "");
        item.CatalogItemName = item.CatalogItemName.replace("..", "");

        item.CatalogItemDescription = i.description;
        item.CatalogItemDeliveryCondition = i.termsOfDelivery;
        item.CatalogItemPaymentCondition = i.paymentTerms ? i.paymentTerms : ' ';
        item.CatalogItemPrice = i.price;
        item.MeasureUnit = i.measurmentUnit ? i.measurmentUnit : (it ? this.measurementUnits.find(x => x.id == it.measurementUnitId)?.name : null);
        item.InStock = i.inStock ? 'true' : 'false';
        item.URLReference = i.site ? i.site : ' ';
        item.CpvCode = i.cpvId ? (this.cpvCodes.store.find(x => x.id == i.cpvId)?.code) : (i.cpv);
        items.push(item);
      }
    })
    
    if (items && items.length > 0) {
      this.notificationService.alert('top', 'center', `Se exporta ` + items.length + ' linii!', NotificationTypeEnum.Green, true);
      this.helperService.json2xml(items);
    }
  }

  salePriceListExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  measurementUnitExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  itemExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  cpvExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  itemChangeEvent(i) {
    if (i && i.event && i.value) {
      let item = this.items.store.find(x => x.id === i.value);
      this.selectedItem.itemMeasurmentUnit = item? item.measurementUnitId : null;
      this.selectedItem.referenceNumber = item? item.code : null;
      this.selectedItem.name = item? item.nameRO : null;
    }
  }

  itemCPVChangeEvent(i) {
    if (i && i.event && i.value) {
      let cpv = this.cpvCodes.store.find(x => x.id === i.value);
      this.selectedItem.cpv = cpv? cpv.code : null;
    }
  }
  

  isValidForDiveded(data) {
    if (data.itemType == 2 || data.itemType == 3 || (data.itemType == 1 && this.importExportItems.filter(x => x.itemParentId === data.id).length > 0)) {
      return false;
    } else return true;
   }

   checkCodeExpr(data) {
    if (data.itemId && data.referenceNumber && this.items && this.items.store.length > 0) {
      var itemCode = this.items.store.find(x => x.id == data.itemId)?.code;
      if (itemCode && itemCode.length === 4 && data.referenceNumber.length == 4) {
        return 'Add - la final'
      }
      if (itemCode && itemCode.length > 4 && data.referenceNumber.length == 4) {
        return 'Adauga tot codul'
      }
      if (itemCode && data.itemType == 2 && itemCode.length == 4 && data.referenceNumber.length == 4 ) {
        return 'Posibila Inconsistenta Cod'
      }
      if (data.referenceNumber.length < 4) {
        return 'Posibila Inconsistenta Cod'
      }
    } else {
      return null;
    }
   }

   isValidForGrouped(data) {
    return data.itemType != 3;
   }

   cancelPopup() {
    this.createItemPopupVisible = false;
   }

  public editMultiple() {
    this.multipleItemUpdate = new ImportExportItemData();
    this.editMultipleItemsPopupVisible = true;
  }

  public createDivided(data) {
    if (data && data.itemType == 1) {
      this.createDividedItemPopupVisible = true;
      this.selectedRefItem = data;
      this.selectedRefItem.isActive = true;
      this.selectedRefItem.inStock = true;

      this.divideFactor = 0;
      this.divideUM = null;
    }
  }

  public createGrouped(data) {
    if (data && data.itemType != 3) {
      this.createItemPopupVisible = true;
      this.isMultipleCreate = false;
      this.selectedItem = new ImportExportItemData();
      this.groupedItemsIds = [];
      this.groupedItemsIds.push(data.id);

      this.selectedItem.itemType = 3;
      this.selectedItem.isActive = true;
      this.selectedItem.inStock = true;

      this.selectedItem.referenceNumber = data.referenceNumber + '-GROUPED';
      this.selectedItem.name = data.name;
      this.selectedItem.description = data.description;

      this.selectedItem.termsOfDelivery = data.termsOfDelivery;
      this.selectedItem.paymentTerms = data.paymentTerms;

      this.selectedItem.cpv = data.cpv;
      this.selectedItem.cpvId = data.cpvId;

      this.selectedItem.itemMeasurmentUnit = data.itemMeasurmentUnit;
    }
  }

  async createDividedItem() {
    if (this.divideFactor > 0 && this.divideUM && this.selectedRefItem && this.selectedRefItem.id) {
      let item = new ImportExportItemData();
      item = Object.assign({}, this.selectedRefItem);
      item.id = 0;
      item.itemType = 2;
      item.itemPriceDividingFactor = this.divideFactor;
      item.measurmentUnit = this.divideUM;
      item.itemParentId = this.selectedRefItem.id;
      item.isActive = true;
      item.inStock = true;
      item.referenceNumber = this.selectedRefItem.referenceNumber + '_dvz' + this.divideFactor;
      let items = [];
      items.push(item);

      this.loaded = true;
      await this.importExportItemService.createAsync(items).then(r => {
        if (r > 0) {
          this.notificationService.alert('top', 'center', 'Datele au fost inserate cu succes!', NotificationTypeEnum.Green, true);
          this.refreshDataGrid();
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true);
        }
        this.loaded = false;
        this.createDividedItemPopupVisible = false;
      });
    }
  }

  public async updateMultipleItems() {
    if(Object.keys(this.multipleItemUpdate).length > 0) {
      this.editMultipleItemsPopupVisible = false;
      await this.importExportItemService.updateMultipleAsync(this.selectedRows.map(x => x.id), this.multipleItemUpdate).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Import Export - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
            this.editMultipleItemsPopupVisible = false;  
        } else {
          this.notificationService.alert('top', 'center', 'Import Export - Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        };
        this.refreshDataGrid();
      });
    }
  }

  itemTemplate = (item: any) => {
    if (!item) {
        return '';
    }

    if (this.importExportItemsIds && this.importExportItemsIds.length > 0 && this.importExportItemsIds.includes(item.id)) {
        return `<div style="background-color: #c4ffdb">
        ${this.itemExpr(item)}
        </div>`;
    } else {
      return this.itemExpr(item);
    }
 }
}

