import { BaseDomain } from "./base-domain.model";

export class PartnerWebsite extends BaseDomain {
    partnerId: number;
    website: string;
    requestsData: number | undefined;
    approved: boolean;
    observations: string;
    connections: string;
    monitoring: boolean;
    partnerType?: number;

    isActive: boolean;
}