import { Injectable } from '@angular/core';
import { CaenCodeDomain } from 'app/models/caencodedomain.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';


@Injectable({
  providedIn: 'root'
})
export class CaenCodeDomainService {
  constructor(private apiService: ApiService) { }

  async getAllCaenCodeDomainsAsync(): Promise<CaenCodeDomain[]> {
    const apiURL = `/CaenCodeDomain/getall`;
    return await this.apiService.GetAsync<CaenCodeDomain[]>(apiURL, Constants.commonApi);
  }

  async createCaenCodeDomainsAsync(caenCodeDomain: CaenCodeDomain): Promise<any> {
    const apiURL = `/CaenCodeDomain/create`;
    return await this.apiService.PostAsync<any>(apiURL, caenCodeDomain, Constants.commonApi);
  }

  async updateCaenCodeDomainsAsync(caenCodeDomain: CaenCodeDomain): Promise<any> {
    const apiURL = `/CaenCodeDomain/update`;
    return await this.apiService.PutAsync<any>(apiURL, caenCodeDomain, Constants.commonApi);
  }

  async deleteCaenCodeDomainsAsync(ids: number[]): Promise<any> {
    const apiURL = `/CaenCodeDomain/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.commonApi);
  }
}
