import { BaseDomain } from "./base-domain.model";

export class SendOfferToPartnerLog extends BaseDomain {
    public customerId: number;
    public partnerId: number;
    public date: Date;
    public offerType: number;
}