import { BaseDomain } from "./base-domain.model";

export class ContactCampaignLocationGeoAreGeoEntities extends BaseDomain {
    contactCampaignLocationXGeoAreaId: number;
    continentId: number;
    countryId: number;
    countyId: number;
    type: number;
    startDate: Date;
    endDate: Date;
}
