import { BaseDomain } from "./base-domain.model";

export class PartnerDailyReceipts extends BaseDomain {
    partnerName: string;
    sum: string;
    currency: string;
    transactionDetails: string;
    processingDate: string;
    transactionType: string;
    partnerBankAccount: string;
    fileLastWriteTime: Date;
}
