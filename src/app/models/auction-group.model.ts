import { BaseDomain } from "./base-domain.model";

export class AuctionGroup extends BaseDomain{
    public code: string;
    public name: string;
    public description: string;

    public existingCpvCodeIds: any[];
    public cpvCodeIds: any;
}