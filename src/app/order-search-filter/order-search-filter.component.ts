import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { OrderSearchFilterVM } from 'app/models/orderSearchFilter.model';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { DxValidatorComponent } from 'devextreme-angular';

@Component({
  selector: 'app-order-search-filter',
  templateUrl: './order-search-filter.component.html',
  styleUrls: ['./order-search-filter.component.css']
})
export class OrderSearchFilterComponent implements OnInit, OnChanges {
  @Input() salesPosts: any[];
  @Input() orderTypes: any[];
  @Input() orderStatusTypes: any[];
  @Input() items: any[];
  @Input() sites: any[];
  @Input() userPostId: number;
  @Input() isUserAdmin: number;
  @Output() searchEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() refreshEvent: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('validationSearchGroup') validationSearchGroup: DxValidatorComponent;
  orderSearchFilter: OrderSearchFilterVM;
  isOwner: boolean;
  loaded: boolean;

  constructor(private translationService: TranslateService,
    private authService: AuthService) {
    this.authService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
  
    this.orderSearchFilter = new OrderSearchFilterVM();
    this.orderSearchFilter.isLegal = true;
    this.orderSearchFilter.isIndividual = true;
    this.orderSearchFilter.isActive = true;
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.userPostId) {
      if (this.userPostId != null) {
        this.orderSearchFilter.salesAgentId = this.userPostId;
      }
    }
  }

  searchOrders() {
    if (this.validationSearchGroup.instance.validate().isValid) {
      this.searchEvent.emit(this.orderSearchFilter);
    }
  }

  partnerChange(e) {
    if (e) {
      this.orderSearchFilter.partnerIds = [];
      this.orderSearchFilter.partnerIds.push(e);
    }
  }

  resetFilters() {
    this.orderSearchFilter = new OrderSearchFilterVM();
    this.refreshEvent.emit(true);
  }

  getDisplayExprItems(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  onMultiTagItemsChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  onMultiTagPartnersChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

}