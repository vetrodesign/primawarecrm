import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { TurnoverPreorderPriceSourceEnum } from 'app/enums/turnoverPreorderPriceSourceEnum';
import { Management } from 'app/models/management.model';
import { PartnerLocationContact } from 'app/models/partnerlocationcontact.model';
import { Site } from 'app/models/site.model';
import { TurnoverPreorder } from 'app/models/turnover-preorder.model';
import { AuthService } from 'app/services/auth.service';
import { ClientTurnoverService } from 'app/services/client-turnover.service';
import { HelperService } from 'app/services/helper.service';
import { ItemService } from 'app/services/item.service';
import { NotificationService } from 'app/services/notification.service';
import { PartnerLocationContactService } from 'app/services/partner-location-contact.service';
import { PartnerService } from 'app/services/partner.service';
import { SendOfferToPartnerLogService } from 'app/services/send-offer-to-partner-log.service';
import { TurnoverPreorderService } from 'app/services/turnover-preorder.service';
import { quantity } from 'chartist';
import { DxDataGridComponent, DxFileUploaderComponent } from 'devextreme-angular';
import { environment } from 'environments/environment';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as xlsx from 'xlsx';

@Component({
  selector: 'app-partner-shopping-cart',
  templateUrl: './partner-shopping-cart.component.html',
  styleUrls: ['./partner-shopping-cart.component.css']
})
export class PartnerShoppingCartComponnet implements OnInit {
  @Input() isShoppingCartPopupOpen: boolean;
  @Input() partnerId: number;
  @Input() tableHtml: string;
  @Input() orderForTheAmount: string;
  @Input() sites: Site[];
  @Input() managements: Management[];
  @Input() itemsDS: any;
  @Input() items: any;
  @Input() selectedPartner: any;
  @Input() customerId: number;
  @Input() isOrderAmountCommunicatedToClient: boolean;
  @Input() alreadyCommunicatedOrderForTheAmount: string;

  @Output() isShoppingCartPopupOpenOutput: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('turnoverPreordersDataGrid') turnoverPreordersDataGrid: DxDataGridComponent;
  @ViewChild('manuallyAddItemsDataGrid') manuallyAddItemsDataGrid: DxDataGridComponent;
  @ViewChild('copyAddItemsDataGrid') copyAddItemsDataGrid: DxDataGridComponent;
  @ViewChild('excelImportAddItemsDataGrid') excelImportAddItemsDataGrid: DxDataGridComponent;
  @ViewChild('excelImportFileUploader') excelImportFileUploader: DxFileUploaderComponent;
  
  loaded: boolean;
  templateVisible: boolean;
  isManuallyAddItemsBtnActive: boolean;
  isCopyAddItemsBtnActive: boolean;
  isExcelImportAddItemsBtnActive: boolean;
  selectedSiteId: number;
  filteredManagements: Management[] = [];
  selectedManagementId: number;
  turnoverPreorders: TurnoverPreorder[] = [];
  originalTurnoverPreorders: TurnoverPreorder[] = [];
  turnoverPreordersSelectedRows: any[];
  turnoverPreordersSelectedRowIndex = -1;
  isBasketVipQuantityPopupOpen: boolean;
  vipTurnoverPreorderIdSmallerQuantity: number;
  basketTotal: { [key: number]: number } = {};
  timeout: number;
  stockTooltipData: any = {};
  locationContactPersons: PartnerLocationContact[];
  offerPopupVisible: boolean;
  emailForTest: string;
  selectedLocationContactPerson: any;
  selectedOfferType: any;
  discountName: string;
  excelFileName: { [key: string]: string } = {};
  emailSubject: { [key: string]: string } = {};
  emailBody: { [key: string]: string } = {};
  partners: any;
  offerTypes: any[] = [];
  isDedicatedOfferPopupOpen: boolean;
  isDailyWhatsappOfferPopupOpen: boolean;
  dedicatedOfferData: string;
  isDedicatedOfferInvalidCodesPopupOpen: boolean;
  dedicatedOfferInvalidCodes: any;
  modifyOfferEmailPopupStates: { [key: string]: boolean } = {};
  manuallyAddItemsDataSource: any;
  copyAddItemsDataSource: any;
  excelImportAddItemsDataSource: any;
  uploadedExcelImportAddedItems: any;
  turnoverPreordersDataSources: { [key: number]: any[] } = {};
  managementItems: any;
  filteredSites: Site[] = [];
  itemsStocksByManagements: any;
  sendOfferToPartnerLogTooltipData: string;
  isSendOfferToPartnerTooltipVisible: boolean;
  
  constructor(
    private helperService: HelperService,
    private partnerLocationContactService: PartnerLocationContactService,
    private itemService: ItemService,
    private turnoverPreorderService: TurnoverPreorderService,
    private notificationService: NotificationService,
    private clientTurnoversService: ClientTurnoverService,
    private partnerService: PartnerService,
    private authService: AuthService,
    private sendOfferToPartnerLogService: SendOfferToPartnerLogService
  ) {

  }

  async ngOnInit() {
    await this.getData().then(async () => {
      this.selectedManagementId = null;
      this.selectedSiteId = null;
      await this.getTurnoverPreorders().then(() => {
        this.updateBasketTotal();
      })
    })

    this.partners = {
      paginate: true,
      pageSize: 15,
      store: []
    };

    this.initializeSendOfferToPartnerData(this.partnerId);
  }

  async getData() {
    this.loaded = true;
    Promise.all([]).then(() => {
      this.loaded = false;
    })
  }

  toggleTemplate() {
    this.templateVisible = !this.templateVisible;
    setTimeout(async () => {
      if (document.getElementById("discountGridsTooltipInfo")) {
        document.getElementById("discountGridsTooltipInfo").innerHTML = this.tableHtml;
      }
    }, 0);
  }

  getDisplayExprSite(item) {
    if (!item) {
       return '';
     }
     return item.code + ' - ' + item.name;
  }

  onSiteChanged(e) {
    
  }

  getDisplayExprManagement(item) {
    if (!item) {
       return '';
     }
     return item.code + ' - ' + item.name;
  }

  async onManagementChanged(e?: any) {
    if (this.selectedManagementId && this.selectedSiteId) {
      var itemIds = this.turnoverPreorders.map(x => x.itemId);
      await this.itemService.getItemStocksOnManagements(itemIds, [this.selectedManagementId]).then(r => {
        if (r) {
          this.turnoverPreorders.forEach(i => {
            if (r.hasOwnProperty(i.itemId)) {
              i.managementStock = r[i.itemId];
              i.managementId = this.selectedManagementId;
            } else {
              i.managementStock = 0;
              i.managementId = null;
            }
          });
        }
        this.turnoverPreordersDataGrid.instance.clearSelection();
      });
    }
  }

  async getTurnoverPreorders() {
    this.filteredManagements = this.managements.filter(f => f.eShop);
    this.filteredSites = this.sites.filter(f => this.filteredManagements?.map(m => m.siteId)?.includes(f.id));
    this.filteredSites.forEach(site => { this.turnoverPreordersDataSources[site.id] = [] })

    this.loaded = true;
    await this.turnoverPreorderService.GetByPartnerIdAsync(this.partnerId).then(async items => {
        this.turnoverPreorders = (items && items.length > 0) ? items : [];

        if (this.turnoverPreorders && this.turnoverPreorders.length > 0) {
          let itemIds = this.turnoverPreorders.map(i => i.itemId);
          await this.itemService.getMultipleItemsStocksOnSiteAsync(itemIds).then(async r => {
            this.itemsStocksByManagements = (r && r.length > 0) ? r : [];

            this.turnoverPreorders = this.turnoverPreorders.map(m => ({ ...m, agentQuantity: m.quantity }));
            this.originalTurnoverPreorders = _.cloneDeep(this.turnoverPreorders);
            
            this.turnoverPreorders.forEach(turnoverPreorder => {
              let stockBySite = this.itemsStocksByManagements.filter(f => f.itemId === turnoverPreorder.itemId);
              
              stockBySite.forEach(f => {
                if (f.stocksOnManagement.some(child => child.isImplicit)) {
                  f.isImplicit = true;
                } else {
                  f.isImplicit = false;
                }
              });

              stockBySite = _.orderBy(stockBySite, ['isImplicit'], ['desc']);
              let remainingQuantity = turnoverPreorder.quantity;

              const implicitStock = stockBySite.find(f => f.isImplicit);
              const nonImplicitStocks = _.orderBy(stockBySite.filter(f => !f.isImplicit), ['totalStockOnSite'], ['desc']);

              if (implicitStock) {
                const allocatedQuantity = Math.min(remainingQuantity, implicitStock.totalStockOnSite);
                remainingQuantity -= allocatedQuantity;

                if (turnoverPreorder.quantity > implicitStock.totalStockOnSite) {
                  remainingQuantity = turnoverPreorder.quantity;
                }

                if (!this.turnoverPreordersDataSources[implicitStock.siteId]) {
                  this.turnoverPreordersDataSources[implicitStock.siteId] = [];
                }
                
                this.turnoverPreordersDataSources[implicitStock.siteId].push({
                  ...turnoverPreorder,
                  agentQuantity: (remainingQuantity > 0 && nonImplicitStocks.length === 0) 
                    ? remainingQuantity : implicitStock.totalStockOnSite === 0 ? turnoverPreorder.quantity : allocatedQuantity,
                  quantity: (remainingQuantity > 0 && nonImplicitStocks.length === 0) 
                    ? remainingQuantity : implicitStock.totalStockOnSite === 0 ? turnoverPreorder.quantity : allocatedQuantity,
                  totalStock: implicitStock.totalStockOnSite,
                  siteId: implicitStock.siteId
                });

                if (implicitStock.totalStockOnSite === 0) remainingQuantity = 0;
              }

              if (remainingQuantity > 0) {
                for (let i = 0; i < nonImplicitStocks.length; i++) {
                  const stock = nonImplicitStocks[i];
                  const isLastStock = i === nonImplicitStocks.length - 1;
                  const allocatedQuantity = isLastStock 
                    ? remainingQuantity
                    : Math.min(remainingQuantity, stock.totalStockOnSite);
          
                  remainingQuantity -= allocatedQuantity;
          
                  if (!this.turnoverPreordersDataSources[stock.siteId]) {
                    this.turnoverPreordersDataSources[stock.siteId] = [];
                  }
          
                  this.turnoverPreordersDataSources[stock.siteId].push({
                      ...turnoverPreorder,
                      agentQuantity: allocatedQuantity,
                      quantity: allocatedQuantity,
                      totalStock: stock.totalStockOnSite,
                      siteId: stock.siteId
                  });

                  if (remainingQuantity <= 0) break;
                }
              }
            });

            await this.turnoverPreorderService.getItemsByManagementIdAsync(this.filteredManagements.map(m => m.id)).then(result => { this.managementItems = result; });   
          });
        }

        this.loaded = false;
    });
  }

  onTurnoverPreordersSelectionChanged(data: any) {
    this.turnoverPreordersSelectedRows = data.selectedRowsData;
    this.turnoverPreordersSelectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  async onRowUpdatedTurnoverPreorders(e) {
    if (e && e.data) {
      let item = e.data;
      
      if (item.priceSource && item.priceSource !== TurnoverPreorderPriceSourceEnum.PriceComposition) {
        let originalItem = this.originalTurnoverPreorders.find(f => f.id === item.id);

        if (item.quantity < originalItem.quantity) {
          this.vipTurnoverPreorderIdSmallerQuantity = item.id;
          this.isBasketVipQuantityPopupOpen = true;
        }
      }
    }

    if (e && e.data && e.data.agentQuantity) {

      let item = new TurnoverPreorder();
      item = e.data;
      item.agentQuantity = e.data.quantity;
  
      let updatedRow = this.turnoverPreorders.find(f => f.id === e.data.id);
      if (updatedRow) {
        updatedRow.agentQuantity = e.data.quantity;
      }

      if (item) {
        await this.turnoverPreorderService.updateTurnoverPreorderAsync(item).then(async r => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Datele au fost modificate cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Datele nu au fost modificate! A aparut o eroare!',
              NotificationTypeEnum.Red, true)
          };
          await this.getTurnoverPreorders().then(() => {
            this.updateBasketTotal();
          })
        });
      }
    }
  }

  updateBasketTotal() {
    this.filteredSites.forEach(site => {
      let total = 0;
      let turnoverPreorders = this.getDataSourceForSite(site.id);

      turnoverPreorders?.forEach(f => {
        total += f.agentQuantity * f.price;
      })

      this.basketTotal[site.id] = Math.ceil(total * 100) / 100;
    })
  }

  onBasketCellPrepared(e) {
    if (e.rowType === 'data') {
      e.cellElement.style.paddingTop = '2px';
      e.cellElement.style.paddingBottom = '1px';
    }
  }

  onBasketRowPrepared(e) {
    if (e.rowType === 'data' && e.data && e.data.totalStock < e.data.quantity ) {
      e.rowElement.style.backgroundColor = '#ffdfcf';
    }
    if (e.rowType === 'data' && e.data && e.data.agentQuantity &&  e.data.managementStock && e.data.managementStock > e.data.agentQuantity ) {
      e.rowElement.style.backgroundColor = '#b8ffd3';
    }
    if (e.rowType === 'data' && e.data && e.data.agentQuantity && e.data.managementStock < e.data.agentQuantity ) {
      e.rowElement.style.backgroundColor = '#ff5842';
    }
  }

  displayCodeExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  async deleteTurnoverPreorderRow(data) {
    if (data.data) {
      let countItemFrequency = 0;
      this.filteredSites.forEach(site => {
        if (this.turnoverPreordersDataSources[site.id].find(s => s.itemId === data.data.itemId)) {
          countItemFrequency++;
        }
      })

      if (countItemFrequency > 1) {
        this.turnoverPreordersDataSources[data.data.siteId] = this.turnoverPreordersDataSources[data.data.siteId].filter(f => f.itemId !== data.data.itemId);
      } else {
        await this.turnoverPreorderService.deleteTurnoverPreorderAsync([data.data.id]).then(async r => {
          if (r) {
            this.notificationService.alert('top', 'center', 'Datele au fost sterse cu succes!',
              NotificationTypeEnum.Green, true)
          } else {
            this.notificationService.alert('top', 'center', 'Datele nu au fost sterse! A aparut o eroare!',
              NotificationTypeEnum.Red, true)
          };
          await this.getTurnoverPreorders().then(() => {
            this.updateBasketTotal();
          })
        });
      }
    }
  }

  async onSaveBasketVipQuantityWithPriceComposition() {
    let item = this.turnoverPreorders.find(f => f.id === this.vipTurnoverPreorderIdSmallerQuantity);

    if (item) {
      item.priceSource = TurnoverPreorderPriceSourceEnum.PriceComposition;

      this.loaded = true;
      await this.turnoverPreorderService.updateTurnoverPreorderAsync(item).then(async r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        };

        await this.getTurnoverPreorders().then(x => {
          this.isBasketVipQuantityPopupOpen = false;
          this.loaded = false;
        })
      });
    }
  }

  onCloseBasketVipQuantity() {
    let modifiedItem = this.turnoverPreorders.find(f => f.id === this.vipTurnoverPreorderIdSmallerQuantity);
    let originalItem = this.originalTurnoverPreorders.find(f => f.id === this.vipTurnoverPreorderIdSmallerQuantity);
    modifiedItem.quantity = originalItem.quantity;
    
    this.isBasketVipQuantityPopupOpen = false;
  }

  onHidingPopup(event: any) {
    this.isShoppingCartPopupOpen = false;
    this.isShoppingCartPopupOpenOutput.emit(false);
  }

  openGeneralOfferPopup() {
    this.getPartnerContact();
    this.emailForTest = null;
    this.offerPopupVisible = true;
  }

  async getPartnerContact(): Promise<void> {
    await this.partnerLocationContactService.getPartnerLocationContactByPartnerID(this.partnerId).then(items => {
      if (items && items.length > 0) {
        this.locationContactPersons = items.filter(f => f.email);
      } else {
        this.locationContactPersons = null;
      }
    });
  }

  async sendOffer(dedicatedOfferItemIds?: number[]) {
    if (this.selectedLocationContactPerson && this.selectedLocationContactPerson.length > 0) {
      let personContactIds = this.selectedLocationContactPerson.map(m => m.id);
      this.loaded = true;

      await this.clientTurnoversService.sendOfferToPartnerContact(this.partnerId, 
        personContactIds, 
        this.emailForTest, 
        this.selectedOfferType.id, 
        this.discountName, 
        this.excelFileName[this.selectedOfferType.id],
        this.emailSubject[this.selectedOfferType.id],
        this.emailBody[this.selectedOfferType.id],
        +(this.authService.getUserPostId()),
        dedicatedOfferItemIds).then(result => {
        if (result) {
          this.notificationService.alert('top', 'center', 'Trimite Oferta - Oferta a fost trimisa!',
          NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Trimite Oferta - Oferta nu a fost trimisa! A aparut o eroare!',
          NotificationTypeEnum.Red, true)
        }

        this.loaded = false;
      })

    } else {
      this.notificationService.alert('top', 'center', 'Trimite Oferta - Persoana de contact selectata nu are un email!',
      NotificationTypeEnum.Red, true)
    }
  }

  initializeSendOfferToPartnerData(clientId: number) {
    this.partnerService.getPartnerByPartnerIdsAsync([clientId]).then(items => {
      this.partners.store.push(...items.filter(x => !this.partners.store.map(y => y.id).includes(x.id)));
      this.partnerId = items[0].id;

      this.offerTypes = [
        { id: 1, text: 'Oferta generala' },
        { id: 2, text: 'Oferta Produse Cumparate Frecvent' },
        { id: 3, text: 'Oferta Produse de Interes' },
        { id: 4, text: 'Oferta Speciala - lichidare, PMV' },
        { id: 5, text: 'Oferta Dedicata' },
        { id: 6, text: 'Oferte Zilnice pentru Whatsapp' },
        { id: 7, text: 'Oferta Produse Cos + Complementare + Doriti' }
      ];
  
      this.selectedOfferType = this.offerTypes[0];

      const currentDate = moment().format("DD.MM.YYYY");

      this.offerTypes.forEach(offer => {
        switch (offer.id) {
          case 1:
            this.excelFileName[offer.id] = `${this.partners.store[0].name} Oferta Generala ${currentDate}`;
            this.emailSubject[offer.id] = `Oferta Generala ${currentDate}`;
            this.emailBody[offer.id] = `Buna ziua! Aceasta este oferta cu toate produsele pe care le puteti achizitiona pentru data: ${currentDate}`;
            break;
          case 2:
            this.excelFileName[offer.id] = `${this.partners.store[0].name} Oferta Produse Cumparate Frecvent ${currentDate}`;
            this.emailSubject[offer.id] = `Oferta Produse Cumparate Frecvent ${currentDate}`;
            this.emailBody[offer.id] = `Buna ziua! Aceasta este oferta cu produsele cumparate frecvent pentru data: ${currentDate}`;
            break;
          case 3:
            this.excelFileName[offer.id] = `${this.partners.store[0].name} Oferta Produse de Interes ${currentDate}`;
            this.emailSubject[offer.id] = `Oferta Produse de Interes ${currentDate}`;
            this.emailBody[offer.id] = `Buna ziua! Aceasta este oferta cu produsele de interes pe care le puteti achizitiona de la noi pentru data: ${currentDate}`;
            break;
          case 4:
            this.excelFileName[offer.id] = `${this.partners.store[0].name} Oferta Speciala - lichidare, PMV ${currentDate}`;
            this.emailSubject[offer.id] = `Oferta Speciala - lichidare, PMV ${currentDate}`;
            this.emailBody[offer.id] = `Buna ziua! Aceasta este oferta speciala - lichidare, PMV pentru data: ${currentDate}`;
            break;
          case 5:
            this.excelFileName[offer.id] = `${this.partners.store[0].name} Oferta Dedicata ${currentDate}`;
            this.emailSubject[offer.id] = `Oferta Dedicata ${currentDate}`;
            this.emailBody[offer.id] = `Buna ziua! Aceasta este oferta dedicata pentru data: ${currentDate}`;
    
            break;

          case 7:
            this.excelFileName[offer.id] = `${this.partners.store[0].name} Oferta Produse Cos + Complementare + Doriti ${currentDate}`;
            this.emailSubject[offer.id] = `Oferta Produse Cos + Complementare + Doriti ${currentDate}`;
            this.emailBody[offer.id] = `Buna ziua! Aceasta este Oferta Produse Cos + Complementare + Doriti pentru data: ${currentDate}`;
    
            break;
          default:
            break;
        }
      })
    });
  }

  getDisplayExprContact(item) {
    if (!item) {
      return '';
    } 

    return item.firstName + ' - ' + item.email + ' - ' + item.phoneNumber;
  }

  async onOfferTypeValueChanged(event: any) {
    if (event && event.value && event.value.id === 5) {
      this.isDedicatedOfferPopupOpen = true;
    }

    this.isDailyWhatsappOfferPopupOpen = false;
    if (event && event.value && event.value.id === 6) {
      this.isDailyWhatsappOfferPopupOpen = true;
    }
  }

  async onSaveDedicatedOffer() {
    const { validCodes, invalidCodes } = this.validateCodes(this.dedicatedOfferData);

    if (invalidCodes.length > 0) {
      this.dedicatedOfferInvalidCodes = invalidCodes.join(', ');
      this.isDedicatedOfferInvalidCodesPopupOpen = true;
    } else {
      await this.processOffer(validCodes);
    }
  }

  async onSaveDedicatedOfferWithInvalidCodes() {
    const { validCodes } = this.validateCodes(this.dedicatedOfferData);
    await this.processOffer(validCodes);
  }

  validateCodes(inputData: string): { validCodes: string[], invalidCodes: string[] } {
    const auxProductCodes = inputData.split(/[\s,;.:?!()\[\]{}|]+/);
    const productCodes = auxProductCodes.filter(code => code.trim() !== "");
    
    let validCodes: string[] = [];
    let invalidCodes: string[] = [];

    productCodes.forEach(code => {
      const existingItem = this.items.find(item => item.code === code);

      if (existingItem) {
        validCodes.push(code);
      } else {
        const substringCode = code.substring(0, 4);
        const matchingItems = this.items.filter(item => item.code.startsWith(substringCode));

        if (!matchingItems || matchingItems.length === 0) {
          invalidCodes.push(code);
        } else {
          matchingItems.forEach(item => validCodes.push(item.code));
        }
      }
    });

    return { validCodes, invalidCodes };
  }

  async processOffer(validCodes: string[]) {
    const itemIds = this.items.filter(f => validCodes.includes(f.code)).map(m => m.id);
    await this.sendOffer(itemIds);
  }

  closeDedicatedOfferInvalidCodesPopup() {
    this.isDedicatedOfferInvalidCodesPopupOpen = false;
  }

  async onViewOfferClick(offer: any) {
    this.loaded = true;

    await this.clientTurnoversService.generateOfferToPartnerContact(this.partnerId, 
      this.selectedLocationContactPerson?.id, 
      this.emailForTest, offer.id, 
      this.discountName, offer.text, 
      this.excelFileName[this.selectedOfferType.id],
      this.emailSubject[this.selectedOfferType.id],
      this.emailBody[this.selectedOfferType.id],
      []).then(result => {  
        if (result && !result.success) {
          this.notificationService.alert('top', 'center', 'A aparut o eroare! Verificati daca partenerul are alocate listele de pret conform ofertei selectate!', NotificationTypeEnum.Red, true);
        }

        this.loaded = false;
      })
  }
  
  onModifyEmailClick(offer: any) {
    this.modifyOfferEmailPopupStates[offer.id] = true;
  }

  onManuallyAddItemsToShoppingCartRowInserting(e: any) {
    this.manuallyAddItemsDataSource = [...this.manuallyAddItemsDataSource, e.data];
  }

  manuallyAddItem() {
    this.manuallyAddItemsDataGrid.instance.addRow();
  }

  onManuallyAddItems() {
    this.manuallyAddItemsDataSource = [];
    this.isManuallyAddItemsBtnActive = true;
    this.isCopyAddItemsBtnActive = false;
    this.isExcelImportAddItemsBtnActive = false;
  }

  onCopyAddItems() {
    this.copyAddItemsDataSource = [];

    setTimeout(() => {
      if (this.copyAddItemsDataGrid) {
        this.initializeGridEvents();
      }
    }, 0);

    this.isManuallyAddItemsBtnActive = false;
    this.isCopyAddItemsBtnActive = true;
    this.isExcelImportAddItemsBtnActive = false;
  }

  onExcelImportAddItems() {
    this.excelImportAddItemsDataSource = [];
    this.isManuallyAddItemsBtnActive = false;
    this.isCopyAddItemsBtnActive = false;
    this.isExcelImportAddItemsBtnActive = true;
  }

  initializeGridEvents(): void {
    if (this.isCopyAddItemsBtnActive && this.copyAddItemsDataGrid) {
      const gridElement = this.copyAddItemsDataGrid.instance.element();
      gridElement.addEventListener('paste', (event: ClipboardEvent) => this.handlePaste(event));
    }
  }

  handlePaste(event: ClipboardEvent) {
    event.preventDefault();

    const clipboardData = event.clipboardData || (window as any).clipboardData;
    const pastedData = clipboardData.getData('text/plain');
    const rowsData = pastedData.split('\n').filter(row => row.trim() !== '');

    rowsData.slice(1).forEach((row, index) => {
      const cells = row.split('\t');
      let itemCode = cells[0]?.trim() || '';
      let itemName = cells[1]?.trim() || '';
      let itemQuantity = cells[2].trim() || '';

      let itemByCode = this.items.find(f => f.code === itemCode);
      let itemByName = this.items.find(f => f.nameRO === itemName);

      this.copyAddItemsDataSource.push({
        index: index,
        itemId: itemByCode ? itemByCode.id : itemByName ? itemByName.id : null,
        quantity: itemQuantity
      });
    });

    this.copyAddItemsDataGrid.instance.refresh();
  }

  deleteManuallyAddedItem(data: any) {
    const rowIndex = this.manuallyAddItemsDataSource.findIndex(item => item.__KEY__ === data.__KEY__);
  
    if (rowIndex !== -1) {
      this.manuallyAddItemsDataGrid.instance.deleteRow(rowIndex);
    }
  }

  deleteCopyAddedItem(data: any) {
    const rowIndex = this.copyAddItemsDataSource.findIndex(item => item.index === data.index);
  
    if (rowIndex !== -1) {
      this.copyAddItemsDataGrid.instance.deleteRow(rowIndex);
    }
  }

  deleteExcelImportAddedItem(data: any) {
    const rowIndex = this.excelImportAddItemsDataSource.findIndex(item => item.index === data.index);
  
    if (rowIndex !== -1) {
      this.excelImportAddItemsDataGrid.instance.deleteRow(rowIndex);
    }
  }
  
  async onSaveManuallyAddedItems() {
    let turnoverPreorders: TurnoverPreorder[] = [];

    this.manuallyAddItemsDataSource?.forEach(f => {
      let j = new TurnoverPreorder();
      j.customerId = this.customerId;
      j.partnerId = this.partnerId;
      j.postId = +(this.authService.getUserPostId());
      j.itemId = f.itemId;
      j.quantity = f.quantity;

      turnoverPreorders.push(j);
    })

    this.loaded = true;
    await this.turnoverPreorderService.createMultipleTurnoverPreorderAsync(turnoverPreorders).then(async () => {
      await this.getTurnoverPreorders().then(() => {
        this.manuallyAddItemsDataSource = [];
        this.updateBasketTotal();
        this.loaded = false;
      })
    })
  }

  async onSaveCopyAddedItems() {
    let turnoverPreorders: TurnoverPreorder[] = [];

    this.copyAddItemsDataSource?.forEach(f => {
      let j = new TurnoverPreorder();
      j.customerId = this.customerId;
      j.partnerId = this.partnerId;
      j.postId = +(this.authService.getUserPostId());
      j.itemId = f.itemId;
      j.quantity = f.quantity;

      turnoverPreorders.push(j);
    })

    this.loaded = true;
    await this.turnoverPreorderService.createMultipleTurnoverPreorderAsync(turnoverPreorders).then(async () => {
      await this.getTurnoverPreorders().then(() => {
        this.copyAddItemsDataSource = [];
        this.updateBasketTotal();
        this.loaded = false;
      })
    })
  }

  async onSaveExcelImportAddedItems() {
    this.excelImportFileUploader.instance.reset();

    let turnoverPreorders: TurnoverPreorder[] = [];

    this.excelImportAddItemsDataSource?.forEach(f => {
      let j = new TurnoverPreorder();
      j.customerId = this.customerId;
      j.partnerId = this.partnerId;
      j.postId = +(this.authService.getUserPostId());
      j.itemId = f.itemId;
      j.quantity = f.quantity;

      turnoverPreorders.push(j);
    })

    this.loaded = true;
    await this.turnoverPreorderService.createMultipleTurnoverPreorderAsync(turnoverPreorders).then(async () => {
      await this.getTurnoverPreorders().then(() => {
        this.excelImportAddItemsDataSource = [];
        this.updateBasketTotal();
        this.loaded = false;
      })
    })
  }

  onUploadExcelFileChange(event: any) {
    const files: FileList = event.target.files;
  
    if (files.length > 0) {
      const file = files[0];
  
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const binaryData = e.target.result;
  
        const workbook = xlsx.read(binaryData, { type: 'binary' });
        const sheetName = workbook.SheetNames[0];
        const sheet = workbook.Sheets[sheetName];
  
        const data = xlsx.utils.sheet_to_json(sheet, { header: 1 });
        
        const rows = data.slice(1);
  
        const structuredData = rows.map((row: any[]) => ({
          itemCode: row[0],
          itemName: row[1],
          itemQuantity: row[2]
        }));

        structuredData.forEach((elem, index) => {
          let itemByCode = this.items.find(f => f.code === elem.itemCode);
          let itemByName = this.items.find(f => f.nameRO === elem.itemName);

          this.excelImportAddItemsDataSource.push({
            index: index,
            itemId: itemByCode ? itemByCode.id : itemByName ? itemByName.id : null,
            quantity: elem.itemQuantity
          });
        })
      };
  
      reader.readAsBinaryString(file);
    }
  }

  hasDataForSite(siteId: number): boolean {
    let management = this.filteredManagements.find(f => f.siteId === siteId);
    return this.turnoverPreordersDataSources[siteId]?.length > 0;
  }

  getDataSourceForSite(siteId: number): any[] {
    let management = this.filteredManagements.find(f => f.siteId === siteId);
    return this.turnoverPreordersDataSources[siteId] || [];
  }

  createOrder(site: any) {
    var selectedRows = this.turnoverPreordersDataSources[site.id].filter(x => x.agentQuantity > 0);

    if (selectedRows && selectedRows.length > 0) {
      let isQuantityOk = true;
      selectedRows.forEach(sr => {
        if (sr.agentQuantity > sr.totalStock) {
          isQuantityOk = false;
        }
      });

      if (!isQuantityOk) {
        this.notificationService.alert('top', 'center', `Comanda de pe locatia ${site.code} - ${site.name} nu a fost creata! Ati selectat o cantitate mai mare ca stocul disponibil!`, NotificationTypeEnum.Red, true);
        return;
      }

      const filteredRows = selectedRows.map(row => {
        let implicitManagementId = this.filteredManagements.find(f => f.siteId === row.siteId && f.isImplicit)?.id;

        return {
          id: row.id,
          itemId: row.itemId,
          partnerId: row.partnerId,
          mngId: implicitManagementId,
          quantity: row.agentQuantity,
          price: row.price
        }
      });
  
      const encodedData = encodeURIComponent(JSON.stringify(filteredRows));
      const targetUrl =  environment.CRMPrimaware +  `/order?data=${encodedData}`;
      window.open(targetUrl, '_blank');

    } else {
      this.notificationService.alert('top', 'center', `Comanda de pe locatia ${site.code} - ${site.name} nu a fost creata! Nu ati selectat unul sau mai multe produse!`,
        NotificationTypeEnum.Red, true)
    }
  }

  async getSendOfferToPartnerLogTooltipData() {
    this.sendOfferToPartnerLogTooltipData = 'Se incarca...'; 
    this.isSendOfferToPartnerTooltipVisible = true;

    await this.sendOfferToPartnerLogService.getLastByPartnerIdAsync(this.partnerId).then(resp => {
      if (resp) {
        let relevantOfferName = this.offerTypes.find(f => f.id === resp.offerType)?.text ?? '';
        let relevantOfferLastDate = moment(resp.date).format('DD.MM.YYYY');

        this.sendOfferToPartnerLogTooltipData = `Ultima Oferta trimisa: ${relevantOfferName} / ${relevantOfferLastDate}`;
      } else {
        this.sendOfferToPartnerLogTooltipData = '';
      }
    })
  }

  hideSendOfferToPartnerLogTooltipData(): void {
    this.isSendOfferToPartnerTooltipVisible = false;
  }
}

