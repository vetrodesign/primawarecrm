export enum ContactCampaignLocationSortTypeEnum {
    NoneSortType = 1,
    Ascending = 2,
    Descending = 3
}