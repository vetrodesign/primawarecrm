import { BaseDomain } from "./base-domain.model";

export class PartnerClientCreditControl extends BaseDomain {
    partnerId: number;
    paymentInstrumentId: number;
    paymentTerm: number;
    graceTerm: number;
    creditLimit: number;
    isBlockedByTermOfGrace: boolean = false;
    isBlockedByCreditLimit: boolean = false;

    constructor(opavPaymentInstrumentId : number = null) {
        super();
        this.paymentInstrumentId = opavPaymentInstrumentId;
        this.paymentTerm = 0;
        this.isBlockedByTermOfGrace = true;
        this.isBlockedByCreditLimit = true;
        this.graceTerm = 2;
        this.creditLimit = 10000;
    }
}




