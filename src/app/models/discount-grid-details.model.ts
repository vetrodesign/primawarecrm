import { BaseDomain } from "./base-domain.model";

export class DiscountGridDetails extends BaseDomain {
    public name: string;
    public discountGridId: number;
    public benefit: number;
    public salePriceListId: number;

    public valueFrom: number;
    public valueTo: number;

    public fromConditionType: number;
    public toConditionType: number;
}
