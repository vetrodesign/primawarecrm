export class SalePriceListFilterVM {
    code: any;
    name: any;
    listTypeIds: any;
    productConventionIds: any;
    currencyIds: any;

    availableFrom: Date;
    availableTo: Date;

    status: boolean = true;
    isImplicit: boolean;
    isIndividual: boolean;
}


export class StockOnItemStockFilterVM {
    itemIds: number[];
}