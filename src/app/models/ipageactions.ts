export interface IPageActions {
    CanView: boolean;
    CanAdd: boolean;
    CanUpdate: boolean;
    CanDelete: boolean;
    CanPrint: boolean;
    CanExport: boolean;
    CanImport: boolean;
    CanDuplicate: boolean;
}
