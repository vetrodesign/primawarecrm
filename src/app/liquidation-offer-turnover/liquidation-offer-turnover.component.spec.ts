import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiquidationOfferTurnoverComponent } from './liquidation-offer-turnover.component';

describe('LiquidationOfferTurnoverComponent', () => {
  let component: LiquidationOfferTurnoverComponent;
  let fixture: ComponentFixture<LiquidationOfferTurnoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiquidationOfferTurnoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiquidationOfferTurnoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
