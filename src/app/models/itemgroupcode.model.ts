import { BaseDomain } from "./base-domain.model";

export class ItemGroupCode extends BaseDomain {
    grpCode: string;
    firstName: string;
    secondName: string;
}