import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Marker, Route } from 'app/models/marker.model';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, OnChanges {
  @Input() selectedRows: any;
  @Input() height: number;
  markers: Marker[] = [];
  routes: Route[] = [];
  mapMarkerUrl: any = "https://js.devexpress.com/Demos/RealtorApp/images/map-marker.png";
  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.selectedRows) {
      this.markers = [];
      if (this.selectedRows) {
        this.selectedRows.forEach(item => {
          if (item.latitude && item.longitude) {
            const marker = {
              location: {
                lat: item.latitude,
                lng: item.longitude
              },
              tooltip: {
                isShown: false,
                text: item.name + ' - ' + item.street
              }
            };
            this.markers.push(marker);
          }
        });
      }
    }
  }

  showMarkersTooltip() {
    if (this.markers.find(m => m.tooltip.isShown === true)) {
      this.markers = this.markers.map(function (item) {
        let newItem = JSON.parse(JSON.stringify(item));
        newItem.tooltip.isShown = false;
        return newItem;
      });
    } else {
      this.markers = this.markers.map(function (item) {
        let newItem = JSON.parse(JSON.stringify(item));
        newItem.tooltip.isShown = true;
        return newItem;
      });
    }
  }

  generateRoute() {
    if (this.routes.length > 0) {
      this.routes = [];
    } else {
      this.routes = [{
        weight: 9,
        color: 'red',
        opacity: 0.5,
        mode: '',
        locations: this.markers.map(x => x.location)
    }];
    }
  }
}
