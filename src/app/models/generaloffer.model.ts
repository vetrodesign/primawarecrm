export class GeneralOffer {
    id: string;
    itemGroupId: number;
    pC_MAX: number;
    customerId: number;
    maxTransport: number;
    packingPrice: number;
    workmanshipPrice: number;
    environmentPrice: number;
    cMP_MAX: number;
    d017: number;
}
