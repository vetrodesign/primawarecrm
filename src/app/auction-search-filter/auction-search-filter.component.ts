import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { AuctionFilterDayEnum } from 'app/enums/auctionFilterDayEnum';
import { AuctionSearchFilter } from 'app/models/auctionSearchFilter.model';
import { AuthService } from 'app/services/auth.service';
import { TranslateService } from 'app/services/translate';
import { UsersService } from 'app/services/user.service';
import { DxValidatorComponent } from 'devextreme-angular';

@Component({
  selector: 'app-auction-search-filter',
  templateUrl: './auction-search-filter.component.html',
  styleUrls: ['./auction-search-filter.component.css']
})
export class AuctionSearchFilterComponent implements OnInit, OnChanges {
  @Input() auctionTypes: any;
  @Input() auctionGroups: any;
  @Input() cpvCodes: any;
  @Input() allPosts: any;
  @Input() userPostId: number;

  @Input() auctionParticipationTypes: any;
  @Input() auctionStatusTypes: any;

  @Output() searchEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() refreshEvent: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('validationSearchGroup') validationSearchGroup: DxValidatorComponent;
  auctionSearchFilter: AuctionSearchFilter;
  isOwner: boolean;
  auctionStates: any[] = [];
  selectedAuctionState: any;

  dayOverEndDateOptions: { id: number; name: string }[] = [];

  constructor(private translationService: TranslateService,
    private userService: UsersService, private authService: AuthService) {
    this.authService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });

    for (let n in AuctionFilterDayEnum) {
      if (typeof AuctionFilterDayEnum[n] === 'number') {
        this.dayOverEndDateOptions.push({
          id: <any>AuctionFilterDayEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
  }

  ngOnInit(): void {
    this.auctionStates = [
      { id: 0, text: 'In termen' },
      { id: 1, text: 'Expirate' },
      { id: 2, text: 'De analizat' }
    ];

    this.auctionSearchFilter = new AuctionSearchFilter();
    this.auctionSearchFilter.hideRejected = true;
    this.selectedAuctionState = this.auctionStates[0];
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.userPostId) {
      if (this.userPostId != null) {
        this.auctionSearchFilter.postId = this.userPostId;
      }
    }
  }

  searchAuctions() {
    if (this.selectedAuctionState.id === 0) {
      this.auctionSearchFilter.isInTerm = true;
      this.auctionSearchFilter.isExpired = false;
      this.auctionSearchFilter.isToAnalyze = false;
    }

    if (this.selectedAuctionState.id === 1) {
      this.auctionSearchFilter.isInTerm = false;
      this.auctionSearchFilter.isExpired = true;
      this.auctionSearchFilter.isToAnalyze = false;
      this.auctionSearchFilter.endingDate = null;
    }

    if (this.selectedAuctionState.id === 2) {
      this.auctionSearchFilter.isInTerm = false;
      this.auctionSearchFilter.isExpired = false;
      this.auctionSearchFilter.isToAnalyze = true;
      this.auctionSearchFilter.endingDate = null;
    }

    if (this.validationSearchGroup.instance.validate().isValid) {
      this.searchEvent.emit(this.auctionSearchFilter);
    }
  }

  resetFilters() {
    this.auctionSearchFilter = new AuctionSearchFilter();

    this.selectedAuctionState = this.auctionStates[0];
    this.auctionSearchFilter.isInTerm = true;
    this.auctionSearchFilter.isExpired = false;
    this.auctionSearchFilter.isToAnalyze = false;

    this.refreshEvent.emit(true);
  }

  onMultiTagSitesChange(e: any) {
    if (e && e.value) {
      if (e.value.length > 50) {
        const allPrevValues = e.previousValue;
        e.component.option("value", allPrevValues);
      }
    }
  }

  endDateEventChange(e: any) {
    if (e && e.value) {
      let date = new Date(e.value);
      if (date.getHours() === new Date().getHours() && date.getMinutes() === new Date().getMinutes())
        date.setHours(23);
      date.setMinutes(59);
      date.setSeconds(0);
      this.auctionSearchFilter.endDate = date;
    }
  }

  getCpvCodeExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.nameRO;
  }

  auctionTypeExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  } 

  displayPostExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

}
