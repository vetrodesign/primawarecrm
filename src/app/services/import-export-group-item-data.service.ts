import { Injectable } from '@angular/core';
import { ImportExportGroupItemData } from 'app/models/importexportgroupitemdata.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ImportExportGroupItemDataService {

  constructor(private apiService: ApiService) { }

  async getAll(): Promise<any[]> {
    const apiURL = `/importexportgroupitemdata/GetAll`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.crmApi);
  }

  async getByItemDataIdAsync(id: number): Promise<any[]> {
    const apiURL = `/importexportgroupitemdata/GetByItemDataId`;
    return await this.apiService.PostAsync<any>(apiURL, id, Constants.crmApi);
  }

  async createMultipleAsync(items: ImportExportGroupItemData[]): Promise<any> {
    const apiURL = `/importexportgroupitemdata/createMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, items, Constants.crmApi);
  }

  async deleteMultipleAsync(items: ImportExportGroupItemData[]): Promise<any> {
    const apiURL = `/importexportgroupitemdata/deleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, items, Constants.crmApi);
  }
}

