import { BaseDomain } from "./base-domain.model";
import { PartnerXItemComment } from "./clientturnover.model";

export class TurnoverPreorder extends BaseDomain{
    partnerId: number;
    itemId: number;
    postId: number;
    quantity: number;

    totalStock: number;
    agentQuantity: number;
    managementStock: number;
    managementId: number;
    price: number;
    priceSource: number;

        // Static method to convert PartnerXItemComment to TurnoverPreorder
        static FromPartnerXItemComment(partnerXItemComment: PartnerXItemComment): TurnoverPreorder {
            const turnoverPreorder = new TurnoverPreorder();
    
            // Assign common properties
            turnoverPreorder.customerId = partnerXItemComment.customerId;
            turnoverPreorder.partnerId = partnerXItemComment.partnerId;
            turnoverPreorder.itemId = partnerXItemComment.itemId;
            turnoverPreorder.postId = partnerXItemComment.postId;
            turnoverPreorder.quantity = partnerXItemComment.shoppingCartQuantity ? partnerXItemComment.shoppingCartQuantity : partnerXItemComment.monthlyOrderedQuantity; 
    
            return turnoverPreorder;
        }
}
