export const environment = {
  production: false,
  magentoApiUrl: 'https://vetro.ro/rest/all',
  magentoUser: 'vetro121',
  magentoPassword: 'CnA-xJ^MQ7dkMy~',
  SSOPrimaware: 'https://192.168.0.89:44300',
  SRMPrimaware: 'https://192.168.0.6:44303',
  googleApiKey: 'AIzaSyA3oUGpF8DOhE-0lNMMv1QL08sLFLjO-WU',
  encryptionKey: 'w7hskLVpTdH6I0MnhVls',

  microserviceApiUrl: '/marketingApi',


  useApiGateway: false,
  apiGatewayUrl: 'https://localhost/PrimawareGateway.Api',
  ssoApi: 'https://localhost/PrimawareAPI.Api/api',
  mrkApi: 'https://localhost/PrimawareMarketing.Api/api',
  srmApi: 'https://localhost/PrimawareSRM.Api/api',
  commonApi: 'https://localhost/PrimawareCommon.Api/api',
  crmApi: 'https://192.168.0.6:44355/api',
  apiPaths: {
     ssoApi: '/ssoApi',
     mrkApi: '/marketingApi',
     srmApi: '/srmApi',
     commonApi: '/commonApi',
     crmApi: '/crmApi'
  }
};
