import { TestBed } from '@angular/core/testing';

import { PartnerActivityService } from './partner-activity.service';

describe('PartnerActivityService', () => {
  let service: PartnerActivityService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerActivityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
