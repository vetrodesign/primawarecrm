import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PartnerSubscriptionAllocation } from 'app/models/partner-subscription-allocation.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerSubscriptionAllocationService {

  constructor(private apiService: ApiService) { }

  async getByPartnerIdAsync(partnerId: number): Promise<PartnerSubscriptionAllocation[]> {
    const apiURL = `/partnersubscriptionallocation/getbypartnerid`;
    return await this.apiService.PostAsync<PartnerSubscriptionAllocation[]>(apiURL, partnerId, Constants.mrkApi);
  }

  async getAllAsync(): Promise<PartnerSubscriptionAllocation[]> {
    const apiURL = `/partnersubscriptionallocation/geAll`;
    return await this.apiService.GetAsync<PartnerSubscriptionAllocation[]>(apiURL, Constants.mrkApi);
  }

  async updateAsync(partnerSubscriptionAllocation: PartnerSubscriptionAllocation) {
    const apiURL = `/partnersubscriptionallocation/update`;
    return await this.apiService.PostAsync<any>(apiURL, partnerSubscriptionAllocation, Constants.mrkApi);
  }
}