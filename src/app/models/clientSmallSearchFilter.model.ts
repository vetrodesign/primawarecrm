export class ClientSmallSearchFilter {
    public code: string;
    public name: string;
    public fiscalCode: string;
    public clientType: number[];
    public nameOrCode: string;
    public email: string;
}