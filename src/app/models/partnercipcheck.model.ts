export class PartnerCIPCheckVM {
    id: number;
    partnerId: number;
    dateOfCIPCheck: Date;
    numberOfIncidentsBelow: number;
    numberOfIncidentsAbove: number;
    dateOfLastIncident: Date;
    IsActive: boolean;
}