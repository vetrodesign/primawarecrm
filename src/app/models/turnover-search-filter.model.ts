export class TurnoverSearchFilter {
    partnerId: any;
    postIds: any;
    code: any;
    name: any;
    itemIds: any;
    citiesIds: any;
    countriesIds: any;
    countiesIds: any;
}
