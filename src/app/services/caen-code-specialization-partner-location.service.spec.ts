import { TestBed } from '@angular/core/testing';

import { CaenCodeSpecializationPartnerLocationService } from './caen-code-specialization-partner-location.service';

describe('CaenCodeSpecializationPartnerLocationService', () => {
  let service: CaenCodeSpecializationPartnerLocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CaenCodeSpecializationPartnerLocationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
