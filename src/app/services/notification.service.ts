import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';

import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { Notification } from 'app/models/notification';
import { TranslateService } from './translate';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  public subject: BehaviorSubject<Notification>;
  constructor() {
    this.subject = new BehaviorSubject<Notification>(null);
   }

  alert(from: string, align: string, text: string, color?: NotificationTypeEnum , addToNotificationArray?: boolean, ) {
      const notification = new Notification();
      notification.from = from;
      notification.text = text;
      notification.align = align;
      notification.color = color;
      notification.addToNotificationArray = addToNotificationArray ? addToNotificationArray : false;
      this.subject.next(notification);
  }

  generalAlert(result: boolean, pageName: string, action: string, lang?: any) {
    const notification = new Notification();
    notification.from = 'top';
    notification.align = 'center';

    if (lang && lang == 'RO') {
      notification.text =  result ? `${pageName} - Datele au fost ${action} cu success!` :
       `${pageName} - Datele nu au fost ${action}! A aparut o eroare, va rugam sa vorbiti cu administratorul de sistem!`;
    } else {
      notification.text =  result ? `${pageName} - Data was ${action} with success!` :
       `${pageName} - Data not saved ${action}! An error occurred, please talk to your system administrator!`;
    }
  
    notification.color = result ? NotificationTypeEnum.Green : NotificationTypeEnum.Red;

    notification.addToNotificationArray = true;
    this.subject.next(notification);
}
}
