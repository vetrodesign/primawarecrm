export enum NotificationTypeEnum {
    Blue = 1,
    Green = 2,
    Yellow = 3,
    Red = 4,
    Primary = 5
  }