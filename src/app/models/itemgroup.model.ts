import { BaseDomain } from "./base-domain.model";

export class ItemGroup extends BaseDomain {
    public code: string;
    public nameRO: string;
    public nameENG: string;
    public shortNameRO: string;
    public shortNameENG: string;

    public pC_MAX: number;  
    public pC_APPR: number;  
    
    public packing: number;  
    public workmanship: number;  
    public environmentalTax: number;  
    public priceCompositionType: number;  

    public trS_MAX: number;
    public additionTrsMax: number;

    public trS_APPR: number;
    public additionTrsAppr: number;
    
    public additionD0: number;
    public additionD0Point: number;

    public additionC0: number;
    public additionC0Point: number;

    public additionR0: number;
    public additionR0Point: number;

    public orderNumber: number;
    
    public cmP_MAX: number;
    public currencyId: number;
    public customCodeId: number;
    public itemGroupCodeId: number;
    public itemGroupCategoryId: number;

    public cpvCodeIds: any;
    public itemGroupCpvCodes: any[];
    public nextCodeAvailable: string;

    public showInGeneralOffer: boolean;
    public isActive: boolean;


    //ItemProposed
    public clientId: number;
    public supplierId: number;
    public identifiedManufacturerId: number;
    public quantity: number;
    public price: number;
    public dateOfPurchase: Date;
}
