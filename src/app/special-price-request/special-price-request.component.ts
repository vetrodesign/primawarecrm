import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { DxDataGridComponent, DxTooltipComponent, DxValidatorComponent } from 'devextreme-angular';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { TranslateService } from 'app/services/translate';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import * as moment from 'moment';
import * as _ from 'lodash';
import { PartnerSearchFilter } from 'app/models/partnerSearchFilter.model';
import { PartnerService } from 'app/services/partner.service';
import { CityService } from 'app/services/city.service';
import { City } from 'app/models/city.model';
import { SmallCounty } from 'app/models/county.model';
import { SmallCountry } from 'app/models/country.model';
import { CountyService } from 'app/services/county.service';
import { CountryService } from 'app/services/country.service';
import { PartnerLocationService } from 'app/services/partner-location.service';
import { PartnerLocation } from 'app/models/partnerlocation.model';
import { PartnerActivityAllocationService } from 'app/services/partner-activity-allocation.service';
import { PartnerActivityAllocation } from 'app/models/partneractivityallocation.model';
import { PartnerActivityService } from 'app/services/partner-activity.service';
import { PartnerActivity } from 'app/models/partneractivity.model';
import { PartnerFinancialInfoService } from 'app/services/partner-financial-info.service';
import { DeliveryTypeEnum, EndureTransportEnum, OrderTypeEnum } from 'app/enums/specialPriceRequestEnum';
import { AuthService } from 'app/services/auth.service';
import { PaymentInstrumentService } from 'app/services/payment-instrument.service';
import { PaymentInstrument } from 'app/models/paymentinstrument.model';
import { PriceApprovedTooltipDataRequest, RequestItemsRowsData, SpecialPriceRequest, SpecialPriceRequestHeaderInfo, SpecialPriceRequestItem, SpecialPriceRequestItemData, UpdateAndValidateSpecialPriceRequestItems, ValidatedSpecialPriceRequestItems } from 'app/models/special-price-request.model';
import { ItemService } from 'app/services/item.service';
import { SpecialPriceRequestItemsService } from 'app/services/special-price-request-items.service';
import { SpecialPriceRequestService } from 'app/services/special-price-request.service';
import { PostService } from 'app/services/post.service';
import { UsersService } from 'app/services/user.service';
import { User } from 'app/models/user.model';
import { Post } from 'app/models/post.model';
import { Constants } from 'app/constants';
import { SalePriceListService } from 'app/services/sale-price-list.service';
import { SalePriceList } from 'app/models/salepricelist.model';
import { PartnerLocationDTO } from 'app/models/partnerlocationdto.model';
import { ProductConventionShortEnum } from 'app/enums/productConventionEnum';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'environments/environment';
import { ItemTurnoverComponent } from 'app/item-turnover/item-turnover.component';
import { on } from 'devextreme/events';
import { TurnoverPreorder } from 'app/models/turnover-preorder.model';
import { TurnoverPreorderService } from 'app/services/turnover-preorder.service';
import { HelperService } from 'app/services/helper.service';
import { VipOfferItemsService } from 'app/services/vip-offer-items.service';
import { VipOfferItems } from 'app/models/vip-offer-items.model';
import { VipOfferItemsDetailsService } from 'app/services/vip-offer-items-details.service';
import { CurrencyRateService } from 'app/services/currency-rate.service';
import { BaseCurrency } from 'app/models/base-currency.model';

const missingConventionPlaceholder: string = '(lipsa conventie)';

@Component({
  selector: 'app-special-price-request',
  templateUrl: './special-price-request.component.html',
  styleUrls: ['./special-price-request.component.css']
})
export class SpecialPriceRequestComponent implements OnInit {
  @Input() partners: any;
  @Input() isSpecialPriceRequest: boolean;

  @ViewChild('requestItemsDataGrid') requestItemsDataGrid: DxDataGridComponent;
  @ViewChild('requestItemsGridToolbar') requestItemsGridToolbar: GridToolbarComponent;

  @ViewChild('specialPriceRequestHistoryDataGrid') specialPriceRequestHistoryDataGrid: DxDataGridComponent;
  @ViewChild('specialPriceRequestHistoryGridToolbar') specialPriceRequestHistoryGridToolbar: GridToolbarComponent;

  @ViewChild('specialPriceRequestsDataGrid') specialPriceRequestsDataGrid: DxDataGridComponent;
  @ViewChild('specialPriceRequestsGridToolbar') specialPriceRequestsGridToolbar: GridToolbarComponent;

  @ViewChild('specialPriceRequestValidationGroup') specialPriceRequestValidationGroup: DxValidatorComponent;

  @ViewChild('itemTurnoverComponent') itemTurnoverComponent: ItemTurnoverComponent;
  @ViewChild('tooltipItemCode') tooltipItemCode: DxTooltipComponent;

  loaded: boolean;
  groupedText: string;

  specialPriceRequestPopupVisible: boolean;
  selectedPartner: any;
  cities: City[] = [];
  citiesDS: any;
  counties: SmallCounty[] = [];
  countiesDS: any;
  countries: SmallCountry[] = [];
  countriesDS: any;
  selectedPartnerImplicitLocation: PartnerLocation;
  partnerActivityAllocations: PartnerActivityAllocation[];
  partnerActivities: PartnerActivity[];
  selectedPartnerVdCurrentYear: number;
  selectedPartnerActivityAllocation: string;
  endureTransportEnum: { id: number; name: string }[] = [];
  orderTypeEnum: { id: number; name: string }[] = [];
  deliveryTypeEnum: { id: number; name: string }[] = [];
  isOwner: boolean;
  paymentInstruments: PaymentInstrument[];

  specialPriceRequest: SpecialPriceRequest = new SpecialPriceRequest();
  requestItemsActions: any;
  specialPriceRequestHistoryActions: any;
  selectedRequestItemsRows: any[];
  selectedRequestItemIndex = -1;
  itemDS: any;
  partnersDS: any;
  requestItems: SpecialPriceRequestItem[] = [];
  specialPriceRequestHistory: SpecialPriceRequestItem[] = [];
  specialPriceRequests: SpecialPriceRequestItem[] = [];
  users: User[];
  posts: Post[];
  isGeneralDirectorFlag: boolean;
  currentUserPost: Post;
  salePriceLists: SalePriceList[];
  clientCode: string;
  selectedClientTurnover: number;
  isDataLoaded: boolean;
  priceApprovedTooltipData: { [key: number]: any } = {};
  allPriceApprovedTooltipData: any[] = [];
  selectedPartnerComputedLocation: string;
  selectedPartnerComputedFinancialInfo: string;
  selectedPartnerComputedAgent: string;
  isRequestItemsSectionHidden: boolean;
  selectedRequestItemsKeys: string[] = [];
  selectedSpecialPriceRequestId: number;
  isItemTurnoverPopupOpened: boolean;
  filterTurnoverItemCode: string;
  isDataLoading: boolean;
  rejectedRequestPriceApprovedTooltipData: { [key: number]: any } = {};
  isFinancialInfoPopupDisplayed: boolean = false;
  shouldDisplayFinancialInfoPopup: boolean = false;
  allTablesContentReadyCounter: number = 0;
  showFinancialInfoPopup: boolean;
  vipOfferItems: VipOfferItems[] = [];
  timeout: number;
  tooltipData: any = {};
  stockTooltipData: any = {};
  currencies: BaseCurrency[] = [];
  currencyName: string;
  isHistoryButtonClicked: boolean;

  offeredValue: number = 0;
  requestedPercentage: number = 0;
  requestedAddition: number = 0;
  approvedValue: number = 0;
  approvedPercentage: number = 0;
  approvedAddition: number = 0;

  requestItemsGrossWeight: number = 0;
  requestItemsVolumetricWeight: number = 0;
  estimatedTransport: number = 0;
  transportPercentage: number = 0;

  isEditAllowOnApprovedQuantity: boolean;

  constructor(
    private translationService: TranslateService,
    private notificationService: NotificationService,
    private partnerService: PartnerService,
    private cityService: CityService,
    private countyService: CountyService,
    private countryService: CountryService,
    private partnerLocationService: PartnerLocationService,
    private partnerActivityAllocationService: PartnerActivityAllocationService,
    private partnerActivityService: PartnerActivityService,
    private financialInfoService: PartnerFinancialInfoService,
    private authenticationService: AuthService,
    private paymentInstrumentService: PaymentInstrumentService,
    private itemsService: ItemService,
    private specialPriceRequestsItemsService: SpecialPriceRequestItemsService,
    private specialPriceRequestService: SpecialPriceRequestService,
    private postService: PostService,
    private userService: UsersService,
    private salePriceListService: SalePriceListService,
    private route: ActivatedRoute,
    private turnoverPreorderService: TurnoverPreorderService,
    private helperService: HelperService,
    private vipOfferItemsService: VipOfferItemsService,
    private vipOfferItemDetailsService: VipOfferItemsDetailsService,
    private currencyService: CurrencyRateService) {
      
    this.setSpecialPriceRequestHistoryGridInstance();
    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();

    if (this.route.snapshot.queryParamMap.get('clientCode') !== undefined && this.route.snapshot.queryParamMap.get('clientCode') !== null) {
      this.clientCode = this.route.snapshot.queryParamMap.get('clientCode'); 

      if (this.route.snapshot.queryParamMap.get('selectedClientTurnover') !== undefined && this.route.snapshot.queryParamMap.get('selectedClientTurnover') !== null) {
        this.selectedClientTurnover = Number(this.route.snapshot.queryParamMap.get('selectedClientTurnover'));
      }

      if (this.route.snapshot.queryParamMap.get('specialPriceRequestId') !== undefined && this.route.snapshot.queryParamMap.get('specialPriceRequestId') !== null) {
        this.selectedSpecialPriceRequestId = Number(this.route.snapshot.queryParamMap.get('specialPriceRequestId'));
      }

      this.getData();
    }

    this.authenticationService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authenticationService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });

    for (let n in EndureTransportEnum) {
      if (typeof EndureTransportEnum[n] === 'number') {
        this.endureTransportEnum.push({
          id: <any>EndureTransportEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }

    for (let n in OrderTypeEnum) {
      if (typeof OrderTypeEnum[n] === 'number') {
        this.orderTypeEnum.push({
          id: <any>OrderTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }

    for (let n in DeliveryTypeEnum) {
      if (typeof DeliveryTypeEnum[n] === 'number') {
        this.deliveryTypeEnum.push({
          id: <any>DeliveryTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }
  }

  ngOnInit(): void {
    if (this.isSpecialPriceRequest) {
      this.openSpecialPriceRequestPopup();
    }

    if (!this.isFinancialInfoPopupDisplayed) this.isFinancialInfoPopupDisplayed = true;
  }

  setSpecialPriceRequestHistoryGridInstance() {
    if (this.specialPriceRequestHistoryDataGrid && this.specialPriceRequestHistoryGridToolbar) {
      this.specialPriceRequestHistoryGridToolbar.dataGrid = this.specialPriceRequestHistoryDataGrid;
      this.specialPriceRequestHistoryGridToolbar.setGridInstance();
    }
  }

  setSpecialPriceRequestsGridInstance() {
    if (this.specialPriceRequestsDataGrid && this.specialPriceRequestsGridToolbar) {
      this.specialPriceRequestsGridToolbar.dataGrid = this.specialPriceRequestsDataGrid;
      this.specialPriceRequestsGridToolbar.setGridInstance();
    }
  }

  ngAfterViewInit() {
    if (this.specialPriceRequestHistoryDataGrid && this.specialPriceRequestHistoryGridToolbar) {
      this.specialPriceRequestHistoryGridToolbar.dataGrid = this.specialPriceRequestHistoryDataGrid;
      this.specialPriceRequestHistoryGridToolbar.setGridInstance();
    }

    if (this.requestItemsGridToolbar && this.requestItemsDataGrid) {
      this.requestItemsGridToolbar.dataGrid = this.requestItemsDataGrid;
      this.requestItemsGridToolbar.setGridInstance();
    }

    if (this.specialPriceRequestsDataGrid && this.specialPriceRequestsGridToolbar) {
      this.specialPriceRequestsGridToolbar.dataGrid = this.specialPriceRequestsDataGrid;
      this.specialPriceRequestsGridToolbar.setGridInstance();
    }
  }

  isGeneralDirector() {
    if (this.posts && this.posts.length > 0 && this.users && this.users.length > 0) {
      let post = this.posts?.find(x => x.id === this.users.find(x => x.id === Number(this.authenticationService.getUserId()))?.postId);
      if (post) {
        this.currentUserPost = post;
        if (Constants.specialPriceRequestValidationAccessPostIds.includes(post.id)) {
          return true;
        } else {
          return false;
        }
      }
    }
  }

  setActions() {
    this.requestItemsActions = { CanView: false, CanAdd: true, CanUpdate: false, CanDelete: true, 
      CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };

    this.specialPriceRequestHistoryActions = { CanView: false, CanAdd: false, CanUpdate: false, CanDelete: false, 
      CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
  }

  async getData() {
    this.loaded = true;
    Promise.all([this.getPartnerData(), this.getCities(), this.getCounties(), this.getCountries(), this.getUsers(), this.getSalePriceLists(),
      this.getPartnerActivityAllocations(), this.getPartnerActivity(), this.getPaymentInstruments(), this.getItems(), this.getPartners(), this.getPosts(),
      this.getVipOfferItems(), this.getCurrencies()
    ]).then(async () => {

      await this.getPartnerVdCurrentYear();
      await this.getPartnerImplicitLocation();

      if (this.selectedSpecialPriceRequestId) {
        await this.getSelectedSpecialPriceRequest();
      }

      await this.getPartnerSpecialPriceRequests();

      if (this.selectedPartnerImplicitLocation.partnerActivityAllocationId) {
        const pActivityAlloc = this.partnerActivityAllocations.find(x => x.id === this.selectedPartnerImplicitLocation.partnerActivityAllocationId);

        if (pActivityAlloc) {
          const pActivity = this.partnerActivities.find(f => f.id === pActivityAlloc.partnerActivityId);

          this.selectedPartnerActivityAllocation = `${pActivityAlloc.saleZoneCode} - ${pActivity.name} - ${pActivityAlloc.partnerActivityClassification}`; 
        }
      }

      this.isGeneralDirectorFlag = this.isGeneralDirector();
      this.requestItems = [];

      if (this.selectedClientTurnover) {
        let requestItem = new SpecialPriceRequestItem();
        requestItem.itemId = this.selectedClientTurnover;
        this.requestItems.push(requestItem);
      }

      let post = this.posts.find(f => f.id === this.selectedPartner.salesAgentId);
      if (post) {
        let user = this.users.find(f => f.postId === post.id);

        if (user) {
          this.selectedPartnerComputedAgent = post.companyPost + " / " + user.firstName + " " + user.lastName;
        } else {
          this.selectedPartnerComputedAgent = post.companyPost + " / ";
        }
      }

      this.isRequestItemsSectionHidden = this.isGeneralDirectorFlag;

      this.specialPriceRequestPopupVisible = true;
      this.isDataLoaded = true;
      this.loaded = false;
    });
  }

  async getVipOfferItems(): Promise<void> {
    await this.vipOfferItemsService.getAllValidAsync().then(items => {
      this.vipOfferItems = (items && items.length > 0) ? items.filter(x => x.isActive == true) : [];
    });
  }

  async getSalePriceLists(): Promise<any> {
    if (this.isOwner) {
      await this.salePriceListService.getAllSalePriceListsWithProductConventionsAsync().then(i => {
        this.salePriceLists = i;
      });
    } else {
      await this.salePriceListService.getSalePriceListsAsyncByIDWithProductConventions().then(i => {
        this.salePriceLists = i;
      });
    }
  }

  async getUsers() {
    if (this.isOwner) {
      await this.userService.getUsersAsync().then(items => {
        this.users = items;
      });
    } else {
      await this.userService.getUserAsyncByID().then(items => {
        this.users = items;
      });
    }
  }

  async getPosts() {
    if (this.isOwner) {
      await this.postService.getAllPostsAsync().then(items => {
        this.posts = items;
      });
    } else {
      await this.postService.getPostsByCustomerId().then(items => {
        this.posts = items;
      });
    }
  }

  async getPartnerData() {
    if (this.clientCode) {
      let searchFilter = new PartnerSearchFilter();
      searchFilter.code = this.clientCode;
      searchFilter.isActive = true;
      await this.partnerService.getPartnersByFilter(searchFilter).then(items => {
        if (items && items.length > 0) {
          this.selectedPartner = items[0];
        }
      })
    }
  }

  async getCurrencies() {
    await this.currencyService.getBaseCurrency().then(items => {
      this.currencies = items;
    });
  }

  async getCities() {
    await this.cityService.getAllCitiesSmallAsync().then(items => {
      this.cities = items;
      this.citiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.cities
      };
    });
  }

  async getCounties() {
    await this.countyService.getAllSmallCountiesAsync().then(items => {
      this.counties = items;
      this.countiesDS = {
        paginate: true,
        pageSize: 15,
        store: this.counties
      };
    });
  }

  async getCountries() {
    await this.countryService.getAllSmallCountriesAsync().then(items => {
      this.countries = items;
      this.countriesDS = {
        paginate: true,
        pageSize: 15,
        store: this.countries
      };
    });
  }

  async getSelectedSpecialPriceRequest() {
    await this.specialPriceRequestService.getBySpecialPriceRequestId(this.selectedSpecialPriceRequestId).then(items => {
      if (items && items.length > 0) {
        this.specialPriceRequest = items[0];
      }
    });
  }

  async getPartnerImplicitLocation() {
    if (this.selectedPartner && this.selectedPartner.id) {
      await this.partnerLocationService.getPartnerLocationsByPartnerID(this.selectedPartner.id).then(items => {
        if (items && items.length > 0) {
          this.selectedPartnerImplicitLocation = items.find(f => f.isImplicit) || items.find(f => f.isActive);

          if (this.selectedPartnerImplicitLocation) {
            this.partnerService.getPartnerProductConventionIdsByLocationsAsync([this.selectedPartnerImplicitLocation.id]).then((r: PartnerLocationDTO[]) => {
              if (r) {        
                const item = r.find(e => e.locationId == this.selectedPartnerImplicitLocation.id);
                if (item) {
                  const conventionId = item.conventionId;

                  if (conventionId) {
                    this.selectedPartnerImplicitLocation.productConventionName = ProductConventionShortEnum[conventionId];
                    this.selectedPartnerImplicitLocation.productConventionId = conventionId;
                  } else {
                    this.selectedPartnerImplicitLocation.productConventionName = missingConventionPlaceholder;
                  }
                } else {
                  this.selectedPartnerImplicitLocation.productConventionName = missingConventionPlaceholder;
                }          
              }
            });

            let countryName = this.countries.find(f => f.id === this.selectedPartnerImplicitLocation.countryId)?.name;
            let countyName = this.counties.find(f => f.id === this.selectedPartnerImplicitLocation.countyId)?.name;
            let cityName = this.cities.find(f => f.id === this.selectedPartnerImplicitLocation.cityId)?.name;
            this.selectedPartnerComputedLocation = countryName + " / " + countyName + " / " + cityName;

            let currency = this.currencies.find(f => f.id === this.selectedPartnerImplicitLocation.baseCurrencyId);
            if (currency) {
              this.currencyName = currency.name;
            }
          }
        }
      });
    }
  }

  async getPartnerActivityAllocations(): Promise<any> {
    await this.partnerActivityAllocationService.getAllPartnerActivityAllocationAsync().then(items => {
      this.partnerActivityAllocations = items;
    });
  }

  async getPartnerActivity() {
    await this.partnerActivityService.getAllPartnerActivityAsync().then(items => {
      this.partnerActivities = items;
    });
  }

  getDisplayExprCities(item) {
    if (!item) {
      return '';
    }
    return item.name + ' - ' + item.countyName + ' - ' + item.countryName;
  }

  getDisplayExprPaymentInstruments(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }



  async getPriceApprovedTooltipData(shouldGetHistory?: boolean) {
    var request = new PriceApprovedTooltipDataRequest();

    if (!shouldGetHistory) {
      request.itemIds = this.specialPriceRequests.map(m => m.itemId);
    } else if (shouldGetHistory) {
      request.itemIds = this.specialPriceRequestHistory.map(m => m.itemId);
    }

    request.partnerId = this.selectedPartner.id;
    request.shouldGetHistory = shouldGetHistory;

    await this.specialPriceRequestsItemsService.getPriceApprovedTooltipData(request).then(data => {
      this.allPriceApprovedTooltipData = data.map(m => {
        let item = this.itemDS.store.find(f => f.id === m.itemId);
        let itemData = null;
        if (item) {
          itemData = item.code + " - " + item.name;
        }

        return {
          ...m, 
          created: moment(m.created).format('DD.MM.YYYY').toString(),
          priceRequested: m.priceRequested != null ? parseFloat(m.priceRequested).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) + " " + this.currencyName : '',
          priceApproved: m.priceApproved != null ? parseFloat(m.priceApproved).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) + " " + this.currencyName : '',
          quantity: Number(m.quantity).toLocaleString('en-US'),
          approvedQuantity: m.approvedQuantity && Number(m.approvedQuantity).toLocaleString('en-US'),
          itemData: itemData
        }
      });
    });
  }

  async getPartnerVdCurrentYear() {
    if (this.selectedPartner && this.selectedPartner.id) {
      await this.financialInfoService.getByPartnerIdAsync(this.selectedPartner.id).then(items => {
        if (items) {
          let currentDate = new Date();
          let currentMonth = currentDate.getMonth();
          let isInJanuaryToJune = currentMonth >= 0 && currentMonth <= 5;
          let isInJulyToDecembre = currentMonth >= 6 && currentMonth <= 11;

          let currentYear = currentDate.getFullYear();

          let aux;
          if (isInJanuaryToJune) {
            aux = items.find(f => f.year === currentYear - 2);
          } else if (isInJulyToDecembre) {
            aux = items.find(f => f.year === currentYear - 1);
          }

          if (aux) {
            this.selectedPartnerVdCurrentYear = aux.turnover;
            if (!aux.ca && !aux.numberOfEmployees && !aux.profitLoss) {
              this.selectedPartnerComputedFinancialInfo = "";
            } else {
              const formattedCa = aux.ca ? this.helperService.setNumberWithCommas(this.helperService.roundUp(aux.ca)) : " ";
              const formattedNumberOfEmployees = aux.numberOfEmployees ? this.helperService.setNumberWithCommas(this.helperService.roundUp(aux.numberOfEmployees)) : " ";
              const formattedProfitLoss = aux.profitLoss ? this.helperService.setNumberWithCommas(this.helperService.roundUp(aux.profitLoss)) : " ";
              
              this.selectedPartnerComputedFinancialInfo = `${formattedCa} / ${formattedProfitLoss} / ${formattedNumberOfEmployees}`;
            }

            if ((!aux.ca || !aux.numberOfEmployees || !aux.profitLoss) && !this.selectedSpecialPriceRequestId) {
              this.shouldDisplayFinancialInfoPopup = true;
            }
          } else {
            this.selectedPartnerVdCurrentYear = 0;
          }
        }
      });
    }
  }

  async getPaymentInstruments() {
    if (this.isOwner) {
      await this.paymentInstrumentService.getAllPaymentInstrumentsAsync().then(items => {
        this.paymentInstruments = items;
      });
    } else {
      await this.paymentInstrumentService.getAllPaymentInstrumentsByCustomerIdAsync().then(items => {
        this.paymentInstruments = items;
      });
    }
  }

  openSpecialPriceRequestPopup() {
    this.getData();
  }

  public refreshRequestItems() {
    this.requestItemsDataGrid.instance.refresh();
  }

  public refreshSpecialPriceRequestHistory() {
    this.getPartnerSpecialPriceRequests(true);
    this.specialPriceRequestHistoryDataGrid.instance.refresh();
  } 

  public refreshSpecialPriceRequests() {
    this.getPartnerSpecialPriceRequests();
    this.specialPriceRequestsDataGrid.instance.refresh();
  } 

  public addRequestItem() {
    this.requestItemsDataGrid.instance.addRow();
  }

  selectionRequestItemChange(data: any) {
    this.selectedRequestItemsRows = data.selectedRowsData;
    this.selectedRequestItemIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  getDisplayExprItems(item) {
    if (!item) {
      return '';
    }

    return item.code + ' - ' + item.name;
  }

  async getItems(): Promise<any> {
    await this.itemsService.getAllItemSmallAsync().then(items => {
      this.itemDS = {
        paginate: true,
        pageSize: 15,
        store: items
      };
    });
  }

  getDisplayExprPartners(partner) {
    if (!partner) {
      return '';
    }

    const partnerName: string = partner.name;
    const partnerCode: string = partner.code;

    let name: string = '(lipsa nume)';
    let code: string = '(lipsa cod)';

    if (partnerName !== undefined && partnerName !== null && partnerName !== '') {
      name = partnerName;
    }

    if (partnerCode !== undefined && partnerCode !== null && partnerCode !== '') {
      code = partnerCode;
    }

    return name + ' - ' + code;
  }

  async getPartners() {
    await this.partnerService.getAllPartnersSmallAsync().then(partners => {
      this.partnersDS = {
        paginate: true,
        pageSize: 15,
        store: partners
      }
    })
  }

  getProductConventionType(partnerProductConventionId: number, item: any): number {
    switch(partnerProductConventionId) {
      case ProductConventionShortEnum.C0:
        return item.c0;
      case ProductConventionShortEnum.D0:
        return item.d0;
      case ProductConventionShortEnum.R0:
        return item.r0;
      default:
        return 0;
    }
  }

  async getPartnerSpecialPriceRequests(shouldGetHistory?: boolean) {
    if (this.selectedPartner && this.selectedPartner.id) {

      if (!shouldGetHistory) {
        this.specialPriceRequests = [];
      }
      
      this.loaded = true;
      await this.specialPriceRequestsItemsService.getAllByPartnerIdAsync(this.selectedPartner.id, shouldGetHistory).then(async items => {
        if (items && items.length > 0) {
          let allRequests = items.map(item => {
            item.createdFormated = moment(new Date(item.created)).format('DD.MM.YYYY');

            let partnerProductConventionName = this.selectedPartnerImplicitLocation?.productConventionName;
            let partnerProductConventionId = this.selectedPartnerImplicitLocation?.productConventionId;

            let salePriceList = this.salePriceLists.find(f => f.id === item.salePriceListId && f.salePriceListXProductConventionList && f.salePriceListXProductConventionList.map(m => m.productConventionId).includes(partnerProductConventionId));

            let salePriceListCode;
            if (salePriceList) {
              if (salePriceList.name && salePriceList.name.toLowerCase().replace(/\s+/g, '').includes(Constants.salePriceListPpName)) {
                salePriceListCode = `PP${partnerProductConventionName}`;
              } else {
                salePriceListCode = `${partnerProductConventionName}`;
              }
            } else {
              salePriceListCode = `${partnerProductConventionName}`;
            }

            let auxBaxValue = item.baxValue ? (item.quantity / item.baxValue) % 1 !== 0 ? ((item.quantity / item.baxValue).toFixed(2)) : (item.quantity / item.baxValue) : 0;
            item.baxValueComputed = auxBaxValue.toString();
            
            let productConventionType = this.getProductConventionType(partnerProductConventionId, item);

            let auxDiscountGrid = productConventionType ? Math.round(((item.priceOfferedWithoutTva - productConventionType) / productConventionType) * 100) : 0;
            let auxCmp = item.discountGridCmp ? Math.round(((item.priceOfferedWithoutTva - item.discountGridCmp) / item.discountGridCmp) * 100) : 0;
            item.discountGridCmpComputed = `${salePriceListCode} ${auxDiscountGrid}%(${auxCmp}%)`; 
            item.auxDiscountGrid = auxDiscountGrid;
            item.auxCmp = auxCmp;
            item.salePriceListCode = salePriceListCode;

            let auxDiscountGridRequested = productConventionType ? Math.round(((item.priceRequestedWithoutTva - productConventionType) / productConventionType) * 100) : 0;
            let auxCmpRequested = item.discountGridCmp ? Math.round(((item.priceRequestedWithoutTva - item.discountGridCmp) / item.discountGridCmp) * 100) : 0;
            item.discountGridCmpRequestedComputed = `${salePriceListCode} ${auxDiscountGridRequested}%(${auxCmpRequested}%)`; 
            item.auxDiscountGridRequested = auxDiscountGridRequested;
            item.auxCmpRequested = auxCmpRequested;
        
            let auxDiscountGridApproved = productConventionType ? Math.round(((item.priceApproved - productConventionType) / productConventionType) * 100) : 0;
            let auxCmpRequestedApproved = item.discountGridCmp ? Math.round(((item.priceApproved - item.discountGridCmp) / item.discountGridCmp) * 100) : 0;
            item.approvedDiscountGridCmp = item.priceApproved ? `${salePriceListCode} ${auxDiscountGridApproved}%(${auxCmpRequestedApproved}%)` : "";
            item.auxDiscountGridApproved = auxDiscountGridApproved;
            item.auxCmpRequestedApproved = auxCmpRequestedApproved;

            if (item.isRejected === true || item.isRejected === false) {
              let user = this.users.find(f => f.id === item.modifiedBy);
              if (user) {
                let post = this.posts?.find(x => x.id === user.postId);
                item.approvingUser = user.firstName + " " + user.lastName;

                if (post) {
                  item.approvingUser = post.code + " " + item.approvingUser;
                }
              }
            }

            item.d0Andc0 = `${item.d0 ? item.d0.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '0.00'} (${item.c0 ? item.c0.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '0.00'})` + " " + this.currencyName;

            item.priceOfferedWithoutTva = item.priceOfferedWithoutTva && +item.priceOfferedWithoutTva.toFixed(2);
            item.priceRequestedWithoutTva = item.priceRequestedWithoutTva && +item.priceRequestedWithoutTva.toFixed(2);
            item.priceApproved = item.priceApproved && +item.priceApproved.toFixed(2);

            let splitTurnoverRs = item.turnOverRs.split(" / ");
            let formattedSplitTurnoverRs = splitTurnoverRs.map(m => {
              return Number(m).toLocaleString('en-US');
            })
            item.turnoverRsComputed = formattedSplitTurnoverRs.join(" / ");

            if (item.isRejected === true) {
              item.priceApproved = item.priceOfferedWithoutTva;
            }

            let auxSplitTurnoverRs = splitTurnoverRs.map(Number);
            let sumTurnoverRs = auxSplitTurnoverRs.reduce((acc, num) => acc + num, 0);
            let averageTurnoverRs = auxSplitTurnoverRs.length > 0 ? sumTurnoverRs / auxSplitTurnoverRs.length : 0;
            item.isStockAvailable = (item.stock - item.quantity) > averageTurnoverRs * 3;

            item.isAtOrder = false;

            return item;
          });
                    
          if (!shouldGetHistory) {
            if (this.selectedSpecialPriceRequestId) {
              this.specialPriceRequests = allRequests.filter(f => (f.isRejected === null || f.isRejected === undefined) && f.specialPriceRequestId === this.selectedSpecialPriceRequestId);
            } else {
              this.specialPriceRequests = allRequests.filter(f => f.isRejected === null || f.isRejected === undefined);
            }
          }

          if (shouldGetHistory) {
            this.specialPriceRequestHistory = allRequests.filter(f => f.isRejected === false || f.isRejected === true);
          }
        }
        
        await this.getPriceApprovedTooltipData(shouldGetHistory);
        
        this.calculateRequestDetails();
        this.loaded = false;
      })
    }
  }

  async onAddSpecialPriceRequestClick() {
    if (this.specialPriceRequestValidationGroup.instance.validate().isValid) {
      if (this.specialPriceRequest && this.selectedPartner && this.selectedPartner.id && this.currentUserPost && this.currentUserPost.id) {
        this.specialPriceRequest.postId = this.currentUserPost.id;
        this.specialPriceRequest.partnerId = this.selectedPartner.id;
        this.loaded = true;
        await this.specialPriceRequestService.createAsync(this.specialPriceRequest).then(async (resp) => {
          
          if (resp) {
            this.requestItems.forEach(f => f.specialPriceRequestId = resp.id);

            let headerInfo = new SpecialPriceRequestHeaderInfo();
            headerInfo.partnerId = this.selectedPartner.id;
            headerInfo.partnerCode = this.selectedPartner.code;
            headerInfo.partnerName = this.selectedPartner.name;
            headerInfo.partnerType = this.selectedPartnerActivityAllocation;
            headerInfo.partnerTradeRegisterNumber = this.selectedPartner.tradeRegisterNumber;
            headerInfo.partnerCountry = this.countries.find(f => f.id === this.selectedPartnerImplicitLocation.countryId)?.name;
            headerInfo.partnerCounty = this.counties.find(f => f.id === this.selectedPartnerImplicitLocation.countyId)?.name;
            headerInfo.partnerCity = this.cities.find(f => f.id === this.selectedPartnerImplicitLocation.cityId)?.name;
            headerInfo.partnerCaLastYear = this.selectedPartner.caLastYear;
            headerInfo.partnerNaLastYear = this.selectedPartner.naLastYear;
            headerInfo.partnerVdCurrentYear = this.selectedPartnerVdCurrentYear;
            headerInfo.endureTransport = this.endureTransportEnum.filter(f => f.id === this.specialPriceRequest.endureTransportId)[0]?.name;
            let auxTransportCity = this.cities.find(f => f.id === this.specialPriceRequest.transportCityId);
            if (auxTransportCity) {
              headerInfo.transportCity = auxTransportCity.name + " - " + auxTransportCity.countyName + " - " + auxTransportCity.countryName;
            }
            headerInfo.cityNemoDistance = this.specialPriceRequest.cityNemoDistance;
            headerInfo.orderType = this.orderTypeEnum.filter(f => f.id === this.specialPriceRequest.orderTypeId)[0]?.name;
            headerInfo.deliveryType = this.deliveryTypeEnum.filter(f => f.id === this.specialPriceRequest.deliveryTypeId)[0]?.name;
            let auxPaymentInstrument = this.paymentInstruments.find(f => f.id === this.specialPriceRequest.paymentInstrumentId);
            headerInfo.paymentInstrument = auxPaymentInstrument?.code + " - " + auxPaymentInstrument?.name;
            headerInfo.paymentTerm = this.specialPriceRequest.paymentTerm;
            headerInfo.deliveryTerm = this.specialPriceRequest.deliveryTerm;
            headerInfo.clientRequestInfo = this.specialPriceRequest.clientRequestInfo;
            headerInfo.clientRequestComments = this.specialPriceRequest.clientRequestComments;
            let currentUser = this.users.find(x => x.id === Number(this.authenticationService.getUserId()));
            headerInfo.postName = this.currentUserPost?.code + " - " + currentUser?.lastName + " " + currentUser?.firstName;

            let specialPriceRequestItemData = new SpecialPriceRequestItemData();
            this.requestItems = this.requestItems.map((m: any) => {
              let { __KEY__, ...validItem } = m;
              return validItem;
            });
            
            headerInfo.requestItemsRowsData = [];
            this.requestItems.forEach(item => {
              let aux = new RequestItemsRowsData();
              aux.itemName = this.itemDS.store.find(f => f.id === item.itemId)?.code + " - " + this.itemDS.store.find(f => f.id === item.itemId)?.name;
              aux.quantity = item.quantity;
              aux.priceOffered = item.priceOfferedWithoutTva;
              aux.priceRequested = item.priceRequestedWithoutTva;
              let auxCompetingPartner = this.partnersDS.store.find(f => f.id === item.competingPartnerId);
              if (auxCompetingPartner) {
                aux.competingPartner = auxCompetingPartner.name + " - " + auxCompetingPartner.code;
              }

              headerInfo.requestItemsRowsData.push(aux);
            })
         
            specialPriceRequestItemData.specialPriceRequestItemsVMs = this.requestItems;
            specialPriceRequestItemData.headerInfo = headerInfo;
            
            await this.specialPriceRequestsItemsService.createMultipleAsync(specialPriceRequestItemData).then(async () => {
  
              await this.getPartnerSpecialPriceRequests().then(() => {
  
                this.specialPriceRequest = new SpecialPriceRequest();
                this.requestItems = [];
  
                this.notificationService.alert('top', 'center', 'Cerere Pret Special - Datele au fost salvate cu succes!', NotificationTypeEnum.Green, true)
                this.loaded = false;
              })
            })
          }
        })
      }
    }
  }

  async validateSpecialPriceRequest() {
    this.loaded = true;
    let validatedSpecialPriceRequestItems = [];
    this.specialPriceRequests = this.specialPriceRequests.map(m => ({ ...m, isSentToSalesAgent: false }));
    let availableRequestItems = this.specialPriceRequests.filter(f => !f.isSentToSalesAgent && (f.isRejected === false || f.isRejected === true));
    
    let availableRequestItemIds = availableRequestItems.map(m => m.id);
    availableRequestItems.forEach(item => {
      let aux = new ValidatedSpecialPriceRequestItems();
      aux.itemId = item.itemId;
      let auxItemName = this.itemDS.store.find(f => f.id === item.itemId);
      aux.itemName = auxItemName?.code + " - " + auxItemName?.name; 
      aux.quantity = item.quantity;
      aux.baxValue = item.baxValueComputed;
      aux.d0 = item.d0;
      aux.c0 = item.c0;
      aux.priceOffered = item.priceOfferedWithoutTva?.toFixed(2);
      aux.priceRequested = item.priceRequestedWithoutTva?.toFixed(2);
      aux.discountGridCmp = item.discountGridCmpComputed;
      aux.discountGridRequestedCmp = item.discountGridCmpRequestedComputed;
      let auxCompetingPartner = this.partnersDS.store.find(f => f.id === item.competingPartnerId);
      if (auxCompetingPartner) {
        aux.competingPartner = auxCompetingPartner.name + " - " + auxCompetingPartner.code;
      }
      aux.stockDragan01 = item.stock;
      aux.turnOverRs = item.turnOverRs;
      aux.priceApproved = item.priceApproved?.toFixed(2);
      aux.approvedPaymentConditions = item.approvedPaymentConditions;
      aux.approvedDiscountGridCmp = item.approvedDiscountGridCmp;
      aux.isRejected = item.isRejected;
      aux.created = item.created;
      aux.approvingUser = item.approvingUser;
      aux.createdBy = item.createdBy;
      aux.approvedQuantity = item.approvedQuantity;
      aux.specialPriceRequestId = this.selectedSpecialPriceRequestId;
      aux.clientRequestComments = this.specialPriceRequest.clientRequestComments;
      aux.cmp = item.discountGridCmp;
      aux.specialPriceRequestItemId = item.id;
      aux.originalBaxValue = item.baxValue;
      aux.isAtOrder = item.isAtOrder;
      aux.currencyName = this.currencyName;

      validatedSpecialPriceRequestItems.push(aux);
    })

    let requestObj = new UpdateAndValidateSpecialPriceRequestItems();
    this.specialPriceRequests = this.specialPriceRequests.map(m => {
      if (availableRequestItemIds.includes(m.id)) {
        m.isSentToSalesAgent = true;
      }
      return m;
    })

    requestObj.specialPriceRequestItemsVMs = this.specialPriceRequests.filter(f => validatedSpecialPriceRequestItems.map(m => m.itemId).includes(f.itemId));
    requestObj.validatedSpecialPriceRequestItems = validatedSpecialPriceRequestItems;

    let createPreorders = [];
    validatedSpecialPriceRequestItems.forEach(item => {
      let createPreoder = new TurnoverPreorder();
      createPreoder.customerId = Number(this.authenticationService.getUserCustomerId());
      createPreoder.partnerId = this.selectedPartner.id;
      createPreoder.postId = Number(this.authenticationService.getUserPostId());
      createPreoder.itemId = item.itemId;
      createPreoder.quantity = item.quantity;
      createPreorders.push(createPreoder);
    })

    await this.specialPriceRequestsItemsService.updateAsync(requestObj).then(async (response) => {
      if (response && !response.isError) {
        await this.getPartnerSpecialPriceRequests().then(async () => {

          await this.turnoverPreorderService.createMultipleTurnoverPreorderAsync(createPreorders).then(() => {
            this.specialPriceRequest.clientRequestInfo = null;
            this.specialPriceRequest.clientRequestComments = null;
            
            this.notificationService.alert('top', 'center', 'Cerere Pret Special - Datele au fost salvate cu succes!', NotificationTypeEnum.Green, true)
            this.loaded = false;
          })
        })
      }

      this.loaded = false;
    })
  }

  onRowUpdated(e: any) {
    if (e && e.data) {
      this.updateRow(e.data);
    }
    this.specialPriceRequests = [...this.specialPriceRequests];
    this.calculateRequestDetails();
  }

  updateRow(updatedRow: any) {
    const rowIndex = updatedRow.id;
    const row = this.specialPriceRequests.find(item => item.id === rowIndex);
  
    if (row) {
      // if (row.approvedQuantity > row.stock) {
      //   if (!row.isAtOrder) {
      //     this.notificationService.alert('top', 'center', 'Nu puteti introduce o cantitate aprobata mai mare decat stocul!', NotificationTypeEnum.Red, true);
      //     row.approvedQuantity = null;
      //   }
      // }

      if (row.isAtOrder && !this.isEditAllowOnApprovedQuantity) {
        updatedRow.approvedQuantity = row.quantity;
      }

      if (row.isAtOrder) {
        this.isEditAllowOnApprovedQuantity = true;
      }

      let partnerProductConventionName = this.selectedPartnerImplicitLocation?.productConventionName;
      let partnerProductConventionId = this.selectedPartnerImplicitLocation?.productConventionId;
      let productConventionType = this.getProductConventionType(partnerProductConventionId, updatedRow);
    
      let salePriceList = this.salePriceLists.find(f => f.id === updatedRow.salePriceListId && f.salePriceListXProductConventionList && f.salePriceListXProductConventionList.map(m => m.productConventionId).includes(partnerProductConventionId));
    
      let salePriceListCode;
      if (salePriceList) {
        if (salePriceList.name && salePriceList.name.toLowerCase().replace(/\s+/g, '').includes(Constants.salePriceListPpName)) {
          salePriceListCode = `PP${partnerProductConventionName}`;
        } else {
          salePriceListCode = `${partnerProductConventionName}`;
        }
      } else {
        salePriceListCode = `${partnerProductConventionName}`;
      }
    
      if (updatedRow.priceApproved) {   
        let auxDiscountGridApproved = productConventionType ? Math.round(((updatedRow.priceApproved - productConventionType) / productConventionType) * 100) : 0;
        let auxCmpRequestedApproved = updatedRow.discountGridCmp ? Math.round(((updatedRow.priceApproved - updatedRow.discountGridCmp) / updatedRow.discountGridCmp) * 100) : 0;
        updatedRow.auxDiscountGridApproved = auxDiscountGridApproved;
        updatedRow.auxCmpRequestedApproved = auxCmpRequestedApproved;
        updatedRow.approvedDiscountGridCmp = `${salePriceListCode} ${auxDiscountGridApproved}%(${auxCmpRequestedApproved}%)`;

        if (updatedRow.priceApproved == updatedRow.priceOfferedWithoutTva) {
          updatedRow.isRejected = true;
        } else {
          updatedRow.isRejected = false;
        }  
      } 
    }
  }

  generateTooltipHistoryId(index: number): string {
    return `priceApprovedInfo-${index}`;
  }

  onMouseEnterHistory(itemId: number, id: number) {
    const data = this.allPriceApprovedTooltipData.filter(entry => entry.itemId === itemId);
    this.priceApprovedTooltipData[id] = data;
  }

  onHidingHistory(e) {
    const isMouseInsidePopover = e.component.content().parentNode.matches(":hover");
    e.cancel = isMouseInsidePopover;
    if (!e.component.content().parentNode.getAttribute("mouselistener")) {
      e.component.content().parentNode.addEventListener("mouseleave", (arg) => {
        e.component.hide();
      });
      e.component.content().parentNode.setAttribute("mouselistener", true);
    }
  }

  selectionChanged(data: any) {
    this.selectedRequestItemsKeys = data.selectedRowsData;
  }

  public deleteRow(data: any) {
    this.requestItemsDataGrid.instance.selectRows(data.key, false);
    this.requestItemsGridToolbar.displayDeleteConfirmation();
  }

  public deleteRecords(data: any) {
    this.selectedRequestItemsKeys.forEach((key) => {
      this.requestItems.splice(+key, 1);
    });
    this.requestItemsDataGrid.instance.refresh();
  }

  openTurnover() {
    if (this.selectedPartner && this.selectedPartner.id) {
      var url = environment.CRMPrimaware + '/' + Constants.turnOver + '?clientId=' + this.selectedPartner.id;
      window.open(url, '_blank');
    }
  }

  openPartnerDetails() {
    if (this.selectedPartner && this.selectedPartner.id) {
      var url = environment.CRMPrimaware + '/' + Constants.partner + '?id=' + this.selectedPartner.id;
      window.open(url, '_blank');
    }
  }

  openSpecialPriceRequestSection() {
    this.isRequestItemsSectionHidden = !this.isRequestItemsSectionHidden;
  }

  openItemTurnover(itemId: number) {
    if (itemId) {
      let itemCode = this.itemDS.store.find(f => f.id === itemId)?.code;
      
      this.filterTurnoverItemCode = itemCode;
      this.isItemTurnoverPopupOpened = true;

      setTimeout(() => {
        if (this.itemTurnoverComponent) {
          this.itemTurnoverComponent.loadData();
        }
      });
    }
  }

  approveAllRequests() {
    this.specialPriceRequests = this.specialPriceRequests.map(m => ({ ...m, isRejected: false }));
  }

  rejectAllRequests() {
    this.specialPriceRequests = this.specialPriceRequests.map(m => {
      m.priceApproved = m.priceOfferedWithoutTva

      let partnerProductConventionName = this.selectedPartnerImplicitLocation?.productConventionName;
      let partnerProductConventionId = this.selectedPartnerImplicitLocation?.productConventionId;
      let productConventionType = this.getProductConventionType(partnerProductConventionId, m);
    
      let salePriceList = this.salePriceLists.find(f => f.id === m.salePriceListId && f.salePriceListXProductConventionList && f.salePriceListXProductConventionList.map(m => m.productConventionId).includes(partnerProductConventionId));
    
      let salePriceListCode;
      if (salePriceList) {
        if (salePriceList.name && salePriceList.name.toLowerCase().replace(/\s+/g, '').includes(Constants.salePriceListPpName)) {
          salePriceListCode = `PP${partnerProductConventionName}`;
        } else {
          salePriceListCode = `${partnerProductConventionName}`;
        }
      } else {
        salePriceListCode = `${partnerProductConventionName}`;
      }

      let auxDiscountGridApproved = productConventionType ? Math.round(((m.priceApproved - productConventionType) / productConventionType) * 100) : 0;
      let auxCmpRequestedApproved = m.discountGridCmp ? Math.round(((m.priceApproved - m.discountGridCmp) / m.discountGridCmp) * 100) : 0;
      m.auxDiscountGridApproved = auxDiscountGridApproved;
      m.auxCmpRequestedApproved = auxCmpRequestedApproved;
      m.approvedDiscountGridCmp = `${salePriceListCode} ${auxDiscountGridApproved}%(${auxCmpRequestedApproved}%)`;

      return { ...m, priceApproved: m.priceOfferedWithoutTva, isRejected: true }
    });

    this.calculateRequestDetails();
  }

  copyPriceRequestedAllRequests() {
    this.specialPriceRequests = this.specialPriceRequests.map(m => {
      const updatedRow = { ...m, priceApproved: m.priceRequestedWithoutTva, isRejected: false };
      this.updateRow(updatedRow); 
      return updatedRow;
    });
    this.specialPriceRequests = [...this.specialPriceRequests];
    this.calculateRequestDetails();
  }

  copyQuantityAllRequests() {
    this.specialPriceRequests = this.specialPriceRequests.map(m => {
      if (m.isStockAvailable) {
        if (m.stock < m.quantity) {
          m.approvedQuantity = m.stock;
        } else {
          m.approvedQuantity = m.quantity;
        }
      } else {
        let splitTurnoverRs = m.turnOverRs.split(" / ");
        let auxSplitTurnoverRs = splitTurnoverRs.map(Number);
        let sumTurnoverRs = auxSplitTurnoverRs.reduce((acc, num) => acc + num, 0);
        let averageTurnoverRs = auxSplitTurnoverRs.length > 0 ? sumTurnoverRs / auxSplitTurnoverRs.length : 0;

        m.approvedQuantity = (m.stock - (averageTurnoverRs * 3)) >= 0 ? m.stock - (averageTurnoverRs * 3) : 0;
      }

      return m;
    });
  }

  rejectQuantityAllRequests() {
    this.specialPriceRequests = this.specialPriceRequests.map(m => ({ ...m, approvedQuantity: 0 }));
  }

  displayItemTurnoverPopupChange(event : any) {
    event ? this.isDataLoading = true : this.isDataLoading = false;
  }

  async viewSpecialPriceRequestsHistory() {
    this.isHistoryButtonClicked = true;
    await this.getPartnerSpecialPriceRequests(true);
  }

  generatePriceApprovedTooltip(id: number): string {
    return `priceApprovedTooltipInfo-${id}`;
  }

  onMouseEnterPriceApprovedHistory(id: number) {
    const data = this.specialPriceRequestHistory.find(entry => entry.id === id);
    this.rejectedRequestPriceApprovedTooltipData[id] = data.isRejected ? 'RESPINS' : '';
  }

  onHidingPriceApprovedHistory(e) {
    const isMouseInsidePopover = e.component.content().parentNode.matches(":hover");
    e.cancel = isMouseInsidePopover;
    if (!e.component.content().parentNode.getAttribute("mouselistener")) {
      e.component.content().parentNode.addEventListener("mouseleave", (arg) => {
        e.component.hide();
      });
      e.component.content().parentNode.setAttribute("mouselistener", true);
    }
  }

  stayOnSpecialPriceRequest() {
    this.shouldDisplayFinancialInfoPopup = false; this.isFinancialInfoPopupDisplayed = false;
    this.showFinancialInfoPopup = false;
  }

  contentReadyNewItems(event: any) {
    this.allTablesContentReadyCounter++;

    if (this.allTablesContentReadyCounter === 3 && this.isFinancialInfoPopupDisplayed && this.shouldDisplayFinancialInfoPopup && !this.isGeneralDirectorFlag) {
      this.showFinancialInfoPopup = true;
    }
  }

  contentReadyRequestedItems(event: any) {
    this.allTablesContentReadyCounter++;

    if (this.allTablesContentReadyCounter === 3 && this.isFinancialInfoPopupDisplayed && this.shouldDisplayFinancialInfoPopup && !this.isGeneralDirectorFlag) {
      this.showFinancialInfoPopup = true;
    }
  }

  contentReadyHistoryItems(event: any) {
    this.allTablesContentReadyCounter++;

    if (this.allTablesContentReadyCounter === 3 && this.isFinancialInfoPopupDisplayed && this.shouldDisplayFinancialInfoPopup && !this.isGeneralDirectorFlag) {
      this.showFinancialInfoPopup = true;
    }
  }

  completePartnerFinancialInfo() {
    this.openPartnerDetails();
    this.stayOnSpecialPriceRequest();
  }

  setItemCodeTooltip(event) {
    if (event && event.rowType === 'data' && !event.column.dataField) {
      on(event.cellElement, 'mouseover', arg => {
        this.timeout = window.setTimeout(async () => {

          this.tooltipData = null;
          let stockPromise = this.vipOfferItemDetailsService.GetTiersForItemAsync(null, event.data.itemId, this.selectedPartner.id).then(r => {
            if (r && r.length > 0) {
              this.tooltipData = r;
            } else { this.tooltipData = []; }
          });

          Promise.all([stockPromise]).then(r => {this.tooltipItemCode.instance.show(arg.target);})
        }, 1000);
      });

      on(event.cellElement, 'mouseout', arg => {
        if (this.timeout) {
          window.clearTimeout(this.timeout);
        }
        this.tooltipItemCode.instance.hide();
      });
    }
  }

  isItemOnVIP(itemId: number) {
    return (this.vipOfferItems.filter(x => x.itemId == itemId).length > 0);
  }

  onCellPreparedHistoryItems(e) {
    this.setItemCodeTooltip(e);

    if (e && e.rowType === 'header' && e.column.dataField === 'itemGrossWeight') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Greutate Unitara");
      ($(cellElement) as any).tooltip({
        placement: 'top'
      });
    }
  }

  onCellPreparedRequestedItems(e) {
    this.setItemCodeTooltip(e);

    if (e && e.rowType === 'header' && e.column.dataField === 'itemGrossWeight') {
      const cellElement = e.cellElement;
      cellElement.setAttribute("title", "Greutate Unitara");
      ($(cellElement) as any).tooltip({
        placement: 'top'
      });
    }

  }

  copyToClipboard(value: string): void {
    if (!value) {
      return;
    }

    navigator.clipboard.writeText(value).then(() => {
      this.notificationService.alert('top', 'center', 'Datele au fost copiate cu succes!', NotificationTypeEnum.Green, true);
    }).catch(err => {
      this.notificationService.alert('top', 'center', 'Datele nu au fost copiate! A aparut o eroare!', NotificationTypeEnum.Red, true);
    });
  }  
  
  calculateRequestDetails() {
    this.offeredValue = 0;
    this.requestedPercentage = 0;
    this.requestedAddition = 0;
    this.approvedValue = 0;
    this.approvedPercentage = 0;
    this.approvedAddition = 0;

    this.requestItemsGrossWeight = 0;
    this.requestItemsVolumetricWeight = 0;
    this.estimatedTransport = 0;
    this.transportPercentage = 0;

    this.specialPriceRequests.forEach(request => {
      this.offeredValue += (request.quantity * request.priceOfferedWithoutTva);
      this.requestedPercentage += request.quantity * (request.priceOfferedWithoutTva - request.priceRequestedWithoutTva);  
      this.requestedAddition += request.quantity * (request.priceRequestedWithoutTva - (request.discountGridCmp ? Number(request.discountGridCmp.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 })) : 0));

      this.approvedValue += (request.quantity * request.priceApproved);
      this.approvedPercentage += request.quantity * (request.priceOfferedWithoutTva - request.priceApproved);
      this.approvedAddition += request.quantity * (request.priceApproved - (request.discountGridCmp ? Number(request.discountGridCmp.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 })) : 0));

      this.requestItemsGrossWeight += request.itemGrossWeight;
      this.requestItemsVolumetricWeight += request.itemVolume ? (request.itemVolume/4000) : 0; // TODO: refactor weight division (4000/5000/6000)

      this.requestItemsGrossWeight = Number(this.requestItemsGrossWeight.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
      this.requestItemsVolumetricWeight = Number(this.requestItemsVolumetricWeight.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));

      this.estimatedTransport = Math.max(this.requestItemsGrossWeight, this.requestItemsVolumetricWeight) + 10; // TODO: refactor formula
      this.transportPercentage = (this.estimatedTransport && this.approvedValue) ? (this.estimatedTransport/this.approvedValue) : 0.00;
    });
  }
}

