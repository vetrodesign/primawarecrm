import { TestBed } from '@angular/core/testing';

import { PartnerLocationSecondaryActivityAllocationService } from './partner-location-secondary-activity-allocation.service';

describe('PartnerLocationSecondaryActivityAllocationService', () => {
  let service: PartnerLocationSecondaryActivityAllocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerLocationSecondaryActivityAllocationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
