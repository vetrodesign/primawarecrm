import { BaseDomain } from "./base-domain.model";

export class Item extends BaseDomain {
    public code: string;
    public itemGroupId: number;
    public measurementUnitId: number;
    public customCodeId: number;
    public nameRO: string;
    public nameENG: string;
    public shortNameRO: string;
    public shortNameENG: string;

    public PC_ACT: string;
    public TRSP_ACT: string;
    public CMP_ACT: string;

    public partnerId: number;
    public itemTypeId: number;
    public syncERPExternalId: string;

    public taxRateId: number;
    public isValid: boolean = false;
    public isStockable: boolean = false;
    public turnOver: number;

    public caenCodeSpecializationIds: any;
    public existingCaenCodeSpecializationIds: any;
}

export class ItemPriceParam {
    public itemId: number;
    public partnerId: number;
    public salePriceListId: number;
}