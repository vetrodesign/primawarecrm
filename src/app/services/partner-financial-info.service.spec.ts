import { TestBed } from '@angular/core/testing';

import { PartnerFinancialInfoService } from './partner-financial-info.service';

describe('PartnerFinancialInfoService', () => {
  let service: PartnerFinancialInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerFinancialInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
