import { Injectable } from '@angular/core';
import { Order, OrderERPSyncOptionsVM } from 'app/models/order.model';
import { OrderSearchFilterVM } from 'app/models/orderSearchFilter.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<any[]> {
    const apiURL = `/order/GetAll`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getByFilter(orderFilter: OrderSearchFilterVM): Promise<any> {
    const apiURL = `/order/GetOrdersByFilter `;
    return await this.apiService.PostAsync<any>(apiURL, orderFilter, Constants.mrkApi);
  }

  async getERPInvoiceSmall(): Promise<any> {
    const apiURL =  `/order/GetERPInvoiceSmall`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.mrkApi);
  }

  async getById(orderId: number): Promise<any> {
    const apiURL = `/order/GetByOrderId`;
    return await this.apiService.PostAsync<any>(apiURL, orderId, Constants.mrkApi);
  }

  async getOrderItemsById(orderId: number): Promise<any> {
    const apiURL = `/order/GetOrderItems`;
    return await this.apiService.PostAsync<any>(apiURL, orderId, Constants.mrkApi);
  }

  async getAsyncByID(): Promise<any[]> {
    const apiURL = `/order/Getbycustomerid`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async createAsync(order: Order): Promise<any> {
    const apiURL = `/order/create`;
    return await this.apiService.PostAsync<any>(apiURL, order, Constants.mrkApi);
  }

  async updateAsync(order: Order): Promise<any> {
    const apiURL =  `/order/update`;
    return await this.apiService.PutAsync<any>(apiURL, order, Constants.mrkApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/order/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }

  async insertToCharismaAsync(orderId: number, userName: string, postName: string): Promise<any> {
    let o = new OrderERPSyncOptionsVM();
    o.orderId = orderId;
    o.historyUserName = userName;
    o.historyPostName = postName;
    const apiURL =  `/order/InsertOrderToERP`;
    return await this.apiService.PostAsync<any>(apiURL, o, Constants.mrkApi);
  }

  async updateToCharismaAsync(orderId: number, userName: string, postName: string): Promise<any> {
    let o = new OrderERPSyncOptionsVM();
    o.orderId = orderId;
    o.historyUserName = userName;
    o.historyPostName = postName;
    const apiURL =  `/order/UpdateOrderToERP`;
    return await this.apiService.PutAsync<any>(apiURL, o, Constants.mrkApi);
  }

  async getByOrderId(orderId: number): Promise<any> {
    const apiURL = `/OrderERPSyncHistory/GetLastHistoryEntryByOrderId`;
    return await this.apiService.PostAsync<any>(apiURL, orderId, Constants.mrkApi);
  }

  async insertInvoiceToCharismaAsync(orderId: number, userName: string, postName: string): Promise<any> {
    let o = new OrderERPSyncOptionsVM();
    o.orderId = orderId;
    o.historyUserName = userName;
    o.historyPostName = postName;
    const apiURL =  `/order/InsertInvoiceFromOrderToERP`;
    return await this.apiService.PostAsync<any>(apiURL, o, Constants.mrkApi);
  }
}