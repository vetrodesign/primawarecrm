import { Component, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { Occupation } from 'app/models/occupation.model';
import { PartnerLocationContact } from 'app/models/partnerlocationcontact.model';
import { PartnerReview } from 'app/models/partnerreview.model';
import { Post } from 'app/models/post.model';
import { AuthService } from 'app/services/auth.service';
import { PartnerReviewService } from 'app/services/partner-review.service';
import { PostService } from 'app/services/post.service';
import { TranslateService } from 'app/services/translate';
import { UsersService } from 'app/services/user.service';
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
  selector: 'app-partner-review-report',
  templateUrl: './partner-review-report.component.html',
  styleUrls: ['./partner-review-report.component.css']
})
export class PartnerReviewReportComponent implements OnInit {
  @ViewChild('partnerReviewsDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('reviewGridToolbar') gridToolbar: GridToolbarComponent;

  partnerReviews: PartnerReview[] = [];
  userPost: Post;
  selectedPartnerReview: PartnerReview;
  selectedRows: any[];
  selectedRowIndex = -1;
  groupedText: string;
  actions: any;
  partnerReviewInfo: string;
  partnerContacts: PartnerLocationContact[] = [];
  occupations: Occupation[] = [];
  loaded: boolean;
  isOwner: boolean;
  users: any[] = [];
  posts: any[] = [];

  constructor(private partnerReviewsService: PartnerReviewService,
    private authService: AuthService, private translationService: TranslateService,
    private userService: UsersService, private postService: PostService) {
    this.setActions();
      this.groupedText = this.translationService.instant('groupedText');
      this.getDisplayExprCreatedBy = this.getDisplayExprCreatedBy.bind(this);
      this.getDisplayExprPartnerContacts = this.getDisplayExprPartnerContacts.bind(this);
    this.partnerReviewInfo = this.translationService.instant("partnerReviewInfo"); 
    this.authService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });
   }

  ngOnInit(): void {
    this.loaded = true;
    Promise.all([this.getUsers(), this.getPosts()]).then(async x => {
      this.getData();
      this.loaded = false;
    });
  }

  async getUsers() {
    if (this.isOwner) {
      await this.userService.getUsersAsync().then(items => {
        this.users = items;
      });
    } else {
      await this.userService.getUserAsyncByID().then(items => {
        this.users = items;
      });
    }
  }

  async getPosts() {
    if (this.isOwner) {
      await this.postService.getAllPostsAsync().then(items => {
        this.posts = items;
      });
    } else {
      await this.postService.getPostsByCustomerId().then(items => {
        this.posts = items;
      });
    }
  }

  async getData() {
    await this.partnerReviewsService.getAllPartnerReviewsAsync().then(async reviews => {
      this.partnerReviews = (reviews && reviews.length > 0) ? reviews.filter(f => f.description) : reviews;

      this.partnerReviews.forEach(review => {
        // Split the description using '|||'
        const parts = review.description.split(' ||| ');
      
        // If more than 5 parts, store the whole description in response1
        if (parts.length > 5) {
          review.response1 = review.description;
        } else {
          // Populate response1 to response5 dynamically
          for (let i = 0; i < 5; i++) {
            review[`response${i + 1}`] = parts[i] || ''; // Fill with empty string if undefined
          }
        }
      });
      

      // await this.partnerLocationContactService.getAllPartnerLocationContactsAsync().then(async contacts => {
      //   if (contacts) {
      //     this.partnerContacts = contacts;

      //     await this.occupationService.getAllOccupationsAsync().then(async items => {
      //       if (items && items.length > 0) {
      //         this.occupations = items;
      //       }
      //     });
      //   } else {
      //     this.partnerContacts = [];
      //   }
      // });
    });
  }

  async getReviews() {
    this.partnerReviewsService.getAllPartnerReviewsAsync().then(reviews => {
      this.partnerReviews = (reviews && reviews.length > 0) ? reviews.filter(f => f.description) : reviews;
      this.partnerReviews.forEach(review => {
        // Split the description using '|||'
        const parts = review.description.split(' ||| ');
      
        // If more than 5 parts, store the whole description in response1
        if (parts.length > 5) {
          review.response1 = review.description;
        } else {
          // Populate response1 to response5 dynamically
          for (let i = 0; i < 5; i++) {
            review[`response${i + 1}`] = parts[i] || ''; // Fill with empty string if undefined
          }
        }
      });
    });
  }

  public refreshReviews() {
    this.getData().then(() => {
      this.dataGrid.instance.refresh();
    })
  }

  canExport() {
    return this.actions.CanExport;
  }

  public add() {
    this.dataGrid.instance.addRow();
  }

  setActions() {
    this.actions = {
      CanView: true,
      CanAdd: false,
      CanUpdate: false,
      CanDelete: false,
      CanPrint: true,
      CanExport: true,
      CanImport: false,
      CanDuplicate: false
    };
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  selectionReviewChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  getDisplayExprCreatedBy(value: any) {
    if (value && this.users && this.posts) {
      let user = this.users.find(f => f.id === value.id);
      if (user) {
        let post = this.posts.find(f => f.id === user.postId);

        if (post) {
          return `${post.companyPost}(${user.firstName} ${user.lastName})`;
        } 
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  // async getPartnerLocationContacts() {
  //   this.partnerContacts = [];
    
  //   await this.occupationService.getAllOccupationsAsync().then(async items => {
  //     if (items && items.length > 0) {
  //       this.occupations = items;

  //       await this.partnerLocationContactService.getAllPartnerLocationContactsAsync().then(contacts => {
  //         if (contacts) {
  //           this.partnerContacts = contacts;
  //         } else {
  //           this.partnerContacts = [];
  //         }
  //       });
  //     } else {
  //       this.occupations = [];
  //     }
  //   });
  // }

  getDisplayExprPartnerContacts(value:any) {
    if (value && this.occupations) {
      let occupation = this.occupations.find(f => f.id === value.position);

      if (occupation) {
        return `${value.firstName} ${value.lastName}(${occupation.code})`;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }
}
