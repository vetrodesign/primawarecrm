export class SalesTargetXPost {
    id: any;
    description: string;
    postId: number;

    salesGroupId: number;
    salesTargetId: number;

    validFrom: Date;
    validTo: Date;

    currencyId: number;
    value: string;
    date: Date;

    orderNumber: number;
    isActive: boolean;
}