import { formatDate } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ERPSyncSource } from 'app/enums/ERPSyncSourceEnum';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { OrderStatusTypeEnum } from 'app/enums/orderStatusTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { BaseCurrency } from 'app/models/base-currency.model';
import { ItemType } from 'app/models/itemtype.model';
import { Management } from 'app/models/management.model';
import { MeasurementUnit } from 'app/models/measurementunit.model';
import { ListPrice, Order, OrderItem } from 'app/models/order.model';
import { PaymentInstrument } from 'app/models/paymentinstrument.model';
import { User } from 'app/models/user.model';
import { AuthService } from 'app/services/auth.service';
import { ItemService } from 'app/services/item.service';
import { NotificationService } from 'app/services/notification.service';
import { OrderService } from 'app/services/order.service';
import { PartnerLocationContactService } from 'app/services/partner-location-contact.service';
import { PartnerLocationService } from 'app/services/partner-location.service';
import { PartnerService } from 'app/services/partner.service';
import { SalePriceListItemService } from 'app/services/sale-price-list-item.service';
import { SalePriceListService } from 'app/services/sale-price-list.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent, DxValidatorComponent } from 'devextreme-angular';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {
  @Input() selectedOrder: Order;
  @Input() measurementUnits: MeasurementUnit[];
  @Input() paymentInstruments: PaymentInstrument[];
  @Input() items: any;
  @Input() managements: Management[];
  @Input() itemTypes: ItemType[];
  @Input() users: User[];

  @Input() orderTypes: any[];
  @Input() orderStatusTypes: any[];
  @Input() actions: any;
  @Input() deliveryConditions: any[];

  @Input() counties: any[];
  @Input() cities: any[];

  @Input() currencies: BaseCurrency[];
  @Input() salesPosts: any[];
  @Input() posts: any[];
  @Input() sites: any[];

  @Input() erpSyncSource: any[];

  @Output() visible: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;
  @ViewChild('secondValidationGroup') secondValidationGroup: DxValidatorComponent;

  @ViewChild('orderItemsToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('orderItemsDataGrid') dataGrid: DxDataGridComponent;

  order: Order;
  itemActions: any;
  isOnSave: boolean;
  editOrder: boolean;
  popupVisible: boolean;
  isOnSaveItem: boolean;

  loaded: boolean;

  showHistory: boolean;
  detailsTabs: any;
  selectedRows: any;

  totalValue: number;
  totalValueText: string;
  totalWithoutVat: number;
  totalWithoutVatText: string;

  isTabActive: string;
  partnerLocations: any[] = [];
  partnerContacts: any[] = [];
  salePriceLists: any[] = [];
  orderItems: OrderItem[] = [];
  selectedOrderItem: OrderItem;
  listPrice: ListPrice;
  selectedCurrencyId: number;
  isOrderValidated: boolean = false;
  isUpdateDisabled: boolean = false;
  isOrderSentToERP: boolean = false;
  hasERPActive: boolean = false;
  erpPopup: boolean = false;
  orderERPSyncHistoryItem: any;
  isOrderInvoiceSentToERP: boolean = false;
  erpInvoicePopup: boolean = false;

  partnerDeliveryLocations: any[] = [];
  constructor(private partnerLocationsService: PartnerLocationService,
    private partnerService: PartnerService,
    private partnerLocationContactService: PartnerLocationContactService,
    private salePriceListService: SalePriceListService,
    private orderService: OrderService,
    private itemService: ItemService,
    private salePriceListItemService: SalePriceListItemService,
    private notificationService: NotificationService,
    private authService: AuthService,
    private translationService: TranslateService,
    private authenticationService: AuthService) {
    this.setItemActions();

    this.hasERPActive = this.authenticationService.getCustomerERPActive();
    this.getDisplayExprItems = this.getDisplayExprItems.bind(this);
    this.itemTypeExpr = this.itemTypeExpr.bind(this);
    this.itemChange = this.itemChange.bind(this);
    this.partnerDeliveryLocationExpr = this.partnerDeliveryLocationExpr.bind(this);

    this.isTabActive = this.authenticationService.getCustomerPageTitleTheme();

    this.detailsTabs = [{
      id: 1,
      name: 'Client Facturat',
      'isActive': true
    },
    {
      id: 2,
      name: 'Autorul platii',
      'isActive': false
    },
    {
      id: 3,
      name: 'Destinatarul livrarii',
      'isActive': false
    },
    {
      id: 4,
      name: 'Client',
      'isActive': false
    }
    ];
  }

  ngOnInit(): void {
    this.listPrice = new ListPrice();
  }

  setItemActions() {
    this.itemActions = {
      CanView: true,
      CanAdd: this.editOrder,
      CanUpdate: false,
      CanDelete: false,
      CanPrint: false,
      CanExport: true,
      CanImport: false,
      CanDuplicate: false
    };
  }

  canExport() {
    return this.actions.CanExport;
  }

  openERPPopup() {
    if (this.validationGroup.instance.validate().isValid) {

      if (this.orderItems.filter(x => x.itemId == -1 || x.itemId == null || x.itemId == undefined).length > 0) {
        this.notificationService.alert('top', 'center', 'Exista produse necunoscute pe comanda! Stergeti sau corectati articolele gresit alocate!',
          NotificationTypeEnum.Red, true)
        return 0;
      }

      if (this.orderItems.filter(x => x.orderPrice == 0).length > 0) {
        this.notificationService.alert('top', 'center', 'Exista produse cu pret 0 pe comanda! Stergeti sau corectati articolele cu pret 0!',
          NotificationTypeEnum.Red, true)
        return 0;
      }

      if (this.items.store.filter(x => this.orderItems.map(y => y.itemId).includes(x.id) && (x.syncERPExternalId == undefined || x.syncERPExternalId == null)).length > 0) {
        this.notificationService.alert('top', 'center', 'Exista produse care nu sunt mapate cu ERP-ul! Comanda nu se paote trimite catre ERP pana aceste produse nu sunt sterse sau mapate!',
          NotificationTypeEnum.Red, true)
        return 0;
      }

      this.erpPopup = true;
    }
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  setGridInstances() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  async getData() {
    await this.orderService.getOrderItemsById(this.order.id).then(items => {
      if (items && items.length > 0) {
        this.orderItems = items;
        this.totalValue = 0;
        this.orderItems.forEach(oi => {
          this.totalValue += (oi.quantity * oi.orderPriceWithVAT);
          let i = this.items.store.find(x => x.id === oi.itemId);
          oi.measurementUnitId = i ? i.measurementUnitId : null;
        });
        this.calculateTotalValue();
      }
    })
  }

  async loadData() {
    this.order = new Order();
    this.isOrderValidated = false;
    this.isUpdateDisabled = false;
    this.isOrderSentToERP = false;
    this.isOrderInvoiceSentToERP = false;
    
    this.orderItems = [];
    if (this.selectedOrder) {
      this.isOnSave = false;
      this.order = this.selectedOrder;
      if (this.order.status == Number(OrderStatusTypeEnum.Validated)) {
        this.isOrderValidated = true;
      }
      this.getData();
    } else {
      this.isOnSave = true;
      this.totalValue = 0;
      this.order.type = 2;

      this.order.managementId = this.managements.find(x => x.isImplicit)?.id ? this.managements.find(x => x.isImplicit)?.id : null;
      this.order.siteId = Number(this.authService.getUserSiteId());
      this.order.isActive = true;
    }
    setTimeout(() => {
      this.setGridInstances();
    }, 200);
  }

  async loadNewOrderFromTurnover(items: any) {
    if (items && items.length > 0) {
      this.order = new Order();
      this.order.status = 2;
      this.order.type = 2;

      this.order.date = new Date();
      this.order.deliveryDate = new Date();
      this.order.deliveryDate.setDate(this.order.deliveryDate.getDate() + 2);

      this.order.managementId = items[0]?.mngId;
      this.order.source = ERPSyncSource.PrimawareCRM;
      this.order.deliveryConditionId = 1;

      this.order.partnerId = items[0].partnerId;
      this.order.paymentOwnerPartnerId = items[0].partnerId;
      this.order.deliveryPartnerId = items[0].partnerId;
      this.order.initialPartnerId = items[0].partnerId;

      this.orderItems = [];
      items.forEach(item => {
        var ori = new OrderItem();
        ori.itemId = item.itemId;
        ori.quantity = item.quantity;
        ori.orderPrice = item.price;
        ori.isActive = true;
        if (this.items && this.items.store && this.items.store.length > 0) {
          let i = this.items?.store?.find(x => x.id === item.itemId);
          ori.measurementUnitId = i ? i.measurementUnitId : null;
        }
        this.orderItems.push(ori);
      });

      if (this.order.partnerId) {
        await this.partnerService.getPartnerDataForOrderAsync(this.order.partnerId).then(async response => {
          if (response) {
            this.order.paymentInstrumentId = response.paymentInstrumentId;
            this.order.paymentDueDate = new Date();
            this.order.paymentDueDate.setDate(this.order.paymentDueDate.getDate() + response.paymentTerm);


            this.order.deliveryPartnerLocationId = response.partnerImplicitLocationId;
            this.order.partnerLocationId = response.partnerImplicitLocationId;
            this.order.partnerLocationContactId = response.partnerLocationContactId;
            this.order.salePriceListId = response.salePriceListId;
            this.order.salesAgentId = response.partnerSaleAgentId;

            const vatItemVms: any[] = this.orderItems.map(orderItem => ({
              id: orderItem.itemId,
              price: orderItem.orderPrice
            }));

            await this.itemService.getItemDetailsVAT(vatItemVms, this.order.partnerId).then(itemResponse => {
              this.orderItems.forEach(x => {
                x.salePriceListId = response.salePriceListId;
                x.dueDate = new Date();
                x.dueDate.setDate(x.dueDate.getDate() + response.paymentTerm);
                x.isActive = true;
                x.rate = itemResponse?.find(i => i.id == x.itemId)?.vatPercent ?? 0;
                x.orderPriceWithVAT = x.rate ? Number(((Math.round(x.orderPrice * 100) / 100) + (x.orderPrice * (x.rate / 100))).toFixed(2)) : x.orderPrice;
              })
              this.calculateTotalValue();
            })
          }
        })
      }
    }
  }

  calculateTotalValue() {
    this.totalValue = 0;
    this.totalWithoutVat = 0;
    this.orderItems.forEach(oi => {
      this.totalValue += (oi.quantity * oi.orderPriceWithVAT);
      this.totalWithoutVat += (oi.quantity * oi.orderPrice);
    });
    this.totalValueText = this.totalValue.toFixed(2);
    this.totalWithoutVatText = this.totalWithoutVat.toFixed(2);
  }

  toggleMode(tab: any) {
    this.detailsTabs.find(x => x.isActive).isActive = false;
    tab.isActive = true;
  }


  /*FIRST TAB will change the partner for all the remaining tabs!*/
  partnerChange(e) {
    if (e) {
      this.order.partnerId = e;
      if (this.isOnSave) {
        this.order.deliveryPartnerId = e;
        this.order.paymentOwnerPartnerId = e;
        // this.order.initialPartnerId = e;
      }
      this.getLocations(this.order.partnerId);
    }
  }

  partnerOwnerChange(e) {
    if (e) {
      this.order.paymentOwnerPartnerId = e;
    }
  }

  partnerDeliveryChange(e) {
    if (e) {
      this.order.deliveryPartnerId = e;

      this.getDeliveryLocations(this.order.deliveryPartnerId);
    }
  }

  initialPartnerChange(e) {
    if (e) {
      this.order.initialPartnerId = e;
    }
  }

  public async openDetails(row: any) {
    this.popupVisible = true;
    if (row) {
      this.selectedOrderItem = row;
      this.isOnSaveItem = false;
    } else {
      this.selectedOrderItem = new OrderItem();
      this.selectedOrderItem.isActive = true;

      this.listPrice.listPrice = null;
      this.listPrice.listPriceWithVAT = null;
      this.listPrice.rate = null;

      if (this.order.salePriceListId) {
        this.selectedOrderItem.salePriceListId = this.order.salePriceListId;
      }
      this.isOnSaveItem = true;
    }
  }

  closePopup() {
    this.popupVisible = false;
  }

  orderStatusChange(e) {
    if (e && e.value) {
      this.editOrder = (e.value === 2) || (e.value === 3);
    }
    this.setItemActions();
  }

  async getLocations(partnerId: number) {
    this.partnerLocations = [];
    this.partnerContacts = [];
    await this.partnerLocationsService.getPartnerLocationsByPartnerID(partnerId).then(locations => {
      if (locations && locations.length > 0) {
        this.partnerLocations = locations;
      }
    })
    await this.partnerLocationContactService.getPartnerLocationContactByPartnerID(partnerId).then(contacts => {
      if (contacts && contacts.length > 0) {
        this.partnerContacts = contacts;
      }
    })
  }

  async getDeliveryLocations(partnerId: number) {
    this.partnerDeliveryLocations = [];
    await this.partnerLocationsService.getPartnerLocationsByPartnerID(partnerId).then(locations => {
      if (locations && locations.length > 0) {
        this.partnerDeliveryLocations = locations;
      }
    })
  }

  async partnerLocationChange(e) {
    if (e && e.value) {
      this.salePriceLists = [];
      await this.salePriceListService.getSalePriceListByLocationId(e.value).then(salePriceLists => {
        if (salePriceLists && salePriceLists.length > 0) {
          this.salePriceLists = salePriceLists;

          if (this.order && this.order.salePriceListId) {
            let list = this.salePriceLists.find(x => x.id === this.order.salePriceListId);
            this.selectedCurrencyId = list ? list.currencyId : null;
          }
        }
      })
    }
  }

  findCurrencyForListEvent(e: any) {
    if (this.order && e && this.salePriceLists.length > 0) {
      let list = this.salePriceLists.find(x => x.id === e.value);
      this.selectedCurrencyId = list ? list.currencyId : null;
    }
  }

  backToOrders() {
    this.order = null;
    this.visible.emit(false);
  }

  async refreshDataGrid() {
    this.getData();
  }

  async salePriceListChange(e) {
    if (e && e.value && this.selectedOrderItem.itemId && this.order.partnerLocationId) {
      await this.setPrices(this.selectedOrderItem.itemId, this.order.partnerId, e.value);
    }
  }

  async itemChange(e) {
    if (e && e.value) {
      if (this.items && this.items.store && this.items.store.length > 0) {
        let i = this.items?.store?.find(x => x.id === e.value);
        this.selectedOrderItem.measurementUnitId = i ? i.measurementUnitId : null;
      }
      if (this.selectedOrderItem.salePriceListId && this.order.partnerLocationId) {
        await this.setPrices(e.value, this.order.partnerId, this.selectedOrderItem.salePriceListId);
      }
    }
  }

  async setPrices(itemId: number, partnerId: number, salePriceListId: number) {
    this.loaded = true;
    await this.salePriceListItemService.getItemPriceAndRate(itemId, partnerId, salePriceListId).then(r => {
      if (r) {
        this.listPrice.listPrice = r.price;
        this.listPrice.rate = r.vatPercent;

        let tax = r.vatPercent / 100;
        this.listPrice.listPriceWithVAT = Number(((Math.round(r.price * 100) / 100) + (r.price * tax)).toFixed(2));
      } else {
        this.listPrice.listPrice = 0;

        /*Get from Item customCode, not from list and not default 0*/
        this.listPrice.rate = 0;

        this.listPrice.listPriceWithVAT = 0;
      }
      this.setOrderPrices();
      this.loaded = false;
    });
  }

  orderPriceChange(e) {
    if (e && e.value) {
      this.setOrderPrices();
    }
  }

  /* Based on certain conditions, order state and others*/
  setOrderPrices() {
    this.selectedOrderItem.rate = this.selectedOrderItem.rate ? this.selectedOrderItem.rate : this.listPrice.rate;
    this.selectedOrderItem.orderPriceWithVAT = this.selectedOrderItem.rate ? Number(((Math.round(this.selectedOrderItem.orderPrice * 100) / 100) + (this.selectedOrderItem.orderPrice * (this.selectedOrderItem.rate / 100))).toFixed(2)) : this.selectedOrderItem.orderPrice;
  }

  /* Also disable if the state of order is on a certain level*/
  shouldDisableOrderItem() {
    return !(this.listPrice.listPrice > 0 || this.listPrice.rate > 0);
  }

  async saveData() {
    if (!this.order.partnerId || !this.order.partnerLocationId || !this.order.paymentOwnerPartnerId || !this.order.deliveryPartnerId) {
      this.notificationService.alert('top', 'center', 'Nu aveti unul din cele 3 tab-uri de client / autorul platii / destinatarul livrarii completate!',
        NotificationTypeEnum.Red, true)
      return 0;
    }
    if (this.validationGroup.instance.validate().isValid) {
      this.loaded = true;
      this.order.source = ERPSyncSource.PrimawareCRM;
      this.order.customerId = Number(this.authService.getUserCustomerId());
      this.order.orderItems = [];
      this.order.orderItems = this.orderItems;
      await this.orderService.createAsync(this.order).then(response => {
        if (response) {
          this.order.id = response.id;
          this.order.number = response.number;
          this.order.rowVersion = response.rowVersion;
          this.order.createdBy = response.createdBy;
          this.order.modifiedBy = response.modifiedBy;
          this.order.created = response.created;
          this.order.modified = response.modified;

          if (this.order.status == Number(OrderStatusTypeEnum.Validated)) {
            this.isOrderValidated = true;
          }

          this.isOnSave = false;
          this.notificationService.alert('top', 'center', 'Datele au fost create cu succes!', NotificationTypeEnum.Green, true)
          //this.backToOrders();
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost create!', NotificationTypeEnum.Red, true)
        }

        this.removeQueryParam();
        this.loaded = false;
        this.popupVisible = false;
      });
    }
  }

  removeQueryParam() {
    const url = new URL(window.location.href);
    url.searchParams.delete('data'); // Remove the 'data' query parameter
    history.replaceState(null, '', url.pathname); // Update the URL without reloading the page
  }

  async updateData() {
    if (!this.order.partnerId || !this.order.partnerLocationId || !this.order.paymentOwnerPartnerId || !this.order.deliveryPartnerId) {
      this.notificationService.alert('top', 'center', 'Nu aveti unul din cele 3 tab-uri de client / autorul platii / destinatarul livrarii completate!',
        NotificationTypeEnum.Red, true)
      return 0;
    }
    if (this.validationGroup.instance.validate().isValid) {
      this.loaded = true;
      this.order.orderItems = [];
      this.order.orderItems = this.orderItems;
      // Disable after first update
     // this.isUpdateDisabled = true;
      await this.orderService.updateAsync(this.order).then(response => {
        if (response) {
          this.order.rowVersion = response.rowVersion;
          if (this.order.status == Number(OrderStatusTypeEnum.Validated)) {
            this.isOrderValidated = true;
          }
          this.notificationService.alert('top', 'center', 'Datele au fost actualizate cu succes!', NotificationTypeEnum.Green, true)
          // this.backToOrders();
        } else {
          this.notificationService.alert('top', 'center', 'Datele nu au fost actualizate!', NotificationTypeEnum.Red, true)
        }
        this.loaded = false;
        this.popupVisible = false;
      });
    }
  }

  async saveItemOrder() {
    if (this.secondValidationGroup.instance.validate().isValid) {
      this.orderItems.push(this.selectedOrderItem);
      this.calculateTotalValue();
      this.popupVisible = false;
    }
  }

  async updateItemOrder() {
    if (this.secondValidationGroup.instance.validate().isValid) {
      this.calculateTotalValue();
      this.popupVisible = false;
    }
  }

  async deleteItemRow(e) {
    this.orderItems.splice(this.orderItems.findIndex(x => x.id === this.selectedRows.map(x => x.id)[0]), 1);
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  getOrderCreated(order: any, isCreate: boolean) {
    if (isCreate) {
      if (order.created && order.createdBy && this.users && this.users.length) {
        let user = this.users.find(u => u.id === order.createdBy)
        if (user) {
          return this.translationService.instant(`Creat in data de ${formatDate(order.created, 'dd-MM-yyyy HH:mm:ss', 'en-US')} de catre ${user.firstName}/${user.lastName}.`);
        }
      } else {
        return this.translationService.instant(`Creat in data de ${formatDate(order.created, 'dd-MM-yyyy HH:mm:ss', 'en-US')} de catre WindowsService.`);
      }
    }
    if (!isCreate) {
      if (order.modified && order.modifiedBy && this.users && this.users.length) {
        let user = this.users.find(u => u.id === order.modifiedBy)
        if (user) {
          return this.translationService.instant(`Ultima modificare in data de ${formatDate(order.modified, 'dd-MM-yyyy HH:mm:ss', 'en-US')} de catre ${user.firstName}/${user.lastName}.`);
        }
      } else {
        return this.translationService.instant(`Ultima modificare in data de ${formatDate(order.created, 'dd-MM-yyyy HH:mm:ss', 'en-US')} de catre WindowsService.`);
      }
    }
    return '';
  }

  salePriceListExpr(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.name;
  }

  partnerContactExpr(item) {
    if (!item) {
      return '';
    }
    return item.firstName + ' - ' + item.lastName;
  }

  getDisplayExprPostsName(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  partnerDeliveryLocationExpr(item) {
    if (!item) {
      return '';
    }
    let parts = [];
    if (item.name) {
      parts.push(item.name);
    }
    let city = this.cities?.find(x => x.id == item.cityId)?.name;
    if (city) {
      parts.push(city);
    }
    let county = this.counties?.find(x => x.id == item.countyId)?.name;
    if (county) {
      parts.push(county);
    }
    if (item.street) {
      parts.push(item.street);
    }
    if (item.streetNumber) {
      parts.push(item.streetNumber);
    }
    return parts.join(' - ');
  }

  itemTypeExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.itemId && this.items && this.items.store && this.items.store.length > 0) {
        let itemTypeId = this.items.store.find(x => x.id === item.itemId)?.itemTypeId;
        let type = itemTypeId ? this.itemTypes.find(y => y.id === itemTypeId) : null;
        return type ? type.name : '';
      }
      else {
        return '';
      }
    }
  }

  itemPriceExpr(item) {
    if (!item) {
      return '';
    } else {
      if (item.quantity && item.orderPriceWithVAT) {
        return (item.quantity * item.orderPriceWithVAT).toFixed(2);
      }
      else {
        return '';
      }
    }
  }

  getDisplayExprItems(item) {
    if (!item) {
      return '';
    }
    let f = (this.itemTypes && this.itemTypes.length > 0) ? this.itemTypes?.find(i => i.id === item.itemTypeId)?.name : null;
    return item.code + ' - ' + item.nameRO + ' - ' + f;
  }

  async insertERP(orderId) {
    if (this.order && this.order.salePriceListId != null && this.validationGroup.instance.validate().isValid && this.order.status === 4) {
      let userPost = null;
      if (this.posts && this.posts.length && this.users && this.users.length) {
        let user = this.users.find(x => x.userName === this.authenticationService.getUserUserName());
        if (user) {
          userPost = this.posts.find(x => Number(x.id) === user.postId);
        }
      }
      this.loaded = true;
      await this.orderService.insertToCharismaAsync(orderId, this.authenticationService.getUserUserName(), userPost ? userPost.code : null).then(async result => {
        this.loaded = false;
        this.erpPopup = false;
        if (result) {
          this.notificationService.alert('top', 'center', 'Comenzi - Datele au fost trimise catre ERP!',
            NotificationTypeEnum.Green, true);
          await this.getOrderERPSyncHistoryItems(orderId).then(x => { this.isOrderSentToERP = true; this.loaded = false; });
        } else {
          this.notificationService.alert('top', 'center', 'Comenzi - Datele nu au fost trimise catre ERP! A aparut o eroare!',
            NotificationTypeEnum.Red, true);
          return;
        }
      }).catch(error => {
        console.log(4);
        this.loaded = false;
        this.erpPopup = false;
        this.notificationService.alert('top', 'center', 'Comenzi - Datele nu au fost trimise catre ERP! A aparut o eroare!',
          NotificationTypeEnum.Red, true);
      });

    }
    else {
      this.erpPopup = false;
      this.notificationService.alert('top', 'center', 'Comenzi - Datele nu au fost trimise catre ERP! Verificati toate campurile partenerului, inclusiv lista de pret!',
        NotificationTypeEnum.Red, true);
      return;
    }
  }

  async getOrderERPSyncHistoryItems(orderId) {
    await this.orderService.getByOrderId(orderId).then(x => {
      this.orderERPSyncHistoryItem = x;
    });
  }

  getOrderERPSyncHistory(itemId) {
    if (itemId) {
      if (this.orderERPSyncHistoryItem) {
        return this.translationService.instant(`Creat in ERP in data de ${formatDate(this.orderERPSyncHistoryItem.created, 'dd-MM-yyyy HH:mm:ss', 'en-US')} de catre ${this.orderERPSyncHistoryItem.postName}/${this.orderERPSyncHistoryItem.userName}.`);
      }
    }
    return '';
  }
  
  openERPInvoicePopup() {
    if (this.validationGroup.instance.validate().isValid) {

      if (this.orderItems.filter(x => x.itemId == -1 || x.itemId == null || x.itemId == undefined).length > 0) {
        this.notificationService.alert('top', 'center', 'Exista produse necunoscute pe comanda! Stergeti sau corectati articolele gresit alocate!',
          NotificationTypeEnum.Red, true)
        return 0;
      }

      if (this.orderItems.filter(x => x.orderPrice == 0).length > 0) {
        this.notificationService.alert('top', 'center', 'Exista produse cu pret 0 pe comanda! Stergeti sau corectati articolele cu pret 0!',
          NotificationTypeEnum.Red, true)
        return 0;
      }

      if (this.items.store.filter(x => this.orderItems.map(y => y.itemId).includes(x.id) && (x.syncERPExternalId == undefined || x.syncERPExternalId == null)).length > 0) {
        this.notificationService.alert('top', 'center', 'Exista produse care nu sunt mapate cu ERP-ul! Comanda nu se poate trimite catre ERP pana aceste produse nu sunt sterse sau mapate!',
          NotificationTypeEnum.Red, true)
        return 0;
      }

      if (this.order.paymentDueDate == null) {
        this.notificationService.alert('top', 'center', 'Campul Scadenta plata nu este completat!', NotificationTypeEnum.Red, true)
        return 0;
      }

      if (this.orderItems.filter(x => x.rate == 0 || x.rate == null || x.rate == undefined).length > 0) {
        this.notificationService.alert('top', 'center', 'Exista produse pe comanda care nu au rata de TVA completata!',
          NotificationTypeEnum.Red, true)
        return 0;
      }

      this.erpInvoicePopup = true;
    }
  }

  async insertInvoiceERP(orderId) {
    if (this.order && this.order.salePriceListId != null && this.validationGroup.instance.validate().isValid && this.order.status === 4) {
      let userPost = null;
      if (this.posts && this.posts.length && this.users && this.users.length) {
        let user = this.users.find(x => x.userName === this.authenticationService.getUserUserName());
        if (user) {
          userPost = this.posts.find(x => Number(x.id) === user.postId);
        }
      }
      this.loaded = true;
      await this.orderService.insertInvoiceToCharismaAsync(orderId, this.authenticationService.getUserUserName(), userPost ? userPost.code : null).then(async result => {
        this.loaded = false;
        this.erpInvoicePopup = false;
        if (result) {
          this.notificationService.alert('top', 'center', 'Comenzi - Factura a fost creata cu succes in ERP!',
            NotificationTypeEnum.Green, true);
          await this.getOrderERPSyncHistoryItems(orderId).then(x => { this.isOrderInvoiceSentToERP = true; this.loaded = false; });
        } else {
          this.notificationService.alert('top', 'center', `Comenzi - Factura nu a fost creata in ERP!`,
            NotificationTypeEnum.Red, true);
          return;
        }
      }).catch(error => {
        console.log(4);
        this.loaded = false;
        this.erpInvoicePopup = false;
        this.notificationService.alert('top', 'center', 'Comenzi - Factura nu a fost creata in ERP! Eroare de comunicatie cu server-ul!',
          NotificationTypeEnum.Red, true);
      });

    }
    else {
      this.erpInvoicePopup = false;
      this.notificationService.alert('top', 'center', 'Comenzi - Factura nu a fost creata in ERP! Verificati toate campurile partenerului, inclusiv lista de pret!',
        NotificationTypeEnum.Red, true);
      return;
    }
  }

}
