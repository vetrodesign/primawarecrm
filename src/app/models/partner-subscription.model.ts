import { BaseDomain } from "./base-domain.model";

export class PartnerSubscription extends BaseDomain {
    customerStatusAndSubscription: string;
    partnerStatus: boolean;
    description: string;
}