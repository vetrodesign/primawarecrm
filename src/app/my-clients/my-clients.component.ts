import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Constants } from 'app/constants';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { IPageActions } from 'app/models/ipageactions';
import { MyClientsFilter } from 'app/models/my-clients.model';
import { HelperService } from 'app/services/helper.service';
import { PartnerFinancialInfoService } from 'app/services/partner-financial-info.service';
import { PartnerService } from 'app/services/partner.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent, DxTooltipComponent } from 'devextreme-angular';
import { environment } from 'environments/environment';
import * as _ from 'lodash';
import { on } from 'devextreme/events';
import { PersonTypeEnum } from 'app/enums/personTypeEnum';

@Component({
  selector: 'app-my-clients',
  templateUrl: './my-clients.component.html',
  styleUrls: ['./my-clients.component.css']
})
export class MyClientsComponent implements OnInit, AfterViewInit {
  myClients: any;

  actions: IPageActions;
  groupedText: string;
  loaded: boolean = false;
  postId: number;
  filter: MyClientsFilter = new MyClientsFilter();
  relevantYears: any = [];
  turnoverTypes: any = [{ id: 1, name: 'Valoric' }, { id: 2, name: 'Numeric' }, { id: 3, name: 'Valoric & Numeric' }];
  inactivityValues: { id: number; name: string }[] = [];
  partnersDS: any;
  lastContactTooltipData: any = {};
  lastContactTooltipTimeout: number;
  personTypes: { id: number; name: string }[] = [];

  myClientsInfo: string;

  @ViewChild('gridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('dataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('lastContactTooltipName') lastContactTooltipName: DxTooltipComponent;

  constructor(
    private translationService: TranslateService,
    private partnerFinancialInfoService: PartnerFinancialInfoService,
    private helperService: HelperService,
    private route: ActivatedRoute,
    private partnerService: PartnerService
  ) {

    if (this.route.snapshot.queryParamMap.get('postId') !== undefined) {
      this.postId = Number(this.route.snapshot.queryParamMap.get('postId'));
    }

    this.groupedText = this.translationService.instant('groupedText');
    this.setActions();

    const currentYear = new Date().getFullYear();
    for (let i = 0; i <= 12; i++) {
      const year = currentYear - i;
      this.relevantYears.push({ id: i, year: year });
    }
    this.inactivityValues = Array.from({ length: 12 }, (_, i) => ({ id: i + 1, name: `${i + 1}` }));

    for (let n in PersonTypeEnum) {
      if (typeof PersonTypeEnum[n] === 'number') {
        this.personTypes.push({
          id: <any>PersonTypeEnum[n],
          name: this.translationService.instant(n)
        });
      }
    }

    this.filter.isMonthlyTurnover = true;
    this.filter.isYearlyTurnover = false;
    this.filter.year = this.relevantYears[0].year;
    this.filter.turnoverType = this.turnoverTypes[2].id;
    this.filter.personType = PersonTypeEnum.Legal;

    this.getData();
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  getData() {
    this.loaded = true;
    Promise.all([this.getPartners()]).then(async r => {
      await this.getMyClients();
      this.loaded = false;
    });
  }

  private async getMyClients() {
    this.filter.postId = this.postId;

    this.myClients = [];
    if (this.filter.isYearlyTurnover) {
      if (this.filter.turnoverType === 1) {
        await this.partnerFinancialInfoService.getClientYearlyValueTurnoverAsync(this.filter).then(items => {
          if (items && items.length > 0) {
            //this.myClients = items.map(item => this.formatValues(item, this.filter.turnoverType));
            this.myClients = this.addInactivityValues(items, 'valueL', false, false);
            this.myClients = _.orderBy(this.myClients, ['inactivityValues'], ['desc']);
          }
        });
      } else if (this.filter.turnoverType === 2) {
        await this.partnerFinancialInfoService.getClientYearlyNumericTurnoverAsync(this.filter).then(items => {
          if (items && items.length > 0) {
            //this.myClients = items.map(item => this.formatValues(item, this.filter.turnoverType));
            this.myClients = this.addInactivityValues(items, 'numericL', false, false);
            this.myClients = _.orderBy(this.myClients, ['inactivityValues'], ['desc']);
          }
        });
      } else if (this.filter.turnoverType === 3) {
        const [valueItems, numericItems] = await Promise.all([
          this.partnerFinancialInfoService.getClientYearlyValueTurnoverAsync({ ...this.filter, turnoverType: 1 }),
          this.partnerFinancialInfoService.getClientYearlyNumericTurnoverAsync({ ...this.filter, turnoverType: 2 })
        ]);
    
        const clientMap = new Map<string, any>();

        if (valueItems && valueItems.length > 0) {
          valueItems.forEach(item => {
            const clientId = item.partnerId;
            if (!clientMap.has(clientId)) {
              clientMap.set(clientId, { clientId });
            }
            Object.assign(clientMap.get(clientId), item);
          });
        }

        if (numericItems && numericItems.length > 0) {
          numericItems.forEach(item => {
            const clientId = item.partnerId; 
            if (!clientMap.has(clientId)) {
              clientMap.set(clientId, { clientId }); 
            }
            Object.assign(clientMap.get(clientId), item);
          });
        }

        this.myClients = Array.from(clientMap.values());
        this.myClients = this.addInactivityValues(this.myClients, 'valueL', true, false);
        this.myClients = _.orderBy(this.myClients, ['inactivityValues'], ['desc']);
      }
    } 

    if (this.filter.isMonthlyTurnover) {
      if (this.filter.turnoverType === 1) {
        await this.partnerFinancialInfoService.getClientMonthlyValueTurnoverAsync(this.filter).then(items => {
          if (items && items.length > 0) {
            //this.myClients = items.map(item => this.formatValues(item, this.filter.turnoverType));
            this.myClients = this.addInactivityValues(items, 'valueL', false, true);
            this.myClients = _.orderBy(this.myClients, ['inactivityValues'], ['desc']);
          }
        });
      } else if (this.filter.turnoverType === 2) {    
        await this.partnerFinancialInfoService.getClientMonthlyNumericTurnoverAsync(this.filter).then(items => {
          if (items && items.length > 0) {
            //this.myClients = items.map(item => this.formatValues(item, this.filter.turnoverType));
            this.myClients = this.addInactivityValues(items, 'numericL', false, true);
            this.myClients = _.orderBy(this.myClients, ['inactivityValues'], ['desc']);
          }
        });
      } else if (this.filter.turnoverType === 3) {
        const [valueItems, numericItems] = await Promise.all([
          this.partnerFinancialInfoService.getClientMonthlyValueTurnoverAsync({ ...this.filter, turnoverType: 1 }),
          this.partnerFinancialInfoService.getClientMonthlyNumericTurnoverAsync({ ...this.filter, turnoverType: 2 })
        ]);
    
        const clientMap = new Map<number, any>();

        if (valueItems && valueItems.length > 0) {
          valueItems.forEach(item => {
            const clientId = item.partnerId;
            if (!clientMap.has(clientId)) {
              clientMap.set(clientId, { clientId });
            }
            Object.assign(clientMap.get(clientId), item);
          });
        }
        
        if (numericItems && numericItems.length > 0) {
          numericItems.forEach(item => {
            const clientId = item.partnerId; 
            if (!clientMap.has(clientId)) {
              clientMap.set(clientId, { clientId }); 
            }
            Object.assign(clientMap.get(clientId), item);
          });
        }

        this.myClients = Array.from(clientMap.values());
        this.myClients = this.addInactivityValues(this.myClients, 'valueL', true, true);
        this.myClients = _.orderBy(this.myClients, ['inactivityValues'], ['desc']);
      }
    }
  }

  public roundUp(x: any): number {
    const num = Number(x); 
    return isNaN(num) ? null : Math.ceil(num); 
  }

  getFormattedValue(data: any, key: string): string {
    if (!data) {
      return ''; 
    }

    const valueKey = `value${key}`;
    const numericKey = `numeric${key}`;
  
    let value = data[valueKey];
    let numeric = data[numericKey];
  
    value = this.roundUp(value);
    numeric = this.roundUp(numeric);
    
    const formattedValue = value !== null && value !== undefined 
      ? this.helperService.setNumberWithCommas(value) 
      : null;
    const formattedNumeric = numeric !== null && numeric !== undefined 
      ? this.helperService.setNumberWithCommas(numeric) 
      : null;
    
    if (value !== null && value !== undefined && 
      !Number.isNaN(value) && value !== "NaN" && numeric !== null && 
      numeric !== undefined && !Number.isNaN(numeric) && numeric !== "NaN") {
      return `${formattedValue} (${formattedNumeric})`;
    }
   
    if ((value === null || value === undefined || 
      Number.isNaN(value) || value === "NaN") && 
      numeric !== null && numeric !== undefined && !Number.isNaN(numeric) && numeric !== "NaN") {
      return `${formattedNumeric}`;
    }
    
    return `${formattedValue ?? ''}`;
  }

  addInactivityValues(dataSource: any[], fieldPrefix: string, shouldCheckForBothPrefixes: boolean, isMonthlyTurnover: boolean): any[] {
    const currentMonth = new Date().getMonth() + 1;
    const currentYear = new Date().getFullYear();
  
    const partnerLookup = new Map(this.partnersDS.store.map(partner => [partner.id, partner.name]));
  
    return dataSource.map(item => {
      let inactivityValues = 0;
  
      if (isMonthlyTurnover) {
        for (let month = currentMonth; month >= 1; month--) {
          if (!shouldCheckForBothPrefixes) {
            const fieldKey = `${fieldPrefix}${month}`;
            if (item[fieldKey] == 0) {
              inactivityValues++;
            } else {
              break;
            }
          } else {
            const firstFieldKey = `valueL${month}`;
            const secondFieldKey = `numericL${month}`;
  
            if (item[firstFieldKey] == 0 && item[secondFieldKey] == 0) {
              inactivityValues++;
            } else {
              break;
            }
          }
        }
      } else {
        for (let i = 12; i >= 1; i--) {
          if (!shouldCheckForBothPrefixes) {
            const fieldKey = `${fieldPrefix}${i}`;
            if (item[fieldKey] == 0) {
              inactivityValues++;
            } else {
              break;
            }
          } else {
            const firstFieldKey = `valueL${i}`;
            const secondFieldKey = `numericL${i}`;
  
            if (item[firstFieldKey] == 0 && item[secondFieldKey] == 0) {
              inactivityValues++;
            } else {
              break;
            }
          }
        }
      }
  
      const partnerName = partnerLookup.get(item.partnerId) || "...";
  
      return {
        ...item,
        inactivityValues,
        partnerName
      };
    });
  }  

  private formatValues(item: any, turnoverType: number): any {
    const formatter = new Intl.NumberFormat('en-US', {
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    });

    const prefix = turnoverType === 1 ? 'valueL' : 'numericL';
    const totalProp = turnoverType === 1 ? 'valueTotal' : 'numericTotal';

    for (let i = 1; i <= 12; i++) {
      const prop = `${prefix}${i}`;
      if (item[prop]) {
        const roundedValue = Math.ceil(item[prop]);
        item[prop] = formatter.format(roundedValue);
      }
    }

    if (item[totalProp]) {
      const roundedValue = Math.ceil(item[totalProp]);
      item[totalProp] = formatter.format(roundedValue);
    }
  
    return item;
  }  

  setActions() {
    this.actions = { CanView: true, CanAdd: false, CanUpdate: false, CanDelete: false, 
    CanPrint: false, CanExport: false, CanImport: false, CanDuplicate: false };
  }

  refreshDataGrid() {
    this.getData();
    this.dataGrid.instance.refresh();
  }

  onSearchClick() {
    this.getData();
  }

  async getPartners() {
    if (!this.partnersDS || (this.partnersDS && !this.partnersDS.store) || (this.partnersDS && this.partnersDS.store && this.partnersDS.store.lenth === 0)) {
      await this.partnerService.getAllPartnersSmallAsync().then(partners => {
        this.partnersDS = {
          paginate: true,
          pageSize: 15,
          store: partners
        }
      })
    }
  }

  toggleTurnoverTypeCheckbox(selected: string, event: any): void {
    this.myClients = [];
    const isChecked = event.value; 

    if (isChecked) {
      if (selected === 'monthly') {
        this.filter.isYearlyTurnover = false;
      } else if (selected === 'yearly') {
        this.filter.isMonthlyTurnover = false;
      }
    }
  }

  
  getDisplayExprPartners(partner) {
    if (!partner) {
      return '';
    }

    const partnerName: string = partner.partnerName;
    const partnerCode: string = partner.code;

    let name: string = '(lipsa nume)';
    let code: string = '(lipsa cod)';

    if (partnerName !== undefined && partnerName !== null && partnerName !== '') {
      name = partnerName;
    } else {
      name = "...";
    }

    if (partnerCode !== undefined && partnerCode !== null && partnerCode !== '') {
      code = partnerCode;
    }

    return name;
  }

  onPartnerNameClick(item) {
    if (item) {
      let partner = this.partnersDS.store.find(f => f.id === item.partnerId);

      if (partner) {
        var url = environment.CRMPrimaware + '/' + Constants.partner + '?id=' + partner.id;
        window.open(url, '_blank');
      }
    }
  }

  goToTurnoverDetails(item) {
    let partner = this.partnersDS.store.find(f => f.id === item.partnerId);

    if (partner) {
      var url = environment.CRMPrimaware + '/' + Constants.turnOver + '?clientId=' + partner.id;
      window.open(url, '_blank');
    }
  }

  onTurnoverTypeValueChanged(event) {
    this.myClients = [];
  }

  getCellStyleValue(data: any, key: string): string {
    if (!data) {
      return 'light-gray';
    }
  
    const valueKey = `value${key}`;
    const numericKey = `numeric${key}`;
  
    const value = this.roundUp(data[valueKey]);
    const numeric = this.roundUp(data[numericKey]);
  
    if (value === 0 || (value === 0 && numeric === 0)) {
      return 'light-gray';
    }
  
    return 'bold-black';
  }

  getCellStyleNumeric(data: any, key: string): string {
    if (!data) {
      return 'light-gray';
    }
  
    const valueKey = `value${key}`;
    const numericKey = `numeric${key}`;
  
    const value = this.roundUp(data[valueKey]);
    const numeric = this.roundUp(data[numericKey]);
  
    if (numeric === 0) {
      return 'light-gray';
    }
  
    return 'bold-black';
  }

  showLastContactTooltip(data: any): void {
    this.lastContactTooltipData = data;
  }

  onHidingLastContactTooltip(e) {
    const isMouseInsidePopover = e.component.content().parentNode.matches(":hover");
    e.cancel = isMouseInsidePopover;
    if (!e.component.content().parentNode.getAttribute("mouselistener")) {
      e.component.content().parentNode.addEventListener("mouseleave", (arg) => {
        e.component.hide();
      });
      e.component.content().parentNode.setAttribute("mouselistener", true);
    }
  }

  onCellPrepared(e) {
    this.setLastContactTooltip(e);
  }

  setLastContactTooltip(event) {
    if (event && event.rowType === 'data' && event.column.dataField === 'lastContactCreated') {
      on(event.cellElement, 'mouseover', arg => {
        this.lastContactTooltipTimeout = window.setTimeout(async () => {

          this.lastContactTooltipData = null;
          let lastContactPromise = this.partnerFinancialInfoService.getLastContactDetailsByPartnerIdAsync(event.data.partnerId).then(r => {
            if (r) {
              this.lastContactTooltipData = r;
            } else { this.lastContactTooltipData = []; }
          });

          Promise.all([lastContactPromise]).then(r => {this.lastContactTooltipName.instance.show(arg.target);})
        }, 1000);
      });

      on(event.cellElement, 'mouseout', arg => {
        if (this.lastContactTooltipTimeout) {
          window.clearTimeout(this.lastContactTooltipTimeout);
        }
        this.lastContactTooltipName.instance.hide();
      });
    }
  }
}
