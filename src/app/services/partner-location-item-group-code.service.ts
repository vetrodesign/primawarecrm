import { Injectable } from '@angular/core';
import { PartnerLocationXItemGroupCode } from 'app/models/partnerlocationxitemgroupcode.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerLocationItemGroupCodeService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<any[]> {
    const apiURL = `/partnerLocationXItemGroupCode/GetAll`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.mrkApi);
  }

  async getAllPartnerLocationItemGroupCodeByPartnerLocationAsync(partnerLocationId: number): Promise<any[]> {
    const apiURL = `/partnerLocationXItemGroupCode/GetAllByPartnerLocationId`;
    return await this.apiService.PostAsync<any>(apiURL, partnerLocationId, Constants.mrkApi);
  }

  async getAllPartnerLocationItemGroupCodeByPartnerAsync(partnerId: number): Promise<any[]> {
    const apiURL = `/partnerLocationXItemGroupCode/GetAllByPartnerId`;
    return await this.apiService.PostAsync<any>(apiURL, partnerId, Constants.mrkApi);
  }

  async createMultiplePartnerLocationItemGroupCodeAsync(i: PartnerLocationXItemGroupCode[]): Promise<any> {
    const apiURL = `/partnerLocationXItemGroupCode/CreateMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }

  async deletePartnerLocationItemGroupCodeAsync(i: PartnerLocationXItemGroupCode[]): Promise<any> {
    const apiURL = `/partnerLocationXItemGroupCode/DeleteMultiple`;
    return await this.apiService.PostAsync<any>(apiURL, i, Constants.mrkApi);
  }
}
