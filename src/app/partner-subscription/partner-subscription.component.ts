import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import * as _ from 'lodash';
import { PartnerSubscription } from 'app/models/partner-subscription.model';
import { PartnerSubscriptionAllocation } from 'app/models/partner-subscription-allocation.model';
import { PartnerSubscriptionService } from 'app/services/partner-subscription.service';
import { PartnerSubscriptionAllocationService } from 'app/services/partner-subscription-allocation.service';
import { Partner } from 'app/models/partner.model';
import { NotificationService } from 'app/services/notification.service';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { AuthService } from 'app/services/auth.service';
import { PartnerSubscriptionTableColumnsTypeEnum } from 'app/enums/partnerSubscriptionTableColumnsTypeEnum';

@Component({
  selector: 'app-partner-subscription',
  templateUrl: './partner-subscription.component.html',
  styleUrls: ['./partner-subscription.component.css']
})
export class PartnerSubscriptionComponent implements OnInit {
  @Input() partner: Partner;
  
  loaded: boolean;
  shouldAllowApprove: boolean;
  subscriptionValue: PartnerSubscription[];
  subscriptionDataSource: PartnerSubscription[];

  partnerSubscriptionAllocation: PartnerSubscriptionAllocation[];
  psaz: PartnerSubscriptionAllocation;
  tableColumns = {
    customerStatusAndSubscription: PartnerSubscriptionTableColumnsTypeEnum.CustomerStatusAndSubscription,
    partnerStatus: PartnerSubscriptionTableColumnsTypeEnum.PartnerStatus,
    description: PartnerSubscriptionTableColumnsTypeEnum.Description
  }
  constructor(private partnerSubscriptionService: PartnerSubscriptionService, private notificationService: NotificationService,
    private authenticationService: AuthService,
    private partnerSubscriptionAllocationService: PartnerSubscriptionAllocationService) { 
      this.setSubscriptionDataSource();
    }

  ngOnInit(): void {
    this.shouldAllowApprove = this.authenticationService.isUserAdmin() || this.authenticationService.isUserOwner();;
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && e.data.approved == true) {
      e.rowElement.style.backgroundColor = '#c9ffc9';
    }
    if (e.rowType === 'data' && e.data && e.data.approved == undefined) {
      e.rowElement.style.backgroundColor = '#fff2cf';
    }
    if (e.rowType === 'data' && e.data && e.data.deleted == true) {
      e.rowElement.style.backgroundColor = '#ffc7c7';
    }
  }

  loadData() {
    this.setPartnerSubscriptionAllocation();
  }

  setSubscriptionDataSource() {
    this.partnerSubscriptionService.getAllAsync().then(response => {
      this.subscriptionDataSource = response;
    });
  }

  subscriptionDisplayExpr(item) {
    return `${item.customerStatusAndSubscription} | ${item.description}`;
  }

  customizeColumns(columns) {
    columns[0].caption = PartnerSubscriptionTableColumnsTypeEnum.CustomerStatusAndSubscription;
    columns[1].caption = PartnerSubscriptionTableColumnsTypeEnum.PartnerStatus;
    columns[2].caption = PartnerSubscriptionTableColumnsTypeEnum.Description;
  }

  showSubscriptionTable() {
    return !!this.subscriptionValue;
  }

  setPartnerSubscriptionAllocation() {
    if(this.partner.id) {
      this.partnerSubscriptionAllocationService.getByPartnerIdAsync(this.partner.id).then(result => {
        this.partnerSubscriptionAllocation = result;
        this.psaz = this.partnerSubscriptionAllocation[0];
        if (this.psaz) {
          this.psaz.deleted = false;
        }
  

        if(this.psaz) {
          this.subscriptionValue = [this.subscriptionDataSource.find(x => x.id === result[0].partnerSubscriptionId)];
        } else {
          this.partnerSubscriptionAllocation.push(new PartnerSubscriptionAllocation());
          this.psaz = this.partnerSubscriptionAllocation[0];
          this.psaz.sms = false;
          this.psaz.email = false;
          this.psaz.whatsApp = false;
          this.psaz.approved = false;
          this.psaz.motivation = "";
        }
      });
    }
  }

  subscriptionOnValueChanged() {
    if (this.subscriptionValue && [1, 2, 3, 4, 5, 6].includes(this.subscriptionValue[0].id)) {
      this.psaz.email = true;
    }

    if (!this.subscriptionValue) {
      this.psaz.sms = false;
      this.psaz.email = false;
      this.psaz.whatsApp = false;
      this.psaz.approved = false;
      this.psaz.motivation = "";
    }
  }

  allowEditingSubscriptionSmsMailWhatsApp() {
    if (this.subscriptionValue && [7, 8, 10].includes(this.subscriptionValue[0].id)) {
      this.psaz.sms = false;
      this.psaz.email = false;
      this.psaz.whatsApp = false;
      return false;
    }
    else {
      return true;
    }
  }

  updatePartnerSubscription() {
    if (this.subscriptionValue && this.psaz.motivation.length == 0)
    {
      this.notificationService.alert('top', 'center', 'Abonare - Datele nu au fost modificate! Campul "Motivatie & Observatii" nu poate fi gol!', NotificationTypeEnum.Red, true);
    } else {
      let partnerSubscriptionToUpdate = new PartnerSubscriptionAllocation();

      if (this.subscriptionValue) {
        partnerSubscriptionToUpdate = Object.assign(partnerSubscriptionToUpdate, {
          partnerId: this.partner.id,
          partnerSubscriptionId: this.subscriptionValue[0].id,
          sms: this.psaz.sms,
          email: this.psaz.email,
          whatsApp: this.psaz.whatsApp,
          approved: this.psaz.approved,
          motivation: this.psaz.motivation,
          isActive: !this.psaz.deleted
        });
      } else {
        partnerSubscriptionToUpdate = Object.assign(partnerSubscriptionToUpdate, {
          partnerId: this.partner.id,
          isActive: !this.psaz.deleted
        });
      }

      this.loaded = true;
      this.partnerSubscriptionAllocationService.updateAsync(partnerSubscriptionToUpdate).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Abonare - Datele au fost modificate!', NotificationTypeEnum.Green, true);
          this.loadData();
          if (this.psaz.deleted) {
            this.subscriptionValue = null;
          }
        }
        this.loaded = false;
      });
    }
  }

}
