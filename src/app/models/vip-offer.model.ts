import { BaseDomain } from "./base-domain.model";

export class VipOffer extends BaseDomain {
    type: number;
    code: string;
    name: string;
    referenceVipId: number;
    formula: number;
    activeFrom: Date;
    activeTo: Date;
    isActive: boolean;
}

export class SyncVipOfferDetailsItem {
    isUpdateD0: boolean;
    isUpdateC0: boolean;
    isUpdateR0: boolean;
  
    isUpdateD0EUR: boolean;
    isUpdateC0EUR: boolean;
    isUpdateR0EUR: boolean;
  
    isUpdateD0USD: boolean;
    isUpdateC0USD: boolean;
    isUpdateR0USD: boolean;
  
    isUpdateD0BGN: boolean;
    isUpdateC0BGN: boolean;
    isUpdateR0BGN: boolean;
  
    isUpdateD0HUF: boolean;
    isUpdateC0HUF: boolean;
    isUpdateR0HUF: boolean;

    vipOfferData: any[];
}
