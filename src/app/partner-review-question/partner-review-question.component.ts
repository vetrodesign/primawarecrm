import { Component, OnInit, ViewChild } from '@angular/core';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { Occupation } from 'app/models/occupation.model';
import { PartnerReviewQuestion } from 'app/models/partner-review-question.model';
import { PartnerLocationContact } from 'app/models/partnerlocationcontact.model';
import { Post } from 'app/models/post.model';
import { AuthService } from 'app/services/auth.service';
import { NotificationService } from 'app/services/notification.service';
import { PartnerReviewQuestionService } from 'app/services/partner-review-question.service';
import { TranslateService } from 'app/services/translate';
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
  selector: 'app-partner-review-question',
  templateUrl: './partner-review-question.component.html',
  styleUrls: ['./partner-review-question.component.css']
})
export class PartnerReviewQuestionComponent implements OnInit {
  @ViewChild('partnerReviewsDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('reviewGridToolbar') gridToolbar: GridToolbarComponent;
  partnerReviewQuestions: PartnerReviewQuestion[] = [];
  userPost: Post;
  selectedPartnerReview: PartnerReviewQuestion;
  selectedRows: any[];
  selectedRowIndex = -1;
  groupedText: string;
  actions: any;
  partnerReviewInfo: string;
  partnerContacts: PartnerLocationContact[] = [];
  occupations: Occupation[] = [];
  loaded: boolean;
  constructor(private partnerReviewQuestionsService: PartnerReviewQuestionService, private notificationService: NotificationService,
    private authService: AuthService, private translationService: TranslateService) {
    this.setActions();
    this.groupedText = this.translationService.instant('groupedText');
    this.partnerReviewInfo = this.translationService.instant("partnerReviewInfo"); 
   }

  ngOnInit(): void {
    this.getReviews();
  }

  async getReviews() {
    await this.partnerReviewQuestionsService.getAllPartnerReviewQuestionsAsync().then(reviews => {
      this.partnerReviewQuestions = (reviews && reviews.length > 0) ? reviews.filter(x => x.isActive): [];
    });
  }

  public refreshReviews() {
    this.getReviews().then(() => {
      this.dataGrid.instance.refresh();
    })
  }

  public async onInitNewRow(data: any) {
    if (data && data.data) {
      data.data.isActive = true;
    }
  } 

  canExport() {
    return this.actions.CanExport;
  }

  public add() {
    this.dataGrid.instance.addRow();
  }

  public async openDetails(row: any) {
    this.dataGrid.instance.editRow(row.rowIndex);
  }


  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data && e.data.isActive != 1) {
      e.rowElement.style.backgroundColor = '#ffb587';
    }
  }


  public async onRowReviewInserting(event: any): Promise<void> {
    if (this.partnerReviewQuestions.filter(x => x.isActive == true).length >= 5) {
      this.notificationService.alert('top', 'center', 'Sugestii Clienti - Nu puteti sa aveti mai mult de 5 intrebari active! A aparut o eroare!',
        NotificationTypeEnum.Red, true);
        this.refreshReviews();
        return;
    }
    let item = new PartnerReviewQuestion();
    item = event.data;
    item.customerId = 5;
    if (item) {
      await this.partnerReviewQuestionsService.createPartnerReviewQuestionAsync(item).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Sugestii Clienti - Datele au fost inserate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Sugestii Clienti - Datele nu au fost inserate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        }
        this.refreshReviews();
      });
    }
  }

  async onRowUpdated(e) {
    if (this.partnerReviewQuestions.filter(x => x.isActive == true).length > 5) {
      this.notificationService.alert('top', 'center', 'Sugestii Clienti - Nu puteti sa aveti mai mult de 5 intrebari active! A aparut o eroare!',
        NotificationTypeEnum.Red, true);
        this.refreshReviews();
        return;
    }
    let item = new PartnerReviewQuestion();
    item = e.data;

    if (item) {
      await this.partnerReviewQuestionsService.updatePartnerReviewQuestionAsync(item,).then(r => {
        if (r) {
          this.notificationService.alert('top', 'center', 'Sugestii Clienti - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Sugestii Clienti - Datele nu au fost modificate! A aparut o eroare!',
            NotificationTypeEnum.Red, true)
        };
        this.refreshReviews();
      });
    }
  }

  setActions() {
    this.actions = {
      CanView: true,
      CanAdd: true,
      CanUpdate: false,
      CanDelete: false,
      CanPrint: true,
      CanExport: true,
      CanImport: false,
      CanDuplicate: false
    };
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  selectionReviewChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  getDisplayExprPartnerContacts(value:any) {
    if (value && this.occupations) {
      let occupation = this.occupations.find(f => f.id === value.position);

      if (occupation) {
        return `${value.firstName} ${value.lastName}(${occupation.code})`;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }
}
