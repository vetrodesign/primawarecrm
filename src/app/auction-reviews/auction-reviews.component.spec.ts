import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionReviewsComponent } from './auction-reviews.component';

describe('AuctionReviewsComponent', () => {
  let component: AuctionReviewsComponent;
  let fixture: ComponentFixture<AuctionReviewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuctionReviewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionReviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
