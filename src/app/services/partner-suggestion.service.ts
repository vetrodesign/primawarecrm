import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PartnerReview } from 'app/models/partnerreview.model';
import { environment } from 'environments/environment';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class PartnerSuggestionService {
  constructor(private apiService: ApiService) { }

  async createPartnerSuggestion(p: PartnerReview): Promise<any> {
    const apiURL = `/partnerreview/CreateFromTurnover`;
    return await this.apiService.PostAsync<any>(apiURL, p, Constants.mrkApi);
  }
}
