import { TestBed } from '@angular/core/testing';

import { LiquidationOfferTurnoverService } from './liquidation-offer-turnover.service';

describe('LiquidationOfferTurnoverService', () => {
  let service: LiquidationOfferTurnoverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LiquidationOfferTurnoverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
