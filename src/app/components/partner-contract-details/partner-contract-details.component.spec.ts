import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerContractDetailsComponent } from './partner-contract-details.component';

describe('PartnerContractDetailsComponent', () => {
  let component: PartnerContractDetailsComponent;
  let fixture: ComponentFixture<PartnerContractDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerContractDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerContractDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
