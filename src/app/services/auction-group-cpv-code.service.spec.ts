import { TestBed } from '@angular/core/testing';

import { AuctionGroupCpvCodeService } from './auction-group-cpv-code.service';

describe('AuctionGroupCpvCodeService', () => {
  let service: AuctionGroupCpvCodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuctionGroupCpvCodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
