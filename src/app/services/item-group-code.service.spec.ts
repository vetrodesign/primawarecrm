import { TestBed } from '@angular/core/testing';

import { ItemGroupCodeService } from './item-group-code.service';

describe('ProductGroupService', () => {
  let service: ItemGroupCodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemGroupCodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
