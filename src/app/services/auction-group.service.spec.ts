import { TestBed } from '@angular/core/testing';

import { AuctionGroupService } from './auction-group.service';

describe('AuctionGroupService', () => {
  let service: AuctionGroupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuctionGroupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
