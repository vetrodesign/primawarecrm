import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerLegalFormComponent } from './partner-legal-form.component';

describe('PartnerLegalFormComponent', () => {
  let component: PartnerLegalFormComponent;
  let fixture: ComponentFixture<PartnerLegalFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerLegalFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerLegalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
