import { BaseDomain } from "./base-domain.model";

export class ImportAuction extends BaseDomain {
    public key: number;
    public customerId: number;
    public postId: number;
    public orderNumber: number;

    public countryId: number;
    public countyId: number;
    public partnerId: number;

    public acquisitionType: string;
    public auctionTypeId: number;
    public auctionGroupId: number;

    public acquisitionNumber: string;
    public acquisitionName: string;
    public acquisitionerName: string;
    public estimatedValue: number;

    public participationTypeId : number;
    public lotsNumberFrom : number;
    public lotsNumberTo: string;
    public lotsToAnalyze: string;

    public status : number;
    public result : number;

    public folderUrl : string;

    // to determine the color
    public endingDate: number;

    public acquisitionDate: Date;
    public endDate: Date;

    public country: string;
    public county: string;

    public code: string;

    public existingCpvCodeIds: any[];
    public cpvCodeIds: any;

    public description: string;
    public hasRequestedPrices: boolean;
    public hasReceivedPrices: boolean;
    public isWithoutDocuments: boolean;
    public isCanceled: boolean;

    public url: string;
    public auctioner: string;
    public location: string;
    public value: string;
    public auctionDates: string;
}


