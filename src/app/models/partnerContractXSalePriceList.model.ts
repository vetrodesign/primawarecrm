import { BaseDomain } from "./base-domain.model";

export class PartnerContractXSalePriceList extends BaseDomain {
    id: number;
    created: Date;
    isActive: boolean;

    partnerContractId: number;
    salePriceListId: number;
}
