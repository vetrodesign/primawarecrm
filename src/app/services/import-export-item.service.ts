import { Injectable } from '@angular/core';
import { ImportExportItemData } from 'app/models/importexportitemdata.model';
import { environment } from 'environments/environment';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ImportExportItemService {

  constructor(private apiService: ApiService) { }

  async getAll(): Promise<any[]> {
    const apiURL = `/importexportitemdata/GetAll`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.crmApi);
  }

  async createAsync(items: ImportExportItemData[]): Promise<any> {
    const apiURL = `/importexportitemdata/Create`;
    return await this.apiService.PostAsync<any>(apiURL, items, Constants.crmApi);
  }

  async updateAsync(item: ImportExportItemData): Promise<any> {
    const apiURL = `/importexportitemdata/update`;
    return await this.apiService.PutAsync<any>(apiURL, item, Constants.crmApi);
  }

    async updateMultipleAsync(itemIds: number[], item: ImportExportItemData): Promise<any> {
    let s = {'item': null, 'itemIds': null};
    s.item = item;
    s.itemIds = itemIds;
  
    const apiURL = `/importexportitemdata/updateMultiple`;
    return await this.apiService.PutAsync<any>(apiURL, s, Constants.crmApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/importexportitemdata/Delete`;
    return await this.apiService.PostAsync<any>(apiURL, ids, Constants.crmApi);
  }
}

