import { Injectable } from '@angular/core';
import { SalesTarget } from 'app/models/salestarget.model';
import { ApiService } from './api.service';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class SalesTargetService {

  constructor(private apiService: ApiService) { }

  async getAllAsync(): Promise<SalesTarget[]> {
    const apiURL = `/SalesTarget/getall`;
    return await this.apiService.GetAsync<SalesTarget[]>(apiURL, Constants.mrkApi);
  }

  async getByCustomerIdAsync(): Promise<SalesTarget[]> {
    const apiURL = `/SalesTarget/GetByCustomerId`;
    return await this.apiService.GetAsync<SalesTarget[]>(apiURL, Constants.mrkApi);
  }

  async createAsync(st: SalesTarget): Promise<any> {
    const apiURL = `/SalesTarget/create`;
    return await this.apiService.PostAsync<any>(apiURL, st, Constants.mrkApi);
  }

  async updateAsync(st: SalesTarget): Promise<any> {
    const apiURL = `/SalesTarget/update`;
    return await this.apiService.PutAsync<any>(apiURL, st, Constants.mrkApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/SalesTarget/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }
}
