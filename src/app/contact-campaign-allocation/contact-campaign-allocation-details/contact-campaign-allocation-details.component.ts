import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { DepartmentTypeEnum } from 'app/enums/departmentTypeEnum';
import { NotificationTypeEnum } from 'app/enums/notificationTypeEnum';
import { GridToolbarComponent } from 'app/helpers/grid-toolbar/grid-toolbar.component';
import { ContactCampaignAllocation } from 'app/models/contact-campaign-allocation.model';
import { ContactCampaign } from 'app/models/contact-campaign.model';
import { Department } from 'app/models/department.model';
import { Office } from 'app/models/office.model';
import { Post } from 'app/models/post.model';
import { AuthService } from 'app/services/auth.service';
import { ContactCampaignAllocationService } from 'app/services/contact-campaign-allocation.service';
import { ContactCampaignService } from 'app/services/contact-campaign-management.service';
import { DepartmentsService } from 'app/services/departments.service';
import { NotificationService } from 'app/services/notification.service';
import { PostService } from 'app/services/post.service';
import { TranslateService } from 'app/services/translate';
import { UsersService } from 'app/services/user.service';
import { DxDataGridComponent, DxValidatorComponent } from 'devextreme-angular';

@Component({
  selector: 'app-contact-campaign-allocation-details',
  templateUrl: './contact-campaign-allocation-details.component.html',
  styleUrls: ['./contact-campaign-allocation-details.component.css']
})
export class ContactCampaignAllocationDetailsComponent implements OnInit {
  @Input() selectedContactCampaignAllocation: ContactCampaignAllocation;
  @Input() items: any;
  @Input() itemsGroup: any;
  @Input() marketingCampaignType: any;

  @Input() actions: any;
  @Output() visible: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('itemsDataGrid') dataGrid: DxDataGridComponent;
  @ViewChild('itemsGridToolbar') gridToolbar: GridToolbarComponent;
  @ViewChild('validationGroup') validationGroup: DxValidatorComponent;

  loaded: boolean;
  contactCampaignAllocation: ContactCampaignAllocation;
  selectedPost: Post;
  isTabActive: string;
  selectedRows: any[];
  selectedRowIndex = -1;

  allocatedItems: any[] = [];

  popupVisible: boolean;
  postIds: any;
  itemIds: any;
  itemsData: any;
  isOwner: boolean = false
  posts: Post[];
  selectedPosts: any[];
  contactCampaigns: ContactCampaign[];
  selectedContactCampaigns: ContactCampaign[];

  itemStates: { id: number; name: string }[] = [];

  contactCampaignAllocationDetailsInfo: string;

  constructor(private authService: AuthService, private contactCampaignAllocationService: ContactCampaignAllocationService, private postService: PostService,
    private notificationService: NotificationService, private translationService: TranslateService, private authenticationService: AuthService,
    private contactCampaignService: ContactCampaignService, private departmentsService: DepartmentsService, private usersService: UsersService) { 
    this.getDisplayExprPosts = this.getDisplayExprPosts.bind(this);
    this.getDisplayExprContactCampaigns = this.getDisplayExprContactCampaigns.bind(this);

    this.authenticationService.currentUserSubject.subscribe(token => {
      if (token) {
        if (this.authenticationService.isUserOwner()) {
          this.isOwner = true;
        }
      }
    });

    this.loaded = true;
    Promise.all([this.setSalesAgentUsers(), this.getContactCampaigns()]).then(x => {
      this.loaded = false;
    });

    this.contactCampaignAllocationDetailsInfo = this.translationService.instant("contactCampaignAllocationDetailsInfo");
  }

  ngOnInit(): void {
    this.isTabActive = this.authService.getCustomerPageTitleTheme();
  }

  async loadData() {
    this.contactCampaignAllocation = new ContactCampaignAllocation();
    this.postIds = [];

    if (this.selectedContactCampaignAllocation) {
      this.contactCampaignAllocation = this.selectedContactCampaignAllocation;
    } else {
      this.contactCampaignAllocation = new ContactCampaignAllocation();
      this.contactCampaignAllocation.customerId = Number(this.authService.getUserCustomerId());
      this.postIds = [];
    }
    setTimeout(() => {
      this.setGridInstances();
      if (this.dataGrid) {
        this.dataGrid.instance.columnOption('isActive', 'filterValue', ['1']);
      }
    }, 200);
  }

  setGridInstances() {
    if (this.gridToolbar && this.dataGrid) {
      this.gridToolbar.dataGrid = this.dataGrid;
      this.gridToolbar.setGridInstance();
    }
  }

  getDisplayExprPost(item) {
    if (!item) {
      return '';
    }
    return item.code + ' - ' + item.companyPost;
  }

  back() {
    this.contactCampaignAllocation = null;
    this.visible.emit(false);
  }

  selectionChanged(data: any) {
    this.selectedRows = data.selectedRowsData;
    this.selectedRowIndex = data.component.getRowIndexByKey(data.selectedRowKeys[0]);
  }

  public async onEditorPreparing(event: any) {
    if (event.parentType == 'filterRow' && event.editorName == 'dxSelectBox')
      event.editorName = "dxTextBox";
  }

  onRowPrepared(e) {
    if (e.rowType === 'data' && e.data)  {
      if (e.data.isActive == false) 
        {
          e.rowElement.style.backgroundColor = '#ff9966';
        }
        else {     
          e.rowElement.style.backgroundColor = '#a8ffd2';
        }
    }
  }

  public deleteRow(data: any) {
    this.dataGrid.instance.selectRows(data.key, false);
    this.gridToolbar.displayDeleteConfirmation();
  }

  public refreshDataGrid() {
    this.dataGrid.instance.refresh();
  }

  async saveData() {
    if (this.validationGroup.instance.validate().isValid) {
      await this.contactCampaignAllocationService.createAsync(this.contactCampaignAllocation).then(result => {
        if (result) {
          this.notificationService.alert('top', 'center', 'Alocare Campanii Contactare - Datele au fost create cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Alocare Campanii Contactare - Datele nu au fost create!', NotificationTypeEnum.Red, true)
        }
        this.back();
      })
    }
  }

  async updateData() {
    if (this.validationGroup.instance.validate().isValid) {
      await this.contactCampaignAllocationService.updateAsync(this.contactCampaignAllocation).then(result => {
        if (result) {
          this.contactCampaignAllocation = result;
          this.notificationService.alert('top', 'center', 'Alocare Campanii Contactare  - Datele au fost modificate cu succes!',
            NotificationTypeEnum.Green, true)
        } else {
          this.notificationService.alert('top', 'center', 'Alocare Campanii Contactare  - Datele nu au fost modificate!', NotificationTypeEnum.Red, true)
        }
      })
    }
  }

  async getContactCampaigns() {
    if (this.isOwner) {
      await this.contactCampaignService.getAllAsync().then(data => {
        this.contactCampaigns = data;
      });
    } else {
      await this.contactCampaignService.getAllByCustomerIdAsync(Number(this.authService.getUserCustomerId())).then(data => {
        this.contactCampaigns = data;
      });
    }
  }

  async setSalesAgentUsers() {
    let departments = [];

    if (this.isOwner) {
      await this.departmentsService.getAllDepartmentsAsync().then(async items => {
        if (items && items.length > 0) {
          departments = items;
          this.posts = await this.getAllSalesPosts(departments); 
        }
      });
    } else {
      await this.departmentsService.getDepartmentsAsyncByID().then(async items => {
        if (items && items.length > 0) {
          departments = items;
          this.posts = await this.getAllSalesPosts(departments)
        };
      });
    }
  }

  private async getAllSalesPosts(departments: Department[]): Promise<Post[]> {
    if (!departments || departments.length == 0) 
      return [];

    let validSalesPosts: Post[] = [];

    await this.usersService.getUsersAsync().then((users) => {
      if (users && users.length > 0) {
        const salesDepartments: Department[] = departments.filter(dep => dep.departmentType === DepartmentTypeEnum.Sales); 
        const salesOffices: Office[] = salesDepartments.reduce((acc: Office[], dept: Department) => acc.concat(dept.offices), []);
        const salesPosts: Post[] = salesOffices.reduce((acc: Post[], office: Office) => acc.concat(office.posts), []);
        const activeSalesPosts = salesPosts.filter((post) => post.isActive === true);

        validSalesPosts = activeSalesPosts.filter(f => users.map(m => m.postId).includes(f.id));
      }
    });

    return validSalesPosts;
  }


  getDisplayExprPosts(item) {
    if (!item) {
      return '';
    }

    return item.code + ' - ' + item.companyPost;
  }

  getDisplayExprContactCampaigns(item) {
    if (!item) {
      return '';
    }

    return item.code + ' - ' + item.name;
  }
}
