import { SupplierPriceListItem } from "./supplierpricelistitem.model";

export class SupplierPriceListItemHistory {
    id: string;
    supplierPriceListItemId: number;
    itemId: number;
    listPrice: number;
    discount: number;
    discountAdvance: number;
    supplierQuantity: number;
    minSupplierQuantity: number;
    details: string;

    constructor(supplierPriceListItem?: SupplierPriceListItem) {
        if (supplierPriceListItem) {
            this.supplierPriceListItemId = Number(supplierPriceListItem.id);
            this.itemId = supplierPriceListItem.itemId;
            this.listPrice = supplierPriceListItem.listPrice;
            this.discount = supplierPriceListItem.discount;
            this.discountAdvance = supplierPriceListItem.discountAdvance;
            this.supplierQuantity = supplierPriceListItem.supplierQuantity;
            this.minSupplierQuantity = supplierPriceListItem.minSupplierQuantity;
            this.details = supplierPriceListItem.details;
        }

    }
}
