export class City {
    public id: number;
    public code: string;
    public name: string;
    public countyId: number;
    public countyName: string;
    public countryId: number;
    public countryName: string;
}