export enum TaxTypeEnum {
    ClientTurnover = 1,
    PartnerActivityTurnover = 2,
    NewItemsTurnover = 3
  }