import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesTargetReportComponent } from './sales-target-report.component';

describe('SalesTargetReportComponent', () => {
  let component: SalesTargetReportComponent;
  let fixture: ComponentFixture<SalesTargetReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesTargetReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesTargetReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
