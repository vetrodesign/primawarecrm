import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { environment } from 'environments/environment';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(private apiService: ApiService) { }

  async getAllItemsAsync(): Promise<any[]> {
    const apiURL = `/item/getall`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getAllItemSmallAsync(): Promise<any> {
    const apiURL = `/item/getallitemsmall`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.mrkApi);
  }

  async getAllItemGroupSmallAsync(): Promise<any> {
    const apiURL =`/item/getallitemgroupsmall`;
    return await this.apiService.GetAsync<any>(apiURL, Constants.mrkApi);
  }

  async getItemByItemId(itemId: number): Promise<any> {
    const apiURL = `/item/GetItemByItemId`;
    return await this.apiService.PostAsync<any>(apiURL, itemId, Constants.mrkApi);
  }

  async getItemDetailsVAT(VATItemVM: any[], partnerId: number): Promise<any> {
    var itemIdsFilterVM = {'items': VATItemVM, 'partnerId': partnerId};
    const apiURL = `/item/GetItemsAndVAT`;
    return await this.apiService.PostAsync<any>(apiURL, itemIdsFilterVM, Constants.mrkApi);
  }

  async getFilteredByCodeSmall(code: string): Promise<any[]> {
    let c = JSON.stringify(code);
    const apiURL = `/item/getfilteredbycodesmall`;
    return await this.apiService.PostAsync<any[]>(apiURL, c, Constants.mrkApi);
  }

  async getItemssAsyncByID(): Promise<any[]> {
    const apiURL =`/item/getbycustomerid`;
    return await this.apiService.GetAsync<any[]>(apiURL, Constants.mrkApi);
  }

  async getItemStocks(itemId: number[]): Promise<any> {
    const apiURL = `/item/GetItemStocks`;
    return await this.apiService.PostAsync<any>(apiURL, itemId, Constants.mrkApi);
  }

  async getItemStocksOnManagements(itemIds: number[], managements: number[]): Promise<any> {
    var body = {"itemIds" : itemIds, "managements" : managements};
    const apiURL = `/item/GetItemStocksOnManagements`;
    return await this.apiService.PostAsync<any>(apiURL, body, Constants.mrkApi);
  }
  
  async GetStocksOnSiteAsync(itemId: number) {
    const apiURL = `/item/GetStocksOnSite`;
    return await this.apiService.PostAsync<any>(apiURL, itemId, Constants.mrkApi);
  }

  async getMultipleItemsStocksOnSiteAsync(itemIds: number[]) {
    const apiURL = `/item/GetMultipleItemsStocksOnSite`;
    return await this.apiService.PostAsync<any>(apiURL, itemIds, Constants.mrkApi);
  }

  async getMultipleItemsStocksOnManagementAsync(itemIds: number[]) {
    const apiURL = `/item/GetMultipleItemsStocksOnManagement`;
    return await this.apiService.PostAsync<any>(apiURL, itemIds, Constants.mrkApi);
  }

  async verifyItemsCertificatesExcelAsync(excelFile: any, partnerId: number): Promise<any> {
    const apiURL = `/item/VerifyItemsCertificatesExcel`;

    const formData = new FormData();
    formData.append('excelFile', excelFile);
    formData.append('partnerId', partnerId.toString());

    return await this.apiService.PostFileAsync<any>(apiURL, formData, Constants.mrkApi);
  }

  async verifyItemsCertificatesAsync(itemsCertificates: any): Promise<any> {
    const apiURL = `/item/VerifyItemsCertificates`;
    return await this.apiService.PostFileAsync<any>(apiURL, itemsCertificates, Constants.mrkApi);
  }
}