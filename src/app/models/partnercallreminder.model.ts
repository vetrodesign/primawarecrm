import { BaseDomain } from "./base-domain.model";

export class PartnerCallReminder extends BaseDomain {
    partnerId: number;
    postId: number;
    dateAndTimeToRecall: Date;

    startDate: Date;
    endDate: Date;
    text: string;
}