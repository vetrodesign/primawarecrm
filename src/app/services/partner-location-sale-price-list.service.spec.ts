import { TestBed } from '@angular/core/testing';

import { PartnerLocationSalePriceListService } from './partner-location-sale-price-list.service';

describe('PartnerLocationSalePriceListService', () => {
  let service: PartnerLocationSalePriceListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerLocationSalePriceListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
