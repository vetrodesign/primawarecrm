import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { VipOfferItemsDetails } from 'app/models/vip-offer-items-details.model';
import { Constants } from 'app/constants';

@Injectable({
  providedIn: 'root'
})
export class VipOfferItemsDetailsService {

  constructor(private apiService: ApiService) { }

  async getByFilterAsync(itemId? : number, productConventionId?: number, currencyId?: number): Promise<VipOfferItemsDetails[]> {
    var apiURL = `/vipOfferItemsDetails/GetByFilter?itemId=${encodeURIComponent(itemId)}&productConventionId=${encodeURIComponent(productConventionId)}&currencyId=${encodeURIComponent(currencyId)}`;
    return await this.apiService.GetAsync<VipOfferItemsDetails[]>(apiURL, Constants.mrkApi);
  }

  async getAllAsync( itemId? : number,productConventionId?: number, currencyId?: number): Promise<VipOfferItemsDetails[]> {
    var apiURL = `/vipOfferItemsDetails/getall?itemId=${encodeURIComponent(itemId)}&productConventionId=${encodeURIComponent(productConventionId)}&currencyId=${encodeURIComponent(currencyId)}`;
    return await this.apiService.GetAsync<VipOfferItemsDetails[]>(apiURL, Constants.mrkApi);
  }

  async getByVipOfferIdAsync(vipOfferId? : number, productConventionId?: number, currencyId?: number): Promise<VipOfferItemsDetails[]> {
    var apiURL = `/vipOfferItemsDetails/GetByVipOfferId?vipOfferId=${encodeURIComponent(vipOfferId)}&productConventionId=${encodeURIComponent(productConventionId)}&currencyId=${encodeURIComponent(currencyId)}`;
    return await this.apiService.GetAsync<VipOfferItemsDetails[]>(apiURL, Constants.mrkApi);
  }

  async createAsync(vpid: VipOfferItemsDetails): Promise<any> {
    const apiURL = `/vipOfferItemsDetails/create`;
    return await this.apiService.PostAsync<any>(apiURL, vpid, Constants.mrkApi);
  }

  async updateAsync(vpid: VipOfferItemsDetails): Promise<any> {
    const apiURL = `/vipOfferItemsDetails/update`;
    return await this.apiService.PutAsync<any>(apiURL, vpid, Constants.mrkApi);
  }

  async deleteAsync(ids: number[]): Promise<any> {
    const apiURL = `/vipOfferItemsDetails/delete`;
    return await this.apiService.DeleteMultipleAsync<any>(apiURL, ids, Constants.mrkApi);
  }

  async SyncTiersAsync(vipOfferId: number, copyFromItemId: number, copyToId: number): Promise<boolean> {
    var body =  {'vipOfferId': vipOfferId, 'copyFromItemId': copyFromItemId, 'copyToId': copyToId};
    const apiURL = `/vipOfferItemsDetails/SyncTiers`;
    return await this.apiService.PostAsync<boolean>(apiURL, body, Constants.mrkApi);
  }

  async GetTiersForItemAsync(itemGroupId: number, itemId: number, partnerId: number): Promise<any> {
    var body =  {'itemGroupId': itemGroupId, 'itemId': itemId, 'partnerId': partnerId};
    const apiURL = `/vipOfferItemsDetails/GetTiersForItem`;
    return await this.apiService.PostAsync<boolean>(apiURL, body, Constants.mrkApi);
  }

  async GetTiersForClientAsync(itemGroupId: number, itemId: number, partnerId: number, isOnlyForClient: boolean): Promise<any> {
    var body =  {'itemGroupId': itemGroupId, 'itemId': itemId, 'partnerId': partnerId, 'isOnlyForClient': isOnlyForClient};
    const apiURL = `/vipOfferItemsDetails/GetTiersForClient`;
    return await this.apiService.PostAsync<boolean>(apiURL, body, Constants.mrkApi);
  }
}
