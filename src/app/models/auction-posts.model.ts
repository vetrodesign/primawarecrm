import { BaseDomain } from "./base-domain.model";

export class AuctionPosts extends BaseDomain{
    public customerId: number;
    public postId: number;
}