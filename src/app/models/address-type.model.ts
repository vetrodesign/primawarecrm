import { BaseDomain } from "./base-domain.model";

export class AddressType extends BaseDomain {
    public code: string;
    public nameRO: string;
    public nameENG: string;
    public isActive: boolean;
}