export class PartnerLocationXItemGroupCategory {
    id: number;
    partnerLocationId: number;
    itemGroupCategoryId: number;
}