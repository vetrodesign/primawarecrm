export class CaenCodeCategory {
    public id: number;
    public code: string;
    public description: string;
    public caenCodeDomainId: number;
}