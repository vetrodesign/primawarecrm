import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerLocationScheduleComponent } from './partner-location-schedule.component';

describe('PartnerLocationScheduleComponent', () => {
  let component: PartnerLocationScheduleComponent;
  let fixture: ComponentFixture<PartnerLocationScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerLocationScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerLocationScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
