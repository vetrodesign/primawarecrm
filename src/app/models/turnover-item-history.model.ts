export class TurnoverItemHistory {
    itemId: number;
    itemCode: string;
    itemName: string;
    partnerId: number;
    partnerName: string;
    caenCode: string;
    caenDescription: string;
    partnerActivity: string;
    partnerResponsableAgent: string;
    turnoverOperationId: number;
    turnoverOperationName: string;
    year: number;
    month: number;
    quantity: number;
    amount: number;
    currencyId: number;
    currencyName: string;
}

export class TurnoverPartnerMonthQuantity {
    documentNo: number;
    documentDate: Date;
    quantity: number;
    pu: number;
}

export class ItemPrices {
    cmp: number;
    d0: number;
    c0: number;
    r0: number;
}
