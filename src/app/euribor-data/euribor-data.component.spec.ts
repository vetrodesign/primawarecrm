import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EuriborDataComponent } from './euribor-data.component';

describe('EuriborDataComponent', () => {
  let component: EuriborDataComponent;
  let fixture: ComponentFixture<EuriborDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EuriborDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EuriborDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
