import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerLocationContactComponent } from './partner-location-contact.component';

describe('PartnerLocationContactComponent', () => {
  let component: PartnerLocationContactComponent;
  let fixture: ComponentFixture<PartnerLocationContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerLocationContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerLocationContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
